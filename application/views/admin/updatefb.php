<?php $this->load->view('layouts/admin/main'); ?>
<div class="col-md-12">
    <h2>Generate Batches</h2>
</div>
<div class="col-md-12">
    <?php 
        $offval = 0;
        $limitval = 100;
        for($i=0; $i<=$tp;$i++){ 
    ?>
    <a href="<?php echo base_url(); ?>batch/getupdatedFB/<?php echo $offval; ?>/<?php echo $limitval; ?>" class="btn btn-primary" style="margin-bottom: 10px;">Batch <?php echo $limitval; ?> rows</a>
    <?php 
            if($i == ($tp-1)){
                $offval = $i*100;
                $limitval = $totalRecs - $offval;
            }
            else {
                $offval = $offval+100;
            }
        } 
    ?>
    
    <div class="alert alert-info">
        Total Rows Fetched ------------> <?php echo "<strong>".$totalRecs."</strong>"; ?><br />
        Total Batched Buttons Created in 100s --> <?php echo "<strong>".$tp."</strong>"; ?>
    </div>
    
</div>