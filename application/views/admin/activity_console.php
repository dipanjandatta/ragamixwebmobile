
<?php $this->load->view('layouts/admin/main'); ?>
<!--<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>-->
<!--<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>-->



<div  class="jsPanel jsPanel-theme-light ui-draggable ui-resizable jsPanel-state-normalized" id="myPanel" style="top: 100px; right: 100px;  min-width: 250px;     z-index: 99999;">
    <div class="jsPanel-hdr jsPanel-theme-light ui-draggable-handle">
        <h3 class="jsPanel-title" >Counters</h3>
        <div class="jsPanel-hdr-r">
            <div class="jsPanel-btn-close" style="background-image: none;"><span class="jsglyph jsglyph-remove" style="background-image: none;"></span></div>

            <div class="jsPanel-btn-norm" style="background-image: none;"><span class="jsglyph jsglyph-normalize" style="background-image: none;"></span></div>
            <div class="jsPanel-btn-min" style="background-image: none;"><span class="jsglyph jsglyph-minimize" style="background-image: none;"></span></div>


        </div>
        <div class="jsPanel-hdr-toolbar jsPanel-clearfix"></div>
    </div>
    <div class="jsPanel-content jsPanel-theme-light" style="overflow: hidden; width: 250px; height: 210px;">
        <div style="padding:10px;">
            <?php
            foreach($abc as $val):
            ?>

            <ul class='consoleul'>
                <li><a href="#">Signups <span><?php echo $val->signup_cnt?></span></a></li>
                <li><a href="#">Updates <span><?php echo $val->update_cnt?></span></a></li>
                <li><a href="#">Uploads <span><?php echo $val->upload_cnt?></span></a></li>
                <li><a href="#">Deletions <span><?php echo $val->delete_cnt?></span></a></li>
                <li><a href="#">Ad Submit <span><?php echo $val->adssubmit_cnt?></span></a></li>
                <li><a href="#">Event Submit <span><?php echo $val->eventsubmit_cnt?></span></a></li>
                <li><a href="#">Messages Sent <span><?php echo $val->msgsent_cnt?></span></a></li>
                <li><a href="#">Current Logins <span><?php echo $val->currlogin_cnt?></span></a></li>
            </ul>

                <?php
            endforeach;
            ?>
        </div>
    </div>

</div>

<div class="console_top" >
    <div class="container">
        <div class="row">


            <div class="col-lg-4">
                <div class="largetext center boldtext"><h3 style="margin: 0px">Activity Timeline Console</h3></div>
            </div>

            <?php echo form_open('dashboard/activity_console')?>
            <div class="col-lg-8">
                <div class="largetext center boldtext">From
<!--                   --><?php //if($from_date== '1990-01-01'){?>
                    <input  class="form-control" type="date" name="from_date" value="<?php echo date_format($from_date,"Y-m-d")?>"  style="height: 30px; width: 165px; display: inline-block;">
                    <!--<input type="text" class="form-control" style="display: inline-block;width:78%;background-color: transparent; border-style:solid !important; height:24px;margin-top:-18px;" datepicker-popup="dd/MM/yyyy" ng-model="from_date" is-open="$parent.opened" min-date="minDate" max-date="'2015-06-22'" datepicker-options="dateOptions"  ng-required="true" close-text="Close">-->
                    To <input class="form-control" style="height: 30px; width: 165px; display: inline-block;" type="date" value="<?php echo date_format($to_date,"Y-m-d")?>"  name="to_date">
                    <input class="btn btn-red btn-radius" style="height: 30px;" type="submit" name="go" value="GO" >
                    <select   name="seltime" class="form-control" style="height: 30px; width: 165px; display: inline-block;" >
                        <option >Select Time Interval</option>
                        <option value="0">All</option>
                        <option value="1"<?php if ($time_interval == '1') echo ' selected="selected"'; ?>>Last 1hrs</option>
                        <option value="2"<?php if ($time_interval == '2') echo ' selected="selected"'; ?>>Last 2hrs</option>
                        <option value="4"<?php if ($time_interval == '4') echo ' selected="selected"'; ?>>Last 4hrs</option>
                        <option value="8"<?php if ($time_interval == '8') echo ' selected="selected"'; ?>>Last 8hrs</option>
                        <option value="24"<?php if ($time_interval == '24') echo ' selected="selected"'; ?>>Last 24hrs</option>
                    </select>

                </div>
            </div>
            <?php form_close()?>


        </div>
    </div>
</div>
<!--<br/>-->
<div class="container">
    <div class="row">
        <div class="">
            <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope" >
                <div class="widget-body medium no-padding" id="gridscroll" style=" " ng-class="classes">

                    <div ng-hide="loading" class="widget-content" >
                        <div class="table-responsive1 ng-scope" style="height: 510px; overflow: auto;">
                            <table id="my_table" class="table"><thead>
                                <tr>
                                    <th class="hath" style="text-align: center">Time</th>
                                    <th  class="hath " style="text-align: center;cursor: pointer;">Date</th>
                                    <th  class="hath" style="text-align: center;cursor: pointer;">Activity</th>
                                    <th  class="hath" style="text-align: center;cursor: pointer;">User ID</th>
                                    <th  class="hath" style="text-align: center;cursor: pointer;">Page</th>
                                    <th  class="hath" style="text-align: center;cursor: pointer;">User Type</th>
                                    <th  class="hath" style="text-align: center;cursor: pointer;">City</th>
                                    <th  class="hath" style="text-align: center;cursor: pointer;">Pincode</th>
                                    <th  class="hath" style="text-align: center;cursor: pointer;">Mail ID</th>
                                    <th  class="hath" style="text-align: center;cursor: pointer;">Mobile No</th>
                                    <th  class="hath" style="text-align: center;cursor: pointer;">Keyword</th>

                                </tr>
                                </thead>

                                <tbody id="people">

                                <?php
                                foreach($retVal as $val):
//                                {
//                                    print_r($val);exit();


                                    ?>
                                <tr style="font-size: 11px;" >
<!--                                    <a href="" >-->
                                    <td class="text-center"><?php echo $val->activity_at_time?></td>
                                    <td class="text-center"><?php echo $val->activity_at_date?></td>
<!--                                    <td class="text-center"><a ng-href="{{con.case}}">--><?php //echo $val->activity_name?><!--</a></td>-->
                                    <?php if($val->activity_name == 'EVENT POSTED'){?>
                                    <td class="text-center"><a href="<?php echo base_url() ?>dashboard/activity_console/event_posted/<?php echo $val->event_ad_id ?>"><?php echo $val->activity_name?></a></td>
                                    <?php }
                                    elseif($val->activity_name == 'ADS'){?>
                                    <td class="text-center"><a href="<?php echo base_url() ?>dashboard/activity_console/ads/<?php echo $val->ad_id ?>"><?php echo $val->activity_name?></a></td>
                                    <?php }
                                    elseif($val->activity_name == 'UPDATE'){?>
                                        <?php if($val->page_name == 'event'){?>
                                        <td class="text-center"><a href="<?php echo base_url() ?>dashboard/activity_console/event_posted/<?php echo $val->event_ad_id ?>"><?php echo $val->activity_name?></a></td>
                                         <?php }
                                        elseif($val->page_name == 'ads'){?>
                                            <td class="text-center"><a href="<?php echo base_url() ?>dashboard/activity_console/ads/<?php echo $val->ad_id ?>"><?php echo $val->activity_name?></a></td>
                                        <?php }
                                        else{?>
                                            <td class="text-center"><a href="<?php echo base_url() ?>dashboard/activity_console/update/<?php echo $val->rm_id ?>"><?php echo $val->activity_name?></a></td>
                                        <?php } ?>
                                    <?php }
                                    else{?>
                                    <td class="text-center"><?php echo $val->activity_name?></td>
                                    <?php }?>
                                    <td class="text-center"><?php echo $val->user_name?></td>
                                    <td class="text-center"><?php echo $val->page_name?></td>
                                    <td class="text-center"><?php echo $val->user_category?></td>
                                    <td class="text-center"><?php echo $val->city_name?></td>
                                    <td class="text-center"><?php echo $val->pincode?></td>
                                    <td class="text-center"><?php echo $val->ad_id?></td>
                                    <td class="text-center"><?php echo $val->event_ad_id?></td>
<!--                                    <td class="text-center">--><?php //echo $val->rm_id?><!--</td>-->
                                    <td class="text-center"></td>
<!--                                    </a>-->
<!--                                    <td class="text-center">--><?php //echo $val->activity_at_time?><!--</td>-->
<!--                                    <td><a href="" ng-click="edit_video(myVideo)"><i class="fa fa-edit"></i> Edit</a></td>-->
                                </tr>
                                    <?php
//                                }
                                    endforeach;
                                ?>

                              </tbody>


                            </table>
                        </div>
                    </div>

                </div>
            </srd-widget-body>
        </div>
    </div>
</div>

<script type="text/javascript">
    var TSort_Data = new Array ('my_table','','d', 's', 's','s','s','s');
</script>