
<?php $this->load->view('layouts/admin/main'); ?>


<!DOCTYPE html>
<html>
<head>
    <script
        src="http://maps.googleapis.com/maps/api/js">
    </script>

    <script>

        var map;
        var geocoder;
        function initialize() {
            var latlng = new google.maps.LatLng(-34.397, 150.644);
            var myOptions =
            {
                zoom: 10,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: false
            };
            map = new google.maps.Map(document.getElementById("googleMap"), myOptions);
        }
        google.maps.event.addDomListener(window, 'load', initialize);

        function FindLocation() {
            geocoder = new google.maps.Geocoder();
            console.log("Geocoder specification");
            console.log(geocoder);
//            initialize();

            var address = document.getElementById("addressinput").value;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });

                }
                else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        function Button1_onclick() {
            FindLocation();

        }

    </script>
</head>

<body>
<div class="row ng-scope">

    <div class="col-sm-7 col-lg-7">

            <div class="dashing">

                <div style="font-size: 24px; font-weight: bold;margin-left: 15px;">Manage Location/City</div>
                <div style="float: left;margin-left: 18px;margin-top: 41px;width: 264px;border: 1px solid #ffffff;padding: 16px;">

<!--                    <div><label class="control-label" >Manage Location/City</label></div>-->

                    <div class="form-group">
                     <label class="control-label" >City</label>
                    <input type="text" style="float: right;">
                    </div>

                    <div class="form-group">
                        <label class="control-label" >Country</label>
                        <input type="text" style="float: right;">
                    </div>

                    <div class="form-group">
                        <label class="control-label" >Zone</label>
                        <input type="text" style="float: right;">
                    </div>

                    <div class="form-group">
                        <label class="control-label" >Latitude</label>
                        <input type="text" style="float: right;">
                    </div>

                    <div class="form-group">
                        <label class="control-label" >Longitude</label>
                        <input type="text" style="float: right;">
                    </div>
                </div>

                <div style="width: 432px;float: right;margin-top: 22px;">
                    <input id="addressinput" type="text" style="width: 366px;margin-top: 18px;margin-bottom: 3px" />
                    <input id="Button1" type="button" value="Find" onclick="return Button1_onclick()" />
                    <div id="googleMap" style="width:411px;height:213px;"></div>

                    <div style="margin-top: 10px">
                            <button class="btn btn-sm btn-info margin7 ng-scope">Add New</button>
                            <button class="btn btn-sm btn-info margin7 ng-scope">Save</button>
                            <button class="btn btn-sm btn-info margin7 ng-scope">Delete</button>
                    </div>
                </div>


            <div class="pull-right othpadding" style="margin-top: 5px;margin-right: 20px;">

            </div>
            </div>
    </div>
    <div class="col-sm-5 col-lg-5">
        <srd-widget>
            <div class="widget" style="border: 1px solid #ffffff;margin-right: 5%; margin-top: 5%;">
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope">
                    <div class="widget-body medium no-padding" style="height: 500px; overflow: scroll" ng-class="classes">
                        <!--                        <rd-loading ng-show="loading" class="ng-hide">-->
                        <!--                            <div class="loading">-->
                        <!--                                <div class="double-bounce1"></div>-->
                        <!--                                <div class="double-bounce2"></div>-->
                        <!--                            </div>-->
                        <!--                        </rd-loading>-->
                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive1 ng-scope">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th >City</th>
                                        <th >Country</th>
                                        <th>Zone</th>
                                        <th>Latitude</th>
                                        <th>Longitude</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </srd-widget-body>

            </div>
        </srd-widget>
    </div>

</div>

</body>
</html>

