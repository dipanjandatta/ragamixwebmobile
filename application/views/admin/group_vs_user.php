
<?php $this->load->view('layouts/admin/main'); ?>


<div class="row ng-scope">

    <div class="col-lg-6">
        <srd-widget>
            <div class="widget" >
                <srd-widget-header icon="fa-tasks" title="Servers" class="ng-scope ng-isolate-scope">
                    <div class="widget-header ng-binding" style="font-size: 24px; font-weight: bold;text-align: center">
                        Group Vs User

                    </div>
                </srd-widget-header>
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope" >

                    <div style="border: 1px solid #ffffff; padding: 6%;    margin-left: 40px;">

                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive1 ng-scope form-horizontal divpadding" >

                                <div class="form-group form-group-lg othpadding">
                                    <label class="col-sm-4 control-label labelcolor" for="lg">Group Name</label>
                                    <div class="col-sm-8">
                                        <!--<input class="form-control" type="text"   ng-model="usergroup_data.group_id" >-->
                                        <select class="form-control"  ng-model="group.group_id"  ng-change="get_user(group.group_id)">
                                            <option value="">Select Group</option>
                                            <option ng-repeat="group in groups_grid_data" value="{{group.group_id}}">{{group.group_name}}</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </srd-widget-body>

            </div>
        </srd-widget>
    </div>
    <div class="col-lg-6">
        <srd-widget>
            <div class="widget" style="border: 1px solid #ffffff;margin-right: 5%; margin-top: 5%;">
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope">
                    <div class="widget-body medium no-padding" style="height: 500px; overflow: scroll" ng-class="classes">
<!--                        <rd-loading ng-show="loading" class="ng-hide">-->
<!--                            <div class="loading">-->
<!--                                <div class="double-bounce1"></div>-->
<!--                                <div class="double-bounce2"></div>-->
<!--                            </div>-->
<!--                        </rd-loading>-->
                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive1 ng-scope">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th >UserName</th>
                                        <th>MobileNo</th>
                                        <!--<th>ProfileName</th>-->
                                    </tr>
                                    </thead>
                                    <tbody>
<!--                                    <tr ng-repeat="user in users_grid_data">-->
<!--                                        <td height="5px">{{user.user_name}}</td>-->
<!--                                        <td height="5px">{{user.mobileno}}</td>-->
<!--                                        <!--<td height="5px"></td>-->
<!--                                    </tr>-->

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </srd-widget-body>

            </div>
        </srd-widget>
    </div>
</div>