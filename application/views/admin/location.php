
<?php $this->load->view('layouts/admin/main'); ?>


<!DOCTYPE html>
<html>
<head>
    <?php echo $map['js']; ?>

</head>

<body>
<div class="row ng-scope">

    <?php
    echo form_open('dashboard/location_save_del');?>

    <div class="col-sm-7 col-lg-7">

            <div class="dashing">

                <div style="font-size: 24px; font-weight: bold;margin-left: 15px;">Manage Location/City</div>
                <div style="float: left;margin-left: 18px;margin-top: 41px;width: 264px;border: 1px solid #ffffff;padding: 16px;">

                    <div class="form-group" hidden>
                        <input type="text" name="location_id" value="<?php echo $editlocationid;?>" style="float: right;color: black;">
                    </div>

                    <div class="form-group">
                     <label class="control-label" >City</label>
                    <input type="text" name="city" value="<?php echo $editlocationcity;?>" style="float: right;color: black;">
                    </div>

                    <div class="form-group">
                        <label class="control-label" >Country</label>
                        <input type="text" name="country" value="<?php echo $editlocationcountry;?>" style="float: right;color: black;">
                    </div>

                    <div class="form-group">
                        <label style="padding-right: 17px" class="control-label" >Zone</label>
                        <select class="form-control" style="display: table-row-group;width: 53%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);" name="zone">
                            <option value="">Please Select</option>
                            <option value="EZ"<?php if ($editlocationzone == 'EZ') echo ' selected="selected"'; ?>>East</option>
                            <option value="WZ"<?php if ($editlocationzone == 'WZ') echo ' selected="selected"'; ?>>West</option>
                            <option value="NZ"<?php if ($editlocationzone == 'NZ') echo ' selected="selected"'; ?>>North</option>
                            <option value="SZ"<?php if ($editlocationzone == 'SZ') echo ' selected="selected"'; ?>>South</option>
                        </select>

<!--                        <input type="text" name="zone" value="--><?php //echo $editlocationzone;?><!--" style="float: right;color: black;">-->
                    </div>
                    <?php if($this->uri->segment(3)=='edit') { ?>
                    <div class="form-group">
                        <label class="control-label" >Latitude</label>

                        <input type="text" name="lat" value="<?php echo $this->session->userdata('adminlat');?>" style="float: right;color: black;">
                    </div>

                    <div class="form-group">
                        <label class="control-label" >Longitude</label>
                        <input type="text" name="long" value="<?php echo $this->session->userdata('adminlong');?>" style="float: right;color: black;">
                    </div>
                    <?php } else {?>
                    <div class="form-group">
                        <label class="control-label" >Latitude</label>

                        <input type="text" name="lat" value="<?php echo $lat;?>" style="float: right;color: black;">
                    </div>

                    <div class="form-group">
                        <label class="control-label" >Longitude</label>
                        <input type="text" name="long" value="<?php echo $long;?>" style="float: right;color: black;">
                    </div>
                    <?php } ?>
                </div>


                <div style="width: 432px;float: right;margin-top: 22px;">
                    <input id="addressinput" type="text" name="address" style="width: 366px;margin-top: 18px;margin-bottom: 3px;color:black" />
                    <?php if($this->uri->segment(3) == 'edit') { ?>
                        <input type="hidden" name="formval" value="edit" style="color:black"/>
                        <input type="hidden" name="hiddentext" value="<?php echo $this->uri->segment(4); ?>" style="color:black"/>
                    <?php } ?>
                    <input id="Button1" type="submit" name=find value="find" />
                    <div  style="width:411px;height:213px;"><?php echo $map['html']; ?></div>

                    <div style="margin-top: 10px">
<!--                            <button class="btn btn-sm btn-info margin7 ng-scope">Add New</button>-->
                            <button type="submit" name="save" value="save" class="btn btn-sm btn-info margin7 ng-scope">Save & Add New</button>
                            <button type="submit" name="delete" value="delete" class="btn btn-sm btn-info margin7 ng-scope">Delete</button>
                    </div>
                </div>


            <div class="pull-right othpadding" style="margin-top: 5px;margin-right: 20px;">
            </div>
            </div>
    </div>
    <div class="col-sm-5 col-lg-5">
        <srd-widget>
            <div class="widget" style="border: 1px solid #ffffff;margin-right: 5%; margin-top: 5%;">
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope">
                    <div class="widget-body medium no-padding" style="height: 500px; overflow: scroll" ng-class="classes">

                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive1 ng-scope">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th >City</th>
                                        <th >Country</th>
                                        <th>Zone</th>
                                        <th>Latitude</th>
                                        <th>Longitude</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($locationdatafetch as $val):
                                        ?>
                                        <tr>
                                            <td height="5px" hidden><?php echo $val->location_id?></td>
                                            <td height="5px"><?php echo $val->city?></td>
                                            <td height="5px"><?php echo $val->country?></td>
                                            <td height="5px"><?php echo $val->zone?></td>
                                            <td height="5px"><?php echo $val->lat?></td>
                                            <td height="5px"><?php echo $val->long?></td>
                                            <td class="text-left columnwidth" height="5px">
                                                <input type="checkbox" name="locationType[]" value="<?php echo $val->location_id?>"/>
                                            </td>

                                            </td>
                                            <td class="text-left columnwidth" height="5px">
                                                <?php echo anchor('dashboard/locations/edit/'.$val->location_id,'Edit', array('class'=>'btn btn-primary')); ?>

                                            </td>
                                        </tr>
                                        <?php
                                    endforeach;
                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </srd-widget-body>

            </div>
        </srd-widget>
    </div>

    <?php form_close();?>

</div>


</body>
</html>

