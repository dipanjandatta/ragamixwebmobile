
<?php $this->load->view('layouts/admin/main'); ?>


<div class="row ng-scope">

    <?php
    echo form_open('dashboard/genre_save_del');?>

    <div class="col-lg-6">
        <srd-widget>
            <div class="widget" >
                <srd-widget-header icon="fa-tasks" title="Servers" class="ng-scope ng-isolate-scope">
                    <div class="widget-header ng-binding" style="font-size: 24px; font-weight: bold;text-align: center">
                        Genre
                    </div>
                </srd-widget-header>
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope" >


                    <div style="border: 1px solid #ffffff; padding: 6%;    margin-left: 40px;">
                        <div ng-hide="loading" class="widget-content" >


                            <div class="table-responsive1 ng-scope form-horizontal">

                                <div class="form-group form-group-lg othpadding" style="margin-top: 15px; margin-right: 1px;">
                                    <label class="col-sm-4 control-label labelcolor">Genre</label>
                                    <div class="col-sm-8" >
                                        <?php if($this->uri->segment(3) == 'edit') { ?>
                                            <input class="form-control" type="text" name="genre" value="<?php echo $editgenrename; ?>">
                                            <input class="form-control" type="hidden" name="genreid" value="<?php echo $editgenreid; ?>">
                                        <?php } else { ?>
                                            <input class="form-control" type="text" name="genre" />
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="pull-right othpadding" style="margin-top: 5px;">
                                    <?php if($this->uri->segment(3) == 'edit') { ?>
                                        <button type="submit" class="btn btn-sm btn-info margin7 ng-scope" name="insert" value="Edit" >Edit</button>
                                    <?php } else { ?>
                                        <button type="submit" class="btn btn-sm btn-info margin7 ng-scope" name="insert" value="insert" >Save</button>
                                    <?php } ?>
                                    <button class="btn btn-sm btn-info margin7 ng-scope" >Cancel</button>
                                </div>
                            </div>

                        </div>
                    </div>


                </srd-widget-body>
            </div>
        </srd-widget>
    </div>
    <div class="col-lg-6">
        <srd-widget>
            <div class="widget" style="border: 1px solid #ffffff;margin-right: 5%; margin-top: 5%;">
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope">
                    <div class="widget-body medium no-padding" style="height: 500px; overflow: scroll" ng-class="classes">
<!--                        <rd-loading ng-show="loading" class="ng-hide">-->
<!--                            <div class="loading">-->
<!--                                <div class="double-bounce1"></div>-->
<!--                                <div class="double-bounce2"></div>-->
<!--                            </div>-->
<!--                        </rd-loading>-->

<!--                        --><?php //$attr2 = array('id' => 'signupForm2');
//                        echo form_open('dashboard/genre_delete',$attr);?>

                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive1 ng-scope">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th >Genre </th>
                                        <th>Select</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($genredata as $val):
                                    ?>
                                    <tr>
                                        <td height="5px"><?php echo $val->genre?></td>
                                        <td class="text-left columnwidth" height="5px">
                                            <input type="checkbox" name="genreType[]" value="<?php echo $val->genre_id?>"/>
                                        </td>
                                        <td class="text-left columnwidth" height="5px">
<!--                                            <button style="margin-top: 10px;">Edit</button>-->
                                            <?php echo anchor('dashboard/genre/edit/'.$val->genre_id,'Edit', array('class'=>'btn btn-primary')); ?>

                                        </td>

                                    </tr>
                                        <?php
                                    endforeach;
                                    ?>

                                    </tbody>
                                </table>
                                <div class="pull-right othpadding" >
                                    <button type="submit" name="delete" value="delete" class="btn btn-sm btn-info margin7 ng-scope"  style="margin-top: 10px;margin-right: 10px;">Delete</button>
                                </div>
                            </div>
                        </div>

                        <?php form_close();?>

                    </div>
                </srd-widget-body>

            </div>
        </srd-widget>

        <!--<div class="pull-right othpadding" >-->
        <!--<button class="btn btn-sm btn-info margin7 ng-scope" ng-click="delete_user_type()">Delete</button>-->
        <!--</div>-->
    </div>
    <?php form_close()?>
</div>
