
<?php $this->load->view('layouts/admin/main'); ?>

<div class="row ">

    <?php
    echo form_open('dashboard/featuredlist_ads');?>

    <div class="col-lg-6">

        <div style="font-size: 24px; font-weight: bold;text-align: center">
            Featured list for ads

        </div>

        <div style="border: 1px solid #ffffff; padding: 6%;    margin-left: 40px;">

            <div>
                <div class="table-responsive form-horizontal divpadding" >


                    <div class="pull-right othpadding" style="margin-top: 5px;">

                        <input type="text" name="ads_title_in" placeholder="Ads Tittle" >
                        <input type="text" name="user_name_in" placeholder="Uploaded By" >
                        <button type="submit" value="find" name="find" class="btn btn-sm btn-info margin7 ng-scope" >Find</button>
                    </div>



                </div>
                <div>
                    <div class="table-responsive1">
                        <table class="table">
                            <thead>
                            <tr>
<!--                                <th >Ads Id</th>-->
                                <th >Ads Title</th>
                                <th >Posted by</th>
                                <th ></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            foreach($findads as $val):
                                ?>
                                <tr>
<!--                                    <td height="5px">--><?php //echo $val->ad_id?><!--</td>-->
                                    <td height="5px"><?php echo $val->ads_title?></td>
                                    <td height="5px"><?php echo $val->user_name?></td>
                                    <td height="5px"><a data-toggle="modal" data-target="#myModal<?php echo $val->ad_id;?>">select</a></td>
                                </tr>
                            <?php endforeach;?>


                            <div class="container">

                                <!-- Modal -->


                            </div>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div style="border: 1px solid #ffffff;margin-right: 5%; margin-top: 5%;">
            <div class=" medium no-padding" style="height: 500px; overflow: scroll" >
                <div  >
                    <div class="table-responsive1">
                        <table class="table">
                            <thead>
                            <tr>
<!--                                <th >Ads Id</th>-->
                                <th >Ads Title</th>
                                <th >Posted By</th>
                                <th >Position</th>
                                <th ></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($featuredadslist as $val):
                                ?>
                                <tr>

<!--                                    <td height="5px">--><?php //echo $val->ad_id?><!--</td>-->
                                    <td height="5px"><?php echo $val->ads_title?></td>
                                    <td height="5px"><?php echo $val->user_name?></td>
                                    <td height="5px"><?php echo $val->position?></td>
                                    <td height="5px"><?php echo anchor('dashboard/featuredlist_ads/remove/'.$val->featured_id,'Remove', array('class'=>'btn btn-primary')); ?>


                                </tr>
                            <?php endforeach;?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php form_close()?>
</div>
</div>

<?php
foreach($findads as $val):
?>
<div class="modal fade" id="myModal<?php echo $val->ad_id; ?>" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ADD POSITION</h4>
            </div>
            <div class="modal-body">
                    <?php echo form_open('dashboard/featuredlist_ads'); ?>
                    <input type="hidden" name="adsid"  value="<?php echo $val->ad_id?>" />
                    <input type="text" name="adstitle" value="<?php echo $val->ads_title ?>" placeholder="Ads Title" />
                    <input type="text" name="username" value="<?php echo $val->user_name ?>" placeholder="Username" />
                    <input type="hidden" name="rmid" value="<?php echo $val->rm_id ?>" />
                    <input type="text" name="position"  placeholder="Position" />
                    <input type="submit" class="contact_btn send_btn" value="add" name="add" />
                <?php echo form_close(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<?php endforeach;?>


<script>
    function myFunction() {
        var x = document.getElementById("mySelect").value;
        if(x=="insert"){
            document.getElementById('demo').style.visibility = 'hidden';
            document.getElementById('demo1').style.visibility = 'visible';
            document.getElementById('demo2').disabled = true;

        }
        if(x=="assign"){
            document.getElementById('demo').style.visibility = 'visible';
            document.getElementById('demo1').style.visibility = 'hidden';
            document.getElementById('demo2').disabled = false;
        }

    }
</script>