<?php $this->load->view('layouts/admin/main'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php echo form_open('batch/featuredProfilesList'); ?>
            <div class="col-md-6 noPad">
                <select name="fprofs" class="selclass">
                    <option value="HomePage">Home Page</option>
                    <option value="Musician">Musicians</option>
                    <option value="Singer">Singers</option>
                    <option value="Band">Bands</option>
                </select>
            </div>
            <div class="col-md-6 noPad">
                <input type="submit" name="submit" value="Get List" class="btn btn-primary" />
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?php if($this->uri->segment(3) != ''){ ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 margTop">
            <div class="col-md-5 col-md-offset-3 margTop">
                <div class="table-responsive1">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>User Name</th>
                          <th>Position</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach($fps as $val): ?>
                        <tr>
                            <td class="smallBlock"><?php echo $val->user_name; ?></td>
                            <td class="smallBlock"><?php echo $val->position; ?></td>
                            <td class="smallBlock">
                                <?php 
                                    if($this->uri->segment(3) == 'HomePage'){
                                        echo anchor('batch/deleteFeaturedProfile/'.$this->uri->segment(3).'/'.$val->featured_id, 'Remove'); 
                                    } else {
                                        echo anchor('batch/deleteFeaturedProfile/'.$this->uri->segment(3).'/'.$val->serial_no, 'Remove'); 
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php endforeach;  ?>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-md-offset-3 margTop">
        <?php echo form_open('batch/featuredProfilesSearch'); ?>
            <input type="hidden" name="fp" value="<?php echo $this->uri->segment(3); ?>" />
            <input type="text" name="uname" size="54" class="ragacontrols" /><input type="submit" name="homepagesubmit" class="btn btn-primary" value="Find" />
        <?php echo form_close(); ?>
    </div>
</div>
<?php } if($this->uri->segment(4) == 1) { ?>
<div class="col-md-12 margTop">
    <div class="col-md-5 col-md-offset-3 margTop">
        <div class="table-responsive1">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>User Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($users as $val): ?>
                <tr>
                    <td class="smallBlock"><?php echo $val->user_name; ?></td>
                    <td><a data-toggle="modal" data-target="#fpvolts<?php echo $val->rm_id; ?>">Select</a></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?php foreach($users as $kalwa): ?>
                                <div class="modal fade" id="fpvolts<?php echo $kalwa->rm_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h1 class="modal-title">Add a Position</h1>
                                      </div>

                                      <div class="modal-body">
                                          <?php echo form_open('batch/postPosition'); ?>
                                            <input type="text" name="uname" value="<?php echo $kalwa->user_name; ?>" />
                                            <input type="text" name="sv" value="<?php echo $this->uri->segment(3); ?>" />
                                            <input type="text" name="positionvector" placeholder="Position Number" />
                                            <input type="hidden" name="rmid" value="<?php echo $kalwa->rm_id; ?>" />
                                            <input type="submit" name="submit" value="Add" class="uibutton large" />
                                          <?php echo form_close(); ?>
                                      </div>
                                      <div class="modal-footer"> 
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php } ?>

