<?php $this->load->view('layouts/admin/main'); ?>

<div class="fullWidthHeader">
    Edit Event
</div>
<!--Something Went Wrong...Please Try Later-->
<?php
$attr = array('enctype' => 'multipart/form-data');
echo form_open('dashboard/editEventsadmin', $attr);
?>
<div class="col-md-12 formModalBack">
    <input type="hidden" name="event_ad_id_in" value="<?php echo $updateData[0]->event_ad_id; ?>" />
    <input type="hidden" name="photo_id_in" value="<?php echo $updateData[0]->photo_id; ?>" />
    <input type="hidden" name="pic_url" value="<?php echo $updateData[0]->pic_url; ?>" />
    <input type="hidden" name="rm_id" value="<?php echo $updateData[0]->rm_id; ?>" />
    <div class="control-group form-group ragaformarFix">
        <div class="controls">
            <p>
                <?php
                $options = array(
                    'Events' => 'Events',
                    'Newsbits' => 'News',
                    'Reviews' => 'Reviews',
                    'interview' => 'Interview'
                );
                $selectedAttr = array($updateData[0]->category);
                $selId = 'class=selclass id=category';
                echo form_dropdown('category',$options,$selectedAttr,$selId);
                ?>
            </p>
        </div>
    </div>
    <div class="col-md-12 noPad">
        <div class="col-md-4" style="padding-left: 0px !important;">
            <div class="control-group form-group ragaformarFix">
                <div class="controls">
                    <input type="text" name="event_location" value="<?php echo $updateData[0]->event_location; ?>" class="form-control ragacontrols textFormControls" placeholder="Venue / Location">
                </div>
            </div>
        </div>
        <div class="col-md-4" style="padding-left: 0px !important;">
            <div class="control-group form-group ragaformarFix">
                <div class="controls">
                    <p>
                        <?php
                        $selectedAttr = array($updateData[0]->event_city);
                        $selId = 'class=selclass id=event_city';
                        echo form_dropdown('event_city',$locationdatafetch,$selectedAttr,$selId);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 noPad">
            <div class="control-group form-group ragaformarFix">
                <div class="controls">
                    <p>
                        <input type="text" id="event_date" name="event_date" value="<?php echo $updateData[0]->event_date; ?>" class="form-control ragacontrols textFormControls" placeholder="Date of Event">
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="control-group form-group ragaformarFix">
        <div class="controls">
            <p>
                <input type="text" id="event_title" name="event_title" value="<?php echo $updateData[0]->event_title; ?>" class="form-control ragacontrols textFormControls" placeholder="Heading">
            </p>
        </div>
    </div>
    <div class="control-group form-group ragaformarFix">
        <div class="controls">
            <textarea rows="3" name="event_desc" placeholder="Details" style="width: 100%;"><?php echo $updateData[0]->event_desc; ?></textarea>
        </div>
    </div>
    <div class="control-group form-group ragaformarFix">
        <div class="controls">
            <input type="text" name="doc_youtube_url" value="<?php echo $updateData[0]->doc_youtube_url; ?>" class="form-control ragacontrols textFormControls" placeholder="Add Youtube Link">
        </div>
    </div>
    <div class="col-md-12 noPad">
        <div class="col-md-4 noPad">
            <img id="previewing" style="max-width: 100%" src="<?php echo $updateData[0]->pic_url; ?>" /></div>
    </div>

    <select name="approval" class="form-control" style="height: 30px; width: 165px; display: inline-block;" >
        <option >Select Time Interval</option>
        <option value="1"<?php if ($updateData[0]->event_approved == '1') echo ' selected="selected"'; ?>>Approved</option>
        <option value="0"<?php if ($updateData[0]->event_approved == '0') echo ' selected="selected"'; ?>>Not Approved</option>
    </select>

    <div class="col-md-8 noPad">
        <input type="file" name="file" id="file" />
    </div>
</div>
<div class="col-md-12 noPad" style="margin-top: 10px;">
    <button type="submit" class="btn btnfix greenbtn" style="width: 100%;">UPDATE EVENT</button>
</div>
</div>
<?php echo form_close(); ?>
