
<?php $this->load->view('layouts/admin/main'); ?>


<div class="fixedhead">
    <div class="container">
        <div class="page-header accountphead clearfix">


            <div class="col-sm-4">

                <h2>{{user_name}}</h2>
            </div>
            <div class="col-sm-4">
                <!--<div style="margin-top: 12px" class="progress progress-striped active">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                        <span >Your Profile 60% updated</span>
                    </div>
                </div>-->

            </div>
            <div class="col-sm-4 accountrightbtn">

                <a style="margin-right: 10px;" href="" class="btn btn-success btn-radius" ng-click="preview_profile()">Preview Profile</a>
                <a style="margin-right: 10px;" href="" class="btn btn-success btn-radius" ng-click="back_search()" >Back</a>
                <a href="" class="btn btn-success btn-radius">Save & Update</a>
            </div>
        </div>
    </div>
</div>

<section>

    <div class="container">
        <div class="single-event accountinnerpage">
            <div class="row">

                <div class="col-sm-12">
                    <div role="tabpanel">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-justified">
                            <li role="presentation" > <a href="#/main/basicprofile" >Basic Profile</a></li>
                            <li role="presentation"><a href="#/main/my_profile" >My Music Profile</a></li>
                            <li role="presentation"><a href="#/main/photos" >Photos</a></li>
                            <li role="presentation" class="active"><a href="#/main/music" >Music</a></li>
                            <li role="presentation"><a href="#/main/videos" >Videos</a></li>
                            <li role="presentation"><a href="#/main/seeking">Seeking</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div  class="tab-pane active" >

                                <div class="row">

                                    <div class="col-sm-12">
                                        <div style="margin-top: 10px;" class="panel panel-default">

                                            <div class="panel-title">
                                                Manage Your Music

                                            </div>

                                            <div  class="panel-body">
                                                <div class="col-sm-6">
                                                    <div class="form-group clearfix">
                                                        <label  class="control-label form-label">Select a file from your hard drive *</label>
                                                        <input id="audiofile" type="file" class="form-control" file-model="myFile" ng-model="audioData.file_name" >
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group clearfix">
                                                        <label  class="control-label form-label">Title</label>
                                                        <input type="text" class="form-control" ng-model="audioData.doc_title">
                                                    </div>
                                                </div>
                                                <!--<div class="col-sm-12">
                                                  <div class="form-group clearfix">
                                                    <label  class="control-label form-label">Comment</label>
                                                    <textarea rows="5" class="form-control" ng-model="audioData.doc_comment"></textarea>
                                                  </div>
                                                </div>-->
                                                <div class="col-sm-12">
                                                    <div class="form-group clearfix">
                                                        <label><input type="checkbox" value="1" ng-model="audioData.doc_allow_listen" ng-checked="audioData.doc_allow_listen" /> Allow people to listen this song.</label>
                                                        <!--<label><input type="checkbox" value="1" ng-model="audioData.doc_allow_download" /> Allow people to download this song.</label>-->
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <div  class="col-sm-12">
                                                        <button class="btn btn-red btn-radius" ng-click="UploadAudio()">UPLOAD</button>
                                                    </div>
                                                </div>
                                                <div  class="col-sm-12">
                                                    <table class="table table-bordered table-striped">

                                                        <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Title</th>
                                                            <th>Type</th>
                                                            <th>Size</th>
                                                            <th>Allow listening</th>
                                                            <th>Play</th>
                                                            <!--<th>Download</th>-->
                                                            <!--<th>Edit</th>-->
                                                            <th>Delete</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr ng-repeat="myaudio in myAudeoList">
                                                            <td>{{myaudio.serial_no}}</td>
                                                            <td>{{myaudio.doc_title}}</td>
                                                            <td>{{myaudio.audio_type}}</td>
                                                            <td>{{myaudio.doc_size}}</td>
                                                            <td>{{myaudio.doc_allow_listen}}</td>
                                                            <td><button id="btnPlay_{{myaudio.serial_no}}" ng-click="play_Audio(myaudio.audio_url,myaudio.serial_no)" class="btn btn-red btn-radius">Play</button>
                                                                <!--<button id="btnStop_{{myaudio.serial_no}}" ng-click="stop_audio()" class="btn btn-red btn-radius">Stop</button>--></td>
                                                            <!--<td>{{myaudio.doc_allow_download}}</td>-->
                                                            <!--<td><a href="" ng-click="edit_audio(myaudio)"><i class="fa fa-edit"></i> Edit</a></td>-->
                                                            <td><a href="" ng-click="del_audio(myaudio.rm_id,myaudio.doc_id,myaudio.doc_path_url)"><i class="fa fa-close"></i> Delete</a></td>
                                                        </tr>

                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>


                </div>

            </div>


        </div>
    </div>
</section>