<?php $this->load->view('layouts/admin/main'); ?>


<div class="row ng-scope">

    <div class="col-lg-12">

        <div class="col-lg-3">
        </div>

        <div class="col-lg-6">
            <srd-widget>
                <div class="widget" >
                    <srd-widget-header icon="fa-tasks" title="Servers" class="ng-scope ng-isolate-scope">
                        <div class="widget-header ng-binding" style="font-size: 24px; font-weight: bold;text-align: center">
                            Upload CSV File

                        </div>
                    </srd-widget-header>
                    <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope" >

                        <div style="border: 1px solid #ffffff; padding: 6%;    margin-left: 40px;">

                            <div ng-hide="loading" class="widget-content" >
                                <div class="table-responsive1 ng-scope form-horizontal">

                                    <div class="form-group form-group-lg othpadding" style="margin-top: 15px; margin-right: 1px;">
                                        <label class="col-sm-4 control-label labelcolor"> Select CSV file</label>
                                        <div class="col-sm-8" >

                                            <input type="file" on-read-file="showContent($fileContent)" />

                                            <!--<input class="form-control" type="text"   ng-model="usertype_data.usertype"  ng-disabled="">-->
                                        </div>
                                    </div>

                                    <div class="pull-right othpadding" style="margin-top: 5px;">
                                        <button class="btn btn-sm btn-info margin7 ng-scope" ng-click="upload_csv_file_new();" ng-disabled="upload_butun_disable">Upload CSV</button>
                                        <button class="btn btn-sm btn-info margin7 ng-scope" ng-click="clear_csv();">Cancel</button>

                                    </div>


                                </div>

                            </div>

                        </div>

                    </srd-widget-body>

                </div>
            </srd-widget>
        </div>
        <div class="col-lg-3">
        </div>
    </div>


    <div class="col-lg-12">

        <srd-widget>
            <div class="widget" style="border: 1px solid #ffffff; margin-top: 5%;">

                <div style=" ">
                    <label>Verified:
                        <input type="checkbox" ng-model="checkboxModel.value" ng-change="check_func(checkboxModel.value)">
                    </label>
                </div>

                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope">
                    <div class="widget-body medium no-padding" style="height: 500px; overflow: scroll" ng-class="classes">
<!--                        <rd-loading ng-show="loading" class="ng-hide">-->
<!--                            <div class="loading">-->
<!--                                <div class="double-bounce1"></div>-->
<!--                                <div class="double-bounce2"></div>-->
<!--                            </div>-->
<!--                        </rd-loading>-->


                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive1 ng-scope">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th >Serial Number</th>
                                        <th >user_name</th>
                                        <th >email_id</th>
                                        <th >mobileno</th>
                                        <th >type_of_user</th>
                                        <th >address_city</th>
                                        <th >address_district</th>
                                        <th >pin</th>
                                        <th >age</th>
                                        <th >gender</th>
                                        <th >first_name</th>
                                        <th >user_desc</th>
                                        <th >experience_in_years</th>
                                        <th >other_stringinstr</th>
                                        <th >other_vocal</th>
                                        <th >other_genre</th>
                                        <th >youtube</th>
                                        <th >music_influence_desc</th>
                                        <th >facebook_link</th>

                                    </tr>
                                    </thead>
                                    <tbody >
<!--                                    <tr data-ng-repeat="pay in payment_arry  | orderBy:predicate:reverse">-->
<!---->
<!--                                        <td>-->
<!--                                            {{$index+1}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.user_name}}-->
<!--                                        </td>-->
<!---->
<!--                                        <td>-->
<!--                                            {{pay.email_id}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.mobileno}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.type_of_user}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.address_city}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.address_district}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.pin}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.age}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.gender}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.first_name}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.user_desc}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.experience_in_years}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.other_stringinstr}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.other_vocal}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.other_genre}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.youtube}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.music_influence_desc}}-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            {{pay.facebook_link}}-->
<!--                                        </td>-->
<!---->
<!--                                    </tr>-->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </srd-widget-body>

            </div>
        </srd-widget>
    </div>
</div>
