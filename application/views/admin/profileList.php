<?php $this->load->view('layouts/admin/main'); ?>
<div class="container">
<div class="pagingholder">
    <div class="filterOps">
        <a href="<?php echo base_url(); ?>admin/profileList?q=1&next=1&clicks=0&page_number=0" style="padding: 10px 50px; border-right: 1px solid #d8d8d8;float: left;">Musicians</a>
        <a href="<?php echo base_url(); ?>admin/profileList?q=2&next=1&clicks=0&page_number=0" style="padding: 10px 50px; border-right: 1px solid #d8d8d8;float: left;">Singers</a>
        <a href="<?php echo base_url(); ?>admin/profileList?q=3&next=1&clicks=0&page_number=0" style="padding: 10px 50px; border-right: 1px solid #d8d8d8;float: left;">Bands</a>
    </div>
                        <?php if($page_number > 0){ ?>
                    <a href="<?php echo base_url(); ?>admin/profileList?q=<?php echo $q; ?>&next=<?php echo $next; ?>&clicks=<?php echo ($clicks-1); ?>&page_number=<?php echo ($page_number-1); ?>&prev=y">
                        <i class="fa fa-chevron-left pageicon"></i>
                    </a>
                    <?php } ?>
                    <a href="<?php echo base_url(); ?>admin/profileList?q=<?php echo $q; ?>&next=<?php echo $next; ?>&clicks=<?php echo $clicks; ?>&page_number=<?php echo $page_number; ?>">
                        <i class="fa fa-chevron-right pageicon"></i>
                    </a>
    </div>
    <?php foreach($getAllSearchM as $val): ?>
     <div class="col-md-2 noPad stillBord">
                            <div class="demo-section k-content stillHeight">
                                <div class="coverClass blurim">
                                    <img src="<?php echo $val->profile_pic_url;?>" class="newImg" />
                                </div>
                                <div class="title grey">
                                    <?php echo $val->user_name; ?>
                                </div>
                                <div class="stnComm greyblack">
                                  <?php
                                        echo $val->city_name. "," .$val->user_category." ".$val->user_category_1;
                                  ?>
                                </div>&nbsp;
                                <?php echo anchor('profile/profileEdit/basic/'.$val->rm_id, 'Edit', array('class'=>'btn btn-primary')); ?>&nbsp;
                                <?php echo anchor('admin/profileList/delete/'.$val->rm_id, 'Delete', array('class'=>'btn btn-danger')); ?>
<!--                                <a data-toggle="modal" data-target="#delprof--><?php //echo $val->rm_id; ?><!--" class="btn btn-danger">Delete</a>-->
                            </div>
                        </div>
                    <div class="modal fade" id="delprof<?php echo $val->rm_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h1 class="modal-title">Confirm Action</h1>
                          </div>
                          <div class="modal-body">
                            The Delete process could not be initiated now. Work in progress. Please try later                            
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div>
    <?php endforeach; ?>
</div>