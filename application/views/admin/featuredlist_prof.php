
<?php $this->load->view('layouts/admin/main'); ?>

<div class="row ng-scope">

    <?php
    echo form_open('dashboard/featuredlist_prof');?>

    <div class="col-lg-6">
        <srd-widget>
            <div class="widget" >
                <srd-widget-header icon="fa-tasks" title="Servers" class="ng-scope ng-isolate-scope">
                    <div class="widget-header ng-binding" style="font-size: 24px; font-weight: bold;text-align: center">
                       Featured list for profile

                    </div>
                </srd-widget-header>
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope" >

                    <div style="border: 1px solid #ffffff; padding: 6%;    margin-left: 40px;">

                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive ng-scope form-horizontal divpadding" >

                                <div class="pull-left othpadding" style="margin-top: 5px;margin-left: 5px;">
                                <select class="form-control"  name="selval">
                                    <option value="">Select Profile type</option>
                                    <?php foreach($rolesrightdata as $val):?>
                                        <option  value="<?php echo $val->group_id?>" selected="selected"><?php echo $val->group_name?></option>
                                    <?php endforeach;?>
                                </select>
                                    </div>
                                <div class="pull-right othpadding" style="margin-top: 5px;">

                                    <input type="text" ></input>
                                    <button class="btn btn-sm btn-info margin7 ng-scope" >Find</button>
                                </div>



                            </div>
                            <div ng-hide="loading" class="widget-content" >
                                <div class="table-responsive1 ng-scope">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th >Username</th>
                                            <th ></th>
                                        </tr>
                                        </thead>
                                        <tbody>
<!--                                        --><?php //foreach($roles as $val):?>
                                            <tr>

                                                <td height="5px">hello</td>
                                                <td height="5px"><a  href="#myPopup" >select</a></td>

                                            </tr>
<div id="myPopup" class="modalDialog">
    <div>
        <a href="#close" title="Close" class="close">X</a>

        <div class="contact_form">
            <div id="fields">
                <?php $attr = array('id' => 'signupForm');
                echo form_open('dashboard/featuredlist_prof',$attr);
                ?>
                <div class="grid_12">
                    <input type="text" name="username" id="name" placeholder="Username" />
                    <input type="text" name="position" id="email" placeholder="Position" />
                </div>
<!--                <div class="grid_12 omega">-->
<!--                    --><?php
//                    if($this->session->flashdata('messageError'))
//                    {
//                        ?>
<!--                        <label class="error">-->
<!--                            --><?php //echo $this->session->flashdata('messageError'); ?>
<!--                        </label>-->
<!--                        --><?php
//                    }
//                    ?>
<!--                </div>-->

                <div class="clear"></div>
<!--                <input type="reset" class="contact_btn" value="Clear Form" />-->
                <input type="submit" class="contact_btn send_btn" value="add" name="add" onclick="return loadSubmit()" />
                <div class="clear"></div>
                <div style="visibility:hidden;" id="progress"/><span class="loading green" original-title="Loading, please wait"></span>
                <div class="showWait">Please wait....</div>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>

</div>
                                </div>

                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                        <div style="float: right">

                        </div>

                    </div>
    </div>
                </srd-widget-body>


        </srd-widget>
    </div>
    <div class="col-lg-6">
        <srd-widget>
            <div class="widget" style="border: 1px solid #ffffff;margin-right: 5%; margin-top: 5%;">
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope">
                    <div class="widget-body medium no-padding" style="height: 500px; overflow: scroll" ng-class="classes">
                        <!--                        <rd-loading ng-show="loading" class="ng-hide">-->
                        <!--                            <div class="loading">-->
                        <!--                                <div class="double-bounce1"></div>-->
                        <!--                                <div class="double-bounce2"></div>-->
                        <!--                            </div>-->
                        <!--                        </rd-loading>-->
                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive1 ng-scope">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th >Username</th>
                                        <th >Fullname</th>
                                        <th >Position</th>
                                        <th ></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($roles as $val):?>
                                        <tr>

                                            <td height="5px">hello<?php echo $val->user_name?></td>

                                        </tr>
                                    <?php endforeach;?>
                                    <!--                                        <td class="text-left" height="5px">-->
                                    <!--                                            <input type="checkbox" ng-model="rgts.rights_id" ng-true-value=true ng-false-value=false ng-click="right_check(rgts,rgts.rights_id)">-->
                                    <!--                                            </td>-->
                                    <!--<input type="checkbox" name="id" ng-model="rgts.right"  ng-click="right_check(rgts.right,rgts)">
                                </td>

<!--                                    </tr>-->

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </srd-widget-body>

            </div>
        </srd-widget>
    </div>

    <?php form_close()?>
</div>

<script>
    function myFunction() {
        var x = document.getElementById("mySelect").value;
        if(x=="insert"){
            document.getElementById('demo').style.visibility = 'hidden';
            document.getElementById('demo1').style.visibility = 'visible';
            document.getElementById('demo2').disabled = true;

        }
        if(x=="assign"){
            document.getElementById('demo').style.visibility = 'visible';
            document.getElementById('demo1').style.visibility = 'hidden';
            document.getElementById('demo2').disabled = false;
        }

    }
</script>