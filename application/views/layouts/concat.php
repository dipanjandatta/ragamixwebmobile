<?php 
//$this->output->cache(10); 
//header("Cache-Control:private, no-cache, no-store, must-revalidate", true);
//header("Pragma: no-cache", true);
//header("Content-Type: text/html; charset=utf-8", true);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="<?= $meta_description ?>" />
<meta name="keywords" content="<?= $meta_keywords ?>" />
<meta http-equiv="expires" content="0" />
<meta name="classification" content="<?= $meta_classification ?>" />
<meta name="Robots" content="index,follow" />
<meta name="revisit-after" content="2 Days" />
<meta name="language" content="en-us" />
<meta property="og:title" content="<?= $pageTitle ?>" />
<meta property="og:description" content="<?= $meta_description ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo current_url(); ?>" />
<?php if(isset($image)){ ?>
<meta property="og:image" content="<?= $image ?>" />
<?php } ?>
<title><?= $pageTitle ?></title>
<link href="<?php echo base_url(); ?>css/compiled.min.css" rel="stylesheet" type="text/css" media="screen" title="default" />

</head>
<?php
function full_flush() {
    ob_flush();
    flush();
}
?>
<body id="home">
    <div id="fb-root"></div>
    <script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.6&appId=1162483030462581"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script>
    <div id="topheader"></div>
    <div id="response"></div>
    <div id="middle_column">
        <div id="overlay" class="animate">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>
    <div id="foots"></div>
        <?php 
            $header_pagelet = $this->load->view('layouts/header', '', TRUE);
            $pageletone = array('id' => "topheader", "html" => $header_pagelet, "css" => "", "js" => "");
        ?>

        <?php 
            $footer_pagelet = $this->load->view('layouts/footer', '', TRUE);
            $pagelettwo = array('id' => "foots", "html" => $footer_pagelet, "css" => "", "js" => "");
        ?>

        
        <?php 
            $pageletthree = array('id' => "middle_column", "html" => $content_body, "css" => "", "js" => "");
        ?>


<!--?= $content_body ?-->
  
</body>
    
<script type="text/javascript" src="<?php echo base_url(); ?>js/main.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/carousel.js" async></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/select2.min.js" async></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/responsiveslider.js"></script>
<?php
    if($js_to_load != '') {
?>
        <script type="text/javascript" src="<?php echo $js_to_load; ?>"></script>
<?php
    }
?>
?>
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>js/ap.js"></script> -->
<script src="https://apis.google.com/js/platform.js" async></script>
<script>var pipe = new OpenPipe();</script>  
        <script>pipe.onPageletArrive(<?php echo json_encode($pageletone); ?>);</script>
        <?php full_flush(); ?>
        <script>pipe.onPageletArrive(<?php echo json_encode($pagelettwo); ?>);</script>
        <?php full_flush(); ?>
        <script>pipe.onPageletArrive(<?php echo json_encode($pageletthree); ?>);</script>
        <?php full_flush(); ?>
         <script>
            pipe.close();
        </script>

<?php 
    if(isset($map)) {
        echo $map['js']; 
    }
    else {
?>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<script>
  var map = new google.maps.Map(document.getElementById('googleMap'), {
      <?php if($this->session->userdata('userlat')) { ?>
    center: {lat: <?php echo $this->session->userdata('userlat'); ?>, lng: <?php echo $this->session->userdata('userlong'); ?>},
      <?php } else { ?>
    center: {lat: 22.5141379, lng: 88.3953893},
      <?php } ?>
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });


  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();
    //console.log(places);
    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };
      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
      
      document.getElementById("latlng").value = place.geometry.location;
      
    });
    map.fitBounds(bounds);
  });
</script>
<?php
    }
?>
<?php if ($this->session->flashdata('messageError') != '') { ?>
	<script type="text/javascript">
	$(document).ready(function() {	
		$.jGrowl("<?php echo $this->session->flashdata('messageError') ?>");	
	});
	</script>
<?php } ?>	
<script type="text/javascript">

var countries=new ddajaxtabs("countrytabs", "countrydivcontainer");
countries.setpersist(true);
countries.setselectedClassTarget("link");
countries.init();
</script> 
   
</html>
