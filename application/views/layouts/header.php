<?php 
$CI =& get_instance();
$CI->load->library('user_agent');
$CI->load->helper('cookie');
?>
<header>
<!-- <div class="col-md-12 topHeader">
    <div class="container">
        <div class="col-md-2 noPad">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url(); ?>images/l6.png" class="pogo" />
            </a>
        </div>
        <div class="col-md-7 noPad">
            <div id="searchContainer"></div>
        </div>
        <div class="col-md-3 noPad" style="text-align: right;">
        <?php if(!$this->session->userdata('user_id')) { ?>
            <a class="btn btnssin su" style="border-right: 1px solid;">Sign In</a>
            <a class="btn btnssin si" id="si">Sign Up</a>
        <?php } else {  ?>
            <div id="msgcnt"></div>
            <a class="account userBlock">
                <div class="userf">
                    <span class="caret"></span>&nbsp;&nbsp;
                        <?php 
                            if(strlen($this->session->userdata('user_name')) > 15){
                                echo substr($this->session->userdata('user_name'),0,15).'..'; 
                            }
                            else {
                                echo $this->session->userdata('user_name');
                            }
                        ?>
                </div> 
                <?php
                    $returnValue = htmlspecialchars($this->session->userdata('profile_pic'));
                ?>
                <div class="headerProfileHolder">
                    <div style='height: 35px; width: 35px; border-radius: 50px;background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $returnValue; ?>);'></div>
                </div>
            </a>
        <?php } ?>    
            <div class="dropdown">
                <?php if(!$this->session->userdata('user_id')) { ?>
                <a class="account">
                    <i class="fa fa-cog"></i>
                </a>
                <div class="submenu" style="display: none; ">
                    <div class="headerName">
                        SETTINGS
                    </div>
                  <ul class="root">
                      <li>
                          <?php echo anchor('profile/profileEdit/basic', 'Basic Profile'); ?>
                      </li>
                      <li>
                          <?php echo anchor('profile/profileEdit/music', 'Music Profile'); ?>
                      </li>
                      <li class="bots"></li>
                      <li >
                        <?php echo anchor('profile/profileEdit/pg', 'Manage Photo Gallery'); ?>
                      </li>


                    <li>
                      <?php echo anchor('profile/profileEdit/audiovideo', 'Manage Audio / Video'); ?>
                    </li>

                    <li>
                      <?php echo anchor('profile/profileEdit/showevents', 'Post Shows / Events'); ?>
                    </li>
                    <li>
                      <?php echo anchor('profile/profileEdit/postads', 'Post Ads'); ?>
                    </li>
                    <li>
                      <?php echo anchor('profile/profileEdit/musicresource', 'Post Seek Musicians'); ?>
                    </li>
                    <li class="bots"></li>
                    <li>
                      <?php echo anchor('welcome/logout', 'Sign Out'); ?>
                    </li>
                  </ul>
                </div>
                <?php } else { ?>
                <div class="submenu" style="display: none; left: 0 !important;">
                    <div class="headerName">
                        SETTINGS
                    </div>
                  <ul class="root">
                      <li>
                          <?php echo anchor('profile/profileEdit/basic', 'Basic Profile'); ?>
                      </li>
                      <li>
                          <?php echo anchor('profile/profileEdit/music', 'Music Profile'); ?>
                      </li>
                      <li class="bots"></li>
                      <li >
                        <?php echo anchor('profile/profileEdit/pg', 'Manage Photo Gallery'); ?>
                      </li>


                    <li>
                      <?php echo anchor('profile/profileEdit/audiovideo', 'Manage Audio / Video'); ?>
                    </li>

                    <li>
                      <?php echo anchor('profile/profileEdit/showevents', 'Post Shows / Events'); ?>
                    </li>
                    <li>
                      <?php echo anchor('profile/profileEdit/postads', 'Post Ads'); ?>
                    </li>
                    <li>
                      <?php echo anchor('profile/profileEdit/musicresource', 'Post Seek Musicians'); ?>
                    </li>
                    <li class="bots"></li>
                    <li>
                      <?php echo anchor('welcome/logout', 'Sign Out'); ?>
                    </li>
                  </ul>
                </div>
                <?php } ?>
                </div>
        </div>
    </div>
</div> -->
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top boxShadsNoBord" style="background: #232f3e !important;">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <i class="fa fa-bars" style="color: white;font-size: 30px;"></i>
          </button>
            <a href="<?php echo base_url(); ?>" class="navbar-brand">
                <img src="<?php echo base_url(); ?>images/favicon.png" class="pogo" style="width: 30px;" />
            </a>
            <div id="searchContainer"></div>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

          <ul class="nav navbar-nav navbar-right">
          	<?php if(!$this->session->userdata('user_id')) { ?>
            <li>
                <div class="col-xs-6 col-sm-3 smallPad" style="text-align: center; margin-bottom: 20px;">
                    <a data-toggle="modal" data-target="#dologin" style="background: none !important; color: white !important;">Login</a>
                </div>
            </li>
            <li>
                <div class="col-xs-6 col-sm-3 smallPad" style="text-align: center; margin-bottom: 20px;">
                    <a id="si" style="background: none !important; color: white !important;">Sign Up</a>
                </div>
            </li>
            <?php } else { ?>
            <li class="liLog" style="padding: 2px;"><div id="msgcnt"></div></li>
			<li class="liLog" style="padding: 2px;">
				<?php echo anchor($CI->config->item('chat_url').'public/user.php?source=nuser&un='.$this->session->userdata('user_name'), '<i class="fa fa-comments-o headIcons"></i>', array('target'=>'_blank')); ?>
			</li>
            <li class="liLog" style="padding: 2px;">
                <a href="<?php echo base_url(); ?>welcome/settings">
                    <i class="fa fa-cog headIcons"></i>
                </a>
            </li>
            <li class="liLog" style="width: 52%;">
			<a class="account userBlock" style="padding-bottom: 7px !important;">
                <div class="userf">
                        <?php 
                            if(strlen($this->session->userdata('user_name')) > 15){
                                echo substr($this->session->userdata('user_name'),0,15).'..'; 
                            }
                            else {
                                echo $this->session->userdata('user_name');
                            }
                        ?>
                </div> 
                <?php
                    $returnValue = htmlspecialchars($this->session->userdata('profile_pic'));
                ?>
                <div class="headerProfileHolder">
                    <div style='height: 35px; width: 35px; border-radius: 50px;background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $returnValue; ?>);'></div>
                </div>
            </a>
            </li>
			<?php } ?>
              <ul class="dropdown dropDownFixMenuLog">
                <?php if($this->session->userdata('user_id')) { ?>
                <li class="logMenu">
                	<?php echo anchor('profile/profileEdit/basic', 'Basic Profile'); ?>
                </li>
                <li class="logMenu">
                	<?php echo anchor('profile/profileEdit/music', 'Music Profile'); ?>
                </li>
                <li class="logMenu">
                	<?php echo anchor('profile/profileEdit/photo', 'Manage Photo Gallery'); ?>
                </li>
                <li class="logMenu">
                	<?php echo anchor('profile/profileEdit/audiovideo', 'Manage Audio / Video'); ?>
                </li>
                <li class="logMenu">
                	<?php echo anchor('profile/profileEdit/showevents', 'Post Shows / Events'); ?>
                </li>
                <li class="logMenu">
                	<?php echo anchor('profile/profileEdit/musicresource', 'Post Seek Musicians'); ?>
                </li>
                <li class="logMenu">
                	<?php echo anchor('profile/profileEdit/postads', 'Post Ads'); ?>
                </li>
                <li class="logMenu">
                	<?php echo anchor('welcome/logout', 'Sign Out'); ?>
                </li>
                <?php } ?>
              </ul>
<!--            <li class="dropdown">-->
<!--              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars menuBarFx"></i><span class="caret"></span></a>-->
<!--              <ul class="drssopdown-menu">-->
                  <div class="col-xs-3 col-sm-3 smallPad">
                      <a href="<?php echo base_url(); ?>musicians/singers">
                          <div class="blockMenuH">
                              <i class="fa fa-microphone iconMenu"></i>
                              <span class="ev">Singers</span>
                          </div>
                      </a>
                  </div>
                  <div class="col-xs-3 col-sm-3 smallPad">
                      <a href="<?php echo base_url(); ?>musicians/view">
                          <div class="blockMenuH">
                              <i class="fa fa-music iconMenu"></i>
                              <span class="ev">Musicians</span>
                          </div>
                      </a>
                  </div>
                  <div class="col-xs-3 col-sm-3 smallPad">
                      <a href="<?php echo base_url(); ?>musicians/band">
                          <div class="blockMenuH">
                              <i class="fa fa-magic iconMenu"></i>
                              <span class="ev">Bands</span>
                          </div>
                      </a>
                  </div>
                  <div class="col-xs-3 col-sm-3 smallPad">
                      <a href="<?php echo base_url(); ?>musicians/educators">
                          <div class="blockMenuH">
                              <i class="fa fa-graduation-cap iconMenu"></i>
                              <span class="ev">Educators</span>
                          </div>
                      </a>
                  </div>
                  <div class="col-xs-3 col-sm-3 smallPad">
                      <a href="<?php echo base_url(); ?>events/view">
                          <div class="blockMenuH">
                              <i class="fa fa-table iconMenu"></i>
                              <span class="ev">Events</span>
                          </div>
                      </a>
                  </div>
                  <div class="col-xs-3 col-sm-3 smallPad">
                      <a href="<?php echo base_url(); ?>ads/view">
                          <div class="blockMenuH">
                              <i class="fa fa-opencart iconMenu"></i>
                              <span class="ev">Ads</span>
                          </div>
                      </a>
                  </div>
                  <div class="col-xs-3 col-sm-3 smallPad">
                      <a href="<?php echo base_url(); ?>events/view">
                          <div class="blockMenuH">
                              <i class="fa fa-film iconMenu"></i>
                              <span class="ev">Reviews</span>
                          </div>
                      </a>
                  </div>
                  <div class="col-xs-3 col-sm-3 smallPad">
                      <a href="<?php echo base_url(); ?>musicians/studios">
                          <div class="blockMenuH">
                              <i class="fa fa-sliders iconMenu"></i>
                              <span class="ev">Studios</span>
                          </div>
                      </a>
                  </div>

<!--            </li>-->
          </ul>
          <!-- <ul class="nav navbar-nav navbar-right">
            <li><a href="../navbar/">Default</a></li>
            <li><a href="../navbar-static-top/">Static top</a></li>
            <li class="active"><a href="./">Fixed top <span class="sr-only">(current)</span></a></li>
          </ul> -->
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<!--div class="subHeader">
	<div class="container">
        <div class="col-md-2 noPad">&nbsp;</div>
        <div class="col-md-10 noPad">
        	<ul class="shl">
        		<li class="lhl">
        			<?php echo anchor('events/view', 'Events', array('class'=>'ahl')); ?>
        		</li>
        		<li class="lhl">
        			<?php echo anchor('musicians/singers', 'Singers', array('class'=>'ahl')); ?>
        		</li>
        		<li class="lhl">
        			<?php echo anchor('events/view', 'News', array('class'=>'ahl')); ?>
        		</li>
        		<li class="lhl">
        			<?php echo anchor('musicians/view', 'Musicians', array('class'=>'ahl')); ?>
        		</li>
        		<li class="lhl">
        			<?php echo anchor('ads/view', 'MarketPlace', array('class'=>'ahl')); ?>
        		</li>
        		<li class="lhl">
        			<?php echo anchor('music/listen', 'Listen', array('class'=>'ahl')); ?>
        		</li>
        		<li class="lhl">
        			<?php echo anchor('musicians/bands', 'Bands', array('class'=>'ahl')); ?>
        		</li>
        	</ul>
        </div>
	</div>
</div-->
</header>




<div class="modal fade" id="dologin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" style="padding: 0 !important;">
            <?php echo form_open('welcome/login'); ?>
	          <div class="panel-body panForm nanan">
	            <div class="signform" id="business">
	              <div class="control-group form-group ragaformarFix">
	                <div class="controls">
	                  <input type="text" name="loginId" class="form-control ragacontrols" placeholder="Email / Username">
	                </div>
	              </div>
	              <div class="control-group form-group ragaformarFix">
	                    <div class="controls">
	                        <input type="password" name="user_password" class="form-control ragacontrols" placeholder="Password">
	                    </div>
	                </div>
	                <div class="control-group form-group ragaformarFix">
	                    <div class="controls col-sm-6 noPad" style="float: left; text-align: right;     margin: 5px 0px 3px 0px;">
	                      <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 45px !important;">LOGIN</button>
	                    </div>
						<div class="controls col-sm-6 noPad" style="float: right; text-align: right;     margin: 5px 0px 3px 0px;">
						  <button type="button" class="btn btnfix greenbtn" style="padding: 7px 40px !important;" data-dismiss="modal">CANCEL</button>
						</div>
	                </div>
	            </div>
	          </div>
				<?php 
					echo form_close(); 
				?>
                <!--div class="socialLoginBarrier">
                    OR
                </div>
                <div class="socialLogin">
                    <a href="<?php echo base_url(); ?>oauthLogin/fblogin">
                        <img src="<?php echo base_url(); ?>images/fb.png" />
                    </a>
                </div-->
			</div>
		</div>
	</div>
</div>



<div class="splashers" style="display: none;">
  <div class="splash-inner">
    <div class="col-md-12" style="text-align: right;">
      <a class="si"><i class="fa fa-times-circle-o bigFontIcons"></i> </a>
    </div>
    <?php 
          $foo = $CI->input->cookie('user',true);
          $bar = $CI->input->cookie('user_img',true);
		  $returnValueOne = htmlspecialchars($bar);
    		if(empty($foo)){
	?>
      <div class="col-md-4-5 col-md-offset-4" style="padding-top: 25px;">
        <div class="panel panel-default noBord">
          <div class="panel-heading panelForm">
            Login
          </div>
            <?php echo form_open('welcome/login'); ?>
          <div class="panel-body panForm nanan">
            <div class="signform" id="business">
              <div class="control-group form-group ragaformarFix">
                <div class="controls">
                  <input type="text" name="loginId" class="form-control ragacontrols" placeholder="Email / Username">
                </div>
              </div>
              <div class="control-group form-group ragaformarFix">
                    <div class="controls">
                        <input type="password" name="user_password" class="form-control ragacontrols" placeholder="Password">
                    </div>
                </div>
                <div class="control-group form-group ragaformarFix">
                    <div class="controls col-sm-6 noPad" style="float: right; text-align: right;     margin: 5px 0px 30px 0px;">
                      <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 50px !important;">LOGIN</button>
                    </div>
                </div>
            </div>
          </div>
      </div>
      </div>
      <?php 
		}
		else {
	  ?>
	  <div class="container">
      <div class="col-md-5" style="padding-top: 25px;">
        <div class="panel panel-default noBord">
          <div class="panel-heading panelForm">
            Login
          </div>
            <?php echo form_open('welcome/login'); ?>
          <div class="panel-body panForm nanan">
            <div class="signform" id="business">
              <div class="control-group form-group ragaformarFix">
                <div class="controls">
                  <input type="text" name="loginId" class="form-control ragacontrols" placeholder="Email / Username">
                </div>
              </div>
              <div class="control-group form-group ragaformarFix">
                    <div class="controls">
                        <input type="password" name="user_password" class="form-control ragacontrols" placeholder="Password">
                    </div>
                </div>
                <div class="control-group form-group ragaformarFix">
                    <div class="controls col-sm-6 noPad" style="float: right; text-align: right;     margin: 5px 0px 30px 0px;">
                      <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 50px !important;">LOGIN</button>
                    </div>
                </div>
            </div>
          </div>
          <?php echo form_close(); ?>

      </div>
      </div>
      <div class="col-md-2 col-md-offset-1">
      	<div class="verticleImage"></div>
      </div>
      <?php echo form_open('welcome/login'); ?>
      <div class="col-md-4" style="padding-top: 25px;">
      	<a class="showAni">
      		<div class="oaa">
	      	<div class="oa" style='height: 200px; width: 200px; background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $returnValueOne; ?>);'></div>
	      	<span class="cookName oa">
	      		<?php echo $foo; ?>
	      	</span>
	      	</div>
      	</a>
      	<div class="cookRem">
      		<?php
      			// $arr = array(
      							// 'cn' => $foo,
      							// 'cim' => $returnValueOne
      			// ); 
				// $query = http_build_query($arr);
      			echo anchor('welcome/remtemp', '<i class="fa fa-times-circle-o"></i>', array('class'=>'recon')); 
      		?>
      	</div>
      	<div class="op">
      		
              <div class="control-group form-group ragaformarFix">
                    <div class="controls">
                    	<input type="hidden" name="loginId" value="<?php echo $foo; ?>">
                        <input type="password" name="user_password" class="form-control ragacontrols" placeholder="Enter Password">
                    </div>
                </div>
                <div class="control-group form-group ragaformarFix">
                    <div class="controls col-sm-6 noPad" style="    width: 100% !important;">
                      <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 50px !important;     width: 100%;">LOGIN</button>
                    </div>
                </div>
           
		</div>
		
      </div>
      <?php echo form_close(); ?>
	</div>
      <?php
      	}
	  ?>
      </div>
</div>
