<footer class="footer">
    <div class="container noPad">
        <div class="footerback">
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <ul style="padding: 0;list-style-type: none;">
                    <li>
                        <?php echo anchor(base_url(),'HOME', array('class'=>'flinks')); ?>
                    </li>
                    <li>
                        <?php echo anchor(base_url().'ads/view', 'CLASSIFIED ADS',array('class'=>'flinks')); ?>
                    </li>
                    <li>
                        <?php echo anchor(base_url().'events/view', 'EVENTS AND ARTICLES',array('class'=>'flinks')); ?>
                    </li>
                    <li>
                        <?php echo anchor(base_url().'musicians/view', 'BROWSE PROFILES',array('class'=>'flinks')); ?>
                    </li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <ul style="padding: 0;list-style-type: none;">
                    <li>
                        <?php echo anchor(base_url().'gen/aboutus','ABOUT US', array('class'=>'flinks')); ?>
                    </li>
                    <li>
                        <?php echo anchor(base_url().'gen/faq', 'FAQs',array('class'=>'flinks')); ?>
                    </li>
                    <li>
                        <?php echo anchor(base_url().'gen/tc', 'TERMS / CONDITIONS',array('class'=>'flinks')); ?>
                    </li>
                    <li>
                        <a class="flinks" href="#">CONTACT US</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3" style="margin-top: 110px;">
                <div class="social" style="padding-top: 5px;">
                    <?php echo anchor('https://www.facebook.com/RagaMixIndia/', '<i class="fa fa-facebook"></i>', array('target'=>'_blank')); ?>
                    <?php echo anchor('https://www.facebook.com/RagaMixIndia/', '<i class="fa fa-twitter"></i>', array('target'=>'_blank')); ?>
					<?php echo anchor('#', '<i class="fa fa-instagram"></i>', array('target'=>'_blank')); ?>
					<?php echo anchor('#', '<i class="fa fa-youtube-square"></i>', array('target'=>'_blank')); ?>
                </div>
                    
            </div>
            <div class="col-md-3" style="padding-top: 10px;">
                &copy; Ragamix 2016 . India. All Rights Reserved
            </div>
        </div>
    </div>
</footer>
