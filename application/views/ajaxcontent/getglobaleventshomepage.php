            <!--ul class="list-group listPad">
             <?php foreach($globalEvents as $val): ?>
                <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">
                    <li class="list-group-item adjustList">
                      <span class="badge caser pacer">
                        <?php echo $val->event_title; ?>
                      </span>
                      <span class="badge caser">
                        <?php echo $val->event_location; ?>
                      </span>
                      <span class="badge caser">
                          <?php
                              $splitTimeDate = explode(' ',$val->event_date);
                              echo date('d M Y', strtotime($splitTimeDate[0])).' @ '.date('h:i A', strtotime($splitTimeDate[1]));
                          ?>
                      </span>
                      <img src="<?php echo $val->pic_url;?>" class="listImgSmall" />
                    </li>
                </a>
              <?php endforeach; ?>
              <li class="km">
                <a href="<?php echo base_url(); ?>events/view" class="shadedlink">Know More</a>
              </li>
            </ul-->
<?php foreach($globalEvents as $val): ?>
<div class="col-md-12 noPad">
	<div style="height:200px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $val->pic_url; ?>);"></div>
	<span class="heventstitle">
		<?php echo $val->event_title; ?>
	</span>
	<div class="latest-text2 hvr-sweep-to-top">
		<h4><?php echo date('d M Y', strtotime($val->event_date)); ?></h4>
	</div>
</div>
<?php endforeach; ?>