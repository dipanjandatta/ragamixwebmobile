                 <?php 
                    if(empty($featuredBandsData)){
                ?>
                        <div class="noNots">
                                No Featured Bands
                        </div>
                <?php
                    }   
                    else {
                ?>
                <ul class="clearfix noo-featured-albums owl-carousel owl-theme">
                            <?php foreach($featuredBandsData as $val): ?>
                                <a href="<?php echo base_url(); ?>profile/details/band/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>">
									<div class="owl-item">
	                			<li>
	                			<div class="sh-featured-albumns-item">
									<?php
	                                	$returnValue = str_replace(' ', '%20', $val->profile_pic_url);
	                                	if($val->profile_pic_url != ''){
	                                ?>
                                        <div style="height:230px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
									<?php } ?>
									<div class="sh-top">
										<h3 class="product_title" style="margin-bottom: 0px !important;">
											<a href="#" class="ahl">
												<?php echo $val->user_name; ?>
											</a>
										</h3>
                                        <?php if($val->genre != ''){ ?>
                                            <span class="gps">
                                                <?php echo $val->genre; ?>
                                            </span>
                                        <?php } if($val->city_name != '') { ?>
                                            <span class="kps" style="color: white !important;">
                                                <?php echo $val->city_name; ?>
                                            </span>
                                        <?php } ?>
									</div>
	                			</div>
	                		</li>
	                	</div>
                	</a>
                	<?php endforeach; ?>
                </ul>
                <?php } ?>
