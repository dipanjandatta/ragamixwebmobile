<div class="list-group bg-white list-group-lg no-bg auto">
	<?php foreach($globalEvents as $val): ?>
		<a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>" class="list-group-item clearfix majorPd">
			<span class="pull-right h2 text-muted m-l">
	
			</span>
			<span class="pull-left thumb-sm-new avatar p-r">
				<img src="<?php echo $val->pic_url;?>" style="height: 70px;" />
			</span>
			<span class="clear">
				<span class="text-ellipsis"><?php echo $val->event_title; ?></span>
			</span>
			<small class="text-muted clear text-ellipsis">
				<?php echo $val->event_location; ?><br />
				<?php echo date('d M Y', strtotime($val->event_date)); ?>
			</small>
		</a>
	<?php endforeach; ?>
</div>
