                 <?php 
                    if(empty($featuredEventsData)){
                ?>
                        <div class="noNots">
                                No Featured Events
                        </div>
                <?php
                    }   
                    else {
                ?>
                <ul class="clearfix noo-featured-albums owl-carousel owl-theme">
						<?php foreach($featuredEventsData as $val): 
                            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->doc_youtube_url, $match)) {
                                $vid = $match[1];
                                $displayedYoutubeVideo = "<iframe width='272' height='225' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                            }    
                        ?>
					<a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">
	                	<div class="owl-item">
	                		<li>
	                			<div class="sh-featured-albumns-item">
									<?php
	                                	$returnValue = str_replace(' ', '%20', $val->pic_url);
	                                	if($val->pic_url != ''){
	                                ?>
	                					<div style="height:230px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
									<?php
										}
										else {
											echo $displayedYoutubeVideo;
										}
									?>
									<div class="sh-top">
										<h3 class="product_title">
											<a href="#" class="ahl">
												<?php echo $val->event_title; ?>
											</a>
										</h3>
									</div>
	                			</div>
	                		</li>
	                	</div>
                	</a>
                	<?php endforeach; ?>
                </ul>
                <?php } ?>