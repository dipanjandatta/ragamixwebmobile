<div class="overFlowScroller">
    <?php
        if(empty($countMsg)){
    ?>
    <div class="raptorscreate">
        <?php echo anchor('profile/conversationwindow', 'See All', array('class'=>'uibutton large')); ?>
    </div>
    <div class="raptors">
        No Messages to Read
    </div>
    <?php
        }
        else {
            foreach($countMsg as $val):
    ?>
    <!--a href="<?php echo base_url(); ?>profile/conversationwindow/<?php echo $val->rm_id; ?>/<?php echo $this->session->userdata('user_id'); ?>" class="convlink"-->
        <div class="notsMsgBlock">
            <div class="msgName">
                <span class="bolder"><?php echo $val->user_name; ?></span> has sent a message
            </div>
            <div class="mm">
                <?php echo $val->body_text; ?>
            </div>
        </div>
    <!--/a-->
    <?php
    endforeach;
    ?>
    <div id="more<?php echo $val->email_tran_id; ?>" class="morebox">
        <a href="#" class="more" id="<?php echo $val->email_tran_id; ?>">More</a>
    </div>
    <div class="raptorscreate" id="rc<?php echo $val->email_tran_id; ?>">
        <?php echo anchor('profile/conversationwindow', 'See All', array('class'=>'uibutton large')); ?>
    </div>
    <?php
        }
    ?>

</div>

<script type="text/javascript">
    $(document).ready(function(){
  $('a.more').click(function() 
    {
        var ID = $(this).attr("id");
            if(ID)
            {
                $("#more"+ID).html('<img src="<?php echo base_url(); ?>images/loading.gif" />');

                $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>ajaxcontent/moremessage",
                data: "lastmsg="+ ID, 
                cache: false,
                success: function(html){
                        $("#displaynotsblock").append(html);
                        $("#more"+ID).remove(); // removing old more button
                        $("#rc"+ID).remove(); // removing old more button
                    }
                });
            }
            else
            {
                $(".morebox").html('The End');// no results
            }

        return false;
    }); 
    });
</script>