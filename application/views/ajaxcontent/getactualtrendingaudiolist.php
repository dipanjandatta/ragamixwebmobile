<ul class="list-group list-group-lg no-bg auto m-b-none m-t-n-xxs playlist">
	<?php 
    	foreach($trendingAudio as $val): 
	?>
		<li audiourl="<?php echo $val->audio_url; ?>" cover="cover1.jpg" artist="<?php echo $val->uploaded_by; ?>" class="list-group-item clearfix">
			<a href="#" class="jp-play-me pull-right m-t-sm m-l text-md">
				<i class="fa fa-play"></i>
			</a>
			<a href="#" class="pull-left m-r">
				<i class="fa fa-music" style="font-size: 40px;"></i>
			</a>
			<span class="block text-ellipsis estcol">
				<?php
					if($val->audio_title != '') {  
						echo $val->audio_title;
					}
					else {
						echo "Untitled Audio";
					}
				?>
			</span>
			<small class="text-muted">
				By <?php echo $val->uploaded_by; ?>
			</small><br />
			<small class="text-muted">
				<i class="fa fa-file"></i>&nbsp;
                <?php $kbSize = (($val->size)/1024);
                	$mbSize = $kbSize/1024;
                    echo round($mbSize,2)." Mb"; 
				?>
			</small>
		</li>
	<?php endforeach ; ?>
</ul>
<div class="poo">
        <div class="player">
            <div class="title"></div>
            <div class="artist"></div>
            <div class="cover"></div>
            <div class="controlsp">
                <div class="play"></div>
                <div class="pause"></div>
                <div class="rew"></div>
                <div class="fwd"></div>
            </div>
            <div class="volume"></div>
            <div class="tracker"></div>
        </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {

    // inner variables
    var song;
    var tracker = $('.tracker');
    var volume = $('.volume');

    function initAudio(elem) {
        var url = elem.attr('audiourl');
        var title = elem.text();
        var cover = elem.attr('cover');
        var artist = elem.attr('artist');

        $('.player .title').text(title);
        $('.player .artist').text(artist);
        $('.player .cover').css('background-image','url(data/' + cover+')');;

        song = new Audio(url);

        // timeupdate event listener
        song.addEventListener('timeupdate',function (){
            var curtime = parseInt(song.currentTime, 10);
            tracker.slider('value', curtime);
        });

        $('.playlist li').removeClass('active');
        elem.addClass('active');
    }
    function playAudio() {
        song.play();

        tracker.slider("option", "max", song.duration);

        $('.play').addClass('hidden');
        $('.pause').addClass('visible');
    }
    function stopAudio() {
        song.pause();

        $('.play').removeClass('hidden');
        $('.pause').removeClass('visible');
    }

    // play click
    $('.play').click(function (e) {
        e.preventDefault();

        playAudio();
    });

    // pause click
    $('.pause').click(function (e) {
        e.preventDefault();

        stopAudio();
    });

    // forward click
    $('.fwd').click(function (e) {
        e.preventDefault();

        stopAudio();

        var next = $('.playlist li.active').next();
        if (next.length == 0) {
            next = $('.playlist li:first-child');
        }
        initAudio(next);
    });

    // rewind click
    $('.rew').click(function (e) {
        e.preventDefault();

        stopAudio();

        var prev = $('.playlist li.active').prev();
        if (prev.length == 0) {
            prev = $('.playlist li:last-child');
        }
        initAudio(prev);
    });

    // show playlist
    $('.pl').click(function (e) {
        e.preventDefault();

        $('.playlist').fadeIn(300);
    });

    // playlist elements - click
    $('.playlist li').click(function () {
        stopAudio();
        initAudio($(this));
    });

    // initialization - first element in playlist
    initAudio($('.playlist li:first-child'));

    // set volume
    song.volume = 1;

    // initialize the volume slider
    volume.slider({
        range: 'min',
        min: 1,
        max: 100,
        value: 80,
        start: function(event,ui) {},
        slide: function(event, ui) {
            song.volume = ui.value / 100;
        },
        stop: function(event,ui) {},
    });

    // empty tracker slider
    tracker.slider({
        range: 'min',
        min: 1, max: 10,
        start: function(event,ui) {},
        slide: function(event, ui) {
            song.currentTime = ui.value;
        },
        stop: function(event,ui) {}
    });
});
</script>