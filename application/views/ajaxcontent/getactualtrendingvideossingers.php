<?php 
	$crushers = 0;
	$counter = 1; 
?>
<div class="list-group bg-white list-group-lg no-bg auto">
<?php
	foreach($trendingVideos as $val): 
	if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->youtube_url, $match)) {
		$vid = $match[1];
        $crushers = 1;
		$displayedYoutubeVideo = "<iframe width='120' height='70' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
	}
    else {
    	$displayedYoutubeVideo = $val->youtube_url;
	}
?>

	<?php if($crushers == 1){ ?>
    	<a href="<?php echo base_url(); ?>musicians/getfullvideo?mediaset=http://www.youtube.com/embed/<?php echo $vid; ?>" class="majax bhover list-group-item clearfix">
	<?php } ?>
		<span class="pull-right h2 text-muted m-l">

		</span>
	<?php 
    	if($displayedYoutubeVideo != ''){
        	if (strpos($val->youtube_url, 'channel') !== false) {
	?>
    	<span class="pull-left thumb-sm avatar m-r">
        	<?php echo anchor($val->youtube_url, 'Visit Channel', array('target' => '_blank', 'class'=>'channelView')); ?>
		</span>
	<?php
    }
    	else if (strpos($val->youtube_url, 'user') !== false) {
	?>
    	<span class="pull-left thumb-sm avatar m-r">
			<?php echo anchor($val->youtube_url, 'Visit User', array('target' => '_blank', 'class'=>'channelView')); ?>
    	</span>
	<?php
    }
    	else { ?>
	<span class="pull-left thumb-sm avatar m-r">
	<?php
        echo $displayedYoutubeVideo;
	?>
	</span>
	<?php
	}
	}
	else {
	?>
	<div class="noVidsShow">
		No Video To Display
	</div>
    <?php
    }
?>

<span class="clear">
	<span class="text-ellipsis"><?php echo $val->youtube_title; ?></span>
</span>
<small class="text-muted clear text-ellipsis">
	<?php echo $val->uploaded_by; ?><br />
		<i class="fa fa-thumbs-up"></i>&nbsp;<?php echo $val->like_count; ?>&nbsp; | &nbsp; 
		<i class="fa fa-eye"></i>&nbsp;<?php echo $val->view_count; ?>
</small>
	<?php if($crushers == 1){ ?>
    	</a>
<?php } $counter++;?>
<?php endforeach; ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".majax").colorbox({width: "85%", top: "-5px"}); 
    });
</script>