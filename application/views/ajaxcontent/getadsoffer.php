<script type="text/javascript">
    $(document).ready(function(){ 
    var track_click = 0; //track user click on "load more" button, righ now it is 0 click
    
    var total_pages = <?php echo $total_pages; ?>;
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>ajaxcontent/getactualadsoffer",
                data: 'page='+track_click,
                cache: false,
                success: function(html)
                    {
                        track_click++;
                        $("#resultsadsoffer").html(html).show();
                    }
                });

    $(".load_more_ao").click(function (e) { //user clicks on button
    
        $(this).hide(); //hide load more button on click
        $('.animation_image_ao').show(); //show loading image

        if(track_click <= total_pages) //user click number is still less than total pages
        {
            //post page number and load returned data into result element
            $.post('<?php echo base_url(); ?>ajaxcontent/getactualadsoffer',{'page': track_click}, function(data) {
            
                $(".load_more_ao").show(); //bring back load more button
                
                $("#resultsadsoffer").append(data); //append data received from server
                
                //scroll page smoothly to button id
                //$("html, body").animate({scrollTop: $("#load_more_button").offset().top}, 10);
                
                //hide loading image
                $('.animation_image_ao').hide(); //hide loading image once data is received
    
                track_click++; //user click increment on load button
            
            }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                alert(thrownError); //alert with HTTP error
                $(".load_more_ao").show(); //bring back load more button
                $('.animation_image_ao').hide(); //hide loading image once data is received
            });
            
            
            if(track_click >= total_pages-1) //compare user click with page number
            {
                //reached end of the page yet? disable load button
                $(".load_more_ao").attr("disabled", "disabled");
            }
         }
          
        });
    });
</script>
<?php if($totC == 0) { ?>
<div class="noNots">
    No Offers to List
</div>
<?php } else { ?>
<div class="col-md-12 noPad">
    <div class="col-md-6">
        
    </div>
    <div class="col-md-6 bannerHeaderRole">
        <span class="counterNots"><?php echo $totC; ?> Offers Found</span>
    </div>
</div>
<div id="resultsadsoffer"></div>
    <button class="load_more_ao morebox borderStrong" id="load_more_ao_button">load More</button>
    <div class="animation_image_ao" style="display:none;"><img src="<?php echo base_url(); ?>images/loading.gif"> Loading...</div>
<?php } ?>