                <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$spotNews[0]->category); ?>/<?php echo $spotNews[0]->event_ad_id; ?>">
                  <div class="col-md-5 uniPad">
                    <div class="demo-section k-content">
                        <div class="coverClass cov">
                            <?php if($spotNews[0]->doc_youtube_url != '') { 
                                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $spotNews[0]->doc_youtube_url, $match)) {
                                        $vid = $match[1];
                                        $displayedYoutubeVideo = "<iframe width='470' height='240' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                                    }
                                    echo $displayedYoutubeVideo;
                            } else { ?>
									<div style='height: 240px; width: 450px; background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $spotNews[0]->pic_url; ?>);'></div>
                            <?php
								}
                            ?>
                        </div>  
                      <div class="title">
                        <?php
                        	$strrOne = substr($spotNews[0]->event_title, 0, 50); 
                        	echo $strrOne."..."; 
                        ?>
                      </div>
                    </div>
                  </div>
                </a>
                <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$spotNews[1]->category); ?>/<?php echo $spotNews[1]->event_ad_id; ?>">
                  <div class="col-md-3 uniPad">
                    <div class="demo-section k-content">
                        <div class="coverClass">
                            <?php if($spotNews[1]->doc_youtube_url != '') { 
                                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $spotNews[1]->doc_youtube_url, $match)) {
                                        $vid = $match[1];
                                        $displayedYoutubeVideo = "<iframe width='355' height='240' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                                    }
                                    echo $displayedYoutubeVideo;
                            } else { ?>
									<div style='height: 200px; width: 135px; background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $spotNews[1]->pic_url; ?>);'></div>
                            <?php
								}
                            ?>
                        </div>
                      <div class="title hard bigPadder">
                        <?php
                        	$strr = substr($spotNews[1]->event_title, 0, 30); 
                        	echo $strr."..."; 
                        ?>
                      </div>
                    </div>
                  </div>
                </a>
                  <div class="col-md-4 uniPad">
                    <ul class="list-group noListPad">
                        <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$spotNews[2]->category); ?>/<?php echo $spotNews[2]->event_ad_id; ?>">
                      <li class="list-group-item adjustList shadowList">
                  <span class="badge caser pacer">
                    <?php echo $spotNews[2]->event_title; ?>
                  </span>
                  <span class="badge caser lacer">
                    <?php
						$strrLio = substr($spotNews[2]->event_desc, 0, 100); 
                        echo htmlspecialchars_decode($strrLio)."...";
                    ?>
                  </span>
                        <div class="coverClass smallHeight">
                            <?php if($spotNews[2]->doc_youtube_url != '') { 
                                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $spotNews[2]->doc_youtube_url, $match)) {
                                        $vid = $match[1];
                                        $displayedYoutubeVideo = "<iframe width='300' height='100' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                                    }
                                    echo $displayedYoutubeVideo;
                            } else { ?>
									<div style='height: 95px; width: 130px; background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $spotNews[2]->pic_url; ?>);'></div>
                            <?php
								}
                            ?>
                        </div>
                      </li>
                        </a>
                        <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$spotNews[3]->category); ?>/<?php echo $spotNews[3]->event_ad_id; ?>">
                      <li class="list-group-item adjustList shadowList">
                  <span class="badge caser pacer">
                    <?php echo $spotNews[3]->event_title; ?>
                  </span>
                  <span class="badge caser lacer">
                    <?php
						$strrLit = substr($spotNews[3]->event_desc, 0, 100); 
                        echo htmlspecialchars_decode($strrLit)."...";
                    ?>
                  </span>
                        <div class="coverClass smallHeight">
                            <?php if($spotNews[3]->doc_youtube_url != '') { 
                                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $spotNews[3]->doc_youtube_url, $match)) {
                                        $vid = $match[1];
                                        $displayedYoutubeVideo = "<iframe width='300' height='100' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                                    }
                                    echo $displayedYoutubeVideo;
                            } else { ?>
									<div style='height: 95px; width: 130px; background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $spotNews[3]->pic_url; ?>);'></div>
                            <?php
								}
                            ?>
                        </div>
                      </li>
                        </a>
                        <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$spotNews[4]->category); ?>/<?php echo $spotNews[4]->event_ad_id; ?>">
                      <li class="list-group-item adjustList shadowList">
                  <span class="badge caser pacer">
                    <?php echo $spotNews[4]->event_title; ?>
                  </span>
                  <span class="badge caser lacer">
                    <?php
						$strrLith = substr($spotNews[4]->event_desc, 0, 100); 
                        echo htmlspecialchars_decode($strrLith)."...";
                    ?>
                  </span>
                       <div class="coverClass smallHeight">
                            <?php if($spotNews[4]->doc_youtube_url != '') { 
                                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $spotNews[4]->doc_youtube_url, $match)) {
                                        $vid = $match[1];
                                        $displayedYoutubeVideo = "<iframe width='300' height='100' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                                    }
                                    echo $displayedYoutubeVideo;
                            } else { ?>
									<div style='height: 95px; width: 130px; background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $spotNews[4]->pic_url; ?>);'></div>
                            <?php
								}
                            ?>
                       </div>
                      </li>
                        </a>
                    </ul>
                  </div>
