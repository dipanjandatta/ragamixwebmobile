            <?php 
            if(empty($messageusers)) { 
            ?>
<div class="noNots">
                No Users To List
</div>
<span class="noNotsSub">
    Looks like you have not exchanged any messages with / from someone. Start by creating a new message
</span>
    
            <?php } else { 
                foreach($messageusers as $val): 
            ?>
            <a href="#" class="usersPanel" id="<?php echo $val->rm_id; ?>">
            <div class="row list-group noMargBot">
                <div class="item  col-md-3 list-group-item oneMargBot sootheCol">
                    <div class="thumbnail morichika">
                        <img class="group list-group-image" src="<?php echo base_url(); ?>images/u2368.jpg" alt="" style="max-width: 13% !important;">
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading smallFont">
                                <?php echo $val->user_name; ?>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            <?php endforeach; }  ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("a.usersPanel").click(function(){
            var element = $(this);
            var I = element.attr("id");
            $.ajax({
             url: "<?php echo base_url(); ?>profile/allmessages?selId="+I, type: "GET",
             beforeSend: function() {
                $("#rightcolumn").html("<img src='<?php echo base_url(); ?>images/loading.gif' />");
             },
             success: function(data) {
                $("#rightcolumn").html(data);
             }
          })
        });
    });
</script> 