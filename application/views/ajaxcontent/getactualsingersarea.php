<?php foreach($trendingMusicians as $val): ?>
<a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>">
<div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
	<div class="itemNew">
		<div class="pos-rlt">
			<div class="test">
				<?php if($val->profile_pic_url != '') { ?> 
					<div style="height:100px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $val->profile_pic_url; ?>);"></div>
				<?php } else { ?>
					<img src="<?php echo base_url(); ?>images/user-dummy-pic.png" alt="" class="r r-2x img-full">
				<?php } ?>
			</div>
		</div>
		<div class="padder-v noPad">
			<a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>" class="text-ellipsis compactViewName">
            	<?php echo $val->user_name; ?>
			</a>
            <a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>" class="text-ellipsis text-xs text-muted nameCol" style="margin-top: -5px;">
				<?php if($val->genre != '') { ?>
                	<?php echo $val->genre.','; } echo $val->city_name; ?>
			</a>
		</div>
	</div>
</div>
</a>
<?php endforeach; ?>
