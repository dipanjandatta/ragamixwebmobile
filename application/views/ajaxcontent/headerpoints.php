<?php
           
                $attrreg = array('id'=>'regform','novalidate'=>'novalidate', 'class'=>'signupnew');
                echo form_open('welcome/signup', $attrreg);
?>
		<div class="col-md-4 col-md-offset-4" style="padding-top: 100px;">
          <div class="panel-body panForm nanan">
              <div class="control-group form-group ragaformarFix">
                  <div class="controls">
                      <p>
                      <?php
                        $css = 'class="formLog"';
                        $selId = 'id=user_cat_in';
                        echo form_dropdown('user_cat_in',$userTypeList,$css,$selId);
                      ?>
                      </p>
                  </div>
              </div>
              <div class="control-group form-group ragaformarFix">
                <div class="controls">
                    <p>
                        <input type="text" name="user_name_in" id="user_name_in" class="form-control ragacontrols" placeholder="RAGAMIX ID">
                    </p>
                </div>
              </div>
              <div class="control-group form-group ragaformarFix">
                <div class="controls">
                    <p>
                    <input type="password" name="password_in" id="password_in" class="form-control ragacontrols" placeholder="Password">
                    </p>
                </div>
              </div>
              <div class="control-group form-group ragaformarFix">
                <div class="controls">
                    <p>
                    <input type="text" name="email_in" id="email_in" class="form-control ragacontrols" placeholder="Email Address">
                    </p>
                </div>
              </div>
              <div class="control-group form-group ragaformarFix">
                <div class="controls">
                  <p>
                  <input type="text" name="mobile_no_in" id="mobile_no_in" class="form-control ragacontrols" placeholder="Mobile Number">
                  </p>
                </div>
              </div>
              <div class="control-group form-group ragaformarFix">
                <div class="controls">
                    <p>
                    <!--<input type="text" name="city_name_in" id="city_name_in" class="form-control ragacontrols" placeholder="City">-->
                        <select id="country" name="city_name_in" class="typeaheadFix"></select>
                    </p>
                </div>
              </div>
                    <div class="controls col-sm-6 noPad" style="float: left; text-align: right;">
                      <button type="submit" class="btn btnfix greenbtn" style="padding: 5px 45px !important;">SIGN UP</button>
                    </div>
                   <div class="controls col-sm-6 noPad" style="float: right; text-align: right;">
                      <button type="button" class="btn btnfix greenbtn" style="padding: 5px 45px !important;" id="siclose">CANCEL</button>
                    </div>
              <div class="col-md-12">
                  <div class="col-md-6" style="padding-right: 0 !important; color: #565656; font-weight: 700; float: left; width: 100%;">
                      Activation Link will be sent to your email
                  </div>
              </div>
          </div>
          </div>
            <?php echo form_close(); ?>
<script type="text/javascript">
  $(function() {
    $("#regform").validate({
        rules: {
            user_name_in: "required",
            user_cat_in: "required",
            city_name_in: "required",
            email_in: {
                required: true,
                email: true
            },
            mobile_no_in: {
                required: true,
                digits: true,
                maxlength: 10,
                minlength: 10
            },
            password_in: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            user_name_in: "Enter Ragamix ID / Username",
            user_cat_in: "Specify a category",
            city_name_in: "Provide a city",
            email_in: "Enter a valid email address",
            username: "Enter a valid username",
            mobile_no_in: {
                required: "Enter Phone Number",
                digits: "Only Digits allowed",
                minlength: "Minimum 10 digits",
                maxlength: "Maximum 10 digits"
            },
            password_in: {
                required: "Provide a password",
                minlength: "At least 5 Characters"
            }
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    }); 
    
    var country = [<?php foreach($cityName as $val): ?>"<?php echo $val->city; ?>", <?php endforeach; ?>];
    $("#country").select2({
      data: country
    });
});
</script>
