<?php
    if(empty($getFeaturedProfile)) {
?>
<div class="noNots" style="color: white !important;">
    No Featured Profiles to Display
</div>
<?php
    } else {
?>
                <!--div id="nav-01" class="crsl-nav">
                    <a href="#" class="previous"><i class="fa fa-chevron-left iconArrow"></i></a>
                    <a href="#" class="next"><i class="fa fa-chevron-right iconArrow"></i></a>
		</div-->
                    <!--div class="crsl-itemss" data-navigation="nav-01">
                        <div class="crsl-wrap"-->
                        	<div class="latest-grids">
                            <?php foreach($getFeaturedProfile as $val): ?>
                            <!--figure class="crsl-item figthat">
                                <a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>">
                                    <div class="col-md-12 noPad stillBord">
                                        <?php
                                            $returnValue = str_replace(' ', '%20', $val->profile_pic_url);
                                        ?>
                                        <div style="height:230px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                        <div class="displayNames">
                                            <span class="dps">
                                                <?php echo $val->user_name; ?>
                                            </span>
                                            <span class="gps">
                                                <?php if($val->genre){ 
                                                        echo "Genre: " .$val->genre;
                                                    } 
                                                ?>
                                            </span>
                                            <span class="kps">
                                                <?php echo $val->city_name; ?>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </figure-->
							<?php
                            	$returnValue = str_replace(' ', '%20', $val->profile_pic_url);
							?>
<a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>">
                            <div class="col-md-3 latest-grid noPad stillBord">
                            	<div class="latest-top">
                            		<div class="img-responsive">
                            			<div style="height:300px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                            		</div>
                            		<div class="latest-text">
                            			<h4><?php echo $val->user_name; ?></h4>
                            		</div>
                            	</div>
                            </div>
</a>
                            <?php endforeach; ?>
                            </div>
                        <!--/div>
                    </div-->
<script type="text/javascript">
    $(document).ready(function(){
	$('.crsl-itemss').carousel({ visible: 4, itemMinWidth: 180, itemMargin: 0 }); 
    });
</script>
    <?php } ?>
