<script type="text/javascript">
    $(document).ready(function(){ 
    var track_click = 0; //track user click on "load more" button, righ now it is 0 click
    
    var total_pages = <?php echo $total_pages; ?>;
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>ajaxcontent/getactualbuysellads",
                data: 'page='+track_click,
                cache: false,
                success: function(html)
                    {
                        track_click++;
                        $("#resultsbuysellads").html(html).show();
                    }
                });

    $(".load_more_bs").click(function (e) { //user clicks on button
    
        $(this).hide(); //hide load more button on click
        $('.animation_image_bs').show(); //show loading image

        if(track_click <= total_pages) //user click number is still less than total pages
        {
            //post page number and load returned data into result element
            $.post('<?php echo base_url(); ?>ajaxcontent/getactualbuysellads',{'page': track_click}, function(data) {
            
                $(".load_more_bs").show(); //bring back load more button
                
                $("#resultsbuysellads").append(data); //append data received from server
                
                //scroll page smoothly to button id
                //$("html, body").animate({scrollTop: $("#load_more_button").offset().top}, 10);
                
                //hide loading image
                $('.animation_image_bs').hide(); //hide loading image once data is received
    
                track_click++; //user click increment on load button
            
            }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                alert(thrownError); //alert with HTTP error
                $(".load_more_bs").show(); //bring back load more button
                $('.animation_image_bs').hide(); //hide loading image once data is received
            });
            
            
            if(track_click >= total_pages-1) //compare user click with page number
            {
                //reached end of the page yet? disable load button
                $(".load_more_bs").attr("disabled", "disabled");
            }
         }
          
        });
    });
</script>
<div class="col-md-12 noPad">
    <div class="col-md-6">
        
    </div>
    <div class="col-md-6 bannerHeaderRole">
        <span class="counterNots"><?php echo $totC; ?> Singers Found</span>
    </div>
</div>
<div id="resultsbuysellads"></div>
    <button class="load_more_bs morebox borderStrong" id="load_more_bs_button">load More</button>
    <div class="animation_image_bs" style="display:none;"><img src="<?php echo base_url(); ?>images/loading.gif"> Loading...</div>
