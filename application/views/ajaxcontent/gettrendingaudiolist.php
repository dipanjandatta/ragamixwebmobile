<script type="text/javascript">
    $(document).ready(function(){ 
    var track_click_audio = 0; //track user click on "load more" button, righ now it is 0 click
    
    var total_pages_audio = <?php echo $total_pages; ?>;

    //$('#results').load("<?php echo base_url(); ?>ajaxcontent/getactualtrendingvideoslist", {'page':track_click}, function() {track_click++;}); //initial data to load
                $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>ajaxcontent/getactualtrendingaudiolist",
                data: 'page='+track_click_audio,
                cache: false,
                success: function(html)
                    {
                        track_click_audio++;
                        $("#resultsaudio").html(html).show();
                    }
                });

    $(".load_more_audio").click(function (e) { //user clicks on button
    
        $(this).hide(); //hide load more button on click
        $('.animation_image_audio').show(); //show loading image

        if(track_click_audio <= total_pages_audio) //user click number is still less than total pages
        {
            //post page number and load returned data into result element
            $.post('<?php echo base_url(); ?>ajaxcontent/getactualtrendingaudiolist',{'page': track_click_audio}, function(data) {
            
                $(".load_more_audio").show(); //bring back load more button
                
                $("#resultsaudio").append(data); //append data received from server
                
                //scroll page smoothly to button id
                //$("html, body").animate({scrollTop: $("#load_more_audio_button").offset().top}, 10);
                
                //hide loading image
                $('.animation_image_audio').hide(); //hide loading image once data is received
    
                track_click_audio++; //user click increment on load button
            
            }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                alert(thrownError); //alert with HTTP error
                $(".load_more_audio").show(); //bring back load more button
                $('.animation_image_audio').hide(); //hide loading image once data is received
            });
            
            
            if(track_click_audio >= total_pages_audio-1) //compare user click with page number
            {
                //reached end of the page yet? disable load button
                $(".load_more_audio").attr("disabled", "disabled");
            }
         }
          
        });
    });
</script>
<div id="resultsaudio"></div>
    <button class="load_more_audio morebox borderStrong" id="load_more_audio_button">load More</button>
    <div class="animation_image_audio" style="display:none;"><img src="<?php echo base_url(); ?>images/loading.gif"> Loading...</div>
