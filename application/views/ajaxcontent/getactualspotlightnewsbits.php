                     <?php 
                        foreach($spotNews as $val): 
                            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->doc_youtube_url, $match)) {
                                $vid = $match[1];
                                $displayedYoutubeVideo = "<iframe width='370' height='265' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                            }
                    ?>
                    <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">
                        <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInUp animated">
                        	<div class="custom_wr">
                                    <?php if($val->doc_youtube_url != '') {
                                    	echo $displayedYoutubeVideo;
                                    } else { ?> 
                                        <div style="height:270px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $val->pic_url; ?>);"></div>
                                    <?php
                                    }    
                                    ?>
                                    <div class="wr">
                                    	<h3>
                                    		<span><?php echo $val->event_title; ?></span>
                                    	</h3>
                                    	<p>
										<?php
	                                        if(strlen($val->event_desc) > 100) {
	                                        // truncate string
	                                         $stringCut = substr($val->event_desc, 0, 100);
	
	                                         // make sure it ends in a word so assassinate doesn't become ass...
	                                         $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
	                                        }
	                                        else {
	                                            $string = $val->event_desc;
	                                        }
	                                        echo $string; 
                                  		?>
                                    	</p>
                                    	<hr />
                                    </div>
                        	</div>

                        </div>
                    </a>
                    <?php endforeach; ?>
