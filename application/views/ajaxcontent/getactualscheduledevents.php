<?php foreach($scheduledEvents as $val): ?>
<a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">
	<div class="col-md-4 col-sm-6">
		<div class="noo_team_item">
			<div class="team_thumbnail">
				<div style="-webkit-filter: grayscale(1);height:270px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $val->pic_url; ?>);"></div>
				<div class="team-info">
					<h4 class="team_name">
						<?php echo $val->event_title; ?>
					</h4>
					<span class="team_position">
						<?php echo $val->event_location; ?><br />
						<?php echo date('d M Y', strtotime($val->event_date)); ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</a>
<?php endforeach; ?>