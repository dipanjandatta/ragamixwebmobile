                <?php 
                    if(empty($featuredSingersData)){
                ?>
                        <div class="noNots">
                                No Featured Singers
                        </div>
                <?php
                    }   
                    else {
                    	foreach($featuredSingersData as $val): ?>
					<a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>">
						<div class="col-xs-4 col-sm-3 smallPad">
							<div class="itemNew">
								<div class="pos-rlt">
									<div class="item-overlay opacity r r-2x bg-black">
										<?php
											$returnValue = str_replace(' ', '%20',$val->profile_pic_url);
										?>
									</div>
									<div class="r r-2x img-full">
										<div style="height:150px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
									</div>
									<div class="padder-v coltext">
										<a href="#" class="text-ellipsis fwc">
											<?php echo $val->user_name; ?>
										</a>
										<?php if($val->genre != ''){ ?>
											<a href="#" class="text-ellipsis bwc" style="color: white !important; font-size: 10px;">
												<?php echo $val->genre; ?>
											</a>
										<?php } ?>
										<a href="#" class="text-ellipsis text-xs text-muted twc">
											<?php echo $val->city_name; ?>
										</a>
									</div>
								</div>
							</div>
						</div>
					</a>
                <?php endforeach; } ?>