<script type="text/javascript">
    $(document).ready(function(){ 
    var track_click = 0; //track user click on "load more" button, righ now it is 0 click
    
    var total_pages = <?php echo $total_pages; ?>;
    var city = '<?php echo $city; ?>'
    
                $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>ajaxcontent/getactualtrendingvideossingers",
                data: 'page='+track_click+'&city='+city,
                cache: false,
                success: function(html)
                    {
                        track_click++;
                        $("#resultssingersvids").html(html).show();
                    }
                });

    $(".load_more_singers_vids").click(function (e) { //user clicks on button
    
        $(this).hide(); //hide load more button on click
        $('.animation_image_singers_vids').show(); //show loading image

        if(track_click <= total_pages) //user click number is still less than total pages
        {
            //post page number and load returned data into result element
            $.post('<?php echo base_url(); ?>ajaxcontent/getactualtrendingvideossingers',{'page': track_click, 'city': city}, function(data) {
            
                $(".load_more_singers_vids").show(); //bring back load more button
                
                $("#resultssingersvids").append(data); //append data received from server
                
                //scroll page smoothly to button id
                //$("html, body").animate({scrollTop: $("#load_more_button").offset().top}, 10);
                
                //hide loading image
                $('.animation_image_singers_vids').hide(); //hide loading image once data is received
    
                track_click++; //user click increment on load button
            
            }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                alert(thrownError); //alert with HTTP error
                $(".load_more_singers_vids").show(); //bring back load more button
                $('.animation_image_singers_vids').hide(); //hide loading image once data is received
            });
            
            
            if(track_click >= total_pages-1) //compare user click with page number
            {
                //reached end of the page yet? disable load button
                $(".load_more_singers_vids").attr("disabled", "disabled");
            }
         }
          
        });
    });
</script>
<div id="resultssingersvids"></div>
    <button class="load_more_singers_vids morebox borderStrong" id="load_more_singers_vids_button">load More</button>
    <div class="animation_image_singers_vids" style="display:none;"><img src="<?php echo base_url(); ?>images/loading.gif"> Loading...</div>
