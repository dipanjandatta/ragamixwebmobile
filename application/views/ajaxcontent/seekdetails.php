<div class="container stanPad">
    <div class="row blog-row bg-color-white" style="padding: 10px;">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row blog-date">
              <!--?php echo date('l jS F Y', strtotime($updated_on)); ?><span class="offw">from <?php echo $city_name; ?></span-->
            </div>
            <!--            <div class="row blog-image center-block">-->
            <!--figure >
                <img style="position: absolute;height: 30%;width: 20%;top: 7px;left: 0px;" src="<?php echo $profile_pic_url; ?>">
            </figure-->

                <!--            </div>-->

            <div class="row blog-content" style="padding: 1px 0px 2px 8px !important;font-size: 18px;">
                <?php echo $keyword; ?>
            </div>
            <div class="row blog-content" style="padding: 1px 0px 2px 8px !important;">
<!--                <div style="width:10px;height: 10px">-->
<!--                <img style="height: 10px;width:10px" src="--><?php //echo $profile_pic_url; ?><!--">-->
                <!--                </div>-->
                <?php echo $description; ?>
            </div>
            <div class="row blog-title" style="padding: 11px 0px 13px 8px !important;">
                <div style="font-size: 8px;position: absolute;left: 13px;">
                    <span class="offw" style="padding: 0px !important;">By </span><a style="font-size:13px;" href="<?php echo base_url(); ?>profile/details/musician/<?php echo $user_name; ?>/<?php echo $rmid; ?>"><?php echo $user_name; ?></a><br>
                    <span >Posted On&nbsp;:&nbsp;<?php echo $updated_on; ?></span>
                </div>
                <div class="fb-share-button" data-href="<?php echo current_url(); ?>" data-layout="button" data-mobile-iframe="true" style="float: right;"></div>

            </div>

            <div class="btn-group btn-group-justified m-b">
                <div class="modal-body">
                    <?php
                    if($this->session->userdata('user_id')) {
                        $atr = array('style'=>'margin-top: 10px');
                        echo form_open('profile/sendmessagefromprofile',$atr);
                        ?>
                        <textarea name="body_text" cols="5" rows="5" placeholder="Send a message to contact <?php echo $user_name; ?>"></textarea>
                        <input type="hidden" name="toid" value="<?php echo $rmid; ?>" />
                        <input type="submit" name="submit" value="Post Message" class="uibutton large btn-success" style="width: 100%;" />
                        <?php
                        echo form_close();
                    }
                    else {
                        echo "Please "?>&nbsp;<a data-toggle="modal" data-target="#dologin" style="text-decoration: underline">Login</a>&nbsp;<?php echo "to Send a message to ".$user_name;
                    }
                    ?>
                </div>
            </div>
            <!--div class="bottomTool">
                <ul class="sek">
            <?php if($location != '') { ?>
                    <li class="sekl">
                    <i class="fa fa-globe"></i> at <?php echo $location; ?>
                    </li>
            <?php } if($keyword != '') { ?>
                    <li class="sekl">
                      <i class="fa fa-bookmark"></i><?php echo $keyword; ?>
                    </li>
            <?php } ?>
                </ul>
                <span class="tagers">
                  <?php echo $hashtags; ?>
                </span>
            </div-->
        </div>
    </div>


    <div class="modal fade" id="sendmessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h1 class="modal-title">Send a Message</h1>
                </div>

                <div class="modal-body">
                    <?php
                    if($this->session->userdata('user_id')) {
                        echo form_open('profile/sendmessagefromprofile');
                        ?>
                        <textarea name="body_text" cols="38" rows="10" placeholder="Create a message"></textarea>
                        <input type="hidden" name="toid" value="<?php echo $rmid; ?>" />
                        <input type="submit" name="submit" value="Post Message" class="uibutton large" />
                        <?php
                        echo form_close();
                    }
                    else {
                        echo "Please Login to Send a message to ".$user_name;
                    }
                    ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(@$err){ ?>
	<div class="error" style="position: absolute; background: red; width: 100%; text-align: center; padding: 20px; top: 100px;  z-index: 999999999 !important; color: white; font-size: 30px;">
		<?php echo @$err; ?>
	</div>
<?php } ?>	
    <!--div class="blog-row-spacing"></div-->

<div class="modal fade" id="dologin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0 !important;">
                <?php echo form_open('welcome/login'); ?>
                <div class="panel-body panForm nanan">
                    <div class="signform" id="business">
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <input type="text" name="loginId" class="form-control ragacontrols" placeholder="Email / Username">
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <input type="password" name="user_password" class="form-control ragacontrols" placeholder="Password">
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls col-sm-6 noPad" style="float: left; text-align: right;     margin: 5px 0px 3px 0px;">
                                <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 45px !important;">LOGIN</button>
                            </div>
                            <div class="controls col-sm-6 noPad" style="float: right; text-align: right;     margin: 5px 0px 3px 0px;">
                                <button type="button" class="btn btnfix greenbtn" style="padding: 7px 40px !important;" data-dismiss="modal">CANCEL</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                echo form_close();
                ?>
                <!--div class="socialLoginBarrier">
                    OR
                </div>
                <div class="socialLogin">
                    <a href="<?php echo base_url(); ?>oauthLogin/fblogin">
                        <img src="<?php echo base_url(); ?>images/fb.png" />
                    </a>
                </div-->
            </div>
        </div>
    </div>
</div>

