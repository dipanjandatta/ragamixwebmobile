<?php $crushers = 0; ?>
                              <?php 
                                foreach($trendingVideos as $val): 
                                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->youtube_url, $match)) {
                                        $vid = $match[1];
                                        $crushers = 1;
                                        $displayedYoutubeVideo = "<iframe width='305' height='250' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                                    }
                                    else {
                                        $displayedYoutubeVideo = $val->youtube_url;
                                    }
                              ?>
<div class="col-md-3 portfolio-item jquery">
	<figure>
		<?php echo $displayedYoutubeVideo; ?>
	</figure>
	<div class="text">
		<span><?php echo $val->uploaded_by; ?></span>
		<h5>
			<?php
				if($val->youtube_title != ''){ 
					echo $val->youtube_title; 
				}
				else {
					echo "No Title";
				}
			?>
		</h5>
		<div class="tracks-btn">
			<i class="fa fa-thumbs-up"></i>&nbsp;<?php echo $val->like_count; ?>
		</div>
		<div class="px-cart">
			<i class="fa fa-eye"></i>&nbsp;<?php echo $val->view_count; ?>
		</div>
	</div>
	
</div>


<?php endforeach; ?>