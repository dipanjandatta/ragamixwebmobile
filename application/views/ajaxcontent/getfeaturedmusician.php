     <?php foreach($featuredMusicianData as $val): ?>
     	<div class="col-md-4 col-sm-6">
     		<div class="noo-sh-grid-event gridEventFix">
     			<div class="noo-thumbnail">
     				<?php
						$returnValue = str_replace(' ', '%20', $val->profile_pic_url);
					?>
     				<div style="height:250px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
     			</div>
     			<div class="noo-shevent-content">
					<div id="ribbon">
						<div id="content">
							<h4>
								<a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>">
									<?php echo $val->user_name; ?>
								</a>
							</h4>
							<div class="sh-meta">
								<span class="sh-address" style="margin-top: -14px;">
									<?php echo $val->city_name; ?>
								</span>
							</div>
						</div>
					</div>
     			</div>
     		</div>
     	</div>
	 <?php endforeach; ?>			