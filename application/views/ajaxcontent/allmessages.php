            <div class="col-md-12 whiteBg noPad">
            <div class="audacity">
                Conversation with <?php echo $userName; ?>
                <span class="rightDiv">
                    <a href="<?php echo base_url(); ?>profile/conversationwindow" class="axlinks uibutton large hs">New Message</a>
                </span>
            </div>
                <form method="post" name="form" action="">
                    <input type="hidden" id="courseboxid" value="<?php echo $sids; ?>" />
                    <!--<div id="displays" class="disp"></div>-->
                    <div class="uiScrollable">
                        <div id="ups">
                        <ol id="update" class="timeline"></ol>
                        <ol class="oldUpdates" style="padding:0;">
                            <?php foreach($getMessages as $val): ?>
                            <li>
                                <?php if($this->session->userdata('user_id') == $val->from_rm_id) { ?>
                                <span class="yourname">
                                    You
                                </span>
                                <?php } ?>
                                <?php 
                                    if($this->session->userdata('user_id') == $val->from_rm_id) {
                                        echo $val->body_text; 
                                        $splitTimeDate = explode(' ',$val->mail_timestamp);
                                        echo "<span class=datetimer>";
                                        echo '<i class="fa fa-clock-o"></i> '.date('d M Y',  strtotime($splitTimeDate[0])).'  at  '.date('H:i A', strtotime($splitTimeDate[1]));
                                        echo "</span>";
                                ?>
                                <?php
                                    }
                                    else {
                                ?>
                                        <span class="textrighter">
                                            <span class="yourname">
                                                <?php echo $userName; ?>
                                            </span>
                                            <?php echo $val->body_text; 
                                        $splitTimeDate = explode(' ',$val->mail_timestamp);
                                        echo "<span class='datetimer leftR'>";
                                        echo '<i class="fa fa-clock-o"></i> '.date('d M Y',  strtotime($splitTimeDate[0])).'  at  '.date('h:i A', strtotime($splitTimeDate[1]));
                                        echo "</span>";
                                            
                                            ?>

                                        </span> 
                                <?php
                                        
                                    }
                                ?>
                            </li>
                            <?php endforeach; ?>
                        </ol>
                        </div>
                        <div id="more<?php echo $val->email_tran_id; ?>" class="morebox">
                            <input type="hidden" id="courseboxid" value="<?php echo $sids; ?>" />
                            <a href="#" class="more" id="<?php echo $val->email_tran_id; ?>,<?php echo $sids; ?>">Load Earlier Messages</a>
                        </div>
                    </div>
                
                    <div class="replySendBlock">
                        <div class="theBlock">
                            <div class="boxDir">
                                <input type="hidden" name="contentsender" id="contentsender" value="<?php echo $this->session->userdata('user_id'); ?>" />
                                <textarea name="content" id="content" class="textBoxMsg" placeholder="Write a message..."></textarea>
                            </div>
                        </div>
                        <div id="flash"></div>
                        <p align="right">
                        <input type="submit" name="submit" value="Send" class="update_button_block uibutton large" />
                        </p>
                    </div>
                </form>
            </div>
<script type="text/javascript">
$(document).ready(function(){
    $(".update_button_block").click(function() {

    var boxval = $("#content").val();
    var from_id = $("#contentsender").val();
    var to_id = $("#courseboxid").val();
    var dataString = 'content='+ boxval +'&contentsender='+ from_id +'&courseboxid='+ to_id;

    if(boxval=='')
    {
    alert("Please Enter Some Text");
    }
    else
    {
    $("#flash").show();
    $("#flash").fadeIn(400).html('<img src="<?php echo base_url(); ?>images/loading.gif" align="absmiddle"> <span class="loading">Sending...</span>');

    $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>ajaxcontent/sendmessage",
    data: dataString,
    cache: false,
    success: function(html){
    $("ol#update").prepend(html);
    $("ol#update li:first").slideDown("slow");
    document.getElementById('content').value='';
    document.getElementById('content').focus();
    $("#flash").hide();
    }
    });
    } return false;
    }); 
    
  $('a.more').click(function() 
    {
        var ID = $(this).attr("id");
        var str = ID.split(",");
            if(str[0])
            {
                $("#more"+str[0]).html('<img src="<?php echo base_url(); ?>images/loading.gif" />');

                $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>ajaxcontent/moreprivatemessage",
                data: "lastmsg="+ str[0] +"&segId="+str[1], 
                cache: false,
                success: function(html){
                        $("ol.oldUpdates").append(html);
                        $("#more"+ID).remove(); // removing old more button
                    }
                });
            }
            else
            {
                $(".morebox").html('The End');// no results
            }

        return false;
    });
    
});
</script>