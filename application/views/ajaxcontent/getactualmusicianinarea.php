<?php 
	$cou = 0;
	foreach($trendingMusicians as $val):
?>
<a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>">
	<div class="col-sm-3 col-xs-6 noPad fivePxBorder">
		<div style="height:180px; max-width: 100%; background-color: black; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $val->profile_pic_url; ?>);"></div>
			<div class="displayNames">
            	<span class="dps">
                	<?php echo $val->user_name; ?>
				</span>
                <span class="gps">
                	<?php if($val->genre != '') { ?>
                    	<?php echo $val->genre; } ?>
				</span>
				<span class="kps">
                	<?php echo $val->city_name; ?>
				</span>
			</div>		
	</div>
</a>
<?php endforeach; ?>
