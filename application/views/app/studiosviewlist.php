<div class="container stanPad">
    <div class="row">
        <div class="col-md-12 noPad">
            <div class="col-md-8">
                <h2 class="font-thin m-b">
                    <i class="fa fa-microphone"></i> Studios
                </h2>
                <div class="row row-sm">
                    <div id="listerstudiosarea"></div>
                </div>
            </div>
        </div>
    </div>
</div>