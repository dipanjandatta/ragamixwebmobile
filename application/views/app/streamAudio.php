      <div class="audio-player">
        <h2>Linkin' Park</h2>
        <audio id="audio-player" src="<?php echo base_url(); ?>audio/cog.mp3" type="audio/mp3" controls="controls"></audio>
      </div><!-- @end .audio-player -->
      <script type="text/javascript">
$(function(){
  $('#audio-player').mediaelementplayer({
    alwaysShowControls: true,
    features: ['playpause','progress','current','duration','tracks','volume'],
    audioVolume: 'horizontal',
//    audioWidth: 500,
//    audioHeight: 100,
    iPadUseNativeControls: true,
    iPhoneUseNativeControls: true,
    AndroidUseNativeControls: true
  });
});
</script>