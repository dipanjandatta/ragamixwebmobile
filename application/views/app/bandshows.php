<!--<div class="col-md-12 noPad">
				<ul class="cbp_tmtimeline">
					<li>
						<time class="cbp_tmtime" datetime="2013-04-10 18:30"><span>4/10/13</span> <span>18:30</span></time>
                                                <div class="cbp_tmicon cbp_tmicon-screen">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
						<div class="cbp_tmlabel">
							<h2>Esthetic Fest '16</h2>
                                                        <p style="border-bottom: 1px solid rgba(255, 168, 1, 0.42);">Welcome All to Esthetic Fest 2016. an event to remember</p>
                                                        <span class="events">
                                                            <i class="fa fa-map"></i> Venue
                                                        </span>
                                                        <span class="evedetails">
                                                            Salt Lake Stadium
                                                        </span>
						</div>
					</li>
					<li>
						<time class="cbp_tmtime" datetime="2013-04-11T12:04"><span>4/11/13</span> <span>12:04</span></time>
                                                <div class="cbp_tmicon cbp_tmicon-screen">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
						<div class="cbp_tmlabel">
							<h2>Esthetic Fest '16</h2>
                                                        <p style="border-bottom: 1px solid rgba(255, 168, 1, 0.42);">Welcome All to Esthetic Fest 2016. an event to remember</p>
                                                        <span class="events">
                                                            <i class="fa fa-map"></i> Venue
                                                        </span>
                                                        <span class="evedetails">
                                                            Salt Lake Stadium
                                                        </span>
						</div>
					</li>
				</ul>
</div>-->

<div class="col-md-12 noPad">
    <div class="cont list">
        <div class="cardRow">
                                <?php if(empty($getEvents)) { ?>
                                    <div class="noNots">
                                        No Shows / Tours To List
                                    </div>
                                <?php } else { 
                                    foreach($getEvents as $val):
                                ?>
                                <div class="card">
                                    <div class="title">
                                        <h2 class="ng-binding"><?php echo $val->event_title; ?>@<?php echo $val->event_location; ?></h2>
                                        <!--<span class="ng-binding">Following: 21,456</span>-->
                                    </div>
                                    <div class="title">
                                        <h2 class="ng-binding">
                                            <?php echo date('d M Y', strtotime($val->event_date)); ?>
                                        </h2>
                                    </div>
                                    <div class="title">
                                        <h2 class="ng-binding"><?php echo $val->event_location; ?></h2>
                                        <span class="ng-binding"><?php echo $val->event_city; ?></span>
                                    </div>
                                    <div class="title">
                                        <h2 class="ng-binding">Pending</h2>
                                    </div>
                                </div>
                                <?php endforeach; } ?>
        </div>
    </div>
</div>