<div class="container stanPad">
	<div class="row" style="margin-top: 20px;">
		<div class="col-md-12 noPad">
			<h3 class="font-thin">
	        	FEATURED EVENTS
			</h3>
		</div>
	</div>
</div>
<div id="featuredevents"></div>
<div class="container stanPad">
	<div class="row" style="margin-top: 20px;">
		<div class="col-md-12 noPad">
			<div class="col-md-8" style="padding-left: 0">
				<h3 class="font-thin">
		        	SCHEDULED EVENTS
				</h3>
				<div id="scev"></div>
			</div>
			<div class="col-md-4">
				<h3 class="font-thin">
		        	GLOBAL EVENTS
				</h3>
				<div id="ge"></div>
			</div>
		</div>
		<div class="col-md-12 noPad">
			<h3 class="font-thin">
				NEWSBITS
			</h3>
				<div id="spnb"></div>	
			
		</div>
		<div class="col-md-12 noPad">
			<h3 class="font-thin">
				REVIEWS
			</h3>
			<div id="gr"></div>
		</div>
	</div>
</div>
<!--div class="container stanPad">
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 noPad">
            <div class="col-md-3 noPad">
<!--                <div class="col-md-12 noPad ader">
                    <span class="adtext">
                        AD SPACE
                    </span>
                </div>-->
                <!--div class="col-md-12 noPad">
                    <div class="panel panel-default noBord">
                      <div class="panel-heading panelSubMain">
                       Events Scheduled
                      </div>
                      <div class="panel-body panFan banban">
                          <div id="scev"></div>
                      </div>
                    </div>
                </div>
                <div class="col-md-12 noPad">
                    <div class="panel panel-default noBord">
                      <div class="panel-heading panelSubMain">
                       Global Events
                      </div>
                      <div class="panel-body panFan banban">
                          <div id="ge"></div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="col-md-12 listingPropaganda">
                    <div class="col-md-12 margBot fontMed headingPropaganda">
                        <div class="col-md-6 noPad">
                            Featured Events
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="featuredevents"></div>
                    </div>
                </div>
                <div class="col-md-12 listingPropaganda">
                    <div class="col-md-12 margBot fontMed headingPropaganda">
                        <div class="col-md-6 noPad">
                            Spotlight(Newsbits)
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="spnb"></div>
                    </div>
                </div>
                <div class="col-md-12 listingPropaganda">
                    <div class="col-md-12 margBot fontMed headingPropaganda">
                        <div class="col-md-6 noPad">
                            Reviews
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="gr"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div-->
<!--<div id="masonry" class="pos-rlt animated fadeInUpBig">
					<?php 
                        foreach($spotNews as $val): 
                            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->doc_youtube_url, $match)) {
                                $vid = $match[1];
                                $displayedYoutubeVideo = "<iframe width='240' height='165' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                            }
                    ?>
                <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">    
				<div class="itemman"> 
					<div class="item-overlay gd animated fadeInUp wrapper bg-info"> 
						<p class="text-white">Watch later</p> 
						<div class="center text-center m-t-n"> 
							<a href="#"><i class="icon-control-play i-2x"></i></a> 
						</div> 
					</div> 
					<div class="bottom gd bg-info wrapper"> 
						<div class="m-t m-b">
							<a href="#" class="b-b b-info h4 text-u-c text-lt font-bold">
								<?php echo $val->event_title; ?>
							</a>
						</div> 
						<p class="hidden-xs">
                                  <?php
                                        if(strlen($val->event_desc) > 100) {
                                        // truncate string
                                         $stringCut = substr($val->event_desc, 0, 100);

                                         // make sure it ends in a word so assassinate doesn't become ass...
                                         $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
                                        }
                                        else {
                                            $string = $val->event_desc;
                                        }
                                        echo $string; 
                                  ?>
						</p> 
					</div> 
					<a href="#">
						<?php if($val->doc_youtube_url != '') {
                        	echo $displayedYoutubeVideo;
						} else { ?> 
                        	<img src="<?php echo $val->pic_url;?>" class="img-full" />
						<?php
                        }    
                        ?>
					</a> 
				</div> 
				</a>
				<?php endforeach; ?>
</div>
-->