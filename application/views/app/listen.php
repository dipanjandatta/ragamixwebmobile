<div class="container stanPad"></div>
<div class="bg-black">
	<div class="col-md-12 noPad">
		<div class="col-md-6 bg noPad">
			<div class="listScrollers" style="margin-top: 3px; height: 590px !important;">
				<div id="listersaudio"></div>
			</div>
		</div>
		<div class="col-md-6 bg noPad">
			<div class="listScrollers" style="margin-top: 3px;">
				<div class="itemv pos-rlt">
					<a href="#" class="item-overlay active opacity wrapper-md font-xs">
						<span class="block h3 font-bold text-info">
							Find
						</span>
						<span class="text-muted">
							Search the music you like
						</span>
						<span class="bottom wrapper-md block">
							<i class="fa fa-arrow-circle-o-right i-lg"></i>
						</span>
					</a>
					<a href="#">
						<img class="img-full" src="<?php echo base_url(); ?>images/m40.jpg" alt="...">
					</a>
				</div>
				<div class="itemv pos-rlt">
					<a href="<?php echo base_url(); ?>music/videolists" class="item-overlay active opacity wrapper-md font-xs">
						<span class="block h3 font-bold text-warning">
							LISTEN and VIEW
						</span>
						<span class="text-muted">
							Videos you like
						</span>
						<span class="bottom wrapper-md block">
							<i class="fa fa-arrow-circle-o-right i-lg"></i>
						</span>
					</a>
					<a href="#">
						<img class="img-full" src="<?php echo base_url(); ?>images/m41.jpg" alt="...">
					</a>
				</div>
			</div>
		</div>
	</div>
</div>