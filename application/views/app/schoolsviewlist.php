<div class="container stanPad">
    <div class="row">
        <div class="col-md-12 noPad">
            <div class="col-md-8">
                <div class="col-xs-6 col-sm-3 smallPad">
                    <h2 class="font-thin m-b">
                        <i class="fa fa-home"></i> Music Schools
                    </h2>
                </div>
                <div class="col-xs-6 col-sm-3 smallPad">
                    <?php echo anchor('musicians/educators', 'Back', array('class' => 'baker')); ?>
                </div>
                <div class="row row-sm">
                    <div id="listerschoolsarea"></div>
                </div>
            </div>
        </div>
    </div>
</div>