<div class="container stanPad">
    <div class="row">
        <div class="col-md-12 noPad">
            <div class="noo-shortcode-events grid">
                <div class="noo-grid-header-events" style="padding: 0px 10px; margin-top: -10px !important;">
                    <div class="sh-event-title font-thin">
                        <i class="fa fa-key"></i> Change Password
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?php echo form_open('welcome/changepassword'); ?>
            <div class="control-group form-group ragaformarFix">
                <div class="controls">
                    <input type="password" name="oldpass" class="form-control ragacontrols" placeholder="Old Password">
                </div>
            </div>
            <div class="control-group form-group ragaformarFix">
                <div class="controls">
                    <input type="password" name="newpass" class="form-control ragacontrols" placeholder="New Password">
                </div>
            </div>
            <div class="control-group form-group ragaformarFix">
                <div class="controls">
                    <input type="password" name="cnfpass" class="form-control ragacontrols" placeholder="Confirm Password">
                </div>
            </div>
            <div class="control-group form-group ragaformarFix">
                <div class="controls col-sm-6 noPad" style="float: left; text-align: right;     margin: 5px 0px 3px 0px;">
                    <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 45px !important;">CHANGE</button>
                </div>
                <div class="controls col-sm-6 noPad" style="float: right; text-align: right;     margin: 5px 0px 3px 0px;">
                    <button type="button" class="btn btnfix greenbtn" style="padding: 7px 50px !important;" data-dismiss="modal">RESET</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>