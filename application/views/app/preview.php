<div class="container">
    <div class="row" style="margin-top: 50px;">
        <div class="previewNots">
            <div class="col-md-12">
                <div class="col-md-6 slimtext">
                    This is how your profile would look like to&nbsp;
                    <select name="prevtype" id="prevtype" class="minorSel" onchange="location = this.options[this.selectedIndex].value;">
                        <?php if($type == 'ms') { ?>
                        <option value="<?php echo base_url(); ?>profile/preview?t=ms&p=previewlink&basedirect=<?php echo base64_encode($baseDirect); ?>" selected="selected">People on RagaMix</option>
                        <option value="<?php echo base_url(); ?>profile/preview?t=mc&p=previewlink&basedirect=<?php echo base64_encode($baseDirect); ?>">Public</option>
                        <?php } else { ?>
                        <option value="<?php echo base_url(); ?>profile/preview?t=ms&p=previewlink&basedirect=<?php echo base64_encode($baseDirect); ?>">People on RagaMix</option>
                        <option value="<?php echo base_url(); ?>profile/preview?t=mc&p=previewlink&basedirect=<?php echo base64_encode($baseDirect); ?>" selected="selected">Public</option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-6 text-right">
                        <?php echo anchor('profile/referrer?returnbase='.base64_encode($baseDirect), 'Return Back', array('class'=>'btn-bck')); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row" style="margin-top: 50px;">
        <div class="col-md-12 noPad">
            <div class="col-md-8">
                <div class="col-md-6 noPad boxShads">
                    <div style="height:300px; max-width: 100%; background-size: contain; background-color: #e9ebee; background-position: center 30%; background-repeat: no-repeat; display: block;background-image: url('<?php echo $previewData[0]->profile_pic_url; ?>');"></div>
                    <div class="hoverparty">
                        <div class="hoverpartymaintxt fontbig">
                            <?php echo $previewData[0]->full_name; ?>
                        </div>
                        <div class="hoverpartysubtext">
                            <?php echo $previewData[0]->usertypename; ?><br />
                            <?php echo $previewData[0]->genre_name; ?>
                        </div>
                        <div class="hoverpartymaintext yelcolfont">
                            <?php echo $previewData[0]->location_info; ?>
                        </div>
                        <img src="<?php echo base_url(); ?>images/gradient.png" class="grads" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="displayHeadText">
                        <i class="fa fa-street-view"></i>  About <?php echo $previewData[0]->full_name; ?>
                    </div>
                    <div class="proftext">
                        <?php 
                            if(strlen($previewData[0]->user_desc) > 500) {
                            // truncate string
                             $stringCut = substr($previewData[0]->user_desc, 0, 500);

                             // make sure it ends in a word so assassinate doesn't become ass...
                             $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a data-toggle="modal" data-target="#seemore">Read More</a>'; 
                            }
                            else {
                                $string = $previewData[0]->user_desc;
                            }
                            echo nl2br($string); 
                        ?>
                    </div>
                    <div class="modal fade" id="seemore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h1 class="modal-title">About <?php echo $previewData[0]->full_name; ?></h1>
                          </div>
                          
                          <div class="modal-body">
                              <?php echo nl2br($previewData[0]->user_desc); ?>
                          </div>
                          <div class="modal-footer"> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="col-md-12 noPad margTop">
                    <ul id="countrytabs" class="shadetabs">
                        <?php if($type == 'ms') { ?>
                        <li><a href="<?php echo base_url(); ?>profile/sa" rel="countrycontainer" class="selected">Songs &amp; Albums</a></li>
                        <li><a href="<?php echo base_url(); ?>profile/pg" rel="countrycontainer">Photo Gallery</a></li>
                        <li><a href="<?php echo base_url(); ?>profile/se" rel="countrycontainer">Shows &amp; Events</a></li>
                        <li><a href="<?php echo base_url(); ?>profile/ads" rel="countrycontainer">Ads</a></li>
                        <?php } else { ?>
                        <li><a href="#" class="selected">Songs &amp; Albums</a></li>
                        <li><a href="#">Photo Gallery</a></li>
                        <li><a href="#">Shows &amp; Events</a></li>
                        <li><a href="#">Ads</a></li>
                        <?php } ?>
                    </ul>
                    <div id="countrydivcontainer" style="margin-top:  10px">
                        <div class="bluroverlay">
                            <span class="blrtxt">
                                <strong><?php echo $previewData[0]->full_name; ?></strong>, Has Uploaded Videos and Photos. To view them Login / Sign Up
                            </span>
                            <div class="col-md-12 noPad" style="opacity: 0.08">
                                
                                <div class="col-md-3 noPad stillBord">
                                    <div style="height:200px; max-width: 100%; background-color: #e9ebee; background-size: cover; background-position: center 30%; background-repeat: no-repeat; display: block; background-image: url(<?php echo base_url().'images/u2368.jpg'; ?>);"></div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <?php if($type == 'ms') { ?>
            <div class="col-md-4 noPad backcol">
                <div class="actualWhiteContent">
                    <span class="kt">
                        Music Preference
                    </span><br />
                    <span class="tt">
                        <?php echo $previewData[0]->music_influence_desc; ?>
                    </span>
                </div>
                <div class="actualMoreContents">
                    <span class="tagline"><?php echo $previewData[0]->freelance_professional; ?></span>
                    <ul class="profilelist">
                        <li>
                            <?php echo $previewData[0]->studio_band_member; ?>
                        </li>
                        <li>
                            <?php echo $previewData[0]->commitment_lvl; ?>
                        </li>
                        <li>
                            <?php echo $previewData[0]->experience_in_years; ?> Years Experience
                        </li>
                        <li>
                            <?php echo $previewData[0]->equipment_owned; ?>
                        </li>
                    </ul>
                </div>
                <div class="actualWhiteContent">
                    <span class="kt">
                        Seeking
                    </span><br />
                    <span class="tt">
                        <?php echo $previewData[0]->seeking_description; ?>
                    </span>
                    <span class="kt">
                        Teaching Details    
                    </span>
                    <br />
                    <span class="tt">
                        <?php echo $previewData[0]->educator_type; ?>
                    </span>
                    <span class="bt">
                        <?php echo $previewData[0]->educator_desc; ?>
                    </span>
                </div>
                <div class="actualWhiteContent">
                    <span class="kt">
                        Band Details
                    </span><br />
                    <span class="tt">
                        <?php echo $previewData[0]->band_type; ?>
                    </span>
                    <span class="bt">
                        <?php echo $previewData[0]->band_desc; ?>
                    </span>
                    <span class="kt">
                        Teaching Details    
                    </span>
                    <br />
                    <span class="tt">
                        <?php echo $previewData[0]->educator_type; ?>
                    </span>
                    <span class="bt">
                        <?php echo $previewData[0]->educator_desc; ?>
                    </span>
                </div>
                <div class="actualNoBackContent">
                    <span class="tagline" style="padding: 10px;">Location</span><br />
                        <?php echo $map['html']; ?>
                </div>
            </div>
            <?php } else { ?>
            <div class="col-md-4">
                    <div class="commentView">
                        <div class="cvName">
                            <?php echo $previewData[0]->full_name; ?>
                        </div>
                        is on Ragamix
                        <div class="cvDetails">
                            To Know more about, <?php echo $previewData[0]->full_name; ?>, sign up or login
                        </div>
                    </div>
                    <div class="fadecontent">
                        Freelancer
                        <div class="blur smtext">Band/Studio, Moderatly committed</div>
                    </div>
                    <div class="fadecontent">
                        Seeking
                        <div class="blur smtext">Band/Studio, Moderatly committed</div>
                    </div>
                    <div class="fadecontent">
                        Other Details
                        <div class="blur smtext">Band/Studio, Moderatly committed</div>
                    </div>
                    <div id="gradient"></div>
                    <div class="col-md-12" style="z-index: 999999;">
                        <div class="col-md-8" style="text-align: right;">
                            <button type="submit" class="btn btnfix yelbtn" style="width: 45%;">Sign In</button>
                        </div>
                        <div class="col-md-4" style="text-align: left;">
                            <button type="submit" class="btn btnfix greenbtn" style="width: 100%;">Sign Up</button>
                        </div>
                    </div>
                <?php } ?>
                </div>
        </div>
    </div>
</div>