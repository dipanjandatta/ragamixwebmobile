<div class="col-md-6 uniPad">
    <?php 
        if(empty($getAllVideos)) { ?>
            <div class="noProfNots">
                <i class="fa fa-music"></i>
            </div>
            <div class="smallNoNots">
                    No Video To List
            </div>
    <?php } else { ?>
<ul class="list-group noListPad">
    
<?php
        foreach($getAllVideos as $val): 
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->youtube_url, $match)) {
            $vid = $match[1];
            $crushers = 1;
            $displayedYoutubeVideo = "<iframe width='112' height='100' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
        }
        else {
            $displayedYoutubeVideo = $val->youtube_url;
        }        
    ?>
                      <li class="list-group-item adjustList shadowList fut">
                        <span class="badge caser pacer">
                          <?php echo $val->youtube_title; ?>
                        </span>
                        <span class="badge caser lacer">
                          <?php echo date('d M Y', strtotime($val->updated_on)); ?>
                        </span>
                                      <?php 
                                        if($displayedYoutubeVideo != ''){
                                            if (strpos($val->youtube_url, 'channel') !== false) {
                                      ?>
                                        <div class="vidChannel">
                                            <?php echo anchor($val->youtube_url, 'Visit Channel', array('target' => '_blank', 'class'=>'channelView')); ?>
                                        </div>
                                      <?php
                                            }
                                            else if (strpos($val->youtube_url, 'user') !== false) {
                                      ?>
                                        <div class="vidChannel">
                                            <?php echo anchor($val->youtube_url, 'Visit User', array('target' => '_blank', 'class'=>'channelView')); ?>
                                        </div>
                                      <?php
                                            }
                                            else {
                                                echo $displayedYoutubeVideo;
                                            }
                                        }
                                        else {
                                      ?>
                                        <div class="noVidsShow">
                                            No Video To Display
                                        </div>
                                      <?php
                                        }
                                      ?>
                      </li>
    <?php endforeach; } ?>
</ul>
</div>  
<div class="col-md-5 uniPad">
        <?php 
        if(empty($getAllAudios)) { ?>
            <div class="noProfNots">
                <i class="fa fa-music"></i>
            </div>
            <div class="smallNoNots">
                    No Audio To List
            </div>
            <?php } else { ?>
              <ul class="list-group listPad">
                <?php foreach($getAllAudios as $val): ?>
                                    <li class="list-group-item adjustList bhover">
                                      <span class="badge caser pacer">
                                        <?php echo $val->audio_title; ?>
                                      </span>
                                      <span class="badge caser ashfont margBot">
                                        By <?php echo $val->uploaded_by; ?>
                                      </span>
                                      <span class="badge caser">
                                              <i class="fa fa-file"></i>&nbsp;
                                                  <?php $kbSize = (($val->size)/1024);
                                                        $mbSize = $kbSize/1024;
                                                        echo round($mbSize,2)." Mb"; ?>
                                              
                                      </span>
                                        <span class="playopt">
                                            <a href = "javascript:void(0)" onclick = "document.getElementById('player<?php echo $val->serial_no; ?>').play();document.getElementById('light<?php echo $val->serial_no; ?>').style.display='block';document.getElementById('fade<?php echo $val->serial_no; ?>').style.display='block'"><i class="fa fa-play-circle"></i></a>
                                        </span>
                                        <i class="fa fa-music" style="font-size: 45px; padding: 10px;"></i>
                                        <div id="light<?php echo $val->serial_no; ?>" class="white_content">
                                            <audio class="fixAudioWidth" id="player<?php echo $val->serial_no; ?>" controls>
                                              <source src="<?php echo $val->audio_url; ?>" type="audio/mpeg">
                                              Your browser does not support the audio element.
                                            </audio>
                                        </div>
                                    </li>
                <?php endforeach; ?>
            </ul>
    <?php } ?>
</div>