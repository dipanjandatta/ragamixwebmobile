<div class="container stanPad">
    <div class="row">
        <div class="col-md-12 noPad">
            <div class="col-md-2 noPad padTop" <!--style="overflow-y: scroll;height: 600px;"-->>
                <ul class="searchNavList">
                    <a href="<?php echo base_url(); ?>profile/searchTop?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=all&type=oo" class="searchNavLink">
                        <?php if($rt=='all') { ?>
                            <li class="searchNavListers searchActive">
                        <?php } else { ?>
                            <li class="searchNavListers">
                        <?php } ?>
                                All
                            </li>
                    </a>                  
                    <a href="<?php echo base_url(); ?>profile/searchMusician?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Singers&type=oA" class="searchNavLink">
                        <?php if($rt=='Musicians') { ?>
                            <li class="searchNavListers searchActive">
                        <?php } else { ?>
                            <li class="searchNavListers">
                        <?php } ?>
                                Musicians
                            </li>
                    </a>  
                    <a href="<?php echo base_url(); ?>profile/searchSingers?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Singers&type=oS" class="searchNavLink">
                        <?php if($rt=='Singers') { ?>
                            <li class="searchNavListers searchActive">
                        <?php } else { ?>
                            <li class="searchNavListers">
                        <?php } ?>
                                Singers
                            </li>
                    </a>  
                    <a href="<?php echo base_url(); ?>profile/searchBands?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Bands&type=oB" class="searchNavLink">
                        <?php if($rt=='Bands') { ?>
                            <li class="searchNavListers searchActive">
                        <?php } else { ?>
                            <li class="searchNavListers">
                        <?php } ?>
                                Bands
                            </li>
                    </a>  
                    <a href="<?php echo base_url(); ?>profile/searchAds?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Ads&type=oD" class="searchNavLink">
                        <?php if($rt=='Ads') { ?>
                            <li class="searchNavListers searchActive">
                        <?php } else { ?>
                            <li class="searchNavListers">
                        <?php } ?>
                                Ads
                            </li>
                    </a>  
                    <a href="<?php echo base_url(); ?>profile/searchEvents?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Events&type=oE" class="searchNavLink">
                        <?php if($rt=='Events') { ?>
                            <li class="searchNavListers searchActive">
                        <?php } else { ?>
                            <li class="searchNavListers">
                        <?php } ?>
                                Events
                            </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchVideos?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Videos&type=oV" class="searchNavLink">
                        <?php if($rt=='Videos') { ?>
                            <li class="searchNavListers searchActive">
                        <?php } else { ?>
                            <li class="searchNavListers">
                        <?php } ?>
                                Videos
                            </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchSolo?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Solo&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Solo') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Solo Artist
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchDj?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=DJ&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='DJ') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Dj
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchComposer?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Composer&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Composer') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Composer
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchEducators?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Educators&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Educators') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Educators
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchEventMarketer?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=EventMarketer&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='EventMarketer') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Event Marketer
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchFan?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Fan&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Fan') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Fan
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchMusicalDuo?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=MusicalDuo&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='MusicalDuo') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Musical Duo
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchShowManager?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=ShowManager&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='ShowManager') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Show Manager
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchStudio?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Studio&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Studio') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Studio
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchTeacher?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Teacher&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Teacher') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Teacher
                        </li>
                    </a>
                    <!--                    <a href="--><?php //echo base_url(); ?><!--profile/searchBuyerSeller?q=--><?php //echo rawurlencode($st); ?><!--&city=--><?php //echo $city; ?><!--&ref=BuyerSeller&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">-->
                    <!--                        --><?php //if($rt=='BuyerSeller') { ?>
                    <!--                        <li class="searchNavListers searchActive">-->
                    <!--                            --><?php //} else { ?>
                    <!--                        <li class="searchNavListers">-->
                    <!--                            --><?php //} ?>
                    <!--                            Buyer/Seller-->
                    <!--                        </li>-->
                    <!--                    </a>-->

                </ul>
            </div>
            <div class="col-md-10 noPad">
                <div class="col-md-12">
                <div class="hori"></div>
                <div class="pom">
                    Videos <span class="cr"><?php echo $mc; ?></span>
                </div>
                <div class="horir"></div>
                <div class="pagingholder">
		<form id="live-search" action="" class="styledLeft" method="post">
                    <input type="text" class="text-input bigInput ragacontrols" id="filter" value="" placeholder="Filter From the current List" />
        		<span id="filter-count"></span>
		</form>
                    <?php if($page_number != 0){ ?>
                    <a href="<?php echo base_url(); ?>profile/searchVideos?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=<?php echo $rt; ?>&type=oD&next=<?php echo $next; ?>&clicks=<?php echo ($clicks-1); ?>&page_number=<?php echo ($page_number-1); ?>&prev=y">
                        <i class="fa fa-chevron-left pageicon"></i>
                    </a>
                    <?php } ?>
                    <a href="<?php echo base_url(); ?>profile/searchVideos?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=<?php echo $rt; ?>&type=oD&next=<?php echo $next; ?>&clicks=<?php echo $clicks; ?>&page_number=<?php echo $page_number; ?>">
                        <i class="fa fa-chevron-right pageicon"></i>
                    </a>
                </div>
                <div class="jewel">
                    <?php 
                    if(empty($getAllSearchM)){
                    ?>
                        <div class="noNots">
                            No More Results to Show
                        </div>
                    <?php
                    }
                    else {
                    foreach($getAllSearchM as $val):
                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->youtube_url, $match)) {
                        $vid = $match[1];
                        $displayedYoutubeVideo = "<iframe width='90' height='75' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                    }
                    ?>
                        <a href="#">
                            <div class="col-md-6 noPad colerBackMg commentList">
                            <div class="display_box ser">
                                    <?php
                                        echo $displayedYoutubeVideo;
                                    ?>
                                <div class="placeholdLeft">
                                    <?php echo $val->youtube_title; ?>
                                    <span class="otlabtext">
                                        <?php echo "By ".$val->uploaded_by; ?>
                                    </span>
                                </div>

                            </div>
                            </div>
                        </a>
                    <?php endforeach; } ?>
                        
                </div> 
                </div>
            </div>
        </div>
    </div>
</div>