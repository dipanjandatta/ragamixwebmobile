     <div class="rslides_container">
      <ul class="rslides" id="slider1">
          <?php foreach($homeslider as $val): ?>
            <li>
                <img src="http://www.ragamix.com/ragaadmin/uploads/<?php echo $val->image_url; ?>" />

                <?php if($val->desc_text != ""){?>
                <span class="tes">
                    <?php echo nl2br($val->desc_text); ?>
                </span>
                <?php }else{ ?>
                <?php } ?>
            </li>
          <?php endforeach; ?>
      </ul>
    </div>
               

	<!--old Code. Refer to oldcode.txt file-->

<!--Floating Ad Code -->
     <div class="floatAds">
         <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2 tooltipidio" style="text-align: center;">
             <i class="fa fa-whatsapp" style="font-size: 25px; color: white;"></i>
             <span class="tooltipidiotext">9748266600</span>
         </div>
         <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2" style="text-align: center;">
             <a data-toggle="modal" data-target="#idiotchat"><i class="fa fa-comments-o" style="font-size: 25px; color: white;"></i></a>
         </div>
         <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2" style="text-align: center;">
             <a href="mailto:admin@ragamix.in?Subject=Query"><i class="fa fa-envelope-o" style="font-size: 25px;color: white;"></i></a>
         </div>
     </div>
     <div class="modal fade" id="idiotchat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-body" style="padding: 0 !important;">
                     <div class="modHeader">
                         Please Provide your Name to Have a Support Chat
                     </div>
                     <?php echo form_open('welcome/chatloginguest'); ?>
                     <div class="panel-body panForm nanan">
                         <div class="signform" id="business">
                             <div class="control-group form-group ragaformarFix">
                                 <div class="controls">
                                     <input type="text" name="uname" class="form-control ragacontrols" placeholder="Enter Your Name">
                                 </div>
                             </div>
                             <div class="control-group form-group ragaformarFix">
                                 <div class="controls col-sm-6 noPad" style="float: left; text-align: right;     margin: 5px 0px 3px 0px;">
                                     <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 30px !important;">PROCEED</button>
                                 </div>
                                 <div class="controls col-sm-6 noPad" style="float: right; text-align: right;     margin: 5px 0px 3px 0px;">
                                     <button type="button" class="btn btnfix greenbtn" style="padding: 7px 40px !important;" data-dismiss="modal">CANCEL</button>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <?php
                        echo form_close();
                     ?>
                 </div>
             </div>
         </div>
     </div>

<div class="container" style="padding-top: 197px;">
	<div class="col-xs-3 col-sm-3 smallPad">
        <a href="<?php echo base_url(); ?>musicians/singers">
            <div class="blockMenu">
                <i class="fa fa-microphone iconMenu"></i>
                <span class="ev">Singers</span>
            </div>
        </a>
	</div>
	<div class="col-xs-3 col-sm-3 smallPad">
        <a href="<?php echo base_url(); ?>musicians/view">
            <div class="blockMenu">
                <i class="fa fa-music iconMenu"></i>
                <span class="ev">Musicians</span>
            </div>
        </a>
	</div>
	<div class="col-xs-3 col-sm-3 smallPad">
        <a href="<?php echo base_url(); ?>musicians/band">
            <div class="blockMenu">
                <i class="fa fa-magic iconMenu"></i>
                <span class="ev">Bands</span>
            </div>
        </a>
	</div>
	<div class="col-xs-3 col-sm-3 smallPad">
        <a href="<?php echo base_url(); ?>musicians/educators">
            <div class="blockMenu">
                <i class="fa fa-graduation-cap iconMenu"></i>
                <span class="ev">Educators</span>
            </div>
        </a>
	</div>
	<div class="col-xs-3 col-sm-3 smallPad">
        <a href="<?php echo base_url(); ?>events/view">
            <div class="blockMenu">
                <i class="fa fa-table iconMenu"></i>
                <span class="ev">Events</span>
            </div>
        </a>
	</div>
	<div class="col-xs-3 col-sm-3 smallPad">
        <a href="<?php echo base_url(); ?>ads/view">
            <div class="blockMenu">
                <i class="fa fa-opencart iconMenu"></i>
                <span class="ev">Ads</span>
            </div>
        </a>
	</div>
	<div class="col-xs-3 col-sm-3 smallPad">
        <a href="<?php echo base_url(); ?>events/view">
            <div class="blockMenu">
                <i class="fa fa-film iconMenu"></i>
                <span class="ev">Reviews</span>
            </div>
        </a>
	</div>
	<div class="col-xs-3 col-sm-3 smallPad">
        <a href="<?php echo base_url(); ?>musicians/studios">
            <div class="blockMenu">
                <i class="fa fa-sliders iconMenu"></i>
                <span class="ev">Studios</span>
            </div>
        </a>
	</div>
</div>
     <div class="rslides_container" style="background: white; margin: 10px; 0px;">
         <span class="dds">Testimonials</span>
         <div class="testimonial-widget" id="slidertesti" style="margin-top: 0px;">
             <?php foreach($testimonials as $val): ?>
                 <div class="testimonial">
                     <figure class="testimonial__mug">
                         <img src="http://www.ragamix.com/ragaadmin/uploads/<?php echo $val['pic_url']; ?>" style="width: 100%; height: 14vh !important;">
                     </figure>
                     <p style="padding: 10px; text-align: left;">&ldquo;<?php echo $val['detail_info']; ?>&rdquo;</p>
                     <p>
                         <strong><?php echo $val['full_name']; ?>, <?php echo $val['location']; ?></strong>
                     </p>
                 </div>
             <?php endforeach; ?>
         </div>
     </div>

<div class="container" style="padding-top: 20px;">
    <span class="dds">Jobs &amp; Opportunities</span>
    <div id="seeking"></div>
</div>
<?php if($this->uri->segment(3) == 'roadBlock'){ ?>
<div class="roadBlockUILayer">
    <div class="roleDialog">
        <div class="actDialog">
            <div class="dialogHead">
                Complete The Process...
            </div>
            
                <?php echo form_open('oauthLogin/roadblockcomplete'); ?>
                <div class="uiLay">
                        <div class="control-group form-group ragaformarFix" style="margin-bottom: 15px !important;">
                          <div class="controls">
                              <input type="text" id="user_name_in" name="user_name_in" class="form-control ragacontrols textFormControls" placeholder="Provide an User Name">
                              <input type="hidden"  name="email_in" value="<?php echo $this->uri->segment(4); ?>" class="form-control ragacontrols textFormControls" >
                              <input type="hidden"  name="facebook_id_in" value="<?php echo $this->uri->segment(5); ?>" class="form-control ragacontrols textFormControls" >
                              <span id="status"></span>
                          </div>
                        </div>
                </div>
                <div class="uiOverlayFooter">
                    <input type="submit" name="submit" value="Finish" class="btn btnfix greenbtn" style="margin-bottom: 3px; margin-top: 3px;" />
                </div>
                <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?php } ?>
