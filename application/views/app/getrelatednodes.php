	<div class="col-md-3 b-l">
		<div class="padder-v">
			<div class="panel">
				<div class="maywant">
					Artists you may want to Hire
				</div>
				<?php foreach($resulthired as $row): ?>
					<a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $row['x']->getProperty('name'); ?>/<?php echo $row['x']->getProperty('id'); ?>">
					<div class="col-md-3 noPad">
							<div style="height:65px; max-width: 100%; background-color: #e9ebee; background-size: cover; background-position: center 30%; background-repeat: no-repeat; display: block; background-image: url('<?php echo $row['x']->getProperty('pic'); ?>');"></div>
							<span class="ift">
								<?php echo str_replace('%20', ' ', $row['x']->getProperty('name')); ?>
							</span>
					</div>
					</a>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

