<div class="container stanPad">
    <div class="row">
        <div class="col-md-12 noPad">
            <div class="col-md-8">
                <h2 class="font-thin m-b">
                    <i class="fa fa-microphone"></i> Educators
                </h2>
                <div class="row row-sm">
                    <div id="listerteachersarea"></div>
                </div>
                <div class="row row-sm">
                    <div id="listerschoolsarea"></div>
                </div>
            </div>
        </div>
    </div>
</div>