                        <div class="col-md-4 noPad">
                            <div class="panel panel-default noBord">
                                <div class="panel-heading panelLightMes">
                                    Songs
                                </div>
                                <div class="panel-body pandreamer banban noPad">
                                <ul class="list-group listPad noPad">
                                  <?php foreach($getAllAudios as $val): ?>
                                                      <li class="list-group-item adjustList bhover">
                                                        <span class="badge caser pacer">
                                                          <?php echo $val->audio_title; ?>
                                                        </span>
                                                        <span class="badge caser">
                                                                <i class="fa fa-file"></i>&nbsp;
                                                                    <?php $kbSize = (($val->size)/1024);
                                                                          $mbSize = $kbSize/1024;
                                                                          echo round($mbSize,2)." Mb"; ?>

                                                        </span>
                                                          <span class="playopt">
                                                              <a href = "javascript:void(0)" onclick = "document.getElementById('player<?php echo $val->serial_no; ?>').play();document.getElementById('light<?php echo $val->serial_no; ?>').style.display='block';document.getElementById('fade<?php echo $val->serial_no; ?>').style.display='block'"><i class="fa fa-play-circle"></i></a>
                                                          </span>
                                                          <i class="fa fa-music" style="font-size: 45px; padding: 10px;"></i>
                                                          <div id="light<?php echo $val->serial_no; ?>" class="white_content">
                                                              <audio class="fixAudioWidth" id="player<?php echo $val->serial_no; ?>" controls>
                                                                <source src="<?php echo $val->audio_url; ?>" type="audio/mpeg">
                                                                Your browser does not support the audio element.
                                                              </audio>
                                                          </div>
                                                      </li>
                                  <?php endforeach; ?>
                              </ul>
                                </div>
                            </div>
                        </div>
<div class="col-md-8">
    <div class="panel panel-default noBord">
        <div class="panel-heading panelLightMes">
            Videos
        </div>
         <div class="panel-body pandreamer banban noPad">
             <?php
                    $bigCounter = 0;
                    $rowGone = 0;
                    foreach($getAllVideos as $val): 
                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->youtube_url, $match)) {
                        $vid = $match[1];
                        $crushers = 1;
                        $displayedYoutubeVideo = "<iframe width='396' height='250' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                    }
                    else {
                        $displayedYoutubeVideo = $val->youtube_url;
                    }  
             
             ?>
             <div class="col-md-6" style="padding-left: 0 !important; padding-top: 10px;">
                 <?php echo $displayedYoutubeVideo; ?>
                 <div class="col-md-12 noPad">
                    <ul class="list-group noListPad">
                        <li class="list-group-item shadowList" style="border-radius: 0 !important; float: left; width: 100%;">
                        <span class="badge caser pacer" style="float: none !important;">
                            <?php 
                                if($val->youtube_title){
                                    echo $val->youtube_title; 
                                } else {
                                    echo "Untitled Video";
                                }
                            ?>
                        </span>
                            <span class="likes"><i class="fa fa-thumbs-up"></i><?php echo $val->view_count; ?></span>
<!--                            <span class="publisher">
                                Arkopot the band
                            </span>-->
                      </li>
                    </ul>
                 </div>
             </div>
             <?php endforeach; ?>
             <!--?php $bigCounter = 1; $rowGone = 1; break; endforeach; ?-->
<!--             <div class="col-md-6" style="padding-left: 0 !important; padding-top: 10px;">
                 <div class="col-md-12 noPad shadowList" style="background: white;">
                     <ul class="list-group noListPad" style="margin-bottom: 0 !important;">
                        <?php 
                            foreach($getAllVideos as $val): 
                            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->youtube_url, $match)) {
                                $vid = $match[1];
                                $crushers = 1;
                                $displayedYoutubeVideo = "<iframe width='126' height='70' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                            }
                            else {
                                $displayedYoutubeVideo = $val->youtube_url;
                            } 
                        ?>
                        <li class="list-group-item adjustList" style="border-bottom: 3px solid #FFF5CE !important;">
                          <span class="badge caser pacer">
                            <?php echo $val->youtube_title; ?> 
                          </span>
                          <span class="badge caser lacer">
                            5 Hours ago
                          </span>
                          <?php echo $displayedYoutubeVideo; ?>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                 </div>
             </div>-->
        </div>
    </div>
</div>