<?php
    $returnValue = str_replace(' ', '%20', $getAdsIdentified[0]->pic_url); 
?>
<div class="container stanPad">
    <div class="row">
        <div class="col-md-12 noPad">
            <div class="col-md-8">
                <div class="col-md-12 noPad blackBg">
                    <div class="col-md-8 noPad">
                        <span class="titles"><?php echo $getAdsIdentified[0]->category; ?> by</span><span class="ut"><?php echo $getAdsIdentified[0]->uploaded_by; ?></span>
                        <br />
                        <span class="newTitleStyle">
                            <?php echo $getAdsIdentified[0]->ads_title; ?>
                        </span>
                    </div>
                    <div class="col-md-4 text-right">
                        <span class="textWhite">
                            Ad Date - <?php echo date('d M y', strtotime($getAdsIdentified[0]->created_on)); ?><br />
                            <?php echo $getAdsIdentified[0]->ads_views; ?> Views
                        </span>
                    </div>
                </div>
                <div class="col-md-6 noPad">
                    <div style="height:400px; max-width: 100%; background-size: cover; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                </div>
                <div class="col-md-6 whiteLh">
                    <?php echo $getAdsIdentified[0]->ads_desc; ?><br /><br />
                        <div class="theBlock darKTbBlock">
                            <div class="boxDir hunHeight">
                                <input type="hidden" name="contentsender" id="contentsender" value="2016050600000003087">
                                <textarea name="content" id="content" class="textBoxMsg gryedTb" placeholder="Write a message..."></textarea>
                                <div class="col-md-12 noPad text-right">
                                    <input type="submit" name="submit" value="SEND MESSAGE" class="msgadsbtn" />
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-12 noPad ader">
                    <span class="adtext">
                        AD SPACE
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-12 noPad bigMargTop">
            <div class="panel panel-default noBord">
                <div class="panel-heading panelSubMain seamseam">
                    Ads you might be interested in
                </div>
                    <?php foreach($getSomeAdsInterest as $val): 
                        $returnValueMaxCount = str_replace(' ', '%20', $val->pic_url);     
                    ?>
                    <a href="<?php echo base_url(); ?>ads/detail/<?php echo $val->ad_id; ?>">
                        <div class="col-md-2-3 noPad stillBord">
                            <div class="demo-section k-content stillHeight">
                                <div style="height:160px; max-width: 100%; background-size: cover; background-position: center 50%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValueMaxCount; ?>);"></div>
                                <div class="stnComm greyblack">
                                    <?php echo $val->category; ?><br />
                                    By <?php echo $val->uploaded_by; ?>
                                </div>
                                <div class="title grey">
                                    <?php echo $val->ads_title; ?>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php endforeach; ?>
                </div>
        </div>
    </div>
</div>