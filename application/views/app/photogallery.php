<div class="col-md-12 uniPad">
    <?php foreach($getPhotos as $val): ?>
    <div class="col-md-3 noPad stillBord">
        <div style="height:200px; max-width: 100%; background-color: #e9ebee; background-size: cover; background-position: center 30%; background-repeat: no-repeat; display: block; background-image: url('<?php echo $val->pic_url; ?>');"></div>
    </div>
    <?php endforeach; ?>
</div>