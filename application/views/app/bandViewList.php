<div class="container stanPad">
	<div class="row">
		<div class="col-md-12 noPad">
			<h3 class="font-thin">
	        	<i class="fa fa-magic"></i> BANDS
			</h3>
		</div>
	</div>
</div>
<div id="featuredbands"></div>
<div class="sectionvill">
	<div class="container">
		<div class="row">
			<div class="col-md-12 noPad">
				<div class="col-md-9">
					<div class="px-event gridb">
						<div id="listerbandarea"></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="widget widget_albums">
						<div class="widget-section-title">
							<h2>Trending Videos</h2>
						</div>
						<ul>
							<div id="listersbandvids"></div>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>