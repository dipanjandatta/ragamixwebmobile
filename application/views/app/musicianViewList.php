<div class="container stanPad">
    <div class="row">
        <div class="col-md-12 noPad">
        	<div class="noo-shortcode-events grid">
        		<div class="noo-grid-header-events" style="padding: 0px 10px; margin-top: -10px !important;">
        			<div class="sh-event-title font-thin">
						<i class="fa fa-music"></i> MUSICIANS
        			</div>
				</div>
				<div class="noo-sh-event-content row">
					<div id="featuredmusician"></div>
				</div>
				<div id="listermusicianarea"></div>
        	</div>
        </div>
    </div>
</div>
<div class="sectionvill">
	<div class="container smallPad">
		<div class="col-md-12 noPad px-portfolio">
			<div class="noo-grid-header-events" style="margin-bottom: 40px !important">
				<div class="sh-event-title font-thin">
        			<i class="fa fa-film"></i> TRENDING VIDEOS
				</div>
			</div>
			<div id="listers"></div>
		</div>
	</div>
</div>


