<div class="col-md-12 noPad">
    <div class="bandsintromain">
        <i class="fa fa-music"></i>&nbsp;The Band's Profile
    </div>
    <div class="col-md-3">
        <div class="bandsintromain smtextband">
            EXPERIENCE
        </div>
        <div class="lab">
            <?php 
                if($previewData[0]->experience_in_years != '') { 
                    echo $previewData[0]->experience_in_years. ' years';
                } else {
                    echo "No Experience Provided";
                }
            ?>
        </div>
        <div class="bandsintromain smtextband">
            COMMITMENT LEVEL
        </div>
        <div class="lab">
            <?php 
                if($previewData[0]->commitment_lvl != '') { 
                    echo $previewData[0]->commitment_lvl;
                } else {
                    echo "No Commitment Level Provided";
                }
            ?>
        </div>
        <div class="bandsintromain smtextband">
            GENRE
        </div>
        <div class="lab">
            <?php 
                if($previewData[0]->genre_name != '') { 
                    echo $previewData[0]->genre_name;
                } else {
                    echo "No Genre Mentioned";
                }
            ?>
        </div>
    </div>
    <div class="col-md-5" style="background: rgba(255, 221, 156, 0.7);">
        <div class="bandsintromain smtextband">
            BIO
        </div>
        <div class="lab biotext">
            <?php 
                if($previewData[0]->user_desc != '') { 
                    echo $previewData[0]->user_desc;
                } else {
                    echo "No Description Given";
                }
            ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="bandsintromain smtextband">
            INFLUENCES
        </div>
        <div class="lab">
            <?php 
                if($previewData[0]->music_influence_desc != '') { 
                    echo $previewData[0]->music_influence_desc;
                } else {
                    echo "No Influencers Given";
                }
            ?>
        </div>
        <div class="bandsintromain smtextband">
            SEEKING
        </div>
        <div class="lab">
            <?php 
                if($previewData[0]->seeking_description != '') { 
                    echo $previewData[0]->seeking_description;
                } else {
                    echo "No Seeking Given";
                }
            ?>
        </div>
        <div class="bandsintromain smtextband">
            Contact Details
        </div>
        <div class="lab">
            <i class="fa fa-envelope"></i>&nbsp;
            <?php 
                if($previewData[0]->email != '') { 
                    echo $previewData[0]->email;
                } else {
                    echo "No Email Contact";
                }
            ?>
        </div>
        <div class="lab">
            <i class="fa fa-desktop"></i>&nbsp;
            <?php 
                if($previewData[0]->website_addr != '') { 
                    echo $previewData[0]->website_addr;
                } else {
                    echo "No Website Contact";
                }
            ?>
        </div>
        <div class="lab">
            <i class="fa fa-home"></i>&nbsp;
            <?php 
                if($previewData[0]->locality != '') { 
                    echo $previewData[0]->locality;
                } else {
                    echo "No Locality Provided";
                }
                echo " &nbsp; <br />City: ".$previewData[0]->city_name;
            ?>
        </div>
    </div>
</div>