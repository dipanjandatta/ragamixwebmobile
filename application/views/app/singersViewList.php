<div class="container stanPad">
    <div class="row">
        <div class="col-md-12 noPad">
            <div class="col-md-8">
				<h2 class="font-thin m-b">
					<i class="fa fa-microphone"></i> Singers
				</h2>
            	<div class="row row-sm">
            		<div id="featuredsingers"></div>
            	</div>
				<div class="row row-sm">
					<div id="listersingersarea"></div>
				</div>
            </div>
            <div class="col-md-4">
            	<h3 class="font-thin">
            		<i class="fa fa-film"></i> Trending Videos
            	</h3>
            	<div class="row row-sm listScrollers">
            		<div id="listerssingersvids"></div>
            	</div>
            </div>
        </div>
    </div>
</div>