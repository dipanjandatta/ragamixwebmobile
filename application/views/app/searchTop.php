<div class="container stanPad">
    <div class="row">
        <?php if($city != '') { ?>
            <a id="hideHomeOne" class="fc kc">
                <i class="fa fa-close biggerFont"></i>
            </a>
            <div class="col-md-12 noPad nbts blah">
                <div class="col-md-2 bigs bigPad haPad">
                    <?php echo $city; ?>
                    <div class="text-center">
                        <a data-toggle="modal" data-target="#cloc" class="junu">Change Location</a>
                    </div>
                </div>
                <div class="col-md-10 noPad">
                    <?php echo $map['html']; ?>
                </div>
                <div class="modal fade" id="cloc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h1 class="modal-title">Change Location</h1>
                            </div>
                            <?php echo form_open('profile/searchTop'); ?>
                            <div class="modal-body" style="float: left;">
                                <input type="text" name="locname" class="typeahead ragacontrols maniac" placeholder="Location" />
                            </div>
                            <div class="modal-footer">
                                <input type="submit" name="submit" class="btn btn-danger" value="Change &amp; Search" onclick="return loadSubmit()" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-md-12 noPad" style="padding-top: 30px !important;">
            <div class="col-md-2 noPad padTop">
                <ul class="searchNavList">
                    <a href="<?php echo base_url(); ?>profile/searchTop?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=all&type=oo" class="searchNavLink">
                        <?php if($rt=='all') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            All
                        </li>
                    </a>                  
                    <a href="<?php echo base_url(); ?>profile/searchMusician?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Musicians&type=oA&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Musicians') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Musicians
                        </li>
                    </a>  
                    <a href="<?php echo base_url(); ?>profile/searchSingers?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Singers&type=oS&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Singers') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Singers
                        </li>
                    </a>  
                    <a href="<?php echo base_url(); ?>profile/searchBands?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Bands&type=oB&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Bands') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Bands
                        </li>
                    </a>  
                    <a href="<?php echo base_url(); ?>profile/searchAds?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Ads&type=oD&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Ads') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Ads
                        </li>
                    </a>  
                    <a href="<?php echo base_url(); ?>profile/searchEvents?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Events&type=oE&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Events') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Events
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchVideos?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Videos&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Videos') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Videos
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchSolo?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Solo&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Solo') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Solo Artist
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchDj?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=DJ&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='DJ') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Dj
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchComposer?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Composer&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Composer') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Composer
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchEducators?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Educators&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Educators') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Educators
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchEventMarketer?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=EventMarketer&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='EventMarketer') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Event Marketer
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchFan?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Fan&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Fan') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Fan
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchMusicalDuo?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=MusicalDuo&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='MusicalDuo') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Musical Duo
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchShowManager?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=ShowManager&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='ShowManager') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Show Manager
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchStudio?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Studio&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Studio') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Studio
                        </li>
                    </a>
                    <a href="<?php echo base_url(); ?>profile/searchTeacher?q=<?php echo rawurlencode($st); ?>&city=<?php echo $city; ?>&ref=Teacher&type=oV&next=1&clicks=0&page_number=0" class="searchNavLink">
                        <?php if($rt=='Teacher') { ?>
                        <li class="searchNavListers searchActive">
                            <?php } else { ?>
                        <li class="searchNavListers">
                            <?php } ?>
                            Teacher
                        </li>
                    </a>
                </ul>
            </div>
            <?php if($type == 'oo'){ ?>
            <div class="col-md-10 noPad">
                <div class="col-md-6">
                
                    <div class="hori"></div>
                    <div class="pom">
                        Musicians <span class="cr"><?php echo $mc; ?></span>
                    </div>
                    <div class="horir"></div>
                    <!--                <div class="contentName">
                    <i class="fa fa-pied-piper-alt iconSD"></i>
                    <span class="con">Musicians</span>
                </div>-->
                    <div class="mitti text-right">
                        <?php echo anchor('profile/searchMusician?q='.rawurlencode($st).'&city='.$city.'&ref=Musicians&type=oA&next=1&clicks=0&page_number=0', 'See All Musicians', array('class' => 'slinks')); ?>
                    </div>
                
                    <div class="jewel">
                        <?php foreach($getAllSearchM as $val):
                            $returnValue = str_replace(' ', '%20', $val->profile_pic_url);
                            ?>
                            <a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>" style="cursor:pointer;">
                                <div class="cardss">
                                    <div class="hoverss">
                                        <div class="thumbss">
                                            <div style="height:124px; max-width: 100%; background-size: cover; background-position: center 35%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                        </div>
                                        <ul class="details">
                                            <li>
                                                <h3 class="cardfont"><?php echo $val->user_name; ?></h3>
                                                <?php
                                                if($val->user_category == '') {
                                                    echo $val->category_other; } elseif($val->category_other == '') { echo $val->user_category; } else {echo $val->user_category; }?>, <br /><?php echo $val->city_name; ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                        <div class="mitiya">
                            ....and <?php $restVar = $mc - count($getAllSearchM); echo $restVar; ?> more
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                
                    <div class="hori"></div>
                    <div class="pom">
                        Singers <span class="cr"><?php echo $sc; ?></span>
                    </div>
                    <div class="horir"></div>
                    <!--                <div class="contentName">
                    <i class="fa fa-pied-piper-alt iconSD"></i>
                    <span class="con">Musicians</span>
                </div>-->
                    <div class="mitti text-right">
                        <?php echo anchor('profile/searchSingers?q='.rawurlencode($st).'&city='.$city.'&ref=Singers&type=oS&next=1&clicks=0&page_number=0', 'See All Singers', array('class' => 'slinks')); ?>
                    </div>
                    <div class="jewel">
                        <?php foreach($getAllSearchS as $val):
                            $returnValue = str_replace(' ', '%20', $val->profile_pic_url);
                            ?>
                            <a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>" style="cursor:pointer;">
                                <div class="cardss">
                                    <div class="hoverss">
                                        <div class="thumbss">
                                            <div style="height:124px; max-width: 100%; background-size: cover; background-position: center 35%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                        </div>
                                        <ul class="details">
                                            <li>
                                                <h3 class="cardfont"><?php echo $val->user_name; ?></h3>
                                                <?php
                                                if($val->user_category == '') {
                                                    echo $val->category_other; } elseif($val->category_other == '') { echo $val->user_category; } else {echo $val->user_category; }?>, <br /><?php echo $val->city_name; ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                        <div class="mitiya">
                            ....and <?php $restVarS = $sc - count($getAllSearchS); echo $restVarS; ?> more
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                
                    <div class="hori"></div>
                    <div class="pom">
                        Bands <span class="cr"><?php echo $bc; ?></span>
                    </div>
                    <div class="horir"></div>
                    <!--                <div class="contentName">
                    <i class="fa fa-pied-piper-alt iconSD"></i>
                    <span class="con">Musicians</span>
                </div>-->
                    <div class="mitti text-right">
                        <?php echo anchor('profile/searchBands?q='.rawurlencode($st).'&city='.$city.'&ref=Bands&type=oS&next=1&clicks=0&page_number=0', 'See All Bands', array('class' => 'slinks')); ?>
                    </div>
                    <div class="jewel">
                        <?php foreach($getAllSearchB as $val):
                            $returnValue = str_replace(' ', '%20', $val->profile_pic_url);
                            ?>
                            <a href="<?php echo base_url(); ?>profile/details/band/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>" style="cursor:pointer;">
                                <div class="cardss">
                                    <div class="hoverss">
                                        <div class="thumbss">
                                            <div style="height:124px; max-width: 100%; background-size: cover; background-position: center 35%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                        </div>
                                        <ul class="details">
                                            <li>
                                                <h3 class="cardfont"><?php echo $val->user_name; ?></h3>
                                                <?php
                                                if($val->user_category == '') {
                                                    echo $val->category_other; } elseif($val->category_other == '') { echo $val->user_category; } else {echo $val->user_category; }?>, <br /><?php echo $val->city_name; ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                        <div class="mitiya">
                            ....and <?php $restVarB = $bc - count($getAllSearchB); echo $restVarB; ?> more
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="hori"></div>
                    <div class="pom">
                        Composer <span class="cr"><?php echo $ac; ?></span>
                    </div>
                    <div class="horir"></div>
                    <!--                <div class="contentName">
                    <i class="fa fa-pied-piper-alt iconSD"></i>
                    <span class="con">Musicians</span>
                </div>-->
                    <div class="mitti text-right">
                        <?php echo anchor('profile/searchComposer?q='.rawurlencode($st).'&city='.$city.'&ref=Composer&type=oS&next=1&clicks=0&page_number=0', 'See All Composers', array('class' => 'slinks')); ?>
                    </div>
                    <div class="jewel">
                        <?php foreach($getAllSearchCom as $val):
                            $returnValue = str_replace(' ', '%20', $val->profile_pic_url);
                            ?>
                            <a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>" style="cursor:pointer;">
                                <div class="cardss">
                                    <div class="hoverss">
                                        <div class="thumbss">
                                            <div style="height:124px; max-width: 100%; background-size: cover; background-position: center 35%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                        </div>
                                        <ul class="details">
                                            <li>
                                                <h3 class="cardfont"><?php echo $val->user_name; ?></h3>
                                                <?php
                                                if($val->user_category == '') {
                                                    echo $val->category_other; } elseif($val->category_other == '') { echo $val->user_category; } else {echo $val->user_category; }?>, <br /><?php echo $val->city_name; ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                        <div class="mitiya">
                            ....and <?php $restVarB = $bc - count($getAllSearchB); echo $restVarB; ?> more
                        </div>
                    </div>
                </div>





                <div class="col-md-6">

                    <div class="hori"></div>
                    <div class="pom">
                        Solo Artist <span class="cr"><?php echo $vc; ?></span>
                    </div>
                    <div class="horir"></div>
                    <!--                <div class="contentName">
                                        <i class="fa fa-pied-piper-alt iconSD"></i>
                                        <span class="con">Musicians</span>
                                    </div>-->
                    <div class="jewel">
                        <?php foreach($getAllSearchSolo as $val):
                            $returnValue = str_replace(' ', '%20', $val->profile_pic_url);
                            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->doc_youtube_url, $match)) {
                                $vid = $match[1];
                                $displayedYoutubeVideo = "<iframe width='90' height='90' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                            }
                            ?>
                            <a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>" style="cursor:pointer;">
                            <div class="display_box capHolderDisplay">
                                    <div class="coverClass searchProfImgHolderBig">
                                        <?php if($val->profile_pic_url != '') { ?>
                                            <div style="height:90px; max-width: 100%; background-size: cover; background-position: center 50%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                        <?php } else {
                                            echo $displayedYoutubeVideo;
                                        }
                                        ?>
                                    </div>
                                    <div class="placeholdLeft">
                                        <?php echo $val->user_name; ?>
                                        <span class="otlabtext">
                                        <?php echo $val->city_name; ?>
                                    </span>
                                    <span class="otlabtext">
                                        <?php echo $val->category_other; ?>
                                    </span>
                                    </div>

                                </div>
                            </a>
                        <?php endforeach; ?>
                        <?php echo anchor('profile/searchSolo?q='.rawurlencode($st).'&city='.$city.'&ref=Solo&type=oE&next=1&clicks=0&page_number=0', '<div class="profiler profilerStyle"><span class="texter">See more</span></div>'); ?>
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="hori"></div>
                    <div class="pom">
                        Educators <span class="cr"><?php echo $ec; ?></span>
                    </div>
                    <div class="horir"></div>
                    <!--                <div class="contentName">
                                        <i class="fa fa-pied-piper-alt iconSD"></i>
                                        <span class="con">Musicians</span>
                                    </div>-->
                    <div class="jewel">
                        <?php foreach($getAllSearchEdu as $val):
                            $returnValue = str_replace(' ', '%20', $val->profile_pic_url);
                            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->doc_youtube_url, $match)) {
                                $vid = $match[1];
                                $displayedYoutubeVideo = "<iframe width='90' height='90' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                            }
                            ?>
                            <a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>" style="cursor:pointer;">

                            <div class="display_box capHolderDisplay">
                                    <div class="coverClass searchProfImgHolderBig">
                                        <?php if($val->profile_pic_url != '') { ?>
                                            <div style="height:90px; max-width: 100%; background-size: cover; background-position: center 50%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                        <?php } else {
                                            echo $displayedYoutubeVideo;
                                        }
                                        ?>
                                    </div>
                                    <div class="placeholdLeft">
                                        <?php echo $val->user_name; ?>
                                        <span class="otlabtext">
                                        <?php echo $val->city_name; ?>
                                    </span>
                                    <span class="otlabtext">
                                        <?php echo $val->category_other; ?>
                                    </span>
                                    </div>

                                </div>
                            </a>
                        <?php endforeach; ?>
                        <?php echo anchor('profile/searchEducators?q='.rawurlencode($st).'&city='.$city.'&ref=Educators&type=oE&next=1&clicks=0&page_number=0', '<div class="profiler profilerStyle"><span class="texter">See more</span></div>'); ?>
                    </div>
                </div>

        </div>
    <?php } ?>
    </div>
    </div>
</div>