<div class="col-md-12 faqclass">
    <div class="col-md-8 col-md-offset-2">
        <span class="fontfaq">
           About Ragamix
        </span>
    </div>
</div>
<div class="container">
    <div class="row" style="margin-top: 50px;">
        <div class="col-md-12 noPad">
            One stop destination dedicated to serve musicians.<br />
            RagaMix provides you with an unlimited possibility of connecting with the right musician and talent in your area and make music.<br />
            Musicians, bands and all individuals inclusive of students, professionals, institutions and the music industry can connect and interact with each other based on matching requirements and similar interests.<br />
            Our team comprises of experts in different walks of life but with a strong passion for music which guides us on this mission. This dedicated team is constantly working on innovative yet simple mechanisms to ensure musicians’ requirements are met in order to facilitate good music beyond physical boundaries<br />
            We invite your suggestions - for updates and improvements.<br />
            Join us and become a part of this musical mission and flow with music.
        </div>
    </div>
</div>