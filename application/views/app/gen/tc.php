<div class="container">
    <div class="row" style="margin-top: 50px;">
        <div class="col-md-12 noPad">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-4 backWhite" style="margin-top: 23px;">
                    <ul class="genroots">
                        <li class="gentc">
                            <a href="#tc">
                                Introduction
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#as">
                                Purpose
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#ac">
                                A brief About us
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#rc">
                                Interpretation
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#dp">
                                Agreement
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#pm">
                                Obligations
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#cc">
                                Your Membership
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#pp">
                                Payment
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#nc">
                                Notices and Service Messages
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#dd">
                                Messages and Sharing
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#ipr">
                                Rights and Limits
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#us">
                                Disclaimer and Limit of Liability
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#idn">
                                Termination
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#nsp">
                                General Terms
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#doi">
                                RagaMix “DOs” &amp; “DON’Ts.”
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#ts">
                                Complaints Regarding Content
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#dw">
                                GOVERNING LAW &amp; DISPUTE RESOLUTION
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#asg">
                                Waiver
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#vt">
                                Amendment
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#cu">
                                Variation
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#resy">
                                Residuary
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#tcr">
                                Counterpart Terms &amp; Conditions reasonable
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#cpt">
                                Counterparts
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#cptn">
                                Competition Policy
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#fme">
                                FORCE MAJEURE
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#cnd">
                                CONFIDENTIALITY &amp; NON-DISCLOSURE
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#pp">
                                Privacy Policy
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#pucp">
                                Prevention of usage, Copy, Plagiarism etc
                            </a>
                        </li>
                        <li class="gentc">
                            <a href="#hcu">
                                How To Contact Us
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-8 noPad">
                    <div class="col-md-12 datlay ">
                        <span class="biggerFont">
                            Ragamix Use Terms
                        </span><br />
                        <span class="termnotice" style="margin-top: 16px;color: #7ba6c3;font-size: 11px;line-height: 21px;">
                            Please carefully go through the terms of use. by using this website you indicate your understanding and acceptance of these terms. If you do not agree to these terms you may not use this website.<br /><br />
                            You confirm that you are at least 18 years old. If you are below 18 years of age, then you confirm that you are visiting the site under the supervision of a parent or a guardian.
                        </span>
                    </div>
<!--                    <span class="col-md-12 backWhite" style="padding: 20px 10px;"></span>-->

                        <span class="classone" id="tc">
                            Introduction
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            We are a network and online platform for Persons who are ,interested or are presently, associated with music, singing, vocal works, recitals, anchoring etc.
                        </span>

                        <span class="classone" id="as">
                            Purpose
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            Our mission is to connect the world's music professionals, vocal artists, etc along with people related with the same, which may include but not be limited to, teachers, coordinators, even managers, music recording studio’s etc, to allow them to be more productive and successful. Our services are designed to promote economic opportunity for our members by enabling various talented singers, musicians, music bands, music teachers  and other professionals to meet, exchange ideas, learn, and find opportunities, work, and make decisions in a network of trusted relationships.
                        </span>

                        <span class="classone" id="ac">
                            A brief About us
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            RagaMix is an Intellectual Property that is presently used by Treble Hub Media LLP, having its office at 4, Duff Lane, Kolkata  by virtue of a leave and licence Agreement. Hence, during the tenure of this Agreement, Treble Hub Media LLP, shall be entitled to take all actions, file and represent suits or matters, receive or make payments, on behalf of RagaMix.
                        </span>

                        <span class="classone" id="rc">
                            Interpretation
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            In this Agreement, unless the context otherwise requires the Agreement shall be interpreted in the following manner:<br><br>

                            <b>I.</b> Words of any gender are deemed to include the other gender;<br>
                            <b>II.</b> Words using the singular or plural number also include the plural or singular number, respectively;<br>
                            <b>III.</b> The terms “hereof”, ‘herein”, “hereby”, “hereto” and derivative or similar words refer to this entire Agreement;<br>
                            <b>IV.</b> The term “Section”, “clauses”, or “Schedules” in this Agreement shall, except where the context otherwise requires refers to the specified “Section”, “clauses”, or “Schedules”  of this Agreement;<br>
                            <b>V.</b> Heading, sub-heading and bold typeface are only for convenience and shall be ignored for the purposes of interpretation;<br>
                            <b>VI.</b> Reference to any legislation or law or to any provision thereof shall include references to any such law as it may, after the date hereof, from time to time,  be amended, supplemented or re-enacted, and any  reference to a statutory provision shall include any  subordinate  legislation   made  from  time  to  time   under   that provision;<br>
                            <b>VII.</b> Any term or expression used but not defined  herein shall  have the same meaning attributable to  it  under applicable laws;<br>
                            <b>VIII.</b> References to the word “include” or “including” shall be construed without limitation;<br>
                            <b>IX.</b> Any reference to day shall mean a reference to a calendar day;<br>
                            <b>X.</b> Any reference to month shall mean a reference to a calendar month;<br>
                            <b>XI.</b> Any reference at any time to any agreement, deed, instrument, license or document of any description shall be construed as reference to that agreement, deed, instrument, license or other document as amended, varied, supplemented, modified or suspended at the time of such reference;<br>
                            <b>XII.</b> Any reference to any period commencing “from” a specified day or date and “till” or “until” a specified day or date shall include both such days or dates;<br>
                            <b>XIII.</b> The schedules and exhibits annexed to this Agreement form an integral part of this Agreement and will be of full force and effect as though they were expressly set out in the body of this Agreement;<br>
                            <b>XIV.</b> In case of any dispute/ confusion occurs with regards to any word/ phrase/ or paragraph, then the general industry practice and/or the meaning with in a legal dictionary shall be construed upon in a harmonious manner;<br>
                            <b>XV.</b> Other terms are same as provided the General Clauses Act, 1897 and/or any other acceptable Legal Dictionary.<br>
                        </span>

                        <span class="classone" id="dp">
                            Agreement
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">A. Definitions</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            <b>I. Accounting Year-</b> would ordinarily mean the financial year as defined in the Limited Liability Partnership Act, 2008;<br>
                            <b>II. Affiliates  -</b>  means and includes, in relation to any Person, any entity Controlled, directly or indirectly, by that Person, any entity that Controls, directly or indirectly, that Person, or any entity under common Control with that Person or, in the case of a natural Person, any Relative of such Person. Without limiting the generality of the above, a holding or subsidiary company of any entity shall be deemed to be an Affiliate of that entity;<br>
                            <b>III. Agreement-</b> shall mean these presents, and shall include any schedule, annexure, appendix hereto, any and all amendments, addition and alteration whether by way of a supplemental agreement or otherwise to the terms contained herein;<br>
                            <b>IV. Applicable Law(s) -</b> shall mean any statute, law, regulation, ordinance, rule, judgment, order, decree, bye-law, clearance, directive, guideline, policy, requirement or other governmental restriction or any similar form of decision of, or determination by, or any interpretation or administration having the force of law of any of the foregoing, by any Governmental Authority having jurisdiction over the matter in question, mandatory accounting standards whether in effect as of the date of this Agreement or thereafter;<br>
                            <b>V. Associates-</b> shall have the same meaning as assigned in Caluse:  unless the context specifically requires to be construed otherwise, shall be assigned the meaning as set in the Human Resource policy Clause herein;<br>
                            <b>VI. Business Day-</b> shall mean a day, except Saturdays and Sundays, on which banks are generally open for business in India;<br>
                            <b>VII. Calendar Day-</b> shall mean all days in a month, including Sundays and any other holidays;<br>
                            <b>VIII. Clause-</b> means a Clause herein this Agreement;<br>
                            <b>IX. Designated partner-</b> means any partner designated as Designated Partner by way of resolution passed by a majority of the Partners or as the circumstances would require;<br>
                            <b>X.</b> Firm herein this Agreement shall ordinarily mean, unless the context specifically requires any other meaning to be assigned, this Limited Liability Partnership or this business i.e Treblehub Media LLP, which shall without prejudice/ derogation/ repugnant to any other law in force shall be treated as a body corporate which is also a separate legal entity from that of its members/ Partners;<br>
                            <b>XI. Financial Year-</b> Shall mean a Financial Year as per the provisions of the Income Tax Act, 1961, where the financial year shall start at 1st of April each year and shall end at 31st March of the next year.<br>
                            <b>XII. Force Majeure-</b> shall have the meaning ascribed in this Agreement herein below;<br>
                            <b>XIII. Good Industry practice-</b> would generally mean those practices, methods, techniques, standards, skills, diligence and prudence which are generally and reasonably expected of and accepted internationally from a reasonably skilled and experienced Person engaged in the same / similar type of activity as envisaged under this Agreement and would mean good engineering practices in the design, engineering, construction and which would be expected to result in the performance of its obligations by the Service Provider in accordance with this Agreement and Applicable laws;<br>
                            <b>XIV.</b> Governmental/ Appropriate Authorities- means any governmental authority, statutory authority, governmental department, government company, agency, commission, board, tribunal or public body or authority, including  appropriate authorities of competent jurisdiction or other entity authorised to make laws, rules or regulations or pass directions having or purporting to have jurisdiction in India or any state or other subdivision thereof or any municipality, district or other subdivision thereof and any authority exercising powers conferred by law;<br>
                            <b>XV. Partner - </b> means any person who becomes a partner in the Limited Liability Partnership in accordance with this Limited Liability Partnership Agreement;<br>
                            <b>XVI. Person(s)-</b> shall mean and include an individual, HUF, BOI, AOP, Artificial Juristic Person, partnership firm, corporation etc;<br>
                            <b>XVII. Registered Office-</b> Shall be the office as specified in the Domicile Clause as provided herein this Agreement, whereby all communications shall be made using the Address in the Registered Office. However nothing in this Agreement shall restrict the firm from having any other office/ place of work for the purpose of carrying out the business of the Firm in India ;<br>
                            <b>XVIII. Taxes/ duties-</b> shall mean all taxes imposed by any Governmental Authority, including but not limited to:<br>
                            <b>(a)</b> any tax based upon or measured by income, gross receipts, sales use or value added;<br>
                            <b>(b)</b> any taxes denominated as ad valorem, transfer, franchise, capital stock, payroll, employment, excise, occupation, property, windfall profits, service, environmental, customers, or withholding taxes;<br>
                            <b>(c)</b> any interest, penalties, or other amounts imposed with respect to any tax.<br>
                        </span>
                        <span class="classone" style=" font-size: 20px;">
                            B.
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            When you use our Services , you are entering into a legal agreement within the meaning of the Indian Contracts of 1872 and you agree to all of these terms.
                            You also agree to our Privacy policy, which covers how we collect and use your personal information.<br>
                            You agree that by clicking “Join Now” “Join RagaMix”, “Sign Up” or similar, registering, accessing or using our services you are entering into a legally binding agreement (even if you are using our Services on behalf of a company).
                            This “Agreement” includes this User Agreement and the Privacy policy, and other terms that will be displayed to you at the time you first use certain features as may be amended by RagaMix from time to time. If you do not agree to this Agreement, do NOT click “Join Now” (or similar) and do not access or otherwise use any of our Services.
                            Registered users of our Services are “Members” and unregistered users are “Visitors”. This Agreement applies to both.
                        </span>

                        <span class="classone" id="pm">
                            Obligations
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            <b>I. Service Eligibility</b><br>
                            Here are some promises you make to us in this Agreement:<br>
                            i. You're eligible to enter into this Agreement and you are at least our “Minimum Age.” In ordinary Course of Business herein the minimum age shall be deemed to be 18 years and if the user is below 18, his guardian may be enter into this agreement on his behalf.<br>
                            ii. To use the Services, you agree that: (1) you must be the “Minimum Age” (defined below) or older; (2) you will only have one RagaMix account (along with other accounts, if applicable), which must be in your real name;<br>
                            iii. “Minimum Age” means  18 years old for all countries, unless the laws of that country prescribe a higher age limit, where in that case the higher age limit shall the deemed as the minimum age. However, if law requires that you must be older in order for RagaMix to lawfully provide the Services to you (including the collection, storage and use of your information) then the Minimum Age is such older age.<br>
                        </span>

                        <span class="classone" id="cc">
                            Your Membership
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            You'll keep your password a secret.
                            You will not share an account with anyone else and will follow our rules and the law.
                            As between you and others, your account belongs to you. You agree to: (1) try to choose a strong and secure password; (2) keep your password secure and confidential; (3) not transfer any part of your account (e.g., connections, groups) and (4) follow the law and the Dos and Don'ts below. You are responsible for anything that happens through your account unless you close it or report misuse.
                            Note that for Premium Services purchased by another party for you to use, the party paying for the Premium Service controls such an account (which is different from your personal account) and may terminate your access to it.
                        </span>

                        <span class="classone" id="pp">
                            Payment
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            You'll honor your payment obligations and you are okay with the payment gateways terms and policy, by which you would make the payment to us  Also, there may be fees and taxes that are added to our fees.
                            We don't guarantee refunds.
                            In case of a Premium membership the user shall be subjected to a separate agreement that will be provided to him at the time of registering for the premium membership.
                            If you purchase any of our paid Services (“Premium Services”), you agree to pay us the applicable fees and taxes. Failure to pay these fees may result in the termination of your subscription. Also:<br>
                            <ul>
                                <li>Your purchase may be subject to foreign exchange fees or differences in prices based on location (e.g. exchange rates).</li>
                                <li>You authorize us to store and continue billing your payment method (e.g. credit card) even after it has expired, to avoid interruptions in your service (e.g. subscriptions) and to facilitate easy payment for new services.</li>
                                <li>You must pay us for applicable fees and taxes unless you cancel the Premium Service, in which case you agree to still pay these fees through the end of the applicable subscription period. Learn how to cancel or change your Premium Services and read about RagaMix's refund policy.</li>
                                <li>Taxes are calculated based on the billing information that you provide us at the time of purchase.</li>
                            </ul>
                        </span>

                        <span class="classone" id="nc">
                            Notifications of Claims of Infrigements
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            RagaMix is not liable for any infringement of copyright arising out of materials posted on or transmitted through the site, or items advertised on the site, by end users or any other third parties.<br />
                            In case you are an owner of intellectual property rights or an authorized agent of the owner of intellectual property rights and believe that any Content infringes upon your intellectual property right or of the owner on whose behalf you are an agent, you may submit a notification to RagaMix together with a request to RagaMix to delete the relevant Content in good faith. The notification and the request must be submitted with the following information:<br />
                            A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;<br />
                            Identification of the intellectual property rights claimed to have been infringed, or, if multiple intellectual property rights at a single online site are covered by a single notification, a representative list of such works at that site;<br />
                            A signed statement that you have a good faith belief that use of the material in the manner complained of is not violated by the intellectual property right-owner, its agent, or the law;<br />
                            A signed statement that the intellectual property-owner hold RagaMix harmless from any claim of any third party in connection with the removing by RagaMix of the relevant content.<br />
                            Identification of the Content (by means of data or communication link, AD ID, etc.) that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled and information reasonably sufficient to permit RagaMix to locate the material;<br />
                            Information reasonably sufficient to permit RagaMix to contact you, such as an address, telephone number, and, if available, an electronic mail address;<br />
                            Notifications must be sent to RagaMixthrough the contents provided on the Website.
                        </span>

                        <span class="classone" id="dd">
                            Notices and Service Messages
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            You're okay with us using our websites, mobile apps, and email to provide you with important notices. This Agreement applies to mobile applications as well. Also, you agree certain additional information can be shared with us.
                            If the contact information you provide isn't up to date, you may miss out on these notices.
                            You agree that we may provide notices to you in the following ways: (1) a banner notice on the Service, or (2) an email sent to an address you provided, or (3) through other means including mobile number, telephone, or mail. You agree to keep your contact information up to date.
                        </span>

                        <span class="classone" id="ipr">
                            Messages and Sharing
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            When you share information, others can see, copy and use that information.
                            Our Services allow messaging and sharing of information in many ways, such as your profile, slide, links to news articles, job postings, Mails and blogs. Information and content that you share or post may be seen by other Members or, if public, by Visitors. Where we have made settings available.
                            We are not obligated to publish any information or content on our Service and can remove it in our sole discretion, with or without notice.
                        </span>

                        <span class="classone" id="us">
                            Rights and Limits
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">I. Your License to RagaMix</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            You own all of the content, feedback, and personal information you provide to us, but you also grant us a non-exclusive license to it.
                            You promise to only provide information and content that you have the right to share, and that your RagaMix profile will be truthful.
                            As between you and RagaMix, you own the content and information that you submit or post to the Services and you are only granting RagaMix the following non-exclusive license: A worldwide, transferable and sublicensable right to use, copy, modify, distribute, publish, and process, information and content that you provide through our Services, without any further consent, notice and/or compensation to you or others. These rights are limited in the following ways:<br>
                            <b>1.</b> You can not end this license for any  content by deleting such content from the Services, or generally by closing your account, in case you wish to remove the same, you are required to make a request to Ragamix, where it is the sole discretion of RagaMix that whether such a content should remain on our site or not and in case the same is decided to be removed you agree on allowing for the reasonable time it takes to remove from backup and other systems.<br>
                            <b>2.</b> We may include your content in advertisements for the products and services of others (including sponsored content) to others without your separate consent. However, we have the right, without compensation to you or others, to serve ads near your content and information, and your comments on sponsored content may be visible as noted in the Privacy policy.<br>
                            <b>3.</b> We will get your consent if we want to give others the right to publish your posts beyond the Service. However, other Members and/or Visitors may access and share your content and information, consistent with your settings and degree of connection with them.<br>
                            <b>4.</b> While we may edit and make formatting changes to your content (such as translating it, modifying the size, layout or file type or inserting and removing metadata), we will not modify the meaning of your expression.<br>
                            <b>5.</b> Because you own your content and information and we only have non-exclusive rights to it, you may choose to make it available to others, including under the terms of any other license. However, you are not entitled to cause RagaMix to delete/modify  any contents from the website of RagaMix.<br>
                            You agree that we may access, store and use any information that you provide in accordance with the terms of the herein and your privacy settings.<br>
                            By submitting suggestions or other feedback regarding our Services to RagaMix, you agree that RagaMix can use and share (but does not have to) such feedback for any purpose without compensation to you.
                            You agree to only provide content or information if that does not violate the law nor anyone's rights (e.g., without violating any intellectual property rights or breaching a contract). You also agree that your profile information will be truthful. RagaMix may be required by law to remove certain information or content in certain countries.
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">II.Service Availability</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            We may change or discontinue any of our Services. We can't promise to store or keep showing any information and content you've posted.
                            We may change, suspend or end any Service, or change and modify prices prospectively in our discretion. To the extent allowed under law, these changes may be effective upon notice provided to you.
                            RagaMix is not a storage service. You agree that we have no obligation to store, maintain or provide you a copy of any content or information that you or others provide, except to the extent required by applicable law and as noted in Section 3.1 of our Privacy policy
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">III.Other Content, Sites and apps</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            When you see or use others' content and information posted on our Services, it's at your own risk.
                            Third parties may offer their own products and services through RagaMix, and we aren't responsible for those third-party activities.
                            By using the Services, you may encounter content or information that might be inaccurate, incomplete, delayed, misleading, illegal, offensive or otherwise harmful. RagaMix generally does not review content provided by our Members. You agree that we are not responsible for third parties' (including other Members') content or information or for any damages as result of your use of or reliance on it.
                            You are responsible for deciding if you want to access or use third party apps or sites that link from our Services. If you allow a third party app or site to authenticate you or connect with your RagaMix account, that app or site can access information on RagaMix related to you and your connections. Third party apps and sites have their own legal terms and privacy policies, and you may be giving others permission to use your information in ways we would not. Except to the limited extent it may be required by applicable law.
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">IV.Limits</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            We have the right to limit how you connect and interact on our Services.
                            We're providing you notice about our intellectual property rights.
                            RagaMix reserves the right to limit your use of the Services, including the number of your connections and your ability to contact other Members. RagaMix reserves the right to restrict, suspend, or terminate your account if RagaMix believes that you may be in breach of this Agreement or law or are misusing the Services (e.g. violating any Do and Don'ts).
                            RagaMix reserves all of its intellectual property rights in the Services.
                        </span>

                        <span class="classone" id="idn">
                            Disclaimer and Limit of Liability
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">I.  No Warranty</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            This is our disclaimer of legal liability for the quality, safety, or reliability of our Services.
                            TO THE EXTENT ALLOWED UNDER LAW, RAGAMIX (AND THOSE THAT RAGAMIX WORKS WITH TO PROVIDE THE SERVICES) (A) DISCLAIM ALL IMPLIED WARRANTIES AND REPRESENTATIONS (E.G. WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, ACCURACY OF DATA, AND NONINFRINGEMENT); (B) DO NOT GUARANTEE THAT THE SERVICES WILL FUNCTION WITHOUT INTERRUPTION OR ERRORS, AND (C) PROVIDE THE SERVICE (INCLUDING CONTENT AND INFORMATION) ON AN “AS IS” AND “AS AVAILABLE” BASIS.
                            SOME LAWS DO NOT ALLOW CERTAIN DISCLAIMERS, SO SOME OR ALL OF THESE DISCLAIMERS MAY NOT APPLY TO YOU.
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">II.  Exclusion of Liability</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            These are the limits of legal liability we may have to you.
                            TO THE EXTENT PERMITTED UNDER LAW (AND UNLESS RAGAMIX HAS ENTERED INTO A SEPARATE WRITTEN AGREEMENT THAT SUPERSEDES THIS AGREEMENT), RAGAMIX (AND THOSE THAT RAGAMIX WORKS WITH TO PROVIDE THE SERVICES) SHALL NOT BE LIABLE TO YOU OR OTHERS FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, OR ANY LOSS OF DATA, OPPORTUNITIES, REPUTATION, PROFITS OR REVENUES, RELATED TO THE SERVICES (E.G. OFFENSIVE OR DEFAMATORY STATEMENTS, DOWN TIME OR LOSS, USE OR CHANGES TO YOUR INFORMATION OR CONTENT).
                            IN NO EVENT SHALL THE LIABILITY OF RAGAMIX IN THE AGGREGATE FOR ALL CLAIMS, AN AMOUNT THAT IS THE LESSER OF (A) THE MOST RECENT MONTHLY OR YEARLY FEE THAT YOU PAID FOR A PREMIUM SERVICE, IF ANY, OR (B) INR s1000.
                            THIS LIMITATION OF LIABILITY IS PART OF THE BASIS OF THE BARGAIN BETWEEN YOU AND RAGAMIX AND SHALL APPLY TO ALL CLAIMS OF LIABILITY (E.G. WARRANTY, TORT, NEGLIGENCE, CONTRACT, LAW) AND EVEN IF RAGAMIX HAS BEEN TOLD OF THE POSSIBILITY OF ANY SUCH DAMAGE, AND EVEN IF THESE REMEDIES FAIL THEIR ESSENTIAL PURPOSE.
                        </span>

                        <span class="classone" id="nsp">
                            Termination
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            Ragamix can end this Agreement anytime it deems necessary.
                            You can not terminate this Agreement at any time with notice to the other. In case any termination is allowed by Ragamix, on termination, you lose the right to access or use the Services. The following shall survive termination:<br>
                            <ul>
                                <li>Our rights to use and disclose your feedback;</li>
                                <li>Members' and/or Visitors' rights to further re-share content and information you shared through the Service to the extent copied or re-shared prior to termination;</li>
                                <li>Any amounts owed by either party prior to termination remain owed after termination.</li>
                            </ul>
                        </span>

                        <span class="classone" id="doi">
                            General Terms
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            Here are some important details about how to read the Agreement.
                            If a court with authority over this Agreement finds any part of it not enforceable, you and us agree that the court should modify the terms to make that part enforceable while still achieving its intent. If the court cannot do that, you and us agree to ask the court to remove that unenforceable part and still enforce the rest of this Agreement. To the extent allowed by law, the English version of this Agreement is binding and other translations are for convenience only. This Agreement (including additional terms that may be provided by us when you engage with a feature of the Services) is the only agreement between us regarding the Services and supersedes all prior agreements for the Services.
                            If we don't act to enforce a breach of this Agreement, that does not mean that RagaMix has waived its right to enforce this Agreement. You may not assign or transfer this Agreement (or your membership or use of Services) to anyone without our consent. However, you agree that RagaMix may assign this Agreement to its affiliates or a party that buys it without your consent. There are no third party beneficiaries to this Agreement.
                            We reserve the right to change the terms of this Agreement and will provide you notice if we do and we agree that changes cannot be retroactive. If you don't agree to these changes, you must stop using the Services.<br>
                            <ul><li>On clicking the I Accept button, you acknowledge that this Agreement has been entered at 4, Duff lane, Kolkata 700006, and the cause of action arises on this place itself. That further all matter, whether Civil (if arbitration is not applicable) or Criminal shall be entertained by the City Civil or Citi Sessions or Metropolitan Magistrates Court at Kolkata.</li></ul>
                        </span>

                        <span class="classone" id="ts">
                            RagaMix “DOs” and “DON’Ts.”
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">I. Dos. You agree that you will:</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            <ul>
                                <li>Comply with all applicable laws, including, without limitation, privacy laws, intellectual property laws, anti-spam laws, export control laws, tax laws, and regulatory requirements;</li>
                                <li>Provide accurate information to us and keep it updated;</li>
                                <li>Use your real name and other personal details on your profile;</li>
                                <li>Use the Services in a professional manner.</li>
                            </ul>
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">I. Dos. You agree that you will:</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            <ul>
                                <li>Act dishonestly or unprofessionally, including by posting inappropriate, inaccurate, or objectionable content;</li>
                                <li>Add content that is not intended for, or inaccurate for, a designated field (e.g. submitting a telephone number in the “title” or any other field, or including telephone numbers, email addresses, street addresses or any personally identifiable information for which there is not a field provided by RagaMix);</li>
                                <li>Use an image that is not your likeness or a head-shot photo for your profile;</li>
                                <li>Create a false identity on RagaMix;</li>
                                <li>Misrepresent your current or previous positions and qualifications;</li>
                                <li>Misrepresent your affiliations with a person or entity, past or present;</li>
                                <li>Misrepresent your identity, including but not limited to the use of a pseudonym;</li>
                                <li>Create a Member profile for anyone other than yourself (a real person);</li>
                                <li>Invite people you do not know to join your network;</li>
                                <li>Use or attempt to use another's account;</li>
                                <li>Harass, abuse or harm another person;</li>
                                <li>Send spam or other unwelcomed communications to others;</li>
                                <li>Send spam or other unwelcomed communications to others;</li>
                                <li>Act in an unlawful, libelous, abusive, obscene, discriminatory or otherwise objectionable manner;</li>
                                <li>Disclose information that you do not have the right to disclose (such as confidential information of others (including your employer));</li>
                                <li>Violate intellectual property rights of others, including patents, trademarks, trade secrets, copyrights or other proprietary rights;</li>
                                <li>Violate the intellectual property or other rights of RagaMix, including, without limitation, using the word “RagaMix” or our logos in any business name, email, or URL except as provided in the guidlines;</li>
                                <li>Use RagaMix invitations to send messages to people who don't know you or who are unlikely to recognize you as a known contact;</li>
                                <li>Post any unsolicited or unauthorized advertising, “junk mail,” “spam,” “chain letters,” “pyramid schemes,” or any other form of solicitation unauthorized by RagaMix;</li>
                                <li>Send messages to distribution lists, newsgroup aliases, or group aliases;</li>
                                <li>Post anything that contains software viruses, worms, or any other harmful code;</li>
                                <li>Manipulate identifiers in order to disguise the origin of any message or post transmitted through the Services;</li>
                                <li>Create profiles or provide content that promotes escort services or prostitution.</li>
                                <li>Creating or operate a pyramid scheme, fraud or other similar practice;</li>
                                <li>Copy or use the information, content or data of others available on the Services (except as expressly authorized);</li>
                                <li>Copy or use the information, content or data on RagaMix in connection with a competitive service (as determined by RagaMix);</li>
                                <li>Copy, modify or create derivative works of RagaMix, the Services or any related technology (except as expressly authorized by RagaMix);</li>
                                <li>Reverse engineer, decompile, disassemble, decipher or otherwise attempt to derive the source code for the Services or any related technology, or any part thereof;</li>
                                <li>Imply or state that you are affiliated with or endorsed by RagaMix without our express consent (e.g., representing yourself as an accredited RagaMix  representative);</li>
                                <li>Rent, lease, loan, trade, sell/re-sell access to the Services or related any information or data;</li>
                                <li>Sell, sponsor, or otherwise monetize a RagaMix website, app or any other feature of the Services, without RagaMix's consent;</li>
                                <li>Remove any copyright, trademark or other proprietary rights notices contained in or on our Service;</li>
                                <li>Remove, cover or obscure any advertisement included on the Services;</li>
                                <li>Collect, use, copy, or transfer any information obtained from RagaMix without the consent of RagaMix;</li>
                                <li>Share or disclose information of others without their express consent;</li>
                                <li>Use manual or automated software, devices, scripts robots, other means or processes to access, “scrape,” “crawl” or “spider” the Services or any related data or information;</li>
                                <li>Use bots or other automated methods to access the Services, add or download contacts, send or redirect messages;</li>
                                <li>Monitor the Services' availability, performance or functionality for any competitive purpose;</li>
                                <li>Engage in “framing,” “mirroring,” or otherwise simulating the appearance or function of the Services;</li>
                                <li>Access the Services except through the interfaces expressly provided by RagaMix, such as its mobile applications, RagaMix.com;</li>
                                <li>Override any security feature of the Services;</li>
                                <li>Interfere with the operation of, or place an unreasonable load on, the Services (e.g., spam, denial of service attack, viruses, gaming algorithms); </li>
                            </ul>
                        </span>

                        <span class="classone" id="dw">
                            Complaints Regarding Content
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">I. Amicable Resolution</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            In the event of any disputes, differences, controversies and questions directly or indirectly arising at any time hereafter between the Parties or their respective representatives or assigns under, out of, in connection with or in relation to this Agreement (or the subject matter of this Agreement) including, without limitation, all disputes, differences, controversies and questions relating to the validity, interpretation, construction, performance and enforcement of any provision of this Agreement, dispute, difference or contention arising between the Parties in relation to any of the provisions of this Agreement or the interpretation hereof, or as to rights, liabilities or duties of the Parties (hereinafter referred to as a “Dispute”), the same shall, in the first instance, be attempted to be resolved amicably by the Parties and failing resolution of the same shall be referred to arbitration in accordance with the terms and conditions herein.
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">II. Arbitration</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            If the dispute is not resolved through such amicable discussion within 5 (five) Business Days after commencement of discussions or such longer period as the Parties agree to in writing, then either Partner may refer the dispute Mr Indrajeet Dey, (e-mail Indrajeetdey2011@gmail.com) any such person as he may appoint/recomend for resolution by arbitration according to the Arbitration and Conciliation Act, 1996. Arbitration shall be conducted in kolkata.<br>
                            <b>i. Language and Venue </b><br>
                            The arbitration proceedings shall be conducted in the English language. The venue of the arbitration shall be in Kolkata.<br>

                            <b>ii. Procedure</b><br>

                            <b>a.</b> The arbitrators shall decide the reference and any application for interim order made pursuant thereto, on the basis of the written and oral statements of the Parties and the documents produced by them by way of affidavits, alone. Due efforts must be taken by the parties and the Arbitrators  so that the Pleadings shall be completed within 1 (one) month of the arbitrator entering on the reference.<br>

                            <b>b.</b> The arbitrators shall proceed with adjudication of the reference and/or any application for interim order made pursuant thereto, notwithstanding any failure to file a written statement or document(s) within time and shall proceed with the reference in the absence of any or all the Parties who after due notice or neglect or refuse to attend at the appointed time and place.<br>

                            <b>c.</b> The arbitrators shall make due efforts to conclude all proceedings and award  the arbitral award within 6 (Six) months from the date of entering upon the reference. For the purpose of this Clause, the arbitrator shall be deemed to have entered upon a reference on the date on which the arbitrator holds the first meeting. <br>

                            <b>d.</b> Adjournment, if any, shall be granted by the arbitrator only in exceptional cases, for bona fide reasons to be recorded in writing. In the event of an adjournment being granted, the arbitrator shall be entitled to direct that Party(s) seeking an adjournment to pay to the other Party(s) such amount as costs, as it deems fit and proper.<br>

                            <b>e.</b> After an award or an order is made, a signed copy of thereof shall be Delivered to each Party within 7 (Seven) Business Days of the date thereon.<br>

                            <b>f.</b> The costs of arbitration shall be fixed by the arbitrator and the arbitrator in the final award shall specify (a) Party entitled to costs; (b) the Party who shall pay costs; (c) the amount of costs; and (d) the manner in which costs shall be paid. For the purpose of this Clause, ‘costs of arbitration’ shall mean the fees and expenses of the arbitrator, legal fees and expenses any administrative fees and any other expense incurred in connection with arbitral proceedings and arbitral award.<br>

                            <b>g.</b> The Arbitrator shall have inherent powers to examine evidence, call upon the Parties, to condone any technical or human errors, to advice the Parties before issuing an award and to take required action as he deems fit in accordance with law. <br>

                            <b>iii. Fees of Arbitrator</b><br>

                            The arbitrator shall fix his / her lump sum (one time) fees payable by each Party in equal share in the first meeting. The said fees shall be paid in advance by each Party. In case, a Party fails, neglects or refuses to pay its part of the arbitrator fees, the other Party shall be responsible for making such payment in advance to the arbitrator and the other Party shall be entitled to recover the same from the defaulting Party as costs in the arbitration. It is clarified that the said lump sum fees shall be exclusive of any expenses or charges towards administration or conduct of arbitration proceedings. The fee of each arbitrator shall not be more than 50 (fifty) Gould Mohur’s on a lump sum basis.
                        </span>
                        <span class="classone" style="font-size: 19px;color: #7ba6c3;margin-bottom: 10px;">III.Governing law and jurisdiction</span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            This Agreement shall be governed by and construed and enforced in accordance with the laws of India, without regard to its principles of conflict of laws, the Parties agree to submit to the exclusive jurisdiction of the courts in Kolkata, India, alone.
                        </span>

                        <span class="classone" id="asg">
                            Waiver
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            Any term and condition of this Agreement may be waived in writing at any time by the Party that is entitled to the benefit thereof. No failure or delay in exercising or omission to exercise any right, power or remedy accruing to any of the Parties under this Agreement or any other agreement or document shall impair any such right, power or remedy or be construed to be a waiver thereof or any acquiescence in such default, nor shall the action or inaction of the Party in respect of any default or any acquiescence by it in any default affect or impair any right, power or remedy of the Party in respect of any other default. A waiver on one occasion will not be deemed to be a waiver of the same or either under breach or non-fulfillment on a future occasion. All remedies and benefits, either under this Agreement, or by law or otherwise afforded, will be cumulative and not alternative and without prejudice to the other remedy or benefit, as the case may be.
                        </span>

                        <span class="classone" id="vt">
                            Amendment
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            This Agreement shall not be altered, modified or amended except in writing duly signed by all the Parties to this Agreement. Whereby such an amendment would be separately included and/ or attached hereto.
                        </span>

                        <span class="classone" id="cu">
                            Variation
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            No variation of this Agreement shall be effective unless reduced to writing and signed by or on behalf of a duly authorised representative of each of the Parties to this Agreement.
                        </span>

                        <span class="classone" id="resy">
                            Residuary
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            Matters that are not covered in this Agreement herein shall be shall be decided unanimously by all the partners in writing.
                        </span>

                        <span class="classone" id="tcr">
                            Terms and Conditions reasonable-
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            The Parties after exercising due diligence and their sense of reasonableness agree that, having regard to all the circumstances, the covenants contained herein are reasonable and necessary for the protection of the Parties. If any such covenant is held to be void as going beyond what is reasonable in all the circumstances, but would be valid if amended as to scope or duration or both, the covenant will apply with such minimum modifications regarding its scope and duration as may be necessary to make it valid and effective.
                        </span>

                        <span class="classone" id="cpt">
                            Counterparts
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            This Agreement may be executed simultaneously in any number of counterparts, including counterparts transmitted by facsimile, each of which shall be deemed an original, but all of which will constitute one and the same instrument, and any Party may execute this Agreement by signing any one or more of such originals or counterparts.
                        </span>

                        <span class="classone" id="cptn">
                            Competition Policy
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            Nothing in this Agreement shall allow or promote any anti competitive agreements, bid rigging tactics, Cartelization or otherwise. Any such arrangement or agreement shall be deemed to be null and void.  In course of time the Firm may make efforts to follow a policy Competition Compliance Policy.
                        </span>

                        <span class="classone" id="fme">
                            FORCE MAJEURE:
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            Force Majeure would mean and include any circumstances beyond the control of the Parties which substantially affect the performance of this Agreement, such as but not limited to:<br>

                            <b>i.</b> natural phenomena, including but not limited to floods, droughts, earthquakes, storm and lightening substantially affecting operation and maintenance of Equipment;<br>

                            <b>ii.</b> acts of any Governmental Authority, including but not limited to war, declared or undeclared priorities, quarantines, embargoes, nationalisation and confiscation;<br>

                            <b>iii.</b> accidents such as fire and explosions;<br>

                            <b>iv.</b> strikes or industrial disputes (which are not related to the breach of an agreement with the employees of either Party) and sabotage;<br>

                            <b>v.</b> riots, civil commotion, insurrection, act of terrorism, belligerence, hostilities and revolution;<br>

                            Povided that either Party shall within [15 (Fifteen) Calendar Days] from the date of occurrence of such a cause notify the other in writing of such causes.<br>

                            <b>i.</b> The Parties shall not be liable under this Agreement for delays in performing its obligations resulting from any Force Majeure event. The term of this agreement shall be extended by a reasonable time by mutual consent of the Parties.<br>

                            <b>ii.</b> The Parties shall consult with each other and take all reasonable steps to minimize the losses of either Party resulting from Force Majeure.
                        </span>

                        <span class="classone" id="cu">
                            CONFIDENTIALITY &amp; NON-DISCLOSURE
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            <b>I.</b> The terms and conditions of this Agreement including their existence, shall be considered confidential information and shall not be disclosed by any Party to any third party except in accordance with the provisions set forth below.<br>

                            <b>II.</b> No announcement regarding the present transaction shall be made in a press release, conference, advertisement, announcement, professional or trade publication, mass marketing materials or otherwise to the general public by either Party without the prior written consent of the other Party.<br>

                            <b>III.</b> Notwithstanding the foregoing any Party may disclose any of the terms of this Agreement to its current or bonafide prospective investors, employees, investment bankers, Lenders, accountants and attorneys, on a need to know basis, in each case only where such persons or entities are under appropriate nondisclosure obligations.<br>

                            <b>IV.</b> In the event that any Party is requested or becomes legally compelled or is required, pursuant to an order of a court or Governmental Authority as so required by such order, or by Applicable Law or regulatory requirements or regulatory body to whose jurisdiction the disclosing Party is subject, (including without limitation, pursuant to securities Laws and regulations) to disclose the existence of this Agreement in contravention of the provisions of this Clause, such Party (the “Disclosing Party”) shall provide the other Parties (the “Non-Disclosing Party”) with prompt written notice of that fact so that the Non Disclosing Party may seek (with the cooperation and reasonable efforts of the other Parties) a protective order, confidential treatment or other appropriate remedy. In such event, Disclosing Party shall furnish only that portion of the information, which is legally required and shall exercise reasonable efforts to obtain reliable assurance that confidential treatment will be accorded to such information to the extent reasonable requested by any Non-Disclosing Party.<br>

                            <b>V.</b> The Parties agree that the provisions of this Clause shall not restrict the Parties from sharing any information with their respective Affiliates.<br>

                            <b>VI.</b> The provisions of this Clause shall be in addition to, and not in substitution for, the provisions of any separate non-disclosure agreement executed by any of the Parties hereto with respect to the transactions contemplated hereby.<br>

                            <b>VII.</b> All notices required under this Clause shall be made pursuant to the Agreement herein.<br>
                        </span>

                        <span class="classone" id="pp">
                            Privacy Policy
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            Your Privacy Policy is solely an internal affair, it may be subjected to any change with out any intimation to the other part. However, may take non mandatory measures not to disclose your phone numbers, email Id etc to any user of RagaMix without our consent. The contents that you upload shall remain with RagaMix, and RagaMix reserves the power to either remove such content at any point of time, or to keep on such a content despite an requests. However, this shall not debar you from sorting reliefs from the arbitrator.
                        </span>

                        <span class="classone" id="pucp">
                            Prevention of usage, Copy, Plagiarism etc
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            No contents of RagaMix can be used, copied, plagarised by any person, without express consent of RagaMix shall be used in any manner whatsoever. Any violation of this clause may result in prosecution or any legal action as deemed fit.
                        </span>

                        <span class="classone" id="hcu">
                            How To Contact Us
                        </span>
                        <span class="co" style="font-size: 11px !important;line-height: 20px !important;">
                            If you want to send us notices or service of process, please contact us:<br>
                            ONLINE at: <br>
                            OR BY MAIL at:<br>
                        </span>
            </div>
        </div>
    </div>
</div>
