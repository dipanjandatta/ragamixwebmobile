<header id="myCarousel" class="carousel slide" style="height: 300px !important;">
                <div class="carousel-inner">
            <div class="item active slideItem">
                <div class="fill" style="background-image:url('<?php echo base_url(); ?>images/ACFhelps.jpg');"></div>
            </div>
                </div>
    </header>
<div class="col-md-12 faqclass">
    <div class="col-md-8 col-md-offset-2">
        <span class="fontfaq">
            Frequently Asked Questions
        </span>
    </div>
</div>
<div class="container">
    <div class="row" style="margin-top: 50px;">
        <div class="col-md-12 noPad">
            <div class="col-md-8 col-md-offset-2">
                To use the features of this system, you need to interact and give inputs in order to get the maximum benefit.<br />
                We are here to guide you along and ensure your requirements are fulfilled.<br />
                Here are some of the <strong>Frequently Asked Questions</strong>
            </div>
            <div class="col-md-8 col-md-offset-2 margTop">
                <div class="col-md-4 backWhite" style="height: 150px;">
                    <span class="hd">
                        <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">How Do I create my Own Profile ?</a>
                    </span>
                </div>
                <div class="col-md-4 backWhite" style="height: 150px;">
                    <span class="hd">
                        <a href = "javascript:void(0)" onclick = "document.getElementById('light1').style.display='block';document.getElementById('fade1').style.display='block'">Unable to Signup/Activation Mail Not Received - What to Do ?</a>
                    </span>
                </div>
                <div class="col-md-4 backWhite" style="height: 150px;">
                    <span class="hd">
                        <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade2').style.display='block'">How do I contact Other Musicians and Members ?</a>
                    </span>
                </div>
                <div class="col-md-4 backWhite" style="height: 150px;">
                    <span class="hd">
                        <a href = "javascript:void(0)" onclick = "document.getElementById('light3').style.display='block';document.getElementById('fade3').style.display='block'">Can I be reached by Other Musicians as a free member ?</a>
                    </span>
                </div>
                <div class="col-md-4 backWhite" style="height: 150px;">
                    <span class="hd">
                        <a href = "javascript:void(0)" onclick = "document.getElementById('light4').style.display='block';document.getElementById('fade4').style.display='block'">How Can I Add a Photo/Image ?</a>
                    </span>
                </div>
                <div class="col-md-4 backWhite" style="height: 150px;">
                    <span class="hd">
                        <a href = "javascript:void(0)" onclick = "document.getElementById('light5').style.display='block';document.getElementById('fade5').style.display='block'">How Do I Upload Audio/Video ?</a>
                    </span>
                </div>
                <div class="col-md-4 backWhite" style="height: 150px;">
                    <span class="hd">
                        <a href = "javascript:void(0)" onclick = "document.getElementById('light6').style.display='block';document.getElementById('fade6').style.display='block'">What is my Membership Status ?</a>
                    </span>
                </div>
                <div class="col-md-4 backWhite" style="height: 150px;">
                    <span class="hd">
                        <a href = "javascript:void(0)" onclick = "document.getElementById('light7').style.display='block';document.getElementById('fade7').style.display='block'">How to Maximize Responses from the messages I send or Ads Posted by me ?</a>
                    </span>
                </div>
                <div class="col-md-4 backWhite" style="height: 150px;">
                    <span class="hd">
                        <a href = "javascript:void(0)" onclick = "document.getElementById('light8').style.display='block';document.getElementById('fade8').style.display='block'">How to remove my listing ?</a>
                    </span>
                </div>
                <div id="light" class="white_contentfaq"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a><br />
                    <span class="faqdets">
                        You need to Sign up. Click on SIGNUP (homepage or LOGIN page).<br />
                        Fill in your basic information and the system will send you a confirmation/activation e-mail which you need to open and click on the activation link to create your account.
                        Once you’re signed up – you can login and update and upload your profile details through the – MYACCOUNT link or the dropdown links on the RHS
                    </span>
                </div>
		<div id="fade" class="black_overlayfaq"></div>
                <div id="light1" class="white_contentfaq"><a href = "javascript:void(0)" onclick = "document.getElementById('light1').style.display='none';document.getElementById('fade1').style.display='none'">Close</a><br />
                    <span class="faqdets">
                        Please contact Support and they will manually activate your account.
                    </span>
                </div>
		<div id="fade1" class="black_overlayfaq"></div>
                <div id="light2" class="white_contentfaq"><a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade2').style.display='none'">Close</a><br />
                    <span class="faqdets">
                        You can Browse Profiles and Classified Ads - do a filtered search to find other members or ads<br />
                        Once you have identified another member by his / her details, you can contact the member from the profile or ad page – by clicking on the MAIL / SMS to send your message.<br />
                        Our system will send an alert / notification to the member with your details.<br />
                        All members can read their messages upon LOGIN
                    </span>
                </div>
		<div id="fade2" class="black_overlayfaq"></div>
                <div id="light3" class="white_contentfaq"><a href = "javascript:void(0)" onclick = "document.getElementById('light3').style.display='none';document.getElementById('fade3').style.display='none'">Close</a><br />
                    <span class="faqdets">
                        Yes. You can read mails / messages sent by other members by logging in to MYACCOUNT or by following the link Check Messages from the right hand side dropdown menu.
                    </span>
                </div>
		<div id="fade3" class="black_overlayfaq"></div>
                <div id="light4" class="white_contentfaq"><a href = "javascript:void(0)" onclick = "document.getElementById('light4').style.display='none';document.getElementById('fade4').style.display='none'">Close</a><br />
                    <span class="faqdets">
                        In your MYACCOUNT page, click on the PHOTOS tab at the top. Select a JPEG file from your hard drive and UPLOAD, set your main profile picture then SAVE & UPDATE.
                    </span>
                </div>
		<div id="fade4" class="black_overlayfaq"></div>
                <div id="light5" class="white_contentfaq"><a href = "javascript:void(0)" onclick = "document.getElementById('light5').style.display='none';document.getElementById('fade5').style.display='none'">Close</a><br />
                    <span class="faqdets">
                        AUDIO<br />
                        In your MYACCOUNT page, you can click on the MUSIC tab (or MANAGE AUDIO TRACKS from the dropdown on the RHS). You can then select the file from your local hard drive (or your cell phone folder) and UPLOAD and then UPDATE.<br />
                        The online Audio streaming player on your page only supports MP3 /MP4 files. To upload audio tracks from CD you need to convert these to MP3 (use a CD ripper software)<br />
                        VIDEO<br />
                        In your MYACCOUNT page, you can click on the VIDEOS tab (or MANAGE VIDEOS from the dropdown on the RHS).<br />
                        Enter Title and the Youtube URL and SAVE
                    </span>
                </div>
		<div id="fade5" class="black_overlayfaq"></div>
                <div id="light6" class="white_contentfaq"><a href = "javascript:void(0)" onclick = "document.getElementById('light6').style.display='none';document.getElementById('fade6').style.display='none'">Close</a><br />
                    <span class="faqdets">
                        You become a FREE Member of RagaMix once you signup.<br />
                        As a Free member you get to use all the facilities and services of RagaMix<br />
                        Contact Support if you are interested in special privileges and listing support
                    </span>
                </div>
		<div id="fade6" class="black_overlayfaq"></div>
                <div id="light7" class="white_contentfaq"><a href = "javascript:void(0)" onclick = "document.getElementById('light7').style.display='none';document.getElementById('fade7').style.display='none'">Close</a><br />
                    <span class="faqdets">
                        In order to maximize responses -<br />
                        <ul>
                            <li>
                                You need to provide details in your profile page in order to generate interest of other musicians and members.
                            </li>
                            <li>
                                Ensure proper settings for SMS / e-mail (SMS/ EMAIL Setting in dropdown menu on the RHS)
                            </li>
                            <li>
                                Send your contact (e-mail address, phone#, etc) along with the message.
                            </li>
                        </ul>
                    </span>
                </div>
		<div id="fade7" class="black_overlayfaq"></div>
                <div id="light8" class="white_contentfaq"><a href = "javascript:void(0)" onclick = "document.getElementById('light8').style.display='none';document.getElementById('fade8').style.display='none'">Close</a><br />
                    <span class="faqdets">
                        You can remove an ad listing or a submitted event fromManage Ads and Manage Events from the dropdown menu on the RHS<br />
                        For removal of profile contact Support and they will manually delete your account.
                    </span>
                </div>
		<div id="fade8" class="black_overlayfaq"></div>
            </div>
        </div>
    </div>
</div>