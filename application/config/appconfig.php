<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//adding config items.
$config['app_url'] = 'http://ragamix.in:30000/';
$config['app_master_url'] = 'http://localhost:31000/master/';
$config['template'] = array(
                                'musician' => 'musician',
                                'band' => 'band'
                        );
$config['layoutconfigdev'] = 'layouts/main_dist';
$config['cdnimageurl'] = 'http://cdn.ragamix.com/ragamix/all-prod/';
$config['cdnaudiourl'] = 'http://cdn.ragamix.com/ragamix/Test-Audio-File/';
$config['cdneventsurl'] = 'http://cdn.ragamix.com/ragamix/all-prod/';
$config['cdnadsurl'] = 'http://cdn.ragamix.com/ragamix/all-prod/';
$config['app_type'] = 1;
?>
