<?php 
/**
 * @author Subhajit
 * @copyright 2016
 */
class Profilemodel extends CI_Model {
 
    function Model() {
        // Call the Model constructor
        parent::__construct();
    }
    
    
    function getBasicProfileData($userId){
        $query = $this->db->query("CALL get_basic_profile_data($userId);");
        $result = $query->result();
        return $result;
    }
    
    function postBasicProfileData($data){
        $query = $this->db->query("CALL save_update_basic_profile('".$data['rm_id']."','".$data['mobile_no_alt']."','".$data['full_name']."','".$data['age']."','".$data['gender']."','".$data['user_category']."','".$data['user_category_1']."','".$data['category_other']."','".$data['email']."','".$data['mobile_no']."','".$data['city_name']."','".$data['country_name']."','".$data['facebook_url']."','".$data['facebook_like_cnt']."','".$data['gmail_id']."','".$data['twitter_id']."','".$data['website_addr']."',".$data['loc_lat'].",".$data['loc_lang'].",'".$data['address']."','".$data['locality']."','".$data['pincode']."','".$data['group_id']."','".$data['profile_pic_url']."','".$data['background_pic_url']."','".$data['page_position']."');");
        return true;
    }
    
    function getUsertypeLists($idval){
        $return[''] = 'Select a Primary Category';
        $query = $this->db->query("CALL get_usertype_list($idval);");
        foreach($query->result_array() as $row){
            $return[$row['usr_type']] = $row['usr_type'];
        }
        return $return;
    }
    
    function getUsertypeListsTwo($idval){
        $return[''] = 'Select an Additional Category';
        $query = $this->db->query("CALL get_usertype_list($idval);");
        foreach($query->result_array() as $row){
            $return[$row['usr_type']] = $row['usr_type'];
        }
        return $return;
    }
    
    function getCityNameOnCountry($countryVal){
        $query = $this->db->query("CALL get_city_list_for_country('$countryVal');");
        $result = $query->result();
        return $result;
    }
    
    function getCity($countryVal){
        $return[''] = 'Select City';
        $query = $this->db->query("CALL get_city_list_for_country('$countryVal');");
        foreach($query->result_array() as $row){
            $return[$row['city']] = $row['city'];
        }
        return $return;
    }
    
    function getCountry(){
        $return[''] = 'Select Country';
        $query = $this->db->query("CALL get_country_list();");
        foreach($query->result_array() as $row){
            $return[$row['country']] = $row['country'];
        }
        return $return;
    }
    
    function getProfilePhotos($userId){
        $query = $this->db->query("CALL get_all_photos_for_profile('$userId');");
        $result = $query->result();
        return $result; 
    }
    
    function insertPhotos($data){
        $p = $data['photo_id_in'];
        $p1 = $data['rm_id'];
        $p2 = $data['main_pic'];
        $p3 = $data['background_pic'];
        $p4 = $data['allow_view'];
        $p5 = $data['pic_url'];
        $p6 = $data['pic_privacy'];
        $p7 = $data['pic_height'];
        $p8 = $data['pic_width'];
        $this->db->query("CALL save_update_profile_photos('$p','$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8');");
        return true;
    }
    
    function deleteSelectedPhotos($data){
        $p = $data['all_photos'];
        $p1 = $data['rm_id_in'];
        $p2 = $data['photo_id_in'];
        $this->db->query("CALL delete_profile_photo('$p','$p1','$p2');");
        return true;
    }
    
    
    function getAudios($userId){
        $query = $this->db->query("CALL get_all_profile_audios('$userId');");
        $result = $query->result();
        return $result; 
    }
    
    function getVideos($userId){
        $query = $this->db->query("CALL get_all_profile_videos('$userId');");
        $result = $query->result();
        return $result; 
    }
    
    
    function insertAudio($data){
        $p = $data['serial_no_in'];
        $p1 = $data['rm_id_in'];
        $p2 = $data['audio_url'];
        $p3 = $data['audio_title'];
        $p4 = $data['size'];
        $p5 = $data['duration'];
        $p6 = $data['allow_download'];
        $p7 = $data['audio_type'];
        $this->db->query("SET @user_name_in := '$p';");
        $this->db->query("CALL save_update_profile_audios(@user_name_in,'$p1','$p2','$p3','$p4','$p5','$p6','$p7');");
        return true;
    }
    
    function insertVideo($data){
        $p = $data['serial_no_in'];
        $p1 = $data['rm_id_in'];
        $p2 = $data['youtube_url'];
        $p3 = $data['youtube_title'];
        $p4 = $data['video_desc'];
        $p5 = $data['pic_height'];
        $p6 = $data['pic_width'];
        $p7 = $data['view_count'];
        $p8 = $data['like_count'];
        $p9 = $data['dislike_count'];
        $this->db->query("CALL save_update_profile_videos('$p','$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9');");
        return true;
    }
    
    function deleteSelectedAudio($data){
        $p = $data['all_audios'];
        $p1 = $data['rm_id_in'];
        $p2 = $data['serial_no_in'];
        $this->db->query("CALL delete_profile_audios('$p','$p1','$p2');");
        return true;
    }
    
    function deleteSelectedVideo($data){
        $p = $data['all_videos'];
        $p1 = $data['rm_id_in'];
        $p2 = $data['serial_no_in'];
        $this->db->query("CALL delete_profile_videos('$p','$p1','$p2');");
        return true;
    }
    
    function getSeekingRequirements($userId){
        $query = $this->db->query("CALL get_all_seeking_info('$userId');");
        $result = $query->result();
        return $result;
    }
    
    function getGenreList(){
        $return[''] = 'Select';
        $query = $this->db->query("CALL get_genre_list();");
        foreach($query->result_array() as $row){
            $return[$row['genre']] = $row['genre'];
        }
        return $return;
    }
    
    
    function getMusicData($userId){
        $query = $this->db->query("CALL get_music_profile_data('$userId');");
        $result = $query->result();
        return $result;
    }
    
    function getVocalsList(){
        $return[''] = 'Select';
        $query = $this->db->query("CALL get_vocal_list();");
        foreach($query->result_array() as $row){
            $return[$row['vocal']] = $row['vocal'];
        }
        return $return;
    }
    
    function getStringInstruments(){
        $query = $this->db->query("CALL get_stringinstr_list();");
        foreach($query->result_array() as $row){
            $return[$row['stringinstr']] = $row['stringinstr'];
        }
        return $return;
    }
    
    function getReedInstruments(){
        $query = $this->db->query("CALL get_reedinstr_list();");
        foreach($query->result_array() as $row){
            $return[$row['reedinstr']] = $row['reedinstr'];
        }
        return $return;
    }
    
    function getPercussions(){
        $query = $this->db->query("CALL get_percussions_list();");
        foreach($query->result_array() as $row){
            $return[$row['percussions']] = $row['percussions'];
        }
        return $return;
    }
    
    function getWindInstruments(){
        $query = $this->db->query("CALL get_windinstr_list();");
        foreach($query->result_array() as $row){
            $return[$row['windinstr']] = $row['windinstr'];
        }
        return $return;
    }
    
    function postMusicProfileData($data){
        //Preparing the data for SP Call
        $p = $data['rm_id_in'];
        $p1 = $data['user_desc'];
        $p2 = $data['experience_in_years'];
        $p3 = $data['studio_band_member'];
        $p4 = $data['commitment_lvl'];
        $p5 = $data['freelance_professional'];
        $p6 = $data['facebook_url'];
        $p7 = $data['equipment_owned'];
        $p8 = $data['genre'];
        $p9 = $data['genre_other'];
        $p10 = $data['music_influence_desc'];
        $p11 = $data['stringinstr'];
        $p12 = $data['stringinstr_other'];
        $p13 = $data['reedinstr'];
        $p14 = $data['reedinstr_other'];
        $p15 = $data['percussions'];
        $p16 = $data['percussions_other'];
        $p17 = $data['windinstr'];
        $p18 = $data['windinstr_other'];
        $p19 = $data['vocal'];
        $p20 = $data['vocal_other'];
        $p21 = $data['band_type'];
        $p22 = $data['band_desc'];
        $p23 = $data['educator_type'];
        $p24 = $data['educator_desc'];
        $p25 = $data['educator_certification'];
        $this->db->query("CALL save_update_music_profile('$p','$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10','$p11','$p12','$p13','$p14','$p15','$p16','$p17','$p18','$p19','$p20','$p21','$p22','$p23','$p24','$p25');");
        return true;
    }
    
    function postSeekRequirements($data){
        $p = $data['serial_no_in'];
        $p1 = $data['rm_id_in'];
        $p2 = $data['keyword'];
        $p3 = $data['location'];
        $p4 = $data['description'];
        $p5 = $data['hashtags'];
        
        $this->db->query("SET @serial_no_in := '$p';");
        $this->db->query("CALL save_update_seeking_info(@serial_no_in,'$p1','$p2','$p3','$p4','$p5');");
        $out_param_query = $this->db->query('select @serial_no_in as out_param;');
        $result = $out_param_query->result();
        return $result[0]->out_param;
    }
    
    function getPreviewData($userId){
        $query = $this->db->query("CALL get_profile_preview_info('$userId')");
        $result = $query->result();
        return $result;
    }
    
    function getShowsEventsData($userId,$show_ev_id){
        $query = $this->db->query("CALL get_all_events_info('$userId','$show_ev_id');");
        $result = $query->result();
        return $result;
    }
    
    function insertEvents($data){
        $p = $data['event_ad_id_in'];
        $p1 = $data['rm_id'];
        $p2 = $data['category'];
        $p3 = $data['event_location'];
        $p4 = $data['event_city'];
        $p5 = $data['event_date'];
        $p6 = $data['event_title'];
        $p7 = $data['event_desc'];
        $p8 = $data['doc_youtube_url'];
        $p9 = $data['event_approved'];
        $p10 = $data['page_position'];
        $p11 = $data['photo_id_in'];
        $p12 = $data['pic_url'];
        $p13 = $data['pic_privacy'];
        $p14 = $data['pic_height'];
        $p15 = $data['pic_width'];
        
        $this->db->query("SET @event_ad_id_in := '$p';");
        $this->db->query("SET @photo_id_in := '$p11';");
        
        $this->db->query("CALL save_update_events_info(@event_ad_id_in,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10',@photo_id_in,'$p12','$p13','$p14','$p15');");
        $out_param_query = $this->db->query('select @event_ad_id_in as out_param;');
        $result = $out_param_query->result();
        return $result[0]->out_param;
    }
    
    function getEventsForUpdates($userid,$evid){
        $query = $this->db->query("select a.*, b.pic_url from events a left join photos b on b.photo_id = a.photo_id where a.rm_id = ? and a.event_ad_id = ?", array('rm_id'=>$userid,'event_ad_id'=>$evid));
        $result = $query->result();
        return $result;
    }
    
    function getCityAsArray($idval){
        $query = $this->db->query("CALL get_city_list_for_country('$idval');");
        foreach($query->result_array() as $row){
            $return[$row['city']] = $row['city'];
        }
        return $return;
    }
    
    function getAdsData($userId,$ad_id,$cat,$off,$lim){
        $this->db->query("SET @rm_id_in := '$userId';");
        $this->db->query("SET @ad_id_in := '$ad_id';");
        $this->db->query("SET @category_in := '$cat';");
        $this->db->query("SET @offset_val := '$off';");
        $this->db->query("SET @limit_in := '$lim';");
        $query = $this->db->query("CALL get_all_ads_info(@rm_id_in, @ad_id_in, @category_in, @offset_val, @limit_in, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return $result;
    }
    
    function insertAds($data){
        $p = $data['ad_id_in'];
        $p1 = $data['rm_id'];
        $p2 = $data['category'];
        $p3 = $data['ads_title'];
        $p4 = $data['ads_desc'];
        $p5 = $data['ads_views'];
        $p6 = $data['ads_approved'];
        $p7 = $data['enable_ads'];
        $p8 = $data['ads_status'];
        $p10 = $data['page_position'];
        $p11 = $data['photo_id_int'];
        $p12 = $data['pic_url'];
        $p13 = $data['pic_privacy'];
        $p14 = $data['pic_height'];
        $p15 = $data['pic_width'];
        //print_r("CALL save_update_events_info('$p','$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10','$p11','$p12','$p13','$p14','$p15');"); exit();
        $this->db->query("SET @ad_id_in := '$p';");
        $this->db->query("SET @photo_id_int := '$p11';");
        //print_r("CALL save_update_ads_info(@ad_id_in,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9',@photo_id_int,'$p12','$p13','$p14','$p15');"); exit();
        $this->db->query("CALL save_update_ads_info(@ad_id_in,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9',@photo_id_int,'$p12','$p13','$p14','$p15');");
        $out_param_query = $this->db->query('select @ad_id_in as out_param;');
        $result = $out_param_query->result();
        return $result[0]->out_param;
    }
    
    
    function delEvents($evid,$photoid){
        $this->db->query("CALL delete_events_info('$evid','$photoid');");
        return true;
    }
    
    function getMessagesLimitTen($userLoggedIn){
        $query = $this->db->query("select a.*, b.email_tran_id, b.body_text from users a, mailbox b where b.from_rm_id = a.rm_id and b.to_rm_id = ? and b.has_read = ?  order by b.email_tran_id desc limit 5", array('b.to_rm_id' => $userLoggedIn,'b.has_read'=>0));
        $result = $query->result();
        return $result;
        
    }
    
    function fetchAllUsers($q){
        $query = $this->db->query("select * from users where user_name like '%$q%'");
        $result = $query->result();
        return $result;
    }
    
    function postMessage($data){
        $this->db->insert('mailbox',$data);
        return true;
    }
    
    function getMessageUsers($loggedInUser){
        $query = $this->db->query("select distinct(a.user_name), a.rm_id from users a, mailbox b where b.to_rm_id = ? and b.from_rm_id = a.rm_id", array('b.to_rm_id' => $loggedInUser));
        $result = $query->result();
        return $result;
    }
    
    function getPrivateMessages($loggedInUser,$selectedUser){
        $queryUpdate = $this->db->query("update mailbox set has_read = ? where from_rm_id = ? and to_rm_id = ?", array('has_read'=>1,'from_rm_id'=>$selectedUser,'to_rm_id'=>$loggedInUser));
        //$resultUpdate = $queryUpdate->result();
        $query = $this->db->query("select a.* from mailbox a where (a.to_rm_id = $loggedInUser and a.from_rm_id = $selectedUser) or (a.to_rm_id = $selectedUser and a.from_rm_id = $loggedInUser) order by email_tran_id desc limit 5");
        $result = $query->result();
        return $result;
    }
    
    function getMorePrivateMessages($lastmsgid, $loggedInUser, $selectedUser){
        //print_r("select a.* from mailbox a where (a.to_rm_id = $loggedInUser and a.from_rm_id = $selectedUser) or (a.to_rm_id = $selectedUser and a.from_rm_id = $loggedInUser) and a.email_tran_id < $lastmsgid order by email_tran_id desc limit 2");
        $query = $this->db->query("select a.* from mailbox a where (a.to_rm_id = $loggedInUser and a.from_rm_id = $selectedUser and a.email_tran_id < $lastmsgid) or (a.to_rm_id = $selectedUser and a.from_rm_id = $loggedInUser and a.email_tran_id < $lastmsgid) order by email_tran_id desc limit 5");
        $result = $query->result();
        return $result;
    }
    
    function getPrivateMessagesOtherName($selectedUser){
        $query = $this->db->query("select user_name from users where rm_id = ?", array('rm_id'=>$selectedUser));
        $result = $query->result();
        return $result[0]->user_name;
    }
    
    function getMoreMessages($lastmsgid){
        $query = $this->db->query("select a.*, b.email_tran_id, b.body_text from users a, mailbox b where b.from_rm_id = a.rm_id and b.to_rm_id = ? and b.has_read = ? and b.email_tran_id < $lastmsgid  order by b.email_tran_id desc limit 5", array('b.to_rm_id' => $this->session->userdata('user_id'),'b.has_read'=>0));
        $result = $query->result();
        return $result;
    }
    
    function getTrendingVideos($category,$position,$items,$city,$rowcnt){
        $this->db->query("SET @user_category_in := '$category';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @city_name_in := '$city';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_trending_videos(@user_category_in, @offset_val, @limit_val, @city_name_in, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
        
    }
    
    function getFullVideoDetails($yu){
        $query = $this->db->query("select v.serial_no, v.rm_id, v.youtube_url, v.youtube_title,v.video_desc,v.view_count, v.like_count, v.dislike_count, u.user_name as uploaded_by, u.user_category from videos v left join users u on v.rm_id = u.rm_id where u.user_category = 'Musician' and v.youtube_url = ?", array('v.youtube_url' => $yu));
        $result = $query->result();
        return $result;
    }
    
    function getTrendingVideosTotalCount($category){
        $query = $this->db->query("select count(v.serial_no) as total_count from videos v left join users u on v.rm_id = u.rm_id where u.user_category = '$category'");
        $result = $query->result();
        return $result[0]->total_count;
        
    }
    
    function getTrendingAudio($category,$position,$items,$city,$rowcnt){
        $this->db->query("SET @user_category_in := '$category';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @city_name_in := '$city';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_latest_audios(@user_category_in, @offset_val, @limit_val, @city_name_in, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
    }
    
    function getMusicianArea($category,$position,$items,$city,$rowcnt){
        $this->db->query("SET @user_category_in := '$category';");
        $this->db->query("SET @city_name_in := '$city';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_profile_in_locality(@user_category_in, @city_name_in, @offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
        
    }
    
    function getScheduledEvents($position,$items,$city,$rowcnt){
        $this->db->query("SET @city_name_in := '$city';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_scheduled_events(@offset_val, @limit_val, @city_name_in, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
        
    }
    
    function getSpotlight($position,$items,$rowcnt){
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_spotlight_newsbit(@offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
        
    }
    
    function getGlobalEvents($position,$items,$city,$rowcnt){
        $this->db->query("SET @event_city_in := '$city';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_global_events(@event_city_in, @offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
        
    }
    
    function getReviews($position,$items,$rowcnt){
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_all_reviews(@offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
        
    }
    
    function getFeaturedMusician($category, $aptype){
        $query = $this->db->query("CALL get_featured_musicians('$category','$aptype');");
        $result = $query->result();
        return $result;
    }
    
    function getFeaturedEvents($aptype){
        $query = $this->db->query("CALL get_featured_events('$aptype');");
        $result = $query->result();
        return $result;
    }
    
    function getEventsByIdentity($rmid, $evid){
        $query = $this->db->query("CALL get_all_events_info('$rmid','$evid');");
        $result = $query->result();
        return $result;
    }
    
    function getAdsByIdentity($rmid, $adid, $category, $position, $items){
        $query = $this->db->query("CALL get_all_ads_info('$rmid','$adid','$category','$position','$items',@totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
    }
    
    function getFeaturedAds($apptype){
        $query = $this->db->query("CALL get_featured_ads('$apptype');");
        $result = $query->result();
        return $result;
    }
    
    function getWantedAds($rm_id,$ad_id_in,$category,$position,$items){
        $this->db->query("SET @rm_id_in := '$rm_id';");
        $this->db->query("SET @ad_id_in := '$ad_id_in';");
        $this->db->query("SET @category_in := '$category';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_in := '$items';");
        $query = $this->db->query("CALL get_all_ads_info(@rm_id_in, @ad_id_in, @category_in, @offset_val, @limit_in, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
    }
    
    function getTopSpots($position,$items){
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $query = $this->db->query("CALL get_topspots_list(@offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
    }
    
    function getFeaturedProfileHome(){
        $query = $this->db->query("CALL get_featured_profile_list();");
        $result = $query->result();
        return $result;
    }
    
    function autoCompSearch($searchVal,$usercat,$city,$position,$items){
        $this->db->query("SET @search_text := '$searchVal';");
        $this->db->query("SET @user_category_in := '$usercat';");
        $this->db->query("SET @city_name_in := '$city';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $query = $this->db->query("CALL search_profiles(@search_text, @user_category_in, @city_name_in, @offset_val, @limit_val, @totalRowCnt);");
//        print_r("CALL search_profiles('$searchVal', '$usercat', '$city', '$position', '$items', @totalRowCnt);"); exit();
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
    }
    
    function autoCompSearchAds($searchVal,$city,$position,$items){
        $this->db->query("SET @search_text := '$searchVal';");
        $this->db->query("SET @city_name_in := '$city';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $query = $this->db->query("CALL search_ads(@search_text, @city_name_in, @offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
    }
    
    function autoCompSearchEvents($searchVal,$city,$position,$items){
        $this->db->query("SET @search_text := '$searchVal';");
        $this->db->query("SET @city_name_in := '$city';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $query = $this->db->query("CALL search_events(@search_text, @city_name_in, @offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
    }
    
    function autoCompSearchVideos($searchVal,$city,$position,$items){
        $this->db->query("SET @search_text := '$searchVal';");
        $this->db->query("SET @city_name_in := '$city';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $query = $this->db->query("CALL search_videos(@search_text, @city_name_in, @offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
    }
    
    function getGlobalEventsHp($position,$items,$city,$rowcnt){
        $this->db->query("SET @city_name_in := '$city';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $query = $this->db->query("CALL get_top_events(@city_name_in, @offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
    }
    
    function getInterviews($position,$items,$rowcnt){
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_all_interviews(@offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
        
    }
    
	
	function getUpdatesWall($prevdate,$currdate,$timeInterval){
        $query = $this->db->query("CALL get_WallUpdateInfo('$prevdate','$currdate','$timeInterval');");
		$result = $query->result();
		return $result;
	}
	
	function getMyNodeId($sessid){
		$query = $this->db->query("select nodeid from graphnodes where user_id = ?", array('user_id' => $sessid));
		$result = $query->result();
		return $result[0]->nodeid; 
	}

	function getSeekingData($rmid,$position,$items,$rowcnt){

        $ckstat=1;
        $this->db->query("SET @rm_id := '$rmid';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_all_seeking_info(@rm_id,'$ckstat', @offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
                            'records'=>$result,
                            'count' => $resultout[0]->out_param
            );
    }

    function getTestimonials(){
        $p0 = 0;
        $success = $this->db->query("CALL get_testimonials_list('$p0');");
        $result = $success->result();
        $designArray = array();
        $i = 0;
        foreach($result as $row)
        {
            //$designArray[$i]['profile_url'] = $row->profile_url;
            $designArray[$i]['pic_url'] = $row->pic_url;
            $designArray[$i]['detail_info'] = $row->detail_info;
            $designArray[$i]['full_name'] = $row->full_name;
            $designArray[$i]['location'] = $row->location;
            $i++;
        }
        return $designArray;

        //return $result;
    }

    function gethomeSliders($sid, $sty, $apty){
        $success = $this->db->query("CALL get_home_page_slider('$sid', '$sty', '$apty');");
        $result = $success->result();
        return $result;
    }
    
}
?>
