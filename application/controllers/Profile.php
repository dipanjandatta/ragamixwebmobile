<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
class Profile extends CI_Controller {
        var $data;

        function __construct(){
            parent::__construct(); // needed when adding a constructor to a controller
            $this->data = array(
                                'layoutmode' => $this->config->item('layoutconfigdev')
            );
            $this->cdnImage = array('cdnimage' => $this->config->item('cdnimageurl'));
            $this->cdnAudio = array('cdnaudio' => $this->config->item('cdnaudiourl'));
            $this->cdnEvents = array('cdnevents' => $this->config->item('cdneventsurl'));
            $this->cdnAds = array('cdnads' => $this->config->item('cdnadsurl'));
			//$this->client = new Everyman\Neo4j\Client('localhost', 7474);
			//$this->client->getTransport()->setAuth('neo4j', 'subhajitneo');
	    	//print_r($this->client->getServerInfo());
            $this->load->model('Profilemodel');
        } 
        

	public function profileEdit()
	{


            if(!$this->session->userdata('user_id')){
                $this->session->set_flashdata('messageError', 'Please LogIn to perform Operations');
                redirect($this->agent->referrer());
            }
		$base_url = base_url();
		 
		//basic info for the header
		 $layout_data['pageTitle'] = "Edit Your Profile - Ragamix";
		 $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
		 $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
		 $layout_data['meta_url'] = "$base_url";
		 $layout_data['meta_classification'] = "home";
                 if($this->uri->segment(3) == 'basic'){
                    $body_data['feeds'] = 0;
                     if($this->uri->segment(4) != ""){
                         $body_data['basicprofiledata'] = $this->Profilemodel->getBasicProfileData($this->uri->segment(4));
                     }else{
                         $body_data['basicprofiledata'] = $this->Profilemodel->getBasicProfileData($this->session->userdata('user_id'));
                     }
                    @$this->db->free_db_resource();
                    $idVal = 0;
                    $body_data['userTypeList'] = $this->Profilemodel->getUsertypeLists($idVal); 
                    @$this->db->free_db_resource();
                    $body_data['userTypeListTwo'] = $this->Profilemodel->getUsertypeListsTwo($idVal);
                    @$this->db->free_db_resource();
                    $body_data['countryName'] = $this->Profilemodel->getCountry();
                    @$this->db->free_db_resource();
                    $body_data['cityName'] = $this->Profilemodel->getCity($countryVal='india');
                    //Get Lat Long data
                    $this->session->set_userdata(
                                                    array(
                                                            'userlat' => $body_data['basicprofiledata'][0]->loc_lat,
                                                            'userlong' => $body_data['basicprofiledata'][0]->loc_lang
                                                )
                                            );
                 }
		 else if($this->uri->segment(3) == 'music'){
                     $body_data['feeds'] = 1;
             if($this->uri->segment(4) != "") {
                 $body_data['musicdata'] = $this->Profilemodel->getMusicData($this->uri->segment(4));
             }else{
                 $body_data['musicdata'] = $this->Profilemodel->getMusicData($this->session->userdata('user_id'));
             }
                     @$this->db->free_db_resource();
                     $body_data['getGenre'] = $this->Profilemodel->getGenreList();
                     @$this->db->free_db_resource();
                     $body_data['getVocals'] = $this->Profilemodel->getVocalsList();
                     @$this->db->free_db_resource();
                     $body_data['getStrIns'] = $this->Profilemodel->getStringInstruments();
                     @$this->db->free_db_resource();
                     $body_data['getReedIns'] = $this->Profilemodel->getReedInstruments();
                     @$this->db->free_db_resource();
                     $body_data['getPer'] = $this->Profilemodel->getPercussions();
                     @$this->db->free_db_resource();
                     $body_data['getWindIns'] = $this->Profilemodel->getWindInstruments();
                 }
                 else if($this->uri->segment(3) == 'photo'){
                     $body_data['feeds'] = 2;
                     if($this->uri->segment(4) != "") {
                         $body_data['getPhotos'] = $this->Profilemodel->getProfilePhotos($this->uri->segment(4));
                     }else{
                         $body_data['getPhotos'] = $this->Profilemodel->getProfilePhotos($this->session->userdata('user_id'));
                     }
                     $body_data['getCdnUrl'] = $this->cdnImage['cdnimage'];
                     //print_r($body_data['getPhotos']); exit();
                 }
		 else if($this->uri->segment(3) == 'audiovideo'){
                     $body_data['feeds'] = 3;
             if($this->uri->segment(4) != "") {
                 $body_data['getAllAudios'] = $this->Profilemodel->getAudios($this->uri->segment(4));
                 @$this->db->free_db_resource();
                 $body_data['getAllVideos'] = $this->Profilemodel->getVideos($this->uri->segment(4));

             }else {
                 $body_data['getAllAudios'] = $this->Profilemodel->getAudios($this->session->userdata('user_id'));
                 @$this->db->free_db_resource();
                 $body_data['getAllVideos'] = $this->Profilemodel->getVideos($this->session->userdata('user_id'));
             }

                     //print_r($body_data['getAllVideos']); exit();
                 }
		 else if($this->uri->segment(3) == 'musicresource'){
                 $body_data['feeds'] = 4;
                 if($this->uri->segment(4) != "") {
                     $body_data['getReqs'] = $this->Profilemodel->getSeekingRequirements($this->uri->segment(4));
                 }else {
                     //$body_data['getReqs'] = $this->Profilemodel->getSeekingRequirements($this->session->userdata('user_id'));
                 }
         }
		 else if($this->uri->segment(3) == 'showevents'){
                    $this->load->library('googlemaps');

                    $config['center'] = '22.572,88.433';
                    $config['zoom'] = '20';
                    $this->googlemaps->initialize($config);

                    $marker = array();
                    $marker['position'] = '22.572,88.433';
                    $this->googlemaps->add_marker($marker);
                    $body_data['map'] = $this->googlemaps->create_map();
                    $body_data['feeds'] = 5;
             if($this->uri->segment(4) != "") {
                 $body_data['getShowsEvents'] = $this->Profilemodel->getShowsEventsData($this->uri->segment(4));
             }else {
                 $body_data['getShowsEvents'] = $this->Profilemodel->getShowsEventsData($this->session->userdata('user_id'), $show_ev_id = 0);

             }
                    @$this->db->free_db_resource();
                    $countryVal = 'india';
                    $body_data['cityName'] = $this->Profilemodel->getCityNameOnCountry($countryVal);
                 }
                 else if($this->uri->segment(3) == 'postads'){
                     $body_data['feeds'] = 6;
                     if($this->uri->segment(4) != "") {
                         $data['records'] = $this->Profilemodel->getWantedAds($this->uri->segment(4), $ad_id = 0, $category = '', $position = 0, $items = 100);
                     } else {
                         $data['records'] = $this->Profilemodel->getWantedAds($this->session->userdata('user_id'), $ad_id = 0, $category = '', $position = 0, $items = 100);

                     }
                     $body_data['getAds'] = $data['records']['records'];
                 }
		 $layout_data['content_body'] = $this->load->view('app/profileEdit', $body_data, true);
		 
		$this->load->view($this->data['layoutmode'], $layout_data);
	}
        
        public function ajaximage(){
            $session_ids = 1;
            $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
                $imagename = $_FILES['file']['name'];
                $size = $_FILES['file']['size'];
                $tmp = $_FILES['file']['tmp_name'];
                if(strlen($imagename))
                {
                    $ext = strtolower($this->getExtension($imagename));
                    if(in_array($ext,$valid_formats))
                    {
                        if($size<(1024*1024)) // Image size max 1 MB
                        {
                            $bucketname="ragamix";
                            $fileObjUri = "ragamix/all-prod/";
                            if (!class_exists('S3'))require_once('S3.php');

                            //AWS access info
                            if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJ3XYGL6LANED6ABA');
                            if (!defined('awsSecretKey')) define('awsSecretKey', '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');

                            //instantiate the class
                            $s3 = new S3(awsAccessKey, awsSecretKey);

                            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                            $fileObjUri = "ragamix/all-prod/";
                            $actual_image_name = time().$session_ids.".".$ext;
                            
                                    if($s3->putObjectFile($tmp, $bucketname , $fileObjUri.$actual_image_name, S3::ACL_PUBLIC_READ) )
                                    {
                                        //Insert upload image files names into user_uploads table
                                        //Preparing Data
                                        $data = array(
                                                        'photo_id_in' => 0,
                                                        'rm_id' => $this->input->post('hiddenval'),
                                                        'main_pic' => 0,
                                                        'background_pic' => 0,
                                                        'allow_view' => 1,
                                                        'pic_url' => $this->cdnImage['cdnimage'].$actual_image_name,
                                                        'pic_privacy' => 1, 
                                                        'pic_height' => 100,
                                                        'pic_width' => 100
                                                );
//                                        print_r($data);exit();
                                        $this->Profilemodel->insertPhotos($data);
                                        //echo "<img src='".$base_url."uploads/".$actual_image_name."' class='preview'>";
                                        redirect($this->agent->referrer());
                                    }
                                    else
                                        echo "S3 Upload Fail";
                                    }
                                else
                                    echo "Image file size max 1 MB"; 
                                }
                            else
                                echo "Invalid file format.."; 
                            }
                        else
                            echo "Please select image..!";
                            exit;
                    }
         
                    
        function compressImage($ext,$uploadedfile,$path,$actual_image_name,$newwidth)
        {

                if($ext=="jpg" || $ext=="jpeg" )
                {
                $src = imagecreatefromjpeg($uploadedfile);
                }
                else if($ext=="png")
                {
                $src = imagecreatefrompng($uploadedfile);
                }
                else if($ext=="gif")
                {
                $src = imagecreatefromgif($uploadedfile);
                }
                else
                {
                $src = imagecreatefrombmp($uploadedfile);
                }

                list($width,$height)=getimagesize($uploadedfile);
                $newheight=($height/$width)*$newwidth;
                $tmp=imagecreatetruecolor($newwidth,$newheight);
                imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
                $filename = $path.$newwidth.'_'.$actual_image_name; //PixelSize_TimeStamp.jpg
                imagejpeg($tmp,$filename,100);
                imagedestroy($tmp);
                return $filename;
        }
        
        function getExtension($str)
        {
            $i = strrpos($str,".");
            if (!$i)
            {
            return "";
            }
            $l = strlen($str) - $i;
            $ext = substr($str,$i+1,$l);
            return $ext;
        }
        
        
        function streamAudio(){
//            $body_data[] = '';
//            
//            $layout_data['content_body'] = $this->load->view('app/streamAudio', $body_data, true);
		 
            $this->load->view('app/streamAudio');
        }
        
        function photoview(){
            $data['img'] = $this->uri->segment(4);
            $data['extension'] = $this->uri->segment(5);
            $data['pid'] = $this->uri->segment(3);
            $this->load->view('app/photoview',$data);
        }
        
        function chat(){
            $this->load->view('app/chat');
        }
        
        function search(){
            $this->load->model('Postmodel');
            $testBoxString = $this->input->post('searchword');
            $exString = explode(' ',$testBoxString);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
            $data['sv'] = $testBoxString;
            $usercat = 'Musician';
            $usercat1 = 'Singer';
            $usercat2 = 'Band';
            $data['city'] = $city = '';
            $data['records'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position = 0,$items=3);
            $data['searchListM'] = $data['records']['records'];
            $this->load->view('app/displaySearch', $data);
        }
        
        function searchTop(){
        if($this->uri->segment(3) == 1){
            if($this->input->post('cooksubmit')){
                $searchval = trim($this->input->post('searchbox'));
                $searchString = explode(' ',$searchval);
                $st = implode(' ',$searchString);
                redirect('profile/searchTop?q='.rawurlencode($st).'&city=&ref=all&type=oo');
            }
        }
        if($this->input->post('submit')){
            $locname = trim($this->input->post('locname'));
            $searchval = trim($this->input->post('searchbox'));
            $searchString = explode(' ',$searchval);
            $st = implode(' ',$searchString);
            redirect('profile/searchTop?q='.rawurlencode($st).'&city='.$locname.'&ref=all&type=oo');
        }
        else {
            $body_data['rt'] = $refType = $_GET['ref'];
//                    print_r($body_data['rt']);exit();
            $body_data['st'] = $st = rawurldecode($_GET['q']);
            $body_data['city'] = $city = $_GET['city'];
            $body_data['type'] = $ty = $_GET['type'];
            $layout_data['pageTitle'] = $st."- Ragamix Search";
            $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
            $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
            $layout_data['meta_url'] = "$base_url";
                    
            // We get the JSON results from this request
            if($city != ''){
                $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($city).'&sensor=false');
                // We convert the JSON to an array
                $geo = json_decode($geo, true);
                // If everything is cool
                if ($geo['status'] = 'OK') {
                    // We set our values
                    $latitude = $geo['results'][0]['geometry']['location']['lat'];
                    $longitude = $geo['results'][0]['geometry']['location']['lng'];
                }

                $latlong = array();
                $latlong[0]=$latitude;
                $latlong[1]=$longitude;

                $implodeLoc = implode(',',$latlong);

                $this->load->library('googlemaps');

                $config['center'] = $implodeLoc;
                $config['zoom'] = '15';
                $this->googlemaps->initialize($config);

                $marker = array();
                $marker['position'] = $implodeLoc;
                $this->googlemaps->add_marker($marker);
                $body_data['map'] = $this->googlemaps->create_map();
            }
            if($st != ''){
                $exString = explode(' ',$st);
                $chars = array();
                for($i=0;$i<sizeof($exString);$i++){
                    $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
                }
                $completeStringToSearch = implode(' ',$chars);
            }
            else {
                $completeStringToSearch = $st;
            }
            if($refType == 'all' && $ty == 'oo'){
                $usercat = 'Musician';
                $usercat1 = 'Singer';
                $usercat2 = 'Band';
                $usercat3 = 'Composer';
                $usercat4 = 'Educators';
                $usercat5 = 'Solo Artist';
                if($city == ''){
                    $city = '';
                }
                else {
                    $city = $city;
                }
                $data['records'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position = 0,$items=4);
                @$this->db->free_db_resource();
                $data['records1'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat1,$city,$position = 0,$items=4);
                @$this->db->free_db_resource();
                $data['records2'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat2,$city,$position = 0,$items=4);
                @$this->db->free_db_resource();
                $data['records3'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat3,$city,$position = 0,$items=4);
                @$this->db->free_db_resource();
                $data['records4'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat4,$city,$position = 0,$items=4);
                @$this->db->free_db_resource();
                $data['records5'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat5,$city,$position = 0,$items=4);
                @$this->db->free_db_resource();

//                $data['records3'] = $this->Profilemodel->autoCompSearchAds($completeStringToSearch,$usercat3,$city,$position = 0,$items=4);
//                @$this->db->free_db_resource();
//                $data['records4'] = $this->Profilemodel->autoCompSearchEvents($completeStringToSearch,$city,$position = 0,$items=4);
//                @$this->db->free_db_resource();
//                $data['records5'] = $this->Profilemodel->autoCompSearchVideos($completeStringToSearch,$city,$position = 0,$items=4);
//                @$this->db->free_db_resource();


                $body_data['getAllSearchM'] = $data['records']['records'];
                @$this->db->free_db_resource();
                $body_data['getAllSearchS'] = $data['records1']['records'];
                @$this->db->free_db_resource();
                $body_data['getAllSearchB'] = $data['records2']['records'];
                @$this->db->free_db_resource();
                $body_data['getAllSearchCom'] = $data['records3']['records'];
                @$this->db->free_db_resource();
                $body_data['getAllSearchEdu'] = $data['records4']['records'];
                @$this->db->free_db_resource();
                $body_data['getAllSearchSolo'] = $data['records5']['records'];
//                print_r($body_data['getAllSearchSolo']);exit();
                @$this->db->free_db_resource();
                $body_data['mc'] = $data['records']['count'];
                @$this->db->free_db_resource();
                $body_data['sc'] = $data['records1']['count'];
                @$this->db->free_db_resource();
                $body_data['bc'] = $data['records2']['count'];
                @$this->db->free_db_resource();
                $body_data['ac'] = $data['records3']['count'];
                @$this->db->free_db_resource();
                $body_data['ec'] = $data['records4']['count'];
                @$this->db->free_db_resource();
                $body_data['vc'] = $data['records5']['count'];
                @$this->db->free_db_resource();
            }
        }
                   
        //print_r($data['records']); exit();
                
        $layout_data['content_body'] = $this->load->view('app/searchTop', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }
        
        function searchMusician(){
                $body_data['rt'] = $refType = $_GET['ref'];
                $body_data['st'] = $st = $_GET['q'];
                $body_data['city'] = $city = $_GET['city'];
                $body_data['type'] = $ty = $_GET['type'];
                $body_data['next'] = $next = $_GET['next'];
                $body_data['clicks'] = $clicks = $_GET['clicks'];
                $body_data['page_number'] = $page_number = $_GET['page_number'];
                $body_data['prev'] = $prev = $_GET['prev'];
                    if($st != ''){
                        $exString = explode(' ',$st);
                        $chars = array();
                        for($i=0;$i<sizeof($exString);$i++){
                            $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
                        }
                        $completeStringToSearch = implode(' ',$chars);
                    }
                    else {
                        $completeStringToSearch = $st;
                    }
                    $usercat = 'Musician';
                        if($city == ''){
                            $city = '';
                        }
                        else {
                            $city = $city;
                        }
                    @$this->db->free_db_resource();
                    if($prev == 'y'){
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks-1;
                    $body_data['page_number'] = $page_number = $clicks;
                    }
                    $position = ($page_number * 20);
                    if($page_number != 0) {
                        
                    }
                    else{
                        $page_number = 0;
                    }
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks+1;
                    $body_data['page_number'] = $page_number = $clicks;
                    $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
                    $body_data['getAllSearchM'] = $body_data['newrecords']['records'];
                    $body_data['mc'] = $body_data['newrecords']['count'];
                $layout_data['content_body'] = $this->load->view('app/searchmusicianlist', $body_data, true);
		$this->load->view($this->data['layoutmode'], $layout_data);
                
        }
        
        function searchSingers(){
                $body_data['rt'] = $refType = $_GET['ref'];
                $body_data['st'] = $st = $_GET['q'];
                $body_data['city'] = $city = $_GET['city'];
                $body_data['type'] = $ty = $_GET['type'];
                $body_data['next'] = $next = $_GET['next'];
                $body_data['clicks'] = $clicks = $_GET['clicks'];
                $body_data['page_number'] = $page_number = $_GET['page_number'];
                $body_data['prev'] = $prev = $_GET['prev'];
                    if($st != ''){
                        $exString = explode(' ',$st);
                        $chars = array();
                        for($i=0;$i<sizeof($exString);$i++){
                            $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
                        }
                        $completeStringToSearch = implode(' ',$chars);
                    }
                    else {
                        $completeStringToSearch = $st;
                    }
                    $usercat = 'Singer';
                        if($city == ''){
                            $city = '';
                        }
                        else {
                            $city = $city;
                        }
                    @$this->db->free_db_resource();
                    if($prev == 'y'){
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks-1;
                    $body_data['page_number'] = $page_number = $clicks;
                    }
                    $position = ($page_number * 20);
                    if($page_number != 0) {
                        
                    }
                    else{
                        $page_number = 0;
                    }
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks+1;
                    $body_data['page_number'] = $page_number = $clicks;
                    $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
                    $body_data['getAllSearchM'] = $body_data['newrecords']['records'];
                    $body_data['mc'] = $body_data['newrecords']['count'];
                $layout_data['content_body'] = $this->load->view('app/searchsingerslist', $body_data, true);
		$this->load->view($this->data['layoutmode'], $layout_data);
                
        }
        
        function searchBands(){
                $body_data['rt'] = $refType = $_GET['ref'];
                $body_data['st'] = $st = $_GET['q'];
                $body_data['city'] = $city = $_GET['city'];
                $body_data['type'] = $ty = $_GET['type'];
                $body_data['next'] = $next = $_GET['next'];
                $body_data['clicks'] = $clicks = $_GET['clicks'];
                $body_data['page_number'] = $page_number = $_GET['page_number'];
                $body_data['prev'] = $prev = $_GET['prev'];
                    if($st != ''){
                        $exString = explode(' ',$st);
                        $chars = array();
                        for($i=0;$i<sizeof($exString);$i++){
                            $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
                        }
                        $completeStringToSearch = implode(' ',$chars);
                    }
                    else {
                        $completeStringToSearch = $st;
                    }
                    $usercat = 'Band';
                        if($city == ''){
                            $city = '';
                        }
                        else {
                            $city = $city;
                        }
                    @$this->db->free_db_resource();
                    if($prev == 'y'){
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks-1;
                    $body_data['page_number'] = $page_number = $clicks;
                    }
                    $position = ($page_number * 20);
                    if($page_number != 0) {
                        
                    }
                    else{
                        $page_number = 0;
                    }
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks+1;
                    $body_data['page_number'] = $page_number = $clicks;
                    $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
                    $body_data['getAllSearchM'] = $body_data['newrecords']['records'];
                    $body_data['mc'] = $body_data['newrecords']['count'];
                $layout_data['content_body'] = $this->load->view('app/searchbandslist', $body_data, true);
		$this->load->view($this->data['layoutmode'], $layout_data);
                
        }
        
        function searchAds(){
                $body_data['rt'] = $refType = $_GET['ref'];
                $body_data['st'] = $st = $_GET['q'];
                $body_data['city'] = $city = $_GET['city'];
                $body_data['type'] = $ty = $_GET['type'];
                $body_data['next'] = $next = $_GET['next'];
                $body_data['clicks'] = $clicks = $_GET['clicks'];
                $body_data['page_number'] = $page_number = $_GET['page_number'];
                $body_data['prev'] = $prev = $_GET['prev'];
                    if($st != ''){
                        $exString = explode(' ',$st);
                        $chars = array();
                        for($i=0;$i<sizeof($exString);$i++){
                            $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
                        }
                        $completeStringToSearch = implode(' ',$chars);
                    }
                    else {
                        $completeStringToSearch = $st;
                    }
                $completeStringToSearch = implode(' ',$chars);
                        if($city == ''){
                            $city = '';
                        }
                        else {
                            $city = $city;
                        }
                    @$this->db->free_db_resource();
                    if($prev == 'y'){
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks-1;
                    $body_data['page_number'] = $page_number = $clicks;
                    }
                    $position = ($page_number * 20);
                    if($page_number != 0) {
                        
                    }
                    else{
                        $page_number = 0;
                    }
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks+1;
                    $body_data['page_number'] = $page_number = $clicks;
                    $body_data['newrecords'] = $this->Profilemodel->autoCompSearchAds($completeStringToSearch,$city,$position,$items=20);
                    $body_data['getAllSearchM'] = $body_data['newrecords']['records'];
                    $body_data['mc'] = $body_data['newrecords']['count'];
                $layout_data['content_body'] = $this->load->view('app/searchadslist', $body_data, true);
		$this->load->view($this->data['layoutmode'], $layout_data);
                
        }
        
        function searchEvents(){
                $body_data['rt'] = $refType = $_GET['ref'];
                $body_data['st'] = $st = $_GET['q'];
                $body_data['city'] = $city = $_GET['city'];
                $body_data['type'] = $ty = $_GET['type'];
                $body_data['next'] = $next = $_GET['next'];
                $body_data['clicks'] = $clicks = $_GET['clicks'];
                $body_data['page_number'] = $page_number = $_GET['page_number'];
                $body_data['prev'] = $prev = $_GET['prev'];
                    if($st != ''){
                        $exString = explode(' ',$st);
                        $chars = array();
                        for($i=0;$i<sizeof($exString);$i++){
                            $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
                        }
                        $completeStringToSearch = implode(' ',$chars);
                    }
                    else {
                        $completeStringToSearch = $st;
                    }
                $completeStringToSearch = implode(' ',$chars);
                        if($city == ''){
                            $city = '';
                        }
                        else {
                            $city = $city;
                        }
                    @$this->db->free_db_resource();
                    if($prev == 'y'){
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks-1;
                    $body_data['page_number'] = $page_number = $clicks;
                    }
                    $position = ($page_number * 20);
                    if($page_number != 0) {
                        
                    }
                    else{
                        $page_number = 0;
                    }
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks+1;
                    $body_data['page_number'] = $page_number = $clicks;
                    $body_data['newrecords'] = $this->Profilemodel->autoCompSearchEvents($completeStringToSearch,$city,$position,$items=20);
                    $body_data['getAllSearchM'] = $body_data['newrecords']['records'];
                    $body_data['mc'] = $body_data['newrecords']['count'];
                $layout_data['content_body'] = $this->load->view('app/searcheventslist', $body_data, true);
		$this->load->view($this->data['layoutmode'], $layout_data);
                
        }
        
        function searchVideos(){
                $body_data['rt'] = $refType = $_GET['ref'];
                $body_data['st'] = $st = $_GET['q'];
                $body_data['city'] = $city = $_GET['city'];
                $body_data['type'] = $ty = $_GET['type'];
                $body_data['next'] = $next = $_GET['next'];
                $body_data['clicks'] = $clicks = $_GET['clicks'];
                $body_data['page_number'] = $page_number = $_GET['page_number'];
                $body_data['prev'] = $prev = $_GET['prev'];
                    if($st != ''){
                        $exString = explode(' ',$st);
                        $chars = array();
                        for($i=0;$i<sizeof($exString);$i++){
                            $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
                        }
                        $completeStringToSearch = implode(' ',$chars);
                    }
                    else {
                        $completeStringToSearch = $st;
                    }
                        if($city == ''){
                            $city = '';
                        }
                        else {
                            $city = $city;
                        }
                    @$this->db->free_db_resource();
                    if($prev == 'y'){
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks-1;
                    $body_data['page_number'] = $page_number = $clicks;
                    }
                    $position = ($page_number * 20);
                    if($page_number != 0) {
                        
                    }
                    else{
                        $page_number = 0;
                    }
                    $data['next'] = $next = $next+1;
                    $body_data['clicks'] = $clicks = $clicks+1;
                    $body_data['page_number'] = $page_number = $clicks;
                    $body_data['newrecords'] = $this->Profilemodel->autoCompSearchVideos($completeStringToSearch,$city,$position,$items=20);
                    $body_data['getAllSearchM'] = $body_data['newrecords']['records'];
                    $body_data['mc'] = $body_data['newrecords']['count'];
                $layout_data['content_body'] = $this->load->view('app/searchvideoslist', $body_data, true);
		$this->load->view($this->data['layoutmode'], $layout_data);
                
        }

//Added By Souvick

function searchTeacher(){
        $body_data['rt'] = $refType = $_GET['ref'];
//        print_r($body_data['rt']);exit();
        $body_data['st'] = $st = $_GET['q'];
        $body_data['city'] = $city = $_GET['city'];
        $body_data['type'] = $ty = $_GET['type'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($st != ''){
            $exString = explode(' ',$st);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
        }
        else {
            $completeStringToSearch = $st;
        }
        $usercat = 'Teacher';
        if($city == ''){
            $city = '';
        }
        else {
            $city = $city;
        }
        @$this->db->free_db_resource();
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
//        print_r($body_data['newrecords']);exit();
        $body_data['getAllSearchTeacher'] = $body_data['newrecords']['records'];
//        print_r($body_data['getAllSearchComposer']);exit();
        $body_data['mc'] = $body_data['newrecords']['count'];
        $layout_data['content_body'] = $this->load->view('app/searchTeacher', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);


    }

    function searchStudio(){
        $body_data['rt'] = $refType = $_GET['ref'];
//        print_r($body_data['rt']);exit();
        $body_data['st'] = $st = $_GET['q'];
        $body_data['city'] = $city = $_GET['city'];
        $body_data['type'] = $ty = $_GET['type'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($st != ''){
            $exString = explode(' ',$st);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
        }
        else {
            $completeStringToSearch = $st;
        }
        $usercat = 'Studio';
        if($city == ''){
            $city = '';
        }
        else {
            $city = $city;
        }
        @$this->db->free_db_resource();
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
//        print_r($body_data['newrecords']);exit();
        $body_data['getAllSearchStudio'] = $body_data['newrecords']['records'];
//        print_r($body_data['getAllSearchComposer']);exit();
        $body_data['mc'] = $body_data['newrecords']['count'];
        $layout_data['content_body'] = $this->load->view('app/searchStudio', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);


    }

    function searchShowManager(){
        $body_data['rt'] = $refType = $_GET['ref'];
//        print_r($body_data['rt']);exit();
        $body_data['st'] = $st = $_GET['q'];
        $body_data['city'] = $city = $_GET['city'];
        $body_data['type'] = $ty = $_GET['type'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($st != ''){
            $exString = explode(' ',$st);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
        }
        else {
            $completeStringToSearch = $st;
        }
        $usercat = 'Show Manager';
        if($city == ''){
            $city = '';
        }
        else {
            $city = $city;
        }
        @$this->db->free_db_resource();
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
//        print_r($body_data['newrecords']);exit();
        $body_data['getAllSearchShowManager'] = $body_data['newrecords']['records'];
//        print_r($body_data['getAllSearchComposer']);exit();
        $body_data['mc'] = $body_data['newrecords']['count'];
        $layout_data['content_body'] = $this->load->view('app/searchShowManager', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);


    }

    function searchMusicalDuo(){
        $body_data['rt'] = $refType = $_GET['ref'];
//        print_r($body_data['rt']);exit();
        $body_data['st'] = $st = $_GET['q'];
        $body_data['city'] = $city = $_GET['city'];
        $body_data['type'] = $ty = $_GET['type'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($st != ''){
            $exString = explode(' ',$st);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
        }
        else {
            $completeStringToSearch = $st;
        }
        $usercat = 'Musical Duo';
        if($city == ''){
            $city = '';
        }
        else {
            $city = $city;
        }
        @$this->db->free_db_resource();
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
//        print_r($body_data['newrecords']);exit();
        $body_data['getAllSearchMusicalDuo'] = $body_data['newrecords']['records'];
//        print_r($body_data['getAllSearchComposer']);exit();
        $body_data['mc'] = $body_data['newrecords']['count'];
        $layout_data['content_body'] = $this->load->view('app/searchMusicalDuo', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);


    }

    function searchFan(){
        $body_data['rt'] = $refType = $_GET['ref'];
//        print_r($body_data['rt']);exit();
        $body_data['st'] = $st = $_GET['q'];
        $body_data['city'] = $city = $_GET['city'];
        $body_data['type'] = $ty = $_GET['type'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($st != ''){
            $exString = explode(' ',$st);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
        }
        else {
            $completeStringToSearch = $st;
        }
        $usercat = 'Fan';
        if($city == ''){
            $city = '';
        }
        else {
            $city = $city;
        }
        @$this->db->free_db_resource();
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
//        print_r($body_data['newrecords']);exit();
        $body_data['getAllSearchFan'] = $body_data['newrecords']['records'];
//        print_r($body_data['getAllSearchComposer']);exit();
        $body_data['mc'] = $body_data['newrecords']['count'];
        $layout_data['content_body'] = $this->load->view('app/searchFan', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);

    }

    function searchEventMarketer(){
        $body_data['rt'] = $refType = $_GET['ref'];
//        print_r($body_data['rt']);exit();
        $body_data['st'] = $st = $_GET['q'];
        $body_data['city'] = $city = $_GET['city'];
        $body_data['type'] = $ty = $_GET['type'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($st != ''){
            $exString = explode(' ',$st);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
        }
        else {
            $completeStringToSearch = $st;
        }
        $usercat = 'Event Markeeter';
        if($city == ''){
            $city = '';
        }
        else {
            $city = $city;
        }
        @$this->db->free_db_resource();
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
//        print_r($body_data['newrecords']);exit();
        $body_data['getAllSearchEventMarketer'] = $body_data['newrecords']['records'];
//        print_r($body_data['getAllSearchComposer']);exit();
        $body_data['mc'] = $body_data['newrecords']['count'];
        $layout_data['content_body'] = $this->load->view('app/searchEventMarketer', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);

    }

    function searchEducators(){
        $body_data['rt'] = $refType = $_GET['ref'];
//        print_r($body_data['rt']);exit();
        $body_data['st'] = $st = $_GET['q'];
        $body_data['city'] = $city = $_GET['city'];
        $body_data['type'] = $ty = $_GET['type'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($st != ''){
            $exString = explode(' ',$st);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
        }
        else {
            $completeStringToSearch = $st;
        }
        $usercat = 'Educators';
        if($city == ''){
            $city = '';
        }
        else {
            $city = $city;
        }
        @$this->db->free_db_resource();
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
//        print_r($body_data['newrecords']);exit();
        $body_data['getAllSearchEducators'] = $body_data['newrecords']['records'];
//        print_r($body_data['getAllSearchComposer']);exit();
        $body_data['mc'] = $body_data['newrecords']['count'];
        $layout_data['content_body'] = $this->load->view('app/searchEducators', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);

    }

    function searchComposer(){
        $body_data['rt'] = $refType = $_GET['ref'];
//        print_r($body_data['rt']);exit();
        $body_data['st'] = $st = $_GET['q'];
        $body_data['city'] = $city = $_GET['city'];
        $body_data['type'] = $ty = $_GET['type'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($st != ''){
            $exString = explode(' ',$st);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
        }
        else {
            $completeStringToSearch = $st;
        }
        $usercat = 'Composer';
        if($city == ''){
            $city = '';
        }
        else {
            $city = $city;
        }
        @$this->db->free_db_resource();
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
//        print_r($body_data['newrecords']);exit();
        $body_data['getAllSearchComposer'] = $body_data['newrecords']['records'];
//        print_r($body_data['getAllSearchComposer']);exit();
        $body_data['mc'] = $body_data['newrecords']['count'];
        $layout_data['content_body'] = $this->load->view('app/searchComposer', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);

    }

    function searchDj(){
        $body_data['rt'] = $refType = $_GET['ref'];
//        print_r($body_data['rt']);exit();
        $body_data['st'] = $st = $_GET['q'];
        $body_data['city'] = $city = $_GET['city'];
        $body_data['type'] = $ty = $_GET['type'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($st != ''){
            $exString = explode(' ',$st);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
        }
        else {
            $completeStringToSearch = $st;
        }
        $usercat = 'DJ';
        if($city == ''){
            $city = '';
        }
        else {
            $city = $city;
        }
        @$this->db->free_db_resource();
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
//        print_r($body_data['newrecords']);exit();
        $body_data['getAllSearchDj'] = $body_data['newrecords']['records'];
//        print_r($body_data);exit();
        $body_data['mc'] = $body_data['newrecords']['count'];
        $layout_data['content_body'] = $this->load->view('app/searchDj', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function searchSolo(){
        $body_data['rt'] = $refType = $_GET['ref'];
//        print_r($body_data['rt']);exit();
        $body_data['st'] = $st = $_GET['q'];
        $body_data['city'] = $city = $_GET['city'];
        $body_data['type'] = $ty = $_GET['type'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($st != ''){
            $exString = explode(' ',$st);
            $chars = array();
            for($i=0;$i<sizeof($exString);$i++){
                $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
            }
            $completeStringToSearch = implode(' ',$chars);
        }
        else {
            $completeStringToSearch = $st;
        }
        $usercat = 'Solo Artist';
        if($city == ''){
            $city = '';
        }
        else {
            $city = $city;
        }
        @$this->db->free_db_resource();
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        $body_data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position, $items=20);
//        print_r($body_data['newrecords']);exit();
        $body_data['getAllSearchSo'] = $body_data['newrecords']['records'];
//        print_r($body_data);exit();
        $body_data['mc'] = $body_data['newrecords']['count'];
        $layout_data['content_body'] = $this->load->view('app/searchsoloartist', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);

    }

    //Added By Souvick End

                
        function details(){
                $item = $this->config->item('template');
                $rmid = $this->uri->segment(5);
                $body_data['previewData'] = $this->Profilemodel->getPreviewData($rmid);
				//Check if Logged In Person has Hired him Already
				// $transaction = $this->client->beginTransaction();
				// $queryC = new Everyman\Neo4j\Cypher\Query($this->client, 'MATCH (n)-[:HIRED]->(x) WHERE x.id = "'.$body_data['previewData'][0]->rm_id.'" AND n.id = "'.$this->session->userdata('user_id').'" RETURN count(n) as hiredCount');
				// $result = $transaction->addStatements($queryC, true);
				// foreach($result as $row){
						// if($row['hiredCount'] == 0){
							// $body_data['hired'] = 1;
						// }
						// else {
							// $body_data['hired'] = 0;
						// }
				// }
				//End of Check
				

				
                $images = str_replace(' ', '%20B',$body_data['previewData'][0]->profile_pic_url);
                if($this->uri->segment(3) == $item['musician']){
                    $templateToLoad = 'app/'.$item['musician'];
                    $layout_data['image'] = "$images";
                }
                if($this->uri->segment(3) == $item['band']){
                    $templateToLoad = 'app/'.$item['band'];
                    $layout_data['image'] = "$images";
                }
                if($this->uri->segment(3) == $item['band']) {
                    $fb = $body_data['previewData'][0]->facebook_url;
                    $body_data['haha'] = $this->facebookLikeCount($fb);
                }

                $layout_data['pageTitle'] = $body_data['previewData'][0]->user_name;
                $layout_data['meta_description'] = $body_data['previewData'][0]->usertypename." ".$body_data['previewData'][0]->genre_name;
                $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
                $layout_data['meta_url'] = "$base_url";
                $latlong = array();
                $latlong[0]=$body_data['previewData'][0]->loc_lat;
                $latlong[1]=$body_data['previewData'][0]->loc_lang;
                $implodeLoc = implode(',',$latlong);
                    $this->load->library('googlemaps');

                    $config['center'] = $implodeLoc; //Must be a String
                    $config['zoom'] = '15';
                    $this->googlemaps->initialize($config);

                    $marker = array();
                    $marker['position'] = $implodeLoc;
                    $this->googlemaps->add_marker($marker);
                    $body_data['map'] = $this->googlemaps->create_map();                
                $layout_data['content_body'] = $this->load->view($templateToLoad, $body_data, true);
		 
		$this->load->view($this->data['layoutmode'], $layout_data); 
        }


	// public function getRelatedNodes(){
		// $transactionS = $this->client->beginTransaction();
		// //$queryD = new Everyman\Neo4j\Cypher\Query($this->client, 'MATCH (n) <- [r:HIRED]-(x) where x.id = "'.$this->session->userdata('user_id').'" RETURN x');
		// $queryD = new Everyman\Neo4j\Cypher\Query($this->client, 'MATCH (n)-[:HIRED*2..2]->(x) WHERE n.id = "'.$this->session->userdata('user_id').'" RETURN x');
		// $resultD = $transactionS->addStatements($queryD, true);
		// $data = array( "resulthired" => $resultD );
		// $this->load->view('app/getrelatednodes', $data);
	// }
// 
	// public function startfollowing(){
            // if(!$this->session->userdata('user_id')){
                // $this->session->set_flashdata('messageError', 'Please LogIn to perform Operations');
                // redirect($this->agent->referrer());
            // }
		// $segVal = $this->uri->segment(4);
		// $segValName = $this->uri->segment(3);
		// $userPic = base64_decode($this->uri->segment(5));
		// $seesid = $this->session->userdata('user_id');
		// $sessname = $this->session->userdata('user_name');
		// $sesspic = $this->session->userdata('profile_pic');
		// $transaction = $this->client->beginTransaction();
		// //$queryC = new Everyman\Neo4j\Cypher\Query($this->client, 'match (n) where n.id = "'.$seesid.'" return ID(n) as columnName');
		// $queryC = new Everyman\Neo4j\Cypher\Query($this->client, 'match (n) where n.id = "'.$seesid.'" return count(n) as columnName');
		// $result = $transaction->addStatements($queryC, true);
		// foreach($result as $row){
			// if($row['columnName'] > 0){
				// $transaction = $this->client->beginTransaction();
				// $queryB = new Everyman\Neo4j\Cypher\Query($this->client, 'match (n) where n.id = "'.$seesid.'" return ID(n) as nodeName');
				// $resultB = $transaction->addStatements($queryB, true);
				// foreach($resultB as $rowb){
					// //print_r($rowb['nodeName']); exit();
					// $arthur = $this->client->getNode($rowb['nodeName']);
				// }
				// $arthur->removeProperty('id')
					// ->removeProperty('name')
					// ->removeProperty('pic')
					// ->setProperty('id',$seesid)
					// ->setProperty('name',$sessname)
					// ->setProperty('pic',$sesspic)
				    // ->save();
			// }
			// else {
			// $arthur = $this->client->makeNode()
						// ->setProperty('id',$seesid)
						// ->setProperty('name',$sessname)
						// ->setProperty('pic',$sesspic)
						// ->save();
			// }
		// }
		// $arthurId = $arthur->getId();
		// $transaction = $this->client->beginTransaction();
		// $queryA = new Everyman\Neo4j\Cypher\Query($this->client, 'match (n) where n.id = "'.$segVal.'" return count(n) as columnName');
		// $resultA = $transaction->addStatements($queryA, true);
		// foreach($resultA as $row){
		// if($row['columnName'] > 0){
				// $transaction = $this->client->beginTransaction();
				// $queryD = new Everyman\Neo4j\Cypher\Query($this->client, 'match (n) where n.id = "'.$segVal.'" return ID(n) as nodeName');
				// $resultD = $transaction->addStatements($queryD, true);
				// foreach($resultD as $rowb){
					// //print_r($rowb['nodeName']); exit();
					// $earth = $this->client->getNode($rowb['nodeName']);
				// }
				// $earth->removeProperty('id')
					// ->removeProperty('name')
					// ->removeProperty('pic')
					// ->setProperty('id',$segVal)
					// ->setProperty('name',$segValName)
					// ->setProperty('pic',$userPic)
				    // ->save();
			// }
		// else {
			// $earth = $this->client->makeNode()
						// ->setProperty('id',$segVal)
						// ->setProperty('name',$segValName)
						// ->setProperty('pic',$userPic)
						// ->save();
			// }
		// }
		// $earthId = $earth->getId();
		// $nodeOne = $this->client->getNode($arthurId);
		// $nodeTwo = $this->client->getNode($earthId);
// 		
		// $myLabel = $this->client->makeLabel('Ragausers');
		// $myOtherLabel = $this->client->makeLabel('Ragausers');
// 		
		// $labelsOne = $nodeOne->addLabels(array($myLabel));
		// $labelsTwo = $nodeTwo->addLabels(array($myOtherLabel));
// 		
		// $arthur->relateTo($earth, 'HIRED')->save();
		// redirect($this->agent->referrer());
	// }
// 	
        // function followers(){
            // if($this->uri->segment(3) != ''){
                // $uriGot = $this->uri->segment(3);
            // }
            // else {
                // $uriGot = $this->session->userdata('user_id');
            // }
			// $transaction = $this->client->beginTransaction();
			// $queryC = new Everyman\Neo4j\Cypher\Query($this->client, 'MATCH (n)-[:HIRED]->(x) WHERE x.id = "'.$uriGot.'" RETURN n');
			// $result = $transaction->addStatements($queryC, true);
			// $data = array( "result" => $result );
            // $this->load->view('app/followers', $data); 
        // }
// 		
// 		
		// function cancelhiring(){
            // if(!$this->session->userdata('user_id')){
                // $this->session->set_flashdata('messageError', 'Please LogIn to perform Operations');
                // redirect($this->agent->referrer());
            // }
			// $getIdtoDelNode = base64_decode($this->uri->segment(3));
			// $transaction = $this->client->beginTransaction();
			// $queryC = new Everyman\Neo4j\Cypher\Query($this->client, 'match (n)-[:HIRED]->(x) where n.id = "'.$this->session->userdata('user_id').'" and x.id = "'.$getIdtoDelNode.'" DETACH DELETE x');
			// $result = $transaction->addStatements($queryC, true);
			// redirect($this->agent->referrer());
		// }
// 		
        
        function facebookLikeCount($url){
            // Query in FQL
            $fql  = "SELECT share_count, like_count, comment_count ";
            $fql .= " FROM link_stat WHERE url = '$url'";

            $fqlURL = "https://api.facebook.com/method/fql.query?format=json&query=" . urlencode($fql);

            // Facebook Response is in JSON
            $response = file_get_contents($fqlURL);
            return json_decode($response);
        }
        

        function sa(){
            if($this->uri->segment(3) != ''){
                $uriGot = $this->uri->segment(3);
            }
            else {
                $uriGot = $this->session->userdata('user_id');
            }
            $data['getAllAudios'] = $this->Profilemodel->getAudios($uriGot);
            @$this->db->free_db_resource();
            $data['getAllVideos'] = $this->Profilemodel->getVideos($uriGot);
            $this->load->view('app/songsalbum', $data); 
        }
        
        function pg(){
            if($this->uri->segment(3) != ''){
                $uriGot = $this->uri->segment(3);
            }
            else {
                $uriGot = $this->session->userdata('user_id');
            }
            $data['getPhotos'] = $this->Profilemodel->getProfilePhotos($uriGot);
            $this->load->view('app/photogallery', $data); 
        }
        function se(){
            if($this->uri->segment(3) != ''){
                $uriGot = $this->uri->segment(3);
            }
            else {
                $uriGot = $this->session->userdata('user_id');
            }
            $data['getEvents'] = $this->Profilemodel->getShowsEventsData($uriGot,$show_ev_id=0);
            $this->load->view('app/showsEventsPreview', $data); 
        }
        
        function ads(){
            if($this->uri->segment(3) != ''){
                $uriGot = $this->uri->segment(3);
            }
            else {
                $uriGot = $this->session->userdata('user_id');
            }
            $data['getAds'] = $this->Profilemodel->getAdsData($uriGot,$ad_id=0,$cat='',$off=0,$lim=100);
            $this->load->view('app/adsPreview', $data);
        }
        
        
        function hoverCards(){
            $data['idtemp'] = $this->uri->segment(3);
            $this->load->view('app/hoverCards', $data);
        }
        
        
        //Save Basic Details
        function savebasic(){

            //latlong Modification
            $latlng = $this->input->post('userlatlng');
            $latlng = substr($latlng,1,-1);
            $newlatlng = explode(',',$latlng);
            
            $lat = $newlatlng[0];
            $exLat = explode('.',$lat);
            $actlat = substr($exLat[1], 0, 4);           
            $concatedLat = $exLat[0].'.'.$actlat;
            
            $lng = $newlatlng[1];
            $exLng = explode('.',$lng);
            $actlng = substr($exLng[1], 0, 4);           
            $concatedLng = $exLng[0].'.'.$actlng;            

            //Preparing Data
            $data = array(
                            'rm_id' => $this->input->post('hiddenval'),
                            'mobile_no_alt' => trim($this->input->post('mobile_no_alt')),
                            'full_name' => trim($this->input->post('full_name')),
                            'age' => trim($this->input->post('age')),
                            'gender' => trim($this->input->post('gender')),
                            'user_category' => trim($this->input->post('usr_type')),
                            'user_category_1' => trim($this->input->post('user_category_1')),
                            'category_other' => trim($this->input->post('category_other')),
                            'email' => trim($this->input->post('email')),
                            'mobile_no' => trim($this->input->post('mobile_no')),
                            'city_name' => trim($this->input->post('city_name')),
                            'country_name' => trim($this->input->post('country_name')),
                            'facebook_url' => trim($this->input->post('facebook_url')),
                            'facebook_like_cnt' => 0,
                            'gmail_id' => trim($this->input->post('gmail_id')),
                            'twitter_id' => trim($this->input->post('twitter_id')),
                            'website_addr' => trim($this->input->post('website_addr')),
                            'loc_lat' => floatval($concatedLat),
                            'loc_lang' => floatval($concatedLng),
                            'address' => trim($this->input->post('address')),
                            'locality' => trim($this->input->post('locality')),
                            'pincode' => trim($this->input->post('pincode')),
                            'group_id' => '0',
                            'profile_pic_url' => '',
                            'background_pic_url' => '',
                            'page_position' =>1
                        );
            //print_r($data); exit();
            try{
                $this->Profilemodel->postBasicProfileData($data);
                $this->session->set_flashdata('messageError', 'Basic Profile Saved Successfully');
                redirect($this->agent->referrer());
            } catch (Exception $ex) {
                
                print_r(show_error($message, $status_code, $heading = 'An Error Was Encountered'));
            }
        }
        
        
        function deletePhoto(){
            $photoId = base64_decode($this->input->post('photoid'));
            $picurl = base64_decode($this->input->post('photourl'));
            //Preparing the data
            $data = array(
                            'all_photos' => 0,
                            'rm_id_in' => $this->session->userdata('user_id'),
                            'photo_id_in' => $photoId
                        );
            $this->Profilemodel->deleteSelectedPhotos($data);
            unlink("uploads/50_".$picurl);
            unlink("uploads/100_".$picurl);
            unlink("uploads/".$picurl);
            $this->session->set_flashdata('messageError', 'Photo Deleted Successfully');
            redirect($this->agent->referrer());
        }
        
               
        function postprofilepic(){
            $picUrl = $this->cdnImage['cdnimage'].$this->input->post('picurl');
            $data = array(
                            'photo_id_in' => $this->input->post('picid'),
                            'rm_id' => $this->session->userdata('user_id'),
                            'main_pic' => 1,
                            'background_pic' => 0,
                            'allow_view' => 1,
                            'pic_url' => $picUrl,
                            'pic_privacy' => 1, 
                            'pic_height' => 100,
                            'pic_width' => 100
                    );
            $this->Profilemodel->insertPhotos($data);
            $this->session->set_flashdata('messageError', 'Update successfully');
            redirect('profile/profileedit/photo');
        }
        
        function postbackpic(){
            $data = array(
                            'photo_id_in' => $this->input->post('picid'),
                            'rm_id' => $this->session->userdata('user_id'),
                            'main_pic' => 0,
                            'background_pic' => 1,
                            'allow_view' => 1,
                            'pic_url' => $this->input->post('picurl'),
                            'pic_privacy' => 1, 
                            'pic_height' => 100,
                            'pic_width' => 100
                    );
            $this->Profilemodel->insertPhotos($data);
            $this->session->set_flashdata('messageError', 'Update successfully');
            redirect('profile/profileedit/photo');
        }
        
        function audioupload(){
            $session_ids = 1;
            $valid_formats = array("mp3", "mp4","wav");
                $imagename = $_FILES['file']['name'];
                $size = $_FILES['file']['size'];
                $tmp = $_FILES['file']['tmp_name'];
                if(strlen($imagename))
                {
                    $ext = strtolower($this->getExtension($imagename));
                    if(in_array($ext,$valid_formats))
                    {
                            $bucketname="ragamix";
                            $fileObjUri = "ragamix/Test-Audio-File/";
                            if (!class_exists('S3'))require_once('S3.php');

                            //AWS access info
                            if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJ3XYGL6LANED6ABA');
                            if (!defined('awsSecretKey')) define('awsSecretKey', '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');

                            //instantiate the class
                            $s3 = new S3(awsAccessKey, awsSecretKey);

                            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                            $actual_image_name = time().$session_ids.".".$ext;
                            
                                    if($s3->putObjectFile($tmp, $bucketname , $fileObjUri.$actual_image_name, S3::ACL_PUBLIC_READ) )
                                    {
                                        //Insert upload image files names into user_uploads table
                                        //Preparing Data
                                        $data = array(
                                                        'serial_no_in' => 0,
                                                        'rm_id_in' => $this->input->post('hiddenval'),
                                                        'audio_url' => $this->cdnAudio['cdnaudio'].$actual_image_name,
                                                        'audio_title' => $this->input->post('title'), 
                                                        'size' => $size,
                                                        'duration' => 0.0,
                                                        'allow_download' => 1,
                                                        'audio_type' => 'audio'
                                                );
                                        $this->Profilemodel->insertAudio($data);
                                        $this->session->set_flashdata('messageError', 'Audio Uploaded Successfully');
                                        redirect($this->agent->referrer());
                                    }
                                else
                                    echo "File Upload Failed"; 
                                }
                            else
                                echo "Invalid file format.."; 
                            }
                        else
                            echo "Please select Audio..!";
                            exit;
                    }
                    
        function addVideo(){
            //$youTubeFormatedData = substr($this->input->post('videourl'), strpos($this->input->post('videourl'), "=") + 1);
            $data = array(
                            'serial_no_in' => 0,
                            'rm_id_in' => $this->input->post('hiddenval'),
                            'youtube_url' => $this->input->post('videourl'),
                            'youtube_title' => $this->input->post('yttile'),
                            'video_desc' => $this->input->post('videotitle'),
                            'pic_height' => $this->input->post('ytpicheight'),
                            'pic_width' => $this->input->post('ytpicwidth'), 
                            'view_count' => $this->input->post('viewcounter'),
                            'like_count' => $this->input->post('likecounter'),
                            'dislike_count' => $this->input->post('dislikescounter')
                    );
                    $this->Profilemodel->insertVideo($data);
                    $this->session->set_flashdata('messageError', 'Video Uploaded Successfully');
                    redirect($this->agent->referrer());
        }              
     
        function deleteAudio(){
            $audioid = base64_decode($this->input->post('audioid'));
            //Preparing the data
            $data = array(
                            'all_audios' => 0,
                            'rm_id_in' => $this->input->post('hiddenval'),
                            'serial_no_in' => $audioid
                        );
            $this->Profilemodel->deleteSelectedAudio($data);
            $this->session->set_flashdata('messageError', 'Audio Deleted Successfully');
            redirect($this->agent->referrer());
        }
        
        function deleteVideo(){
            $vidid = base64_decode($this->input->post('vidid'));
            //Preparing the data
            $data = array(
                            'all_videos' => 0,
                            'rm_id_in' => $this->input->post('hiddenval'),
                            'serial_no_in' => $vidid
                        );
            $this->Profilemodel->deleteSelectedVideo($data);
            $this->session->set_flashdata('messageError', 'Video Deleted Successfully');
            redirect($this->agent->referrer());
        }
        
        
        function saveMusicProfile(){
            //Filtering Genre
            $getGenreValuesConcat = implode(',',  $this->input->post('genre'));
            //Filtering Strings
            $getStringsInstrumentConcat = implode(',', $this->input->post('strins'));
            //Filtering Reed
            $getReedInstrumentConcat = implode(',', $this->input->post('reedinstr'));
            //Filtering Wind
            $getWindInstrumentConcat = implode(',', $this->input->post('windinstr'));
            //Filtering Percussions
            $getPecussionsConcat = implode(',', $this->input->post('percussions'));
            //Filtering Vocals
            $getVocalsConcat = implode(',', $this->input->post('vocal'));

//            print_r($this->input->post('hiddenval'));exit();
            $data = array(
                            'rm_id_in' => $this->input->post('hiddenval'),
                            'user_desc' => htmlspecialchars($this->input->post('user_desc'),ENT_QUOTES),
                            'experience_in_years' => trim($this->input->post('experience_in_years')),
                            'studio_band_member' => trim($this->input->post('studio_band_member')),
                            'commitment_lvl' => trim($this->input->post('commitment_lvl')),
                            'freelance_professional' => trim($this->input->post('freelance_professional')),
                            'facebook_url' => 'https://www.facebook.com',
                            'equipment_owned' => trim($this->input->post('equipment_owned')),
                            'genre' => $getGenreValuesConcat,
                            'genre_other' => trim($this->input->post('genre_other')),
                            'music_influence_desc' => htmlspecialchars($this->input->post('music_influence_desc'),ENT_QUOTES),
                            'stringinstr' => $getStringsInstrumentConcat,
                            'stringinstr_other' => trim($this->input->post('stringinstr_other')),
                            'reedinstr' => $getReedInstrumentConcat,
                            'reedinstr_other' => '',
                            'percussions' => $getPecussionsConcat,
                            'percussions_other' => '',
                            'windinstr' => $getWindInstrumentConcat,
                            'windinstr_other' => '',
                            'vocal' => $getVocalsConcat,
                            'vocal_other' => trim($this->input->post('vocal_other')),
                            'band_type' => trim($this->input->post('band_type')),
                            'band_desc' => htmlspecialchars($this->input->post('band_desc'),ENT_QUOTES),
                            'educator_type' => trim($this->input->post('educator_type')),
                            'educator_desc' => htmlspecialchars($this->input->post('educator_desc'),ENT_QUOTES),
                            'educator_certification' => trim($this->input->post('educator_certification'))
                    );
//            print_r($data); exit();
            $this->Profilemodel->postMusicProfileData($data);
            $this->session->set_flashdata('messageError', 'Music Profile Updated Successfully');
            redirect($this->agent->referrer());
        }
        
        function seekRequirement(){
            try{
                $getFormSegment = $this->uri->segment(3);
                if($getFormSegment == 1){
                    if($this->input->post('serial_no_in') != 0){
                        $this->session->set_flashdata('messageError', 'There was an error while posting..Please try again');
                        redirect($this->agent->referrer());
                    }
                $data = array(
                                'serial_no_in' => trim($this->input->post('serial_no_in')),
                                'rm_id_in' => $this->session->userdata('user_id'),
                                'keyword' => trim($this->input->post('keyword')),
                                'location' => trim($this->input->post('location')),
                                'description' => htmlspecialchars($this->input->post('description'),ENT_QUOTES),
                                'hashtags' => trim($this->input->post('hashtags'))
                        );
                }
                else if($getFormSegment == 2) {
                $data = array(
                                'serial_no_in' => trim($this->input->post('serial_no_in')),
                                'rm_id_in' => $this->session->userdata('user_id'),
                                'keyword' => trim($this->input->post('keyword')),
                                'location' => trim($this->input->post('location')),
                                'description' => htmlspecialchars($this->input->post('description'),ENT_QUOTES),
                                'hashtags' => trim($this->input->post('hashtags'))
                        );
                }
                else {
                    $this->session->set_flashdata('messageError', 'There was an error while posting..Please try again');
                    redirect($this->agent->referrer()); 
                }
                $lastId = $this->Profilemodel->postSeekRequirements($data);
                if(!empty($lastId)){
                    $this->session->set_flashdata('messageError', 'Requirement Posted Successfully');
                    redirect($this->agent->referrer());
                }
                else {
                    $this->session->set_flashdata('messageError', 'There was an error while posting..Please try again');
                    redirect($this->agent->referrer());
                }
            } catch (Exception $ex) {
                print_r(show_error($message, $status_code, $heading = 'An Error Was Encountered'));
            }
        }
        
        function preview(){
            $body_data['type'] = $_GET['t'];
            $body_data['linkType'] = $_GET['p'];
            $body_data['baseDirect'] = base64_decode($_GET['basedirect']);
            $base_url = base_url();
		 
            //basic info for the header
            $layout_data['pageTitle'] = "Profile Preview";
            $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
            $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
            $layout_data['meta_url'] = "$base_url";
            $layout_data['meta_classification'] = "home";
            $body_data['previewData'] = $this->Profilemodel->getPreviewData($this->session->userdata('user_id'));
            $latlong = array();
            $latlong[0]=$body_data['previewData'][0]->loc_lat;
            $latlong[1]=$body_data['previewData'][0]->loc_lang;
            $implodeLoc = implode(',',$latlong);
                    $this->load->library('googlemaps');

                    $config['center'] = $implodeLoc; //Must be a String
                    $config['zoom'] = '15';
                    $this->googlemaps->initialize($config);

                    $marker = array();
                    $marker['position'] = $implodeLoc;
                    $this->googlemaps->add_marker($marker);
                    $body_data['map'] = $this->googlemaps->create_map();
            
            $layout_data['content_body'] = $this->load->view('app/preview', $body_data, true);
		 
            $this->load->view($this->data['layoutmode'], $layout_data);
        }
        
        function referrer(){
            $getReferrer = base64_decode($_GET['returnbase']);
            redirect($getReferrer);
        }
        
     
        function addShowsEvents(){
            $path = "eventsimage/";
            $base_url = base_url();
            $session_ids = 1;
            $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
                $imagename = $_FILES['file']['name'];
                $size = $_FILES['file']['size'];
                $tmp = $_FILES['file']['tmp_name'];
                if(strlen($imagename))
                {
                    $ext = strtolower($this->getExtension($imagename));
                    if(in_array($ext,$valid_formats))
                    {
                        if($size<(1024*1024)) // Image size max 1 MB
                        {
                            $bucketname="ragamix";
                            $fileObjUri = "ragamix/all-prod/";
                            if (!class_exists('S3'))require_once('S3.php');

                            //AWS access info
                            if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJ3XYGL6LANED6ABA');
                            if (!defined('awsSecretKey')) define('awsSecretKey', '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');

                            //instantiate the class
                            $s3 = new S3(awsAccessKey, awsSecretKey);

                            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                            $actual_image_name = time().$session_ids.".".$ext;
                            
                                    if($s3->putObjectFile($tmp, $bucketname , $fileObjUri.$actual_image_name, S3::ACL_PUBLIC_READ) )
                                    {
                                        $data = array(
                                                        'event_ad_id_in' => 0,
                                                        'rm_id' => $this->input->post('hiddenval'),
                                                        'category' => $this->input->post('category'),
                                                        'event_location' => $this->input->post('event_location'),
                                                        'event_city' => $this->input->post('event_city'),
                                                        'event_date' => $this->input->post('event_date'),
                                                        'event_title' => htmlspecialchars($this->input->post('event_title'),ENT_QUOTES),
                                                        'event_desc' => htmlspecialchars($this->input->post('event_desc'),ENT_QUOTES),
                                                        'doc_youtube_url' => $this->input->post('doc_youtube_url'),
                                                        'event_approved' => 0,
                                                        'page_position' => 0,
                                                        'photo_id_in' => 0,
                                                        'pic_url' => $this->cdnEvents['cdnevents'].$actual_image_name,
                                                        'pic_privacy' => 1,
                                                        'pic_height' => 100,
                                                        'pic_width' => 100
                                                );
//                                        print_r($data); exit();
                                        $lastEventsId = $this->Profilemodel->insertEvents($data);
                                        $this->session->set_flashdata('messageError', 'Event Posted Successfully');
                                        redirect($this->agent->referrer());
                                    }
                                    else
                                        echo "failed";
                                    }
                                else
                                    echo "Image file size max 1 MB"; 
                                }
                            else
                                echo "Invalid file format.."; 
                            }
                        else
                            echo "Please select image..!";
                            exit;
        }
        
        
        function editEvents(){
            $data['evid'] = base64_decode($this->uri->segment(3));
            $countryVal = 'india';
            $data['cityName'] = $this->Profilemodel->getCityAsArray($countryVal);
            @$this->db->free_db_resource();
            $data['updateData'] = $this->Profilemodel->getEventsForUpdates($this->session->userdata('user_id'),$data['evid']);
            $this->load->view('ajaxcontent/editEvents', $data);
        }
        
        function editEventsShows(){
            if(!empty($_FILES['file']['name'])){
            $session_ids = 1;
            $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
                $imagename = $_FILES['file']['name'];
                $size = $_FILES['file']['size'];
                $tmp = $_FILES['file']['tmp_name'];
                if(strlen($imagename))
                {
                    $ext = strtolower($this->getExtension($imagename));
                    if(in_array($ext,$valid_formats))
                    {
                        if($size<(1024*1024)) // Image size max 1 MB
                        {
                            $bucketname="ragamix";
                            $fileObjUri = "ragamix/all-prod/";
                            if (!class_exists('S3'))require_once('S3.php');

                            //AWS access info
                            if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJ3XYGL6LANED6ABA');
                            if (!defined('awsSecretKey')) define('awsSecretKey', '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');

                            //instantiate the class
                            $s3 = new S3(awsAccessKey, awsSecretKey);

                            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                            $actual_image_name = time().$session_ids.".".$ext;
                            
                                    if($s3->putObjectFile($tmp, $bucketname , $fileObjUri.$actual_image_name, S3::ACL_PUBLIC_READ) )
                                    {
                                        $picUrl = $this->cdnEvents['cdnevents'].$actual_image_name;
                                    }
                                    else
                                        echo "failed";
                                    }
                                else
                                    echo "Image file size max 1 MB"; 
                                }
                            else
                                echo "Invalid file format.."; 
                            }
                        else
                            echo "Please select image..!";
                            
            }
            else {
                    $picUrl = $this->input->post('pic_url');
            }
            $data = array(
                            'event_ad_id_in' => $this->input->post('event_ad_id_in'),
                             'rm_id' => $this->session->userdata('user_id'),
                            'category' => $this->input->post('category'),
                            'event_location' => $this->input->post('event_location'),
                            'event_city' => $this->input->post('event_city'),
                            'event_date' => $this->input->post('event_date'),
                            'event_title' => htmlspecialchars($this->input->post('event_title'),ENT_QUOTES),
                            'event_desc' => htmlspecialchars($this->input->post('event_desc'),ENT_QUOTES),
                            'doc_youtube_url' => $this->input->post('doc_youtube_url'),
                            'event_approved' => 0,
                            'page_position' => 1,
                            'photo_id_in' => $this->input->post('photo_id_in'),
                            'pic_url' => $picUrl,
                            'pic_privacy' => 1,
                            'pic_height' => 100,
                            'pic_width' => 100
                    );
                    //print_r($data); exit();
                    $lastEventsId = $this->Profilemodel->insertEvents($data);
                    $this->session->set_flashdata('messageError', 'Event Updated Successfully');
                    redirect('profile/profileEdit/showevents');
        }
        
        function deleteEvents(){
            $evid = base64_decode($this->input->post('evid'));
            $photoid = base64_decode($this->input->post('photo_id'));
            $this->Profilemodel->delEvents($evid,$photoid);
            $this->session->set_flashdata('messageError', 'Event Deleted Successfully');
            redirect($this->agent->referrer());
        }
        
        function addAds(){
            $session_ids = 1;
            $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
                $imagename = $_FILES['file']['name'];
                $size = $_FILES['file']['size'];
                $tmp = $_FILES['file']['tmp_name'];
                if(strlen($imagename))
                {
                    $ext = strtolower($this->getExtension($imagename));
                    if(in_array($ext,$valid_formats))
                    {
                        if($size<(1024*1024)) // Image size max 1 MB
                        {
                            $bucketname="ragamix";
                            $fileObjUri = "ragamix/all-prod/";
                            if (!class_exists('S3'))require_once('S3.php');

                            //AWS access info
                            if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJ3XYGL6LANED6ABA');
                            if (!defined('awsSecretKey')) define('awsSecretKey', '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');

                            //instantiate the class
                            $s3 = new S3(awsAccessKey, awsSecretKey);

                            $s3->putBucket($bucketname, S3::ACL_PUBLIC_READ);
                            $actual_image_name = time().$session_ids.".".$ext;
                            
                                    if($s3->putObjectFile($tmp, $bucketname , $fileObjUri.$actual_image_name, S3::ACL_PUBLIC_READ) )
                                    {
                                        $data = array(
                                                        'ad_id_in' => 0,
                                                        'rm_id' => $this->input->post('hiddenval'),
                                                        'category' => $this->input->post('category'),
                                                        'ads_title' => $this->input->post('ads_title'),
                                                        'ads_desc' => $this->input->post('ads_desc'),
                                                        'ads_views' => 0,
                                                        'ads_approved' => 0,
                                                        'enable_ads' => 1,
                                                        'ads_status' => '',
                                                        'page_position' => 0,
                                                        'photo_id_int' => 0,
                                                        'pic_url' => $this->cdnAds['cdnads'].$actual_image_name,
                                                        'pic_privacy' => 1,
                                                        'pic_height' => 100,
                                                        'pic_width' => 100
                                                );
                                        //print_r($data); exit();
                                        $lastAdsId = $this->Profilemodel->insertAds($data);
                                        $this->session->set_flashdata('messageError', 'Ad EventPosted Successfully');
                                        redirect($this->agent->referrer());
                                    }
                                    else
                                        echo "failed";
                                    }
                                else
                                    echo "Image file size max 1 MB"; 
                                }
                            else
                                echo "Invalid file format.."; 
                            }
                        else
                            echo "Please select image..!";
                            exit;
        }
        
        
        function conversationwindow(){
            if(!$this->session->userdata('user_id')){
                $this->session->set_flashdata('messageError', 'Please LogIn to perform Operations');
                redirect($this->agent->referrer());
            }
		$base_url = base_url();
		 
		//basic info for the header
		 $layout_data['pageTitle'] = "Messages - Ragamix";
		 $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
		 $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
		 $layout_data['meta_url'] = "$base_url";
		 $layout_data['meta_classification'] = "messages";                   
                 
		 $layout_data['content_body'] = $this->load->view('app/conversationwindow', $body_data, true);

		$this->load->view($this->data['layoutmode'], $layout_data);
        }

        function allmessages(){
                     $data['sids'] = $segMentedId = $_GET['selId'];
                     $data['getMessages'] = $this->Profilemodel->getPrivateMessages($this->session->userdata('user_id'),$segMentedId);
                     $data['userName'] = $this->Profilemodel->getPrivateMessagesOtherName($segMentedId); //Bad Coding
                     $this->load->view('ajaxcontent/allmessages', $data);
                     
        }
        
        function sendmessagefromprofile(){
            $toData = $this->input->post("toid");
            $fromData = $this->session->userdata('user_id');
            $contentData = $this->input->post("body_text");
            $data = array(
                            'from_rm_id' => $fromData,
                            'to_rm_id' => $toData,
                            'subject_line' => '',
                            'body_text' => $contentData,
                            'view_type' => 'View',
                            'mail_direction' => 0,
                            'mail_timestamp' => date('Y-m-d H:i:s'),
                            'has_attachment' => 0,
                            'has_read' => 0,
                            'from_email_id' => '',
                            'to_email_id' => '',
                            'from_user' => '',
                            'to_user' => ''
                    );
            $this->Profilemodel->postMessage($data);
            $this->session->set_flashdata('messageError', 'Message Sent Successfully');
            redirect($this->agent->referrer());
        }
    }
?>
