<?php defined('BASEPATH') OR exit('No direct script access allowed');
class OauthLogin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                $fb_config = array(
                                    'appId'  => '1162483030462581',
                                    'secret' => '0b2601dac0da430039f8c6caad4fa16f'
                        );

                $this->load->library('facebook', $fb_config);
                $this->load->model('Postmodel');
	}

        
        function fblogin(){
                $dta['test'] = $this->facebook->getLoginUrl(array('redirect_uri' => site_url('oauthLogin/flogin'),'scope' => array("email")));
                redirect($dta['test']);
        }
        
    //TEST

    public function test(){

        exit();

    }

	public function flogin()
	{
	    $user = "";
	    $userId = $this->facebook->getUser();
        if ($userId) {
            try {
                $user = $this->facebook->api('/me?fields=name,email');
            } catch (FacebookApiException $e) {
                $user = "";
            }
        }
        else {
            echo 'Please try again.'; exit;
        }
        if($user!=""){
                $dataFB = array(
                                'user_name_in' => $user['id'],
                                'password_in' => '',
                                'fb_login' => 1
                    );
                $data['retVal'] = $this->Postmodel->postLoginDetails($dataFB);
                //print_r($data['retVal']); exit();
                if($data['retVal'][0]->rm_out_param > 0 && $data['retVal'][0]->active_out_param == 1){
                    //Assign Session Variables
                    $this->session->set_userdata(
                                                    array(
                                                            'user_id' => $data['retVal'][0]->rm_out_param,
                                                            'user_name' => $data['retVal'][0]->user_name_param,
                                                    )
                                            );


                    redirect($this->agent->referrer());
                }
                else {
                    //redirect('welcome/index/roadBlock/'.base64_encode($user['email']).'/'.base64_encode($user['id']).'/XyZ');
                    //SIGNUP the User
                    $actKey = uniqid();
                    $data = array(
                        'user_cat_in' => '',
                        'user_name_in' => $user['email'],
                        'facebook_id_in' => $user['id'],
                        'password_in' => '',
                        'email_in' => $user['email'],
                        'mobile_no_in' => '',
                        'city_name_in' => '',
                        'country_name_in' => 'India',
                        'view_count' => 0,
                        'user_type_in' => 0,
                        'signup_date' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s'),
                        'activation_key_in' => $actKey
                    );

                    $rmIdVal = $this->Postmodel->getSignUp($data);
                    if($rmIdVal > 0){
                        $this->activateLoginThroughFB($user['email'], $actKey);
                    }
                }
                
            
        }
        else{
                $dta['test'] = $this->facebook->getLoginUrl(array('redirect_uri' => site_url('oauthLogin/flogin'),'scope' => array("email")));
                redirect($dta['test']);
        }
        
    }
    
    function activateLoginThroughFB($userEmail, $actKey){
        $data = array(
            'activation_key_in' => $actKey
        );
        $this->Postmodel->updateActivation($data);
        $this->userLoggedInThroughFB($userEmail);
        //redirect('welcome');
    }

    function userLoggedInThroughFB($userEmail){
        $dataLog = array(
            'user_name_in' => $userEmail,
            'password_in' => '',
            'fb_login' => 1
        );
        $data['retVal'] = $this->Postmodel->postLoginDetails($dataLog);
        //print_r($data['retVal']); exit();
        if($data['retVal'][0]->rm_out_param > 0 && $data['retVal'][0]->active_out_param == 1){
            //Assign Session Variables
            $this->session->set_userdata(
                array(
                    'user_id' => $data['retVal'][0]->rm_out_param,
                    'user_name' => $data['retVal'][0]->user_name_param,
                )
            );


            redirect($this->agent->referrer());
        }
    }

}