<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Ajaxcontent extends CI_Controller {
    function __construct(){
        parent::__construct(); // needed when adding a constructor to a controller
            $this->load->model('Profilemodel');
            $this->appType = array('apptype' => $this->config->item('app_type'));
        } 
    function msgcontent(){
        $this->load->model('Postmodel');
        $data['msg_cnt'] = $this->Postmodel->getMsgCnt($this->session->userdata('user_id'));
        $this->load->view('app/messages', $data);
    }
    
    
    function headerpoints(){
        $idVal = 0;
        $data['userTypeList'] = $this->Profilemodel->getUsertypeLists($idVal);
        @$this->db->free_db_resource();
        $countryVal = 'india';
        $data['cityName'] = $this->Profilemodel->getCityNameOnCountry($countryVal);
        $this->load->view('ajaxcontent/headerpoints',$data);
    }
    
    function msgnotification(){
        $data['countMsg'] = $this->Profilemodel->getMessagesLimitTen($this->session->userdata('user_id'));
        $this->load->view('ajaxcontent/messagenotsblock', $data);
    }
    
    function getuserslist(){
        $q = $this->input->post('searchword');
        $data['obs'] = $this->Profilemodel->fetchAllUsers($q);
        $this->load->view('app/messagelisters', $data);
        
    }
    
    function sendmessage(){
        $toData = $this->input->post("courseboxid");
        $fromData = $this->input->post("contentsender");
        $contentData = $this->input->post("content");
        $data = array(
                        'from_rm_id' => $fromData,
                        'to_rm_id' => $toData,
                        'subject_line' => '',
                        'body_text' => $contentData,
                        'view_type' => 'View',
                        'mail_direction' => 0,
                        'mail_timestamp' => date('Y-m-d H:i:s'),
                        'has_attachment' => 0,
                        'has_read' => 0,
                        'from_email_id' => '',
                        'to_email_id' => '',
                        'from_user' => '',
                        'to_user' => ''
                );
        $this->Profilemodel->postMessage($data);
        echo "<li>$contentData</li>";
    }
    
    function moremessage(){
        $lastMsgId = $this->input->post('lastmsg');
        $data['countMsg'] = $this->Profilemodel->getMoreMessages($lastMsgId);
        $this->load->view('ajaxcontent/messagenotsblock', $data);
    }
    
    function moreprivatemessage(){
        $lastMsgId = $this->input->post('lastmsg');
        $data['sids'] = $segId = $this->input->post('segId');
        $data['getMoreMessages'] = $this->Profilemodel->getMorePrivateMessages($lastMsgId, $this->session->userdata('user_id'), $segId);
        $data['userName'] = $this->Profilemodel->getPrivateMessagesOtherName($segId); //Bad Coding
        $this->load->view('ajaxcontent/allmessagespaginateview', $data);
    }
    
    function getmessageusers(){
        $data['messageusers'] = $this->Profilemodel->getMessageUsers($this->session->userdata('user_id'));
        $this->load->view('ajaxcontent/messageusers', $data);
    }
    
    function gettrendingvideoslist(){
        $category = 'Musician';
        $city = '';
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getTrendingVideos($category,$position=0,$items=5,$city,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/5); //Where 10 is the items per page
        $this->load->view('ajaxcontent/gettrendingvideolist', $data);
    }
    
    function getactualtrendingvideoslist(){
        $category = 'Musician';
        $page_number = $this->input->post('page');
        $items = 5;
        $city = '';
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getTrendingVideos($category,$position,$items,$city,$rowcnt);
        $data['trendingVideos'] = $data['records']['records'];
        $this->load->view('ajaxcontent/getactualtrendingvideolist',$data);
    }
    
    function gettrendingaudiolist(){
        $category = 'Musician';
        $city = '';
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getTrendingAudio($category,$position=0,$items=6,$city,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/6); //Where 10 is the items per page
        $this->load->view('ajaxcontent/gettrendingaudiolist', $data);
    }
    
    function getactualtrendingaudiolist(){
        $category = 'Musician';
        $page_number = $this->input->post('page');
        $items = 6;
        $city = '';
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getTrendingAudio($category,$position,$items,$city,$rowcnt);
        $data['trendingAudio'] = $data['records']['records'];
        $this->load->view('ajaxcontent/getactualtrendingaudiolist',$data);
    }
    
    function getmusicianinarea(){
        $category = 'Musician';
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getMusicianArea($category,$position=0,$items=20,$city,$rowcnt);
        $data['totC'] = $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getmusicianinarea', $data);
    }
    
    function getactualmusicianarealist(){
        $category = 'Musician';
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 20;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getMusicianArea($category,$position,$items,$city,$rowcnt);
        $data['trendingMusicians'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualmusicianinarea',$data);
    }
    
    function getscheduledevents(){
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getScheduledEvents($position=0,$items=5,$city,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getscheduledevents', $data);
    }
    
    function getactualscheduledevents(){
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 5;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getScheduledEvents($position,$items,$city,$rowcnt);
        $data['scheduledEvents'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualscheduledevents',$data);
    }
    
    function getspotlightnewsbits(){
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getSpotlight($position=0,$items=20,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getspotlightnewsbits', $data);
    }
    
    function getactualspotlightnewsbits(){
        $page_number = $this->input->post('page');
        $items = 20;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getSpotlight($position,$items,$rowcnt);
        $data['spotNews'] = $data['records']['records'];        
        $this->load->view('ajaxcontent/getactualspotlightnewsbits',$data);
    }
    
    function getglobalevents(){
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getGlobalEvents($position=0,$items=5,$city,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getglobalevents', $data);
    }
    
    function getactualglobalevents(){
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 5;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getGlobalEvents($position,$items,$city,$rowcnt);
        $data['globalEvents'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualglobalevents',$data);
    }
    
    function getreviews(){
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getReviews($position=0,$items=10,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getreviews', $data);
    }
    
    function getactualreviews(){
        $page_number = $this->input->post('page');
        $items = 10;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getReviews($position,$items,$rowcnt);
        $data['reviews'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualreviews',$data);
    }
    
    function getbandsarea(){
        $category = 'Band';
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getMusicianArea($category,$position=0,$items=20,$city,$rowcnt);
        $data['totC'] = $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getbandarea', $data);
    }
    
    function getactualbandarea(){
        $category = 'Band';
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 20;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getMusicianArea($category,$position,$items,$city,$rowcnt);
        $data['trendingMusicians'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualbandarea',$data);
    }
    
    function gettrendingvideosband(){
        $category = 'Band';
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getTrendingVideos($category,$position=0,$items=5,$city,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/5); //Where 10 is the items per page
        $this->load->view('ajaxcontent/gettrendingvideosband', $data);
    }
    
    function getactualtrendingvideosband(){
        $category = 'Band';
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 5;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getTrendingVideos($category,$position,$items,$city,$rowcnt);
        $data['trendingVideos'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualtrendingvideosband',$data);
    }
    
    function gettrendingaudioband(){
        $category = 'Band';
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getTrendingAudio($category,$position=0,$items=5,$city,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/5); //Where 10 is the items per page
        $this->load->view('ajaxcontent/gettrendingaudioband', $data);
    }
    
    function getactualtrendingaudioband(){
        $category = 'Band';
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 5;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getTrendingAudio($category,$position,$items,$city,$rowcnt);
        $data['trendingAudio'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualtrendingaudioband',$data);
    }
    
    function getsingersarea(){
        $category = 'Singer';
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getMusicianArea($category,$position=0,$items=20,$city,$rowcnt);
        $data['totC'] = $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getsingersarea', $data);
    }
    
    function getactualsingersarea(){
        $category = 'Singer';
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 20;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getMusicianArea($category,$position,$items,$city,$rowcnt);
        $data['trendingMusicians'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualsingersarea',$data);
    }
    
    function gettrendingvideossingers(){
        $category = 'Singer';
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getTrendingVideos($category,$position=0,$items=5,$city,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/5); //Where 10 is the items per page
        $this->load->view('ajaxcontent/gettrendingvideossingers', $data);
    }
    
    function getactualtrendingvideossingers(){
        $category = 'Singer';
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 5;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getTrendingVideos($category,$position,$items,$city,$rowcnt);
        $data['trendingVideos'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualtrendingvideossingers',$data);
    }
    
    function gettrendingaudiosingers(){
        $category = 'Singer';
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getTrendingAudio($category,$position=0,$items=5,$city,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/5); //Where 10 is the items per page
        $this->load->view('ajaxcontent/gettrendingaudiosingers', $data);
    }
    
    function getactualtrendingaudiosingers(){
        $category = 'Singer';
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 5;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getTrendingAudio($category,$position,$items,$city,$rowcnt);
        $data['trendingAudio'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualtrendingaudiosingers',$data);
    }
    
    function getfeaturedmusician(){
        $category = 'Musician';
        $data['featuredMusicianData'] = $this->Profilemodel->getFeaturedMusician($category, $this->appType['apptype']);
        $this->load->view('ajaxcontent/getfeaturedmusician', $data);
    }
    
    function getfeaturedsingers(){
        $category = 'Singer';
        $data['featuredSingersData'] = $this->Profilemodel->getFeaturedMusician($category, $this->appType['apptype']);
        $this->load->view('ajaxcontent/getfeaturedsingers', $data);
    }
    
    function getfeaturedbands(){
        $category = 'Band';
        $data['featuredBandsData'] = $this->Profilemodel->getFeaturedMusician($category, $this->appType['apptype']);
        $this->load->view('ajaxcontent/getfeaturedbands', $data);
    }
    
    function getfeaturedevents(){
        $data['featuredEventsData'] = $this->Profilemodel->getFeaturedEvents($this->appType['apptype']);
        $this->load->view('ajaxcontent/getfeaturedevents', $data);
    }
    
    function getglobaleventshomepage(){
        $rowcnt = 0;
        $data['records'] = $this->Profilemodel->getGlobalEventsHp($position=0,$items=5,$city='',$rowcnt);
        $data['globalEvents'] = $data['records']['records'];
        $this->load->view('ajaxcontent/getglobaleventshomepage',$data);
    }
    
    function getspotlightnewsbitshomepage(){
        $rowcnt = 0;              
        $data['records'] = $this->Profilemodel->getSpotlight($position=0,$items=5,$rowcnt);
        $data['spotNews'] = $data['records']['records'];
        $this->load->view('ajaxcontent/getnewsbitshomepage',$data);
    }
    
    function getadshomepage(){
        $rowcnt = 0;              
        $data['records'] = $this->Profilemodel->getWantedAds($rm_id=0,$ad_id_in=0,$category='',$position=0,$items=5);
        $data['getAdsHome'] = $data['records']['records'];
        $this->load->view('ajaxcontent/getadshomepage',$data);
    }
    
    function gethomepagereviews(){
        $items = 10;
        $rowcnt = 0;               
        $data['records'] = $this->Profilemodel->getReviews($position=0,$items,$rowcnt);
        $data['reviews'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/gethomepagereviews',$data);
    }
    
    function getfeaturedads(){
        $data['featuredAds'] = $this->Profilemodel->getFeaturedAds($this->appType['apptype']);
        $this->load->view('ajaxcontent/getfeaturedads', $data);
    }
    
    function getwantedads(){
        $category = 'Wanted';
        $thearray = $this->Profilemodel->getWantedAds($rm_id=0,$ad_id_in=0,$category,$position=0,$items=5);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/5); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getwantedads', $data);
    }
    
    function getactualwantedads(){
        $category = 'Wanted';
        $page_number = $this->input->post('page');
        $items = 5;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getWantedAds($rm_id=0,$ad_id_in=0,$category,$position,$items);
        $data['getAdsHome'] = $data['records']['records'];
        $this->load->view('ajaxcontent/getadswantedpage',$data);
    }
    
    function getbuysellads(){
        $category = 'Buy/Sell';
        $thearray = $this->Profilemodel->getWantedAds($rm_id=0,$ad_id_in=0,$category,$position=0,$items=20);
        $data['totC'] = $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getbuysellads', $data);
    }
    
    function getactualbuysellads(){
        $category = 'Buy/Sell';
        $page_number = $this->input->post('page');
        $items = 18;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getWantedAds($rm_id=0,$ad_id_in=0,$category,$position,$items);
        $data['getBuySell'] = $data['records']['records'];
        $this->load->view('ajaxcontent/getactualbuysellads',$data);
    }
    
    function getadsoffer(){
        $category = 'Offers';
        $thearray = $this->Profilemodel->getWantedAds($rm_id=0,$ad_id_in=0,$category,$position=0,$items=20);
        $data['totC'] = $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getadsoffer', $data);
    }
    
    function getactualadsoffer(){
        $category = 'Offers';
        $page_number = $this->input->post('page');
        $items = 18;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getWantedAds($rm_id=0,$ad_id_in=0,$category,$position,$items);
        $data['getOffers'] = $data['records']['records'];
        $this->load->view('ajaxcontent/getactualadsoffer',$data);
    }
    
    function gettopspots(){
        $items = 6;           
        $data['records'] = $this->Profilemodel->getTopSpots($position=0,$items);
        $data['getSpots'] = $data['records']['records'];
        $this->load->view('ajaxcontent/gettopspots',$data);
    }
    
    function getfeaturedprofilehome(){   
        //To be used when featued profiles get completed from the admin side
        $data['getFeaturedProfile'] = $this->Profilemodel->getFeaturedProfileHome();
        //Temporary Views
        //$data['getFeaturedProfile'] = $this->Profilemodel->getFeaturedMusician($category='Musician');
        $this->load->view('ajaxcontent/getfeaturedprofilehome',$data);
    }
    
    function searchcontainer(){
        $countryVal = 'india';
        $data['cityName'] = $this->Profilemodel->getCityNameOnCountry($countryVal);
        $this->load->view('ajaxcontent/searchcontainer',$data);
    }
    
    function getinterviews(){
        $items = 10;
        $rowcnt = 0;               
        $data['records'] = $this->Profilemodel->getInterviews($position=0,$items,$rowcnt);
        $data['interviews'] = $data['records']['records'];
        //print_r($data['records']); exit();
        $this->load->view('ajaxcontent/getinterviewshome',$data);
    }
	
	/*function gethappening(){
	$this->load->model('Postmodel');
	$datacounter = array(
				'from_date' => date('Y-m-d', strtotime('-7 days')),
				'to_date' => date('Y-m-d'),
				'time_interval' => 0
		);
		$data['retVal'] = $this->Postmodel->get_all_user_activity($datacounter);
print_r($data['retVal']); exit();
		$this->load->view('ajaxcontent/happeningbits',$data);
	}*/
	
	function updateswall(){
		$prevdate = date('Y-m-d',strtotime("-2 days"));
		$currdate = date('Y-m-d');
		$timeInterval = 0;
        $data['records'] = $this->Profilemodel->getUpdatesWall($prevdate,$currdate,$timeInterval);
        //print_r($data['records']); exit();
        $this->load->view('ajaxcontent/updates',$data);
	}
	
    function getvideoslistonly(){
        $category = 'Musician';
        $city = '';
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getTrendingVideos($category,$position=0,$items=20,$city,$rowcnt);
        $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/5); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getvideolistonly', $data);
    }
	
    function getactualvideoslistonly(){
        $category = 'Musician';
        $page_number = $this->input->post('page');
        $items = 18;
        $city = '';
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page
               
        $data['records'] = $this->Profilemodel->getTrendingVideos($category,$position,$items,$city,$rowcnt);
        $data['trendingVideos'] = $data['records']['records'];
        $this->load->view('ajaxcontent/getactualvideolistonly',$data);
    }

    function getMiddlewareSeeking(){
//        $rmid = '';
//        $rowcnt = 0;
//        $thearray = $this->Profilemodel->getSeekingData($rmid,$position=0,$items=0,$rowcnt);
//        $getTotalRecords = $thearray['count'];
//        $data['total_pages'] = ceil($getTotalRecords/5); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getMiddlewareSeeking');
    }

    function getSeeking(){
        $rmid = '';
        $page_number = $this->input->post('page');
        $items = 6;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page

        $data['records'] = $this->Profilemodel->getSeekingData($rmid,$position,$items,$rowcnt);
        $getTotalRecords = $data['records']['count'];
        $data['total_pages'] = ceil($getTotalRecords/5);
        $data['getSeekingData'] = $data['records']['records'];

//        print_r($data['getSeekingData']);exit();
        $this->load->view('ajaxcontent/getSeeking',$data);
    }

	function seekdetails(){
	$this->load->library('user_agent');
	if(!$this->agent->is_mobile()){
//		$data['err'] = "This is not a mobile device. Links from <i>http://m.ragamix.com/</i> should be opened in mobile / tab devices.";

        $seg=$this->uri->segment(3);
        redirect('http://www.ragamix.com/seek/seekdetails/'.$seg);
	}
            $this->data = array(
                                'layoutmode' => $this->config->item('layoutconfigdev')
            );
		/*$data['sid'] = @$_GET['serial_no'];
		$data['rmid'] = @$_GET['rm_id'];
		$data['keyword'] = @$_GET['keyword'];
		$data['location'] = @$_GET['location'];
		$data['description'] = @$_GET['description'];
		$data['hashtags'] = @$_GET['hashtags'];
		$data['updated_on'] = @$_GET['updated_on'];
		$data['profile_pic_url'] = @$_GET['profile_pic_url'];
		$data['user_name'] = @$_GET['user_name'];
		$data['city_name'] = @$_GET['city_name'];*/
		$query = $this->db->query("select a.*, b.user_name from seeking a, users b where a.serial_no = ? and a.rm_id = b.rm_id", array('serial_no'=>$this->uri->segment(3)));
		$result = $query->result();

//        print_r($result);exit();
		$data['sid'] = @$result[0]->serial_no;
		$data['rmid'] = @$result[0]->rm_id;
		$data['keyword'] = @$result[0]->keyword;
		$data['location'] = @$result[0]->location;
		$data['description'] = @$result[0]->description;
		$data['hashtags'] = @$result[0]->hashtags;
		$data['updated_on'] = date('Y-m-d', strtotime(@$result[0]->updated_on));
		$data['user_name'] = @$result[0]->user_name;
//		 $layout_data['pageTitle'] = "Ragamix Opportunities";
		 $layout_data['pageTitle'] = @$result[0]->keyword;
		 $layout_data['meta_description'] = substr(@$data['description'], 0, 500)."...";
		 $layout_data['meta_keywords'] = $data['keyword']."".substr($data['description'], 0, 500).@$result[0]->hashtags;
		 $layout_data['meta_url'] = "";
		 $layout_data['meta_classification'] = "Opportunities";
		$layout_data['image'] = base_url()."images/ragamixnewlogo.jpg";
		$data['js_to_load'] = '';
		 $layout_data['content_body'] = $this->load->view('ajaxcontent/seekdetails', $data, true);

		$this->load->view($this->data['layoutmode'], $layout_data);
	}

    function getstudiosarea(){
        $category = 'Studio';
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getMusicianArea($category,$position=0,$items=20,$city,$rowcnt);
        $data['totC'] = $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getstudiosarea', $data);
    }

    function getactualstudiosarea(){
        $category = 'Studio';
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 18;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page

        $data['records'] = $this->Profilemodel->getMusicianArea($category,$position,$items,$city,$rowcnt);
        $data['trendingMusicians'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualstudiosarea',$data);
    }

    function getteachersarea(){
        $category = 'Teacher';
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getMusicianArea($category,$position=0,$items=20,$city,$rowcnt);
        $data['totC'] = $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getteachersarea', $data);
    }

    function getactualteachersarea(){
        $category = 'Teacher';
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 18;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page

        $data['records'] = $this->Profilemodel->getMusicianArea($category,$position,$items,$city,$rowcnt);
        $data['trendingMusicians'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualteachersarea',$data);
    }

    function getschoolsarea(){
        $category = 'Music School';
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $rowcnt = 0;
        $thearray = $this->Profilemodel->getMusicianArea($category,$position=0,$items=20,$city,$rowcnt);
        $data['totC'] = $getTotalRecords = $thearray['count'];
        $data['total_pages'] = ceil($getTotalRecords/20); //Where 10 is the items per page
        $this->load->view('ajaxcontent/getschoolsarea', $data);
    }

    function getactualschoolsarea(){
        $category = 'Music School';
        $page_number = $this->input->post('page');
        if($this->input->post('city') != ''){
            $city = $data['city'] = $this->input->post('city');
        }
        else {
            $city = $data['city'] = '';
        }
        $items = 18;
        $rowcnt = 0;
        //get current starting point of records
        $position = ($page_number * $items); // 10 is the items pe page

        $data['records'] = $this->Profilemodel->getMusicianArea($category,$position,$items,$city,$rowcnt);
        $data['trendingMusicians'] = $data['records']['records'];
        //print_r($data['trendingMusicians']); exit();
        $this->load->view('ajaxcontent/getactualschoolsarea',$data);
    }
       
}
    
?>
