<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
class Admin extends CI_Controller {
    
    function __construct() {
        parent::__construct();
            if(!$this->session->userdata('user_id')){
                $this->session->set_flashdata('messageError', 'Please LogIn to perform Operations');
                redirect($this->agent->referrer());
            }
            $this->load->model('Batchmodel');
            $this->load->model('Profilemodel');
            $this->load->model('Postmodel');

    }
    
    function profileList(){
        $data['q'] = $_GET['q'];
        $data['next'] = $next = $_GET['next'];
        $data['clicks'] = $clicks = $_GET['clicks'];
        $data['page_number'] = $page_number = $_GET['page_number'];
        $data['prev'] = $prev = $_GET['prev'];
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $data['clicks'] = $clicks = $clicks+1;
        $data['page_number'] = $page_number = $clicks;
        if($data['q'] == 1){
            $usercat = 'Musician';
        }
        elseif($data['q'] == 2){
            $usercat = 'Singer';
        }
        elseif($data['q'] == 3){
            $usercat = 'Band';
        }
        elseif($this->uri->segment(3) == 'delete'){

            $data['deleteprof']= $this->Postmodel->delete_profile_info($this->uri->segment(4));
            $this->session->set_flashdata('messageError', 'Deleted...');
            redirect($this->agent->referrer());
//             print_r($this->uri->segment(4));exit();
        }
        else {
            $this->session->set_flashdata('messageError', 'Could not find the resource');
            redirect($this->agent->referrer());
        }
        $data['newrecords'] = $this->Profilemodel->autoCompSearch($completeStringToSearch='',$usercat,$city='',$position, $items=24);
        $data['getAllSearchM'] = $data['newrecords']['records'];
        $this->load->view('admin/profileList', $data);
    }
    
    
}
?>