<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
class Gen extends CI_Controller {
        var $data;

        function __construct(){
            parent::__construct(); // needed when adding a constructor to a controller
            $this->data = array(
                                'layoutmode' => $this->config->item('layoutconfigdev')
            );
            $this->load->model('Profilemodel');
        } 
        
        function tc(){
            $layout_data['pageTitle'] = "Ragamix - Terms and Conditions";
            $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
            $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
            $layout_data['meta_url'] = "$base_url";
            $layout_data['image'] = "".base_url()."images/band.jpg";   
            $layout_data['content_body'] = $this->load->view('app/gen/tc', $body_data, true);	 
            $this->load->view($this->data['layoutmode'], $layout_data); 
        }
        
        function faq(){
            $layout_data['pageTitle'] = "Ragamix - Frequently Asked Questions";
            $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
            $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
            $layout_data['meta_url'] = "$base_url";
            $layout_data['image'] = "".base_url()."images/band.jpg";   
            $layout_data['content_body'] = $this->load->view('app/gen/faq', $body_data, true);	 
            $this->load->view($this->data['layoutmode'], $layout_data); 
        }
        
        function aboutus(){
            $layout_data['pageTitle'] = "About Ragamix";
            $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
            $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
            $layout_data['meta_url'] = "$base_url";
            $layout_data['image'] = "".base_url()."images/band.jpg";   
            $layout_data['content_body'] = $this->load->view('app/gen/aboutus', $body_data, true);	 
            $this->load->view($this->data['layoutmode'], $layout_data); 
        }
}

?>