<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));


class Dashboard extends CI_Controller {

    function __construct(){
        parent::__construct(); // needed when adding a constructor to a controller
        if(!$this->session->userdata('user_id')){
            $this->session->set_flashdata('messageError', 'Please LogIn to perform Operations');
            redirect($this->agent->referrer());
        }
    }

    function view(){
//        print_r($this->session);exit();

        $this->load->view('admin/dashboard');
    }
    function profile(){
        $this->load->view("admin/search_result");
    }

    function usertype(){
        $this->load->model('Postmodel');
        $data['usertypedata'] = $this->Postmodel->get_usertype_list();
//        print_r($data['usertypedata'] );exit();

        if($this->uri->segment(3) == 'edit'){
            for($i=0;$i<sizeof($data['usertypedata']);$i++){
                if($data['usertypedata'][$i]->usr_type_id == $this->uri->segment(4)){
                    $data['editusertypeid'] = $data['usertypedata'][$i]->usr_type_id;
                    $data['editusertypename'] = $data['usertypedata'][$i]->usr_type;
                }
            }
            //print_r($data['editvocalid']); print_r($data['editvocalname']);exit();
        }
//        print_r($data);exit();
        $this->load->view("admin/usertype",$data);
    }

    function usertype_save_del(){

        if($this->input->post('insert') != '') {

            $this->load->model('Postmodel');
            $hello=$this->input->post('usrtypeid');
            $datalist= array(
                'usr_type_id' => $hello,
                'usr_type' => $this->input->post('usrtype')

            );

//        print_r($datalist);exit();
            $data['usertypedata'] = $this->Postmodel->save_update_usertype($datalist);
            redirect("dashboard/usertype");
        }
        elseif($this->input->post('delete') != ''){

            $this->load->model('Postmodel');
            $abc=$this->input->post('usertypeType[]');
            $r=count($abc);

            if($abc==null){
                redirect("dashboard/usertype");
            }
            else {
                for($i=0;$i<$r;$i++){
// print_r($abc[$i]);exit();
                    $datalist = array(
                        'usr_type_id' => $abc[$i],
                    );
//        print_r($data);exit();
                    $data['usertypedel'] = $this->Postmodel->delete_usertype_info($datalist);
                }
                redirect("dashboard/usertype");
//        print_r($abc);exit();
            }
        }
    }


    function genre(){
        $this->load->model('Postmodel');
        $data['genredata'] = $this->Postmodel->get_genre_list();
//        print_r($data['genredata'] );exit();

        if($this->uri->segment(3) == 'edit'){
            for($i=0;$i<sizeof($data['genredata']);$i++){
                if($data['genredata'][$i]->genre_id == $this->uri->segment(4)){
                    $data['editgenreid'] = $data['genredata'][$i]->genre_id;
                    $data['editgenrename'] = $data['genredata'][$i]->genre;
                }
            }
            //print_r($data['editvocalid']); print_r($data['editvocalname']);exit();
        }
        $this->load->view("admin/genre",$data);
    }

    function genre_save_del(){

        if($this->input->post('insert') != '') {
//            print_r("hello inside save");
//            exit();

            $this->load->model('Postmodel');
            $hello=$this->input->post('genreid');
            $data = array(
                'genre_id' => $hello,
                'genre' => $this->input->post('genre')

            );

//        print_r($data);exit();
            $data['genredata'] = $this->Postmodel->save_update_genre($data);

//        print_r($data['genredata']);exit();
            redirect("dashboard/genre");
        }

        elseif($this->input->post('delete') != ''){
//            print_r("hello inside delete");
//            exit();
            $this->load->model('Postmodel');
            $abc=$this->input->post('genreType[]');
            $r=count($abc);

            if($abc==null){
                redirect("dashboard/genre");
            }
            else {
                for($i=0;$i<$r;$i++){
// print_r($abc[$i]);exit();
                    $data = array(
                        'genre_id' => $abc[$i],
                    );
//        print_r($data);exit();
                    $data['genredel'] = $this->Postmodel->delete_genre_info($data);
                }
                redirect("dashboard/genre");
//        print_r($abc);exit();
            }
        }
    }


    function vocal(){
        $this->load->model('Postmodel');
        $data['vocaldata'] = $this->Postmodel->get_vocal_list ();
        if($this->uri->segment(3) == 'edit'){
            for($i=0;$i<sizeof($data['vocaldata']);$i++){
                if($data['vocaldata'][$i]->vocal_id == $this->uri->segment(4)){
                    $data['editvocalid'] = $data['vocaldata'][$i]->vocal_id;
                    $data['editvocalname'] = $data['vocaldata'][$i]->vocal;
                }
            }
            //print_r($data['editvocalid']); print_r($data['editvocalname']);exit();
        }

        $this->load->view("admin/vocal",$data);
    }

    function vocal_save_del(){

        if($this->input->post('insert') != '') {

            $this->load->model('Postmodel');
            $hello=$this->input->post('vocalid');
            $data = array(
                'vocal_id' => $hello,
                'vocal' => $this->input->post('vocal')

            );
//        print_r($data);exit();
            $data['vocaldata'] = $this->Postmodel->save_update_vocal($data);

//        print_r($data['genredata']);exit();
            redirect("dashboard/vocal");
        }

        elseif($this->input->post('delete') != ''){

            $this->load->model('Postmodel');
            $abc=$this->input->post('vocalType[]');
            $r=count($abc);

            if($abc==null){
                redirect("dashboard/vocal");

            }
            else {

                for($i=0;$i<$r;$i++) {
                    $data = array(
                        'vocal_id' => $abc[$i],
                    );

                    $data['vocaldel'] = $this->Postmodel->delete_vocal_info($data);
                }
                redirect("dashboard/vocal");
            }
        }
        else{
//            print_r("hello inside edit");
//            print_r($this->input->post('edit'));
//            exit();

            $abc=$this->input->post('edit');
//            $abc=0;
//                            print_r($abc);exit();


            redirect("dashboard/vocal/".$abc);

//            $this->load->view("admin/vocal",$abc);
//        print_r($abc);exit();
        }
    }


    function reedinstrument(){

        $this->load->model('Postmodel');
        $data['reedinstrdata'] = $this->Postmodel->get_reed_list ();
        if($this->uri->segment(3) == 'edit'){
            for($i=0;$i<sizeof($data['reedinstrdata']);$i++){
                if($data['reedinstrdata'][$i]->reedinstr_id == $this->uri->segment(4)){
                    $data['editreedinstrid'] = $data['reedinstrdata'][$i]->reedinstr_id;
                    $data['editreedinstrname'] = $data['reedinstrdata'][$i]->reedinstr;
                }
            }
            //print_r($data['editvocalid']); print_r($data['editvocalname']);exit();
        }
        $this->load->view("admin/reedinstrument",$data);
    }

    function reedinstr_save_del(){

        if($this->input->post('insert') != '') {
//            print_r("hello inside save");
//            exit();

            $this->load->model('Postmodel');
            $hello=$this->input->post('reedinstrid');
            $data = array(
                'reedinstr_id' => $hello,
                'reedinstr' => $this->input->post('reedinstr')
            );

//        print_r($data);exit();
            $data['reedinstrdata'] = $this->Postmodel->save_update_reedinstr($data);

//        print_r($data['genredata']);exit();
            redirect("dashboard/reedinstrument");
        }

        elseif($this->input->post('delete') != ''){
//            print_r("hello inside delete");
//            exit();
            $this->load->model('Postmodel');
            $abc=$this->input->post('reedinstrType[]');
            $r=count($abc);

            if($abc==null){
                redirect("dashboard/reedinstrument");

//                print_r("The value is null");exit();
            }
            else{
                for($i=0;$i<$r;$i++) {
                    $data = array(
                        'reedinstr_id' => $abc[$i],
                    );

                    $data['reedinstrdel'] = $this->Postmodel->delete_reedinstr_info($data);
                }
                redirect("dashboard/reedinstrument");
            }

        }

    }

    function rolesright(){

        $this->load->model('Postmodel');
        if($this->input->post('save') != ''){

            function gen_uid($l){
                return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, $l);
            }


            $selgroup=$this->input->post('selgroup');
            $mca=$this->input->post('textsel');
            $bca=$this->input->post('usertypeo');
            $heavy=$this->input->post('heavy[]');
            $count=count($heavy);
            $insertassign=$this->input->post('insertassign');
//
////            print_r($bca);

            if($insertassign=='insert'){
                $abc=gen_uid(9);
                $datalist = array(
                    'group_id' => $abc,
                    'group_name' => $mca,
                    'group_type' => $bca,
                );
//                print_r($datalist);
//                exit();

                $data['rolesave'] = $this->Postmodel->add_user_group($datalist);
                redirect("dashboard/rolesright");
            }
            if($insertassign=='assign') {
                for($i=0;$i<$count;$i++) {
                    $datalist = array(
                        'group_id' => $selgroup,
                        'group_type' => $bca,
                        'rm_id' => $heavy[$i],
                    );
//                print_r($datalist);
//                exit();

                    $data['rolesave'] = $this->Postmodel->set_user_group($datalist);
                }
                redirect("dashboard/rolesright");
            //            print_r($data);
            //            exit();
            }

        }
        elseif($this->input->post('result') != '') {

            $abc=$this->input->post('selval');
//            print_r($abc);exit();

            $data = array(
                'group_id' => $abc
            );
//            print_r($data);exit();
//            $data['rolesrightdata'] = $this->Postmodel->get_all_user_groups ();
            $data['roles'] = $this->Postmodel->get_assigned_users($data);
            @$this->db->free_db_resource();
            $data['rolesrightdata'] = $this->Postmodel->get_all_user_groups();
            @$this->db->free_db_resource();
            $data['rolesuserdata'] = $this->Postmodel->get_users_for_group();

//        print_r($data);exit();
            $this->load->view("admin/rolesright",$data);
        }
        else{
//            print_r("Inside else");exit();
            $data['rolesrightdata'] = $this->Postmodel->get_all_user_groups();
            @$this->db->free_db_resource();
            $data['rolesuserdata'] = $this->Postmodel->get_users_for_group();

//        print_r($data);exit();
            $this->load->view("admin/rolesright",$data);

        }


    }

    function rolesright_save_del(){

        $this->load->model('Postmodel');

//        print_r("Inside rolesright");
        $abc=$this->input->post('selval');

        $data = array(
            'group_id' => $abc
        );

        $data['roles'] = $this->Postmodel->get_assigned_users($data);
//        print_r($data['roles']);exit();

//        print_r($data['genredata']);exit();

//        $this->load->view("admin/rolesright",$data);
        redirect("dashboard/rolesright");

//        print_r($abc);
//        exit();
    }

    function stringinstrument(){

        $this->load->model('Postmodel');
        $data['stringinstrdata'] = $this->Postmodel->get_stringinstr_list ();
        if($this->uri->segment(3) == 'edit'){
            for($i=0;$i<sizeof($data['stringinstrdata']);$i++){
                if($data['stringinstrdata'][$i]->stringinstr_id == $this->uri->segment(4)){
                    $data['editstringinstrid'] = $data['stringinstrdata'][$i]->stringinstr_id;
                    $data['editstringinstrname'] = $data['stringinstrdata'][$i]->stringinstr;
                }
            }
            //print_r($data['editvocalid']); print_r($data['editvocalname']);exit();
        }
        $this->load->view("admin/stringinstrument",$data);
    }

    function stringinstr_save_del(){

        if($this->input->post('insert') != '') {
//            print_r("hello inside save");
//            exit();

            $this->load->model('Postmodel');
            $hello=$this->input->post('stringinstrid');
            $data = array(
                'stringinstr_id' => $hello,
                'stringinstr' => $this->input->post('stringinstr')
            );

//        print_r($data);exit();
            $data['stringinstrdata'] = $this->Postmodel->save_update_stringinstr($data);

//        print_r($data['genredata']);exit();
            redirect("dashboard/stringinstrument");
        }

        elseif($this->input->post('delete') != ''){
//            print_r("hello inside delete");
//            exit();
            $this->load->model('Postmodel');
            $abc=$this->input->post('stringinstrType[]');
            $r=count($abc);
//        print_r($abc);exit();
            if($abc==null){
                redirect("dashboard/stringinstrument");
            }
            else {

                for($i=0;$i<$r;$i++) {
                    $data = array(
                        'stringinstr_id' => $abc[$i],
                    );

//        print_r($data);exit();
                    $data['stringinstrdel'] = $this->Postmodel->delete_stringinstr_info($data);
                }
                redirect("dashboard/stringinstrument");
//        print_r($abc);exit();
            }
        }

    }

    function windinstrument(){

        $this->load->model('Postmodel');
        $data['windinstrdata'] = $this->Postmodel->get_windinstr_list ();

        if($this->uri->segment(3) == 'edit'){
            for($i=0;$i<sizeof($data['windinstrdata']);$i++){
                if($data['windinstrdata'][$i]->windinstr_id == $this->uri->segment(4)){
                    $data['editwindinstrid'] = $data['windinstrdata'][$i]->windinstr_id;
                    $data['editwindinstrname'] = $data['windinstrdata'][$i]->windinstr;
                }
            }
            //print_r($data['editvocalid']); print_r($data['editvocalname']);exit();
        }
        $this->load->view("admin/windinstrument",$data);
    }

    function windinstr_save_del(){

        if($this->input->post('insert') != '') {

            $this->load->model('Postmodel');
            $hello=$this->input->post('windinstrid');
            $datalist = array(
                'windinstrid' => $hello,
                'windinstr' => $this->input->post('windinstr')
            );

//        print_r($datalist);exit();
            $data['windinstrdata'] = $this->Postmodel->save_update_windinstr($datalist);

//        print_r($data['genredata']);exit();
            redirect("dashboard/windinstrument");
        }

        elseif($this->input->post('delete') != ''){
//            print_r("hello inside delete");
//            exit();
            $this->load->model('Postmodel');
            $abc=$this->input->post('windinstrType[]');
            $r=count($abc);

//        print_r($abc);exit();
            if($abc==null){
                redirect("dashboard/windinstrument");
            }
            else {
                for($i=0;$i<$r;$i++) {
                    $data = array(
                        'windinstr_id' => $abc[$i],
                    );

//        print_r($data);exit();
                    $data['windinstrdel'] = $this->Postmodel->delete_windinstr_info($data);
                }
                redirect("dashboard/windinstrument");
//        print_r($abc);exit();
            }
        }

    }



    function batchprocess(){
        $this->load->view("admin/batchprocess");
    }

    function group_vs_user(){
        $this->load->view("admin/group_vs_user");
    }

    function music(){
        $this->load->view("admin/music");
    }



    /*EVENT START*/
    function events(){
        $this->load->model('Postmodel');
        $rm_id=0;
        $event_ad_id=$this->uri->segment(3);
        $datacounter = array(
            'rm_id' => $rm_id,
            'event_ad_id' => $event_ad_id,
        );

        $data['updateData'] = $this->Postmodel->get_all_events_info($datacounter);
        @$this->db->free_db_resource();
        $data['locationdatafetch'] = $this->Postmodel->getStringLocationInfo();
//        print_r($data['updateData']);exit();

        $this->load->view('admin/editEvent',$data);
    }

    function editEventsadmin(){

        $this->load->model('Postmodel');
        if(!empty($_FILES['file']['name'])){
            $session_ids = 1;
            $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
            $imagename = $_FILES['file']['name'];
            $size = $_FILES['file']['size'];
            $tmp = $_FILES['file']['tmp_name'];
            if(strlen($imagename))
            {
                $ext = strtolower($this->getExtension($imagename));
                if(in_array($ext,$valid_formats))
                {
                    if($size<(1024*1024)) // Image size max 1 MB
                    {
                        $bucketname="ragamix";
                        $fileObjUri = "ragamix/all-prod/";
                        if (!class_exists('S3'))require_once('S3.php');

                        //AWS access info
                        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJ3XYGL6LANED6ABA');
                        if (!defined('awsSecretKey')) define('awsSecretKey', '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');

                        //instantiate the class
                        $s3 = new S3(awsAccessKey, awsSecretKey);

                        $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                        $actual_image_name = time().$session_ids.".".$ext;

                        if($s3->putObjectFile($tmp, $bucketname , $fileObjUri.$actual_image_name, S3::ACL_PUBLIC_READ) )
                        {
                            $picUrl = "http://cdn.ragamix.com/ragamix/all-prod/".$actual_image_name;
                        }
                        else
                            echo "failed";
                    }
                    else
                        echo "Image file size max 1 MB";
                }
                else
                    echo "Invalid file format..";
            }
            else
                echo "Please select image..!";

        }
        else {
            $picUrl = $this->input->post('pic_url');
        }
        $evid = $this->input->post('event_ad_id_in');
        $data = array(
            'event_ad_id_in' => $this->input->post('event_ad_id_in'),
            'rm_id' => $this->input->post('rm_id'),
            'category' => $this->input->post('category'),
            'event_location' => $this->input->post('event_location'),
            'event_city' => $this->input->post('event_city'),
            'event_date' => $this->input->post('event_date'),
            'event_title' => htmlspecialchars($this->input->post('event_title'),ENT_QUOTES),
            'event_desc' => htmlspecialchars($this->input->post('event_desc'),ENT_QUOTES),
            'doc_youtube_url' => $this->input->post('doc_youtube_url'),
            'event_approved' => $this->input->post('approval'),
            'page_position' => 1,
            'photo_id_in' => $this->input->post('photo_id_in'),
            'pic_url' => $picUrl,
            'pic_privacy' => 1,
            'pic_height' => 100,
            'pic_width' => 100
        );
//        print_r($data); exit();
        $lastEventsId = $this->Postmodel->insertEventsadmin($data);
        $this->session->set_flashdata('messageError', 'Event Updated Successfully');
        redirect('dashboard/events/'.$evid);
    }

//    function eventlist(){
//
//        $this->load->model('Postmodel');
//        $rm_id=0;
//        $event_ad_id=0;
//        $datacounter = array(
//            'rm_id' => $rm_id,
//            'event_ad_id' => $event_ad_id,
//        );
//
//        $data['updateData'] = $this->Postmodel->get_all_events_list($datacounter);
//
//        $this->load->view('admin/eventlist',$data);
//
//    }

    function evlist(){
        $this->load->model('Batchmodel');
        $data['q'] = $_GET['q'];
        $data['next'] = $next = $_GET['next'];
        $data['clicks'] = $clicks = $_GET['clicks'];
        $data['page_number'] = $page_number = $_GET['page_number'];
        $data['prev'] = $prev = $_GET['prev'];
        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $data['clicks'] = $clicks = $clicks+1;
        $data['page_number'] = $page_number = $clicks;

        if($data['q'] == 1){
            $data['getEventsList'] = $this->Batchmodel->getAllEventsAdminApproved($position,$items=20,$qu=1);
        }
        else if($data['q'] == 0){
            $data['getEventsList'] = $this->Batchmodel->getAllEventsAdminApproved($position,$items=20,$qu=0);
        }
        else{
            $data['rec'] = $this->Batchmodel->getAllEventsAdmin($position,$items=20,$rowcnt='');
            $data['getEventsList'] = $data['rec']['records'];
        }
        $this->load->view('admin/evlist',$data);
    }
    /*EVENT END*/

    /*AD START*/
    function Ads(){
        $this->load->model('Postmodel');
        $rm_id=0;
        $ad_id=$this->uri->segment(3);
        $category_in=0;
        $offset_val=0;
        $limit_in=0;

        $datacounter = array(
            'rm_id' => $rm_id,
            'ad_id' => $ad_id,
            'category_in' => $category_in,
            'offset_val' => $offset_val,
            'limit_in' => $limit_in,
        );

        $data['updateData'] = $this->Postmodel->get_all_ads_info($datacounter);
//        print_r($data['updateData']);exit();

//        @$this->db->free_db_resource();
//        $data['locationdatafetch'] = $this->Postmodel->getStringLocationInfo();

        $this->load->view('admin/editAds',$data);
    }

    function editadsadmin(){

        $this->load->model('Postmodel');
        if(!empty($_FILES['file']['name'])){
            $session_ids = 1;
            $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
            $imagename = $_FILES['file']['name'];
            $size = $_FILES['file']['size'];
            $tmp = $_FILES['file']['tmp_name'];
            if(strlen($imagename))
            {
                $ext = strtolower($this->getExtension($imagename));
                if(in_array($ext,$valid_formats))
                {
                    if($size<(1024*1024)) // Image size max 1 MB
                    {
                        $bucketname="ragamix";
                        $fileObjUri = "ragamix/all-prod/";
                        if (!class_exists('S3'))require_once('S3.php');

                        //AWS access info
                        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJ3XYGL6LANED6ABA');
                        if (!defined('awsSecretKey')) define('awsSecretKey', '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');

                        //instantiate the class
                        $s3 = new S3(awsAccessKey, awsSecretKey);

                        $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                        $actual_image_name = time().$session_ids.".".$ext;

                        if($s3->putObjectFile($tmp, $bucketname , $fileObjUri.$actual_image_name, S3::ACL_PUBLIC_READ) )
                        {
                            $picUrl = "http://cdn.ragamix.com/ragamix/all-prod/".$actual_image_name;
                        }
                        else
                            echo "failed";
                    }
                    else
                        echo "Image file size max 1 MB";
                }
                else
                    echo "Invalid file format..";
            }
            else
                echo "Please select image..!";

        }
        else {
            $picUrl = $this->input->post('pic_url');
        }
        $adid = $this->input->post('ad_id_in');

        $data = array(
            'ad_id_in' => $adid,
            'rm_id' => $this->input->post('rm_id'),
            'category' => $this->input->post('category'),
            'ads_title' => htmlspecialchars($this->input->post('ads_title'),ENT_QUOTES),
            'ads_desc' => htmlspecialchars($this->input->post('ads_desc'),ENT_QUOTES),
            'ads_views' => $this->input->post('ads_views'),
            'ads_approved' => $this->input->post('approval'),
            'ads_status' => $this->input->post('ads_status'),
            'page_position' => 1,
            'photo_id_int' => $this->input->post('photo_id_int'),
            'pic_url' => $picUrl,
            'pic_privacy' => 1,
            'pic_height' => 100,
            'pic_width' => 100
        );
//        print_r($data); exit();
        $lastEventsId = $this->Postmodel->insertAdsadmin($data);
        $this->session->set_flashdata('messageError', 'Ads Updated Successfully');
        redirect('dashboard/ads/'.$adid);
    }
    /*AD END*/



    function getExtension($str)
    {
        $i = strrpos($str,".");
        if (!$i)
        {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }


    function activity_console(){

        $this->load->model('Postmodel');
//        $time_interval = $this->input->post('go');

        if($this->uri->segment(3)=="event_posted"){
            redirect("dashboard/events/".$this->uri->segment(4));
        }
        elseif($this->uri->segment(3)=="ads"){
            redirect("dashboard/ads/".$this->uri->segment(4));
        }
        elseif($this->uri->segment(3)=="update"){
//            print_r($this->uri->segment(4));exit();
            redirect("profile/profileEdit/basic/".$this->uri->segment(4));
        }


        if($this->input->post('go')!= '') {

            if ($this->input->post('seltime') > 0) {
                $from_date = 0;
                $to_date = 0;
                $time_interval = $this->input->post('seltime');
            } else {
                if (($this->input->post('from_date') == '') || ($this->input->post('to_date') == '')) {
//                    $from_date = '1990-01-01';
//                    $to_date = date("Y-m-d");
                    $from_date = '1990-01-01';
                    $to_date = date("Y-m-d");
                    $time_interval = 0;
                } else {
                    $from_date = $this->input->post('from_date');
                    $to_date = $this->input->post('to_date');
                    $time_interval = 0;

                }

            }
        }

        else {
            $from_date = 0;
            $to_date = 0;
            $time_interval = 4;
        }

            $datacounter = array(
                'from_date' => $from_date,
                'to_date' => $to_date,
                'time_interval' => $time_interval,
            );

        $from_date=date_create($datacounter['from_date']);
//        print_r($datacounter['from_date']);exit();
        $to_date=date_create($datacounter['to_date']);
        $data=array('from_date' => $from_date,'to_date' => $to_date,'time_interval' => $time_interval);

            $data['retVal'] = $this->Postmodel->get_all_user_activity($datacounter);
            @$this->db->free_db_resource();
            $data['abc'] = $this->Postmodel->get_activity_counter($datacounter);
//                    print_r($data['retVal']);exit();


        $this->load->view("admin/activity_console",$data);

    }


    /*LOCATION  START*/
    function  locations(){

        $this->load->model('Postmodel');
        $data['locationdatafetch'] = $this->Postmodel->get_location_info();

        $this->load->library('googlemaps');

        if($this->uri->segment(3) == 'edit'){
            for($i=0;$i<sizeof($data['locationdatafetch']);$i++){
                if($data['locationdatafetch'][$i]->location_id == $this->uri->segment(4)){


                    $lat=$data['locationdatafetch'][$i]->lat;
                    $long=$data['locationdatafetch'][$i]->long;
                    $data['editlocationid'] = $data['locationdatafetch'][$i]->location_id;
                    $data['editlocationcity'] = $data['locationdatafetch'][$i]->city;
                    $data['editlocationcountry'] = $data['locationdatafetch'][$i]->country;
                    $data['editlocationzone'] = $data['locationdatafetch'][$i]->zone;
                    $data['editlocationlat'] = $lat;
                    $data['editlocationlong'] = $long;

                }
            }

            $config['center'] = $lat.','.$long;
            $config['zoom'] = '15';
            $this->googlemaps->initialize($config);

            $marker = array();
            $marker['position'] =  $lat.','.$long;
            $this->googlemaps->add_marker($marker);
            $data['map'] = $this->googlemaps->create_map();
        }
     elseif($this->uri->segment(5) == '1'){
         $adminlat=$this->session->userdata('adminlat');
         $adminlong=$this->session->userdata('adminlong');
            $config['center'] = $adminlat.','.$adminlong;
            $config['zoom'] = '15';
            $this->googlemaps->initialize($config);

            $marker = array();
            $marker['position'] = $adminlat.','.$adminlong;
            $this->googlemaps->add_marker($marker);
            $data['map'] = $this->googlemaps->create_map();

         $data['lat']=$adminlat;
         $data['long']=$adminlong;
        }
        else{

            $config['center'] = '20.5937,78.9629';
            $config['zoom'] = '3';
            $this->googlemaps->initialize($config);

//            $marker = array();
//            $marker['position'] = '22,88';
//            $this->googlemaps->add_marker($marker);
            $data['map'] = $this->googlemaps->create_map();

        }
        $this->load->view("admin/location",$data);
    }


    function location_save_del(){

        $this->load->model('Postmodel');
        if($this->input->post('save') != ''){

            $datalist = array(
                'location_id' =>$this->input->post('location_id'),
                'city' => $this->input->post('city'),
                'country' => $this->input->post('country'),
                'zone' => $this->input->post('zone'),
                'lat' => $this->input->post('lat'),
                'long' => $this->input->post('long'),
            );
            $data['locationdatafetch'] = $this->Postmodel->save_update_location_info($datalist);
            redirect('dashboard/locations/add/0/1');
        }
        elseif($this->input->post('find') != '') {
            if($this->input->post('formval') && $this->input->post('hiddentext')){
                $address1 = $this->input->post('hiddentext');
                $formval = $this->input->post('formval');
                $address = $this->input->post('address');

                $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
                // We convert the JSON to an array
                $geo = json_decode($geo, true);
                // If everything is cool
                if ($geo['status'] = 'OK') {
                    // We set our values
                    $latitude = $geo['results'][0]['geometry']['location']['lat'];
                    $longitude = $geo['results'][0]['geometry']['location']['lng'];
                }

                $latlong = array();
                $latlong[0] = $latitude;
                $latlong[1] = $longitude;

                $this->session->set_userdata(
                    array(
                        'adminlat' => $latlong[0],
                        'adminlong' =>  $latlong[1],
                    )
                );
                redirect("dashboard/locations/".$formval."/".$address1."/1");
            }

    else {

        $address = $this->input->post('address');

        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
        // We convert the JSON to an array
        $geo = json_decode($geo, true);
        // If everything is cool
        if ($geo['status'] = 'OK') {
            // We set our values
            $latitude = $geo['results'][0]['geometry']['location']['lat'];
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
        }

        $latlong = array();
        $latlong[0] = $latitude;
        $latlong[1] = $longitude;

        $this->session->set_userdata(
            array(
                'adminlat' => $latlong[0],
                'adminlong' => $latlong[1],
            )
        );

        redirect("dashboard/locations/add/0/1");
    }

   }
}
    /*LOCATION END*/



    /*FEATURED LIST PROFILE START*/

//    function featuredlist_prof(){
//
//        $this->load->model('Postmodel');
//
//        if($this->input->post('find') != ""){
//            $datacounter = array(
//                'event_title_in' => $this->input->post('event_title_in'),
//                'user_name_in' => $this->input->post('user_name_in'),
//            );
//
//            $data['findprofile'] = $this->Postmodel->get_events_for_featured($datacounter);
////            print_r($data['findevents']);exit();
//        }
//
//        if($this->input->post('add') != ""){
//            $datacounter = array(
//                'event_id' => $this->input->post('eventid'),
//                'event_title' => $this->input->post('eventtitle'),
//                'user_name' => $this->input->post('rmid'),
//                'position' => $this->input->post('position'),
//            );
////                        print_r($datacounterpart);exit();
//
//            $data['addevents'] = $this->Postmodel->add_featured_events($datacounter);
//
//        }
//
//        @$this->db->free_db_resource();
//        $data['featuredprofilelist'] = $this->Postmodel->get_featured_events();
////        print_r($data['featuredeventlist']);exit();
//        $this->load->view("admin/featuredlist_prof",$data);
//    }
    /*FEATURED LIST PROFILE END*/


    /*FEATURED LIST EVENTS START*/
    function featuredlist_events(){

        $this->load->model('Postmodel');

        if($this->uri->segment(3) == "remove"){
            $datacounter = array(
                'featured_id' => $this->uri->segment(4),
            );
            $data['eventsdel'] = $this->Postmodel->delete_featured_events_info($datacounter);
        }

        if($this->input->post('add') != ""){
            $datacounter = array(
                'event_id' => $this->input->post('eventid'),
                'event_title' => $this->input->post('eventtitle'),
                'user_name' => $this->input->post('rmid'),
                'position' => $this->input->post('position')
            );
//            print_r($datacounter);exit();
            $data['addevents'] = $this->Postmodel->add_featured_events($datacounter);

            if($data['addevents'][0]->statusMsg !=""){
                $this->session->set_flashdata('messageError', $data['addevents'][0]->statusMsg);
//                print_r($data['addevents'][0]->statusMsg);exit();
            }

        }

        if($this->input->post('find') != ""){
            $datacounter = array(
                'event_title_in' => $this->input->post('event_title_in'),
                'user_name_in' => $this->input->post('user_name_in'),
            );

            $data['findevents'] = $this->Postmodel->get_events_for_featured($datacounter);
//            print_r($data['findevents']);exit();
        }

        @$this->db->free_db_resource();
        $data['featuredeventlist'] = $this->Postmodel->get_featured_events();
//        print_r($data['featuredeventlist']);exit();
        $this->load->view("admin/featuredlist_events",$data);
    }
    /*FEATURED LIST EVENTS END*/



    /*FEATURED LIST ADS START*/
    function featuredlist_ads(){

        $this->load->model('Postmodel');
        if($this->uri->segment(3) == "remove"){

            $datacounter = array(
                'featured_id' => $this->uri->segment(4),
            );
            $data['adsdel'] = $this->Postmodel->delete_featured_ads_info($datacounter);
        }

        if($this->input->post('find')){
            $datacounter = array(
                'ads_title_in' => $this->input->post('ads_title_in'),
                'user_name_in' => $this->input->post('user_name_in'),
            );
            $data['findads'] = $this->Postmodel-> get_ads_for_featured($datacounter);
        }


        if($this->input->post('add')){
            $datacounter = array(
                'ads_id' => $this->input->post('adsid'),
                'ads_title' => $this->input->post('adstitle'),
                'user_name' => $this->input->post('rmid'),
                'position' => $this->input->post('position'),
            );

            $data['addads'] = $this->Postmodel->add_featured_ads($datacounter);

            if($data['addads'][0]->statusMsg !=""){
                $this->session->set_flashdata('messageError', $data['addevents'][0]->statusMsg);
//                print_r($data['addevents'][0]->statusMsg);exit();
            }
//            print_r($datacounter);exit();
        }

        @$this->db->free_db_resource();
        $data['featuredadslist'] = $this->Postmodel->get_featured_ads();

        $this->load->view("admin/featuredlist_ads",$data);
    }
    /*FEATURED LIST ADS END*/


    /*BASIC PROFILE START */
    function update(){

        $this->load->model('Postmodel');
        $rm_id=$this->uri->segment(3);

        $datacounter = array(
            'rm_id' => $rm_id,
        );

        $data['basicprofiledata'] = $this->Postmodel->get_basic_profile_data($datacounter);
        @$this->db->free_db_resource();
        $data['locationdatafetch'] = $this->Postmodel->getStringLocationInfo();

        $this->load->view('admin/editProfile',$data);
    }

    /*BASIC PROFILE END */


}
?>
