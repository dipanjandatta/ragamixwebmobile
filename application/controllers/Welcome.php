<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
    var $data;

    function __construct(){
        parent::__construct(); // needed when adding a constructor to a controller
        $this->data = array(
                            'layoutmode' => $this->config->item('layoutconfigdev')
        );
        $this->load->model('Profilemodel');
    } 
	public function index()
	{
		$base_url = base_url();
		 
		//basic info for the header
                
		 $layout_data['pageTitle'] = "Ragamix - Musicians' Ultimate Destination";
		 $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
		 $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
		 $layout_data['meta_url'] = "$base_url";
		 $layout_data['meta_classification'] = "home";
		 $body_data['js_to_load'] = base_url()."js/seeking.js";
         $body_data['testimonials'] = $this->Profilemodel->getTestimonials();
         @$this->db->free_db_resource();
         $body_data['homeslider'] = $this->Profilemodel->gethomeSliders($slider_id=0, $slider_type=0, $app_type=1);
         @$this->db->free_db_resource();
		 $layout_data['content_body'] = $this->load->view('app/home', $body_data, true);
		 
		$this->load->view($this->data['layoutmode'], $layout_data);
	}

        
        function login(){
            try{
                $this->load->model('Postmodel');
				$this->load->helper('cookie');
                $item = $this->config->item('app_url');
                $data = array(
                                            'user_name_in' => trim($this->input->post('loginId')),
                                            'password_in' => trim($this->input->post('user_password')),
                                            'fb_login' => 0
                           
                    );

                $data['retVal'] = $this->Postmodel->postLoginDetails($data);
                if($data['retVal'][0]->rm_out_param > 0 && $data['retVal'][0]->active_out_param == 1){
                    if($data['retVal'][0]->user_type_id == 0) {
                        $this->session->set_flashdata('messageError', 'User was blocked by admin');
                        redirect($this->agent->referrer());
                    }
                    else if($data['retVal'][0]->user_type_id == 1){
  					$cookie= array(
      							'name'   => 'user',
      							'value'  => $data['retVal'][0]->user_name_param,
       							'expire' => '86500',
  					);
					$cookieOne = array(
								'name'   => 'user_img',
      							'value'  => $data['retVal'][0]->profile_pic,
       							'expire' => '86500',
					);
  					$this->input->set_cookie($cookie);
					$this->input->set_cookie($cookieOne);
                    //Assign Session Variables
                        $this->session->set_userdata(
                                                    array(
                                                            'user_id' => $data['retVal'][0]->rm_out_param,
                                                            'user_name' => $data['retVal'][0]->user_name_param,
                                                            'user_type_id' => $data['retVal'][0]->user_type_id,
                                                            'user_group_id' => $data['retVal'][0]->user_group_id,
                                                            'profile_pic' => $data['retVal'][0]->profile_pic
                                                    )
                                            );
                    }
                    else if($data['retVal'][0]->user_type_id == 2){
                        $this->session->set_userdata(
                                                    array(
                                                            'user_id' => $data['retVal'][0]->rm_out_param,
                                                            'user_name' => $data['retVal'][0]->user_name_param,
                                                            'user_type_id' => $data['retVal'][0]->user_type_id,
                                                            'user_group_id' => $data['retVal'][0]->user_group_id,
                                                            'profile_pic' => $data['retVal'][0]->profile_pic,
                                                            'premium_flag' => '1'
                                                    )
                                            );
                    }
                    else if($data['retVal'][0]->user_type_id == 3) {
                        $this->session->set_userdata(
                                                    array(
                                                            'user_id' => $data['retVal'][0]->rm_out_param,
                                                            'user_name' => $data['retVal'][0]->user_name_param,
                                                            'user_type_id' => $data['retVal'][0]->user_type_id,
                                                            'user_group_id' => $data['retVal'][0]->user_group_id,
                                                    )
                                            );
                        redirect('dashboard/view');
                    }

                    redirect($this->agent->referrer());

                }
                else{
                    $this->session->set_flashdata('messageError', 'Mismatch in Data. Please Retry');
                    redirect($this->agent->referrer());
                }
            }
            catch (Exception $ex){
                print_r(show_error($message, $status_code, $heading = 'An Error Was Encountered'));
            }
        }
        
        
        function signup(){
            try{
                $this->load->model('Postmodel');
                $actKey = uniqid();
                $data = array(
                                'user_cat_in' => trim($this->input->post('user_cat_in')),
                                'user_name_in' => trim($this->input->post('user_name_in')),
                                'facebook_id_in' => '0',
                                'password_in' => trim($this->input->post('password_in')),
                                'email_in' => trim($this->input->post('email_in')),
                                'mobile_no_in' => trim($this->input->post('mobile_no_in')),
                                'city_name_in' => trim($this->input->post('city_name_in')),
                                'country_name_in' => 'India',
                                'view_count' => 0,
                                'user_type_in' => 0,
                                'signup_date' => date('Y-m-d H:i:s'),
                                'updated_on' => date('Y-m-d H:i:s'),
                                'activation_key_in' => $actKey
                );
                
                $rmIdVal = $this->Postmodel->getSignUp($data);
                if($rmIdVal > 0){

			$this->load->library('MY_PHPMailer');
			$mail = new PHPMailer();
			$mail->IsSMTP(); // we are going to use SMTP
			$mail->SMTPAuth   = true; // enabled SMTP authentication
			$mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
			$mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
			$mail->Port       = 465;                   // SMTP port to connect to GMail
			$mail->Username   = "admin@ragamix.in";  // user email address
			$mail->Password   = "ziplock15";            // password in GMail
			$mail->SetFrom('admin@ragamix.in', 'Admin');  //Who is sending the email
			//$mail->AddReplyTo("admin@ragamix.in","Admin");  //email address that receives the response
			$mail->Subject    = "RagaMix Account Activation";
			$mail->Body      = "<html><head><body>Hello <strong>".trim($this->input->post('user_name_in'))."</strong>..<br /><p>You have been registered successfully. Click below link to activate the account.</p><br /><p><a href='http://m.ragamix.com/welcome/activateLogin?userkey=".$actKey."'>Activate My Account</a></p><br /><p>Best Regards,</p><br /><p>Ragamix Team</p></body></head></html>";
			$mail->SMTPDebug = 2;
			$mail->AltBody    = "Plain text message";
			$destino = trim($this->input->post('email_in')); // Who is addressed the email to
			$mail->AddAddress($destino, $this->input->post('user_name_in'));
			if(!$mail->Send()) {
			    $data["message"] = "Error: " . $mail->ErrorInfo;
			} else {
		            $this->session->set_flashdata('messageError', 'An activation link has been sent to your mail ID.');
		            redirect('welcome');
			}
                }
                else {
                    $this->session->set_flashdata('messageError', 'Ragamix ID or Username already exists..Choose another and sign up');
                    redirect($this->agent->referrer());
                }
            }
            catch (Exception $ex){
                print_r(show_error($message, $status_code, $heading = 'An Error Was Encountered'));
            }
        }
        
        
        function activateLogin(){
            try{
                $this->load->model('Postmodel');
                $getParametricUserKeyValue = $_GET['userkey'];
                $data = array(
                                'activation_key_in' => $getParametricUserKeyValue
                        );
                $this->Postmodel->updateActivation($data);
                $this->session->set_flashdata('messageError', 'Your Account is Activated. Please Login');
                redirect($this->agent->referrer());
            }
            catch (Exception $ex){
                print_r(show_error($message, $status_code, $heading = 'An Error Was Encountered'));
            }
        }
        
        function logout(){
                $this->load->model('Postmodel');
                $this->Postmodel->logOutUser($this->session->userdata('user_id'));
		$this->session->sess_destroy();
		redirect('welcome');
	}

        function sendmail(){
        $this->load->library('MY_PHPMailer');
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "admin@ragamix.in";  // user email address
        $mail->Password   = "ziplock15";            // password in GMail
        $mail->SetFrom('admin@ragamix.in', 'Admin');  //Who is sending the email
        //$mail->AddReplyTo("admin@ragamix.in","Admin");  //email address that receives the response
        $mail->Subject    = "RagaMix Account Activation";
        $mail->Body      = "<html><head><body>Hello Pagla..<br /><p>Your account is to be activated</p></body></head></html>";
        $mail->SMTPDebug = 2;
        $mail->AltBody    = "Plain text message";
        $destino = "subhajit.sc@gmail.com"; // Who is addressed the email to
        $mail->AddAddress($destino, "John Doe");
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
        
            print_r($data["message"]);



        }

	function remtemp(){
		$this->load->helper('cookie');
		$cookie = array(
		    'name'   => 'user',
		    'value'  => '',
		    'expire' => '0',
		);

		$cookieOne = array(
		    'name'   => user_img,
		    'value'  => '',
		    'expire' => '0',
		);
		
		$this->input->set_cookie($cookie);
		$this->input->set_cookie($cookieOne);
		redirect($this->agent->referrer());
	}

	function settings(){
        $base_url = base_url();

        //basic info for the header

        $layout_data['pageTitle'] = "Ragamix - Settings";
        $layout_data['meta_description'] = "Security, Password Change";
        $layout_data['meta_keywords'] = "Security, Password Change";
        $layout_data['meta_url'] = "$base_url";
        $layout_data['meta_classification'] = "Settings";
        $body_data['js_to_load'] = '';
        $layout_data['content_body'] = $this->load->view('app/settings', $body_data, true);

        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function changepassword(){
        if($this->input->post('oldpass') == '') {
            $this->session->set_flashdata('messageError', 'All Fields are mandatory');
            redirect($this->agent->referrer());
        }
        if($this->input->post('newpass') == '') {
            $this->session->set_flashdata('messageError', 'All Fields are mandatory');
            redirect($this->agent->referrer());
        }
        if($this->input->post('cnfpass') == '') {
            $this->session->set_flashdata('messageError', 'All Fields are mandatory');
            redirect($this->agent->referrer());
        }
        $query = $this->db->query("select password from users where rm_id = ?", array('rm_id'=>$this->session->userdata('user_id')));
        $result = $query->result();
        if($result[0]->password != $this->input->post('oldpass')) {
            $this->session->set_flashdata('messageError', 'Old Password Mismatch');
            redirect($this->agent->referrer());
        }
        else {
            if($this->input->post('newpass') == $this->input->post('cnfpass')) {
                $data = array(
                                'password'=>$this->input->post('newpass')
                );
                $this->db->where('rm_id', $this->session->userdata('user_id'));
                $this->db->update('users', $data);
                $this->session->set_flashdata('messageError', 'Password Changed Successfully');
                redirect($this->agent->referrer());
            }
            else {
                $this->session->set_flashdata('messageError', 'New and Confirm Password Does Not Match');
                redirect($this->agent->referrer());
            }
        }
    }


    function chatloginguest(){
        $uname = $this->input->post('uname');
        redirect('http://www.ragamix.com/ragasupportchat/public/user.php?source=nuser&un='.$uname);
    }

}
