<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

class Music extends CI_Controller {
		var $data;
        function __construct(){
            parent::__construct(); // needed when adding a constructor to a controller
            $this->data = array(
                                'layoutmode' => $this->config->item('layoutconfigdev')
            );
        } 
        
        function listen(){
                    $layout_data['pageTitle'] = "Listen to Music, Hindi, English, Regional - Ragamix";
                    $layout_data['meta_description'] = "Listen to Music, Hindi, English, Regional";
                    $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock,hindi, english,listen";
                    $layout_data['meta_url'] = "$base_url";
                $layout_data['content_body'] = $this->load->view('app/listen', $body_data, true);
		 
		$this->load->view($this->data['layoutmode'], $layout_data);
        } 
		
        function videolists(){
                    $layout_data['pageTitle'] = "Watch Videos, Hindi, English, Regional - Ragamix";
                    $layout_data['meta_description'] = "atch Videos, Hindi, English, Regional";
                    $layout_data['meta_keywords'] = "video, lyrics,song,songs,words,hardore,metal,emo,rock,hindi, english,listen";
                    $layout_data['meta_url'] = "$base_url";
                $layout_data['content_body'] = $this->load->view('app/videolists', $body_data, true);
		 
		$this->load->view($this->data['layoutmode'], $layout_data);
        }

	function videodetail(){
		if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $_GET['youtube_url'], $match)) {
			$vid = $match[1];
		}
		$body_data['data_youtube_url'] = $vid;
		$body_data['data_youtube_title'] = $_GET['youtube_title'];
		$body_data['data_viewcount'] = $_GET['view_count'];
		$body_data['data_like_count'] = $_GET['like_count'];
		$body_data['data_uploaded_by'] = $_GET['uploaded_by'];
		$body_data['data_dislike_count'] = $_GET['dislike_count'];
		$body_data['data_user_category'] = $_GET['user_category'];

                    $layout_data['pageTitle'] = "video detail - Ragamix";
                    $layout_data['meta_description'] = "Watch Videos, Hindi, English, Regional";
                    $layout_data['meta_keywords'] = "video, lyrics,song,songs,words,hardore,metal,emo,rock,hindi, english,listen";
                    $layout_data['meta_url'] = "$base_url";
                $layout_data['content_body'] = $this->load->view('app/videodetail', $body_data, true);
		 
		$this->load->view($this->data['layoutmode'], $layout_data);
	}
    }
?>
