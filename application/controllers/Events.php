<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
class Events extends CI_Controller {
        var $data;

        function __construct(){
            parent::__construct(); // needed when adding a constructor to a controller
            $this->data = array(
                                'layoutmode' => $this->config->item('layoutconfigdev')
            );
            $this->load->model('Profilemodel');
        }
    function view(){
        $layout_data['pageTitle'] = "RAGAMIX - Events";
        $layout_data['meta_description'] = "One stop destination dedicated to serve music makers";
        $layout_data['meta_keywords'] = "Events - News - Reviews - Interviews";
        $layout_data['meta_url'] = "$base_url";
        $layout_data['image'] = "".base_url()."images/ragamixnewlogo.jpg";
        $body_data[]='';
        $layout_data['content_body'] = $this->load->view('app/events/eventsview', $body_data, true);
		 
        $this->load->view($this->data['layoutmode'], $layout_data);
    }
        
    function detail(){
        $evid = trim($this->uri->segment(4));
        $catid = trim($this->uri->segment(3));
        $body_data['getParticularEvent'] = $this->Profilemodel->getEventsByIdentity($rm_id_in = 0, $evid);
        $images = $body_data['getParticularEvent'][0]->pic_url;
        $layout_data['pageTitle'] = $body_data['getParticularEvent'][0]->event_title;
        $layout_data['meta_description'] = $body_data['getParticularEvent'][0]->event_desc.",".$body_data['getParticularEvent'][0]->event_location;
        $layout_data['meta_keywords'] = "Events,news,lyrics,song,songs,words,hardore,metal,emo,rock";
        $layout_data['meta_url'] = "$base_url";
        $layout_data['image'] = "$images";
        $address = $body_data['getParticularEvent'][0]->event_location;
        // We get the JSON results from this request
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
        // We convert the JSON to an array
        $geo = json_decode($geo, true);
        // If everything is cool
        if ($geo['status'] = 'OK') {
            // We set our values
            $latitude = $geo['results'][0]['geometry']['location']['lat'];
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
        }
                    
        $latlong = array();
        $latlong[0]=$latitude;
        $latlong[1]=$longitude;
                    
        $implodeLoc = implode(',',$latlong);
                
        $this->load->library('googlemaps');

        $config['center'] = $implodeLoc;
        $config['zoom'] = '15';
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['position'] = $implodeLoc;
        $this->googlemaps->add_marker($marker);
        $body_data['map'] = $this->googlemaps->create_map();
        @$this->db->free_db_resource();
        if($this->uri->segment(3) == 'Newsbits'){
            $items = 5;
            $rowcnt = 0;

            $body_data['records'] = $this->Profilemodel->getSpotlight($position=0,$items,$rowcnt);
            $body_data['spotNews'] = $body_data['records']['records'];
            $body_data['cat'] = 1;
        }
        @$this->db->free_db_resource();
        if($this->uri->segment(3) == 'Reviews'){
            $items = 5;
            $rowcnt = 0;

            $body_data['records'] = $this->Profilemodel->getReviews($position=0,$items,$rowcnt);
            $body_data['reviews'] = $body_data['records']['records'];
            $body_data['cat'] = 2;
        }
                    
                    
        $layout_data['content_body'] = $this->load->view('app/events/details', $body_data, true);
		 
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

}
?>
