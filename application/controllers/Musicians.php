<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
class Musicians extends CI_Controller {
        var $data;

        function __construct(){
            parent::__construct(); // needed when adding a constructor to a controller
            $this->data = array(
                                'layoutmode' => $this->config->item('layoutconfigdev')
            );
            $this->load->model('Profilemodel');
        } 
        function view(){
                    $layout_data['pageTitle'] = "Musican List in - Ragamix";
                    $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
                    $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
                    $layout_data['meta_url'] = "$base_url";
                    $layout_data['image'] = "".base_url()."images/band.jpg";
                    $body_data['js_to_load'] = base_url()."js/musicians.js";
                $layout_data['content_body'] = $this->load->view('app/musicianViewList', $body_data, true);
		 
		$this->load->view($this->data['layoutmode'], $layout_data);
        }
        
        function band(){
                    $layout_data['pageTitle'] = "Bands List in - Ragamix";
                    $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
                    $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
                    $layout_data['meta_url'] = "$base_url";
                    $layout_data['image'] = "".base_url()."images/band.jpg";
                    $body_data['counter'] = $this->Profilemodel->getMusicianArea($category='Band',$position=0,$items=0,$city='',$rowcnt=0);
                    $body_data['js_to_load'] = base_url()."js/bands.js";
                $layout_data['content_body'] = $this->load->view('app/bandViewList', $body_data, true);
		 
		$this->load->view($this->data['layoutmode'], $layout_data);
        }
        
        function singers(){
                    $layout_data['pageTitle'] = "Singers List in - Ragamix";
                    $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
                    $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
                    $layout_data['meta_url'] = "$base_url";
                    $layout_data['image'] = "".base_url()."images/band.jpg";
                    $body_data['js_to_load'] = base_url()."js/singers.js";
                $layout_data['content_body'] = $this->load->view('app/singersViewList', $body_data, true);
		 
		$this->load->view($this->data['layoutmode'], $layout_data);
        }

        function studios(){
            $layout_data['pageTitle'] = "Studios List in - Ragamix";
            $layout_data['meta_description'] = "Studios in India, Kolkata";
            $layout_data['meta_keywords'] = "Recording, Slots Booking";
            $layout_data['meta_url'] = "$base_url";
            $layout_data['image'] = "".base_url()."images/band.jpg";
            $body_data['js_to_load'] = base_url()."js/studios.js";
            $layout_data['content_body'] = $this->load->view('app/studiosviewlist', $body_data, true);

            $this->load->view($this->data['layoutmode'], $layout_data);
        }

        function educators(){
            $layout_data['pageTitle'] = "Educators - Ragamix";
            $layout_data['meta_description'] = "Educators in India, Kolkata";
            $layout_data['meta_keywords'] = "Learn Music, Music, Lessons";
            $layout_data['meta_url'] = "$base_url";
            $layout_data['image'] = "".base_url()."images/band.jpg";
            $body_data['js_to_load'] = base_url()."js/educators.js";
            $layout_data['content_body'] = $this->load->view('app/educatorsview', $body_data, true);

            $this->load->view($this->data['layoutmode'], $layout_data);
        }

        function teachers(){
            $layout_data['pageTitle'] = "Teachers in Educators - Ragamix";
            $layout_data['meta_description'] = "Teachers in India, Kolkata";
            $layout_data['meta_keywords'] = "Learn Music, Music, Lessons";
            $layout_data['meta_url'] = "$base_url";
            $layout_data['image'] = "".base_url()."images/band.jpg";
            $body_data['js_to_load'] = base_url()."js/teachers.js";
            $layout_data['content_body'] = $this->load->view('app/teachersviewlist', $body_data, true);

            $this->load->view($this->data['layoutmode'], $layout_data);
        }

        function schools(){
            $layout_data['pageTitle'] = "Music Schools in Educators - Ragamix";
            $layout_data['meta_description'] = "Music Schools in India, Kolkata";
            $layout_data['meta_keywords'] = "Learn Music, Music, Lessons";
            $layout_data['meta_url'] = "$base_url";
            $layout_data['image'] = "".base_url()."images/band.jpg";
            $body_data['js_to_load'] = base_url()."js/schools.js";
            $layout_data['content_body'] = $this->load->view('app/schoolsviewlist', $body_data, true);

            $this->load->view($this->data['layoutmode'], $layout_data);
        }
                
        function getfullvideo(){
            //$yu = $_GET['mediaset'];
            $data['yu'] = $_GET['mediaset'];
            //$data['videodetails'] = $this->Profilemodel->getFullVideoDetails($yu);
            $this->load->view('ajaxcontent/getfullvideo', $data);
        }
                    
                    
    }
?>
