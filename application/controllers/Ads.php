<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
class Ads extends CI_Controller {
        var $data;

        function __construct(){
            parent::__construct(); // needed when adding a constructor to a controller
            $this->data = array(
                                'layoutmode' => $this->config->item('layoutconfigdev')
            );
            $this->load->model('Profilemodel');
        } 
        function view(){
                    $layout_data['pageTitle'] = "Ads List - Ragamix";
                    $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
                    $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
                    $layout_data['meta_url'] = "$base_url";
                    $layout_data['image'] = "".base_url()."images/band.jpg";
               
                $layout_data['content_body'] = $this->load->view('app/ads/adsview', $body_data, true);
		 
		$this->load->view($this->data['layoutmode'], $layout_data); 
        }
        
        function detail(){
                    $adid = trim($this->uri->segment(3));
                    $body_data['getParticularEvent'] = $this->Profilemodel->getAdsByIdentity($rm_id_in = 0, $adid, $category='', $position=0, $items = 1);
                    $images = $body_data['getParticularEvent']['records'][0]->pic_url;
                    $layout_data['pageTitle'] = "Ads - ".$body_data['getParticularEvent']['records'][0]->ads_title."- ".$body_data['getParticularEvent']['records'][0]->category." - Ragamix";
                    $layout_data['meta_description'] = $body_data['getParticularEvent']['records'][0]->ads_desc;
                    $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
                    $layout_data['meta_url'] = "$base_url";
                    $layout_data['image'] = "$images";                 
                    $body_data['getAdsIdentified'] = $body_data['getParticularEvent']['records'];
                    
                    $body_data['getSomeAds'] = $this->Profilemodel->getAdsByIdentity($rm_id_in = 0, $adid=0, $category='', $position=0, $items = 5);
                    $body_data['getSomeAdsInterest'] = $body_data['getSomeAds']['records'];
                    $layout_data['content_body'] = $this->load->view('app/ads/detail', $body_data, true);
		 
                    $this->load->view($this->data['layoutmode'], $layout_data);
        }
                    
                    
    }
?>
