		$(function() {
			$("#search-txt").keyup(function() {
				$("#video-data-1, #video-data-2").empty();
				var videoid = $("#search-txt").val();
				var matches = videoid.match(/^http:\/\/www\.youtube\.com\/.*[?&]v=([^&]+)/i) || videoid.match(/^http:\/\/youtu\.be\/([^?]+)/i) || videoid.match(/^https:\/\/www\.youtube\.com\/.*[?&]v=([^&]+)/i);
				if (matches) {
					videoid = matches[1];
				}
				if (videoid.match(/^[a-z0-9_-]{11}$/i) === null) {
					$("<p style='color: #F00;'>Unable to parse Video ID/URL.</p>").appendTo("#video-data-1");
					return;
				}
				$.getJSON("https://www.googleapis.com/youtube/v3/videos", {
					key: "AIzaSyBhEsWdKigo4OsNCKBR7ucq3Bh0BvtmaVE",
					part: "snippet,statistics",
					id: videoid
				}, function(data) {
					if (data.items.length === 0) {
						$("<p style='color: #F00;'>Video not found.</p>").appendTo("#video-data-1");
						return;
					}
                                        //Passing the data to php to be submmitted
                                        document.getElementById('viewcounter').value = data.items[0].statistics.viewCount;
                                        document.getElementById('likecounter').value = data.items[0].statistics.likeCount;
                                        document.getElementById('dislikescounter').value = data.items[0].statistics.dislikeCount;
                                        document.getElementById('yttile').value = data.items[0].snippet.title;
                                        document.getElementById('ytpicwidth').value = data.items[0].snippet.thumbnails.medium.width;
                                        document.getElementById('ytpicheight').value = data.items[0].snippet.thumbnails.medium.height;
                                        
					$("<img>", {
						src: data.items[0].snippet.thumbnails.medium.url,
						width: data.items[0].snippet.thumbnails.medium.width,
						height: data.items[0].snippet.thumbnails.medium.height,
                                                class: 'myclass'
					}).appendTo("#video-data-1");
					$("<h1 class='smallhead'></h1>").text(data.items[0].snippet.title).appendTo("#video-data-1");
//					$("<p></p>").text(data.items[0].snippet.description).appendTo("#video-data-1");
//					$("<li></li>").text("Published at: " + data.items[0].snippet.publishedAt).appendTo("#video-data-2");
					$("<li class='yv'></li>").text("Views: " + data.items[0].statistics.viewCount).appendTo("#video-data-1");
//					$("<li></li>").text("Favorite count: " + data.items[0].statistics.favoriteCount).appendTo("#video-data-2");
					$("<li class='yv'></li>").text("Likes: " + data.items[0].statistics.likeCount).appendTo("#video-data-1");
					$("<li class='yv'></li>").text("Dislikes: " + data.items[0].statistics.dislikeCount).appendTo("#video-data-1");
                                        return;
				}).fail(function(jqXHR, textStatus, errorThrown) {
					$("<p style='color: #F00;'></p>").text(jqXHR.responseText || errorThrown).appendTo("#video-data-1");
				});
			});
		});


  $(function() {

      $("#basic").validate({
        rules: {
            city_name: "required",
            country_name: "required",
            full_name: "required",
            user_category: "required",
            latlng: "required",
            mobile_no: {
                required: true,
                digits: true,
                maxlength: 10,
                minlength: 10
            }
        },
        messages: {
            city_name: "City Required",
            country_name: "Country Required",
            full_name: "Name Required",
            user_category: "Category Required",
            latlng: "Location Required",
            mobile_no: {
                required: "Primary No Required",
                digits: "Only Digits allowed",
                minlength: "Minimum 10 digits",
                maxlength: "Maximum 10 digits"
            }
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });
    
      $("#seeker").validate({
        rules: {
            keyword: "required",
            location: "required",
            description: "required",
            hashtags: "required"
        },
        messages: {
            keyword: "Keywords Required",
            location: "Location Required",
            description: "Description Required",
            hashtags: "Hashtags Required"
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });
    
      $("#eventsform").validate({
        rules: {
            category: "required",
            event_date: "required",
            event_title: "required"
        },
        messages: {
            category: "Select a Category",
            event_date: "Date Required",
            event_title: "A Heading Required"
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    }); 
    
      $("#adsform").validate({
        rules: {
            category: "required",
            ads_title: "required",
            ads_desc: "required"
        },
        messages: {
            category: "Select a Category",
            ads_title: "A Title is Required",
            ads_desc: "A Description Required"
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    }); 


  });

        $(function() {

        // Example 1 - ... in one line of code
        $('#basic').areYouSure();
//        $('#musicprof').areYouSure();
        $('#seeker').areYouSure();
        $('#eventsform').areYouSure();
        $('#adsform').areYouSure();
    });
    
$(document).ready(function(e) 
{ 
$('#event_date').dcalendarpicker();
//	$("#uploadimage").on('submit',(function(e) {
//		e.preventDefault();
//		$("#message").empty(); 
//		//$('#loading').show();
//		$.ajax({
//        	url: window.location.origin+"/ragamixreloaded/profile/ajaximage",   	// Url to which the request is send
//			type: "POST",      				// Type of request to be send, called as method
//			data:  new FormData(this), 		// Data sent to server, a set of key/value pairs representing form fields and values 
//			contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
//    	    cache: false,					// To unable request pages to be cached
//			processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
//			success: function(data)  		// A function to be called if request succeeds
//		    {
//			$('#loading').hide();
//			$("#message").html(data);			
//		    }	        
//	   });
//	}));

// Function to preview image
	$(function() {
        $("#file").change(function() {
			$("#message").empty();         // To remove the previous error message
			var file = this.files[0];
			var imagefile = file.type;
			var match= ["image/jpeg","image/png","image/jpg"];	
			if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
			{
			$('#previewing').attr('src','noimage.png');
			$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
			return false;
			}
            else
			{
                var reader = new FileReader();	
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }		
        });
    });
	function imageIsLoaded(e) { 
		$("#file").css("color","green");
        $('#image_preview').css("display", "block");
        $('#previewing').attr('src', e.target.result);
		$('#previewing').attr('width', '250px');
		$('#previewing').attr('height', '230px');
	};


	$("#si").click(function(){
            $.ajax({
             url: window.location.origin+"/ragamixwebmobile/ajaxcontent/headerpoints", type: "GET",
             beforeSend: function() {
                $("#response").show();
                $("#response").html('<div class="signupnew"><div class="col-md-4 col-md-offset-4" style="padding-top: 100px;"><div class="panel-body panForm nanan">LOADING...PLEASE WAIT</div></div></div>');
             },
             success: function(data) {
                $("#response").show();
                $("#response").html(data);
                $("#siclose").click(function(){
                   $("#response").hide(); 
                });
             }
          });
	});
        
   $(".op").hide();
    $(".showAni").click(function(){
        $(".oa").animate({
            left: '250px',
            height: '100px',
            width: '100px'
        });
        $(".op").show();
    });
        
        $('.su').click(function() {
                $('.splashers').slideToggle("slow");
        });
        $('#notser').click(function() {
                $('#notifications').fadeToggle('fast', 'linear')
                return false;
        });
        
        $('#notserp').click(function() {
                $('#notificationsp').fadeToggle('fast', 'linear')
                return false;
        });




       $(document).click(function () {
            $('#notifications').hide();
        });
        
       $(document).click(function () {
            $('#notificationsp').hide();
        });
        
        $(document).mouseup(function (e)
        {
            var container = $("#displaynotsblock");

            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                $('#displaynotsblock').removeClass('loddiv');
                container.hide();
            }
        });
                $('#ei-slider').eislideshow({
					animation			: 'side',
					autoplay			: true,
					slideshow_interval	: 5000,
					titlesFactor		: 0
                });

$(".account").click(function()
{

var X=$(this).attr('id');

if(X==1)
{
$(".submenu").hide();
$(this).attr('id', '0');	
}
else
{

$(".submenu").show();
$(this).attr('id', '1');
}
	
});

//Mouseup textarea false
$(".submenu").mouseup(function()
{
return false
});
$(".account").mouseup(function()
{
return false
});


//Textarea without editing.
$(document).mouseup(function()
{
$(".submenu").hide();
$(".account").attr('id', '');

});



				$(".ajax").colorbox({width: "100%", height:"23%", position: "fixed", bottom: "-20px"});
                $(".rajax").colorbox({width: "35%", height: "100%", top: "-5px"});
                $(".majax").colorbox({width: "85%", top: "-5px"});
                $(".pajax").colorbox({width: "100%", height: "100%"});

});

$(window).scroll(function () {
  if ( $(this).scrollTop() > 200 && !$('header').hasClass('open') ) {
    $('header').addClass('open');
    $('header').slideDown();
   } else if ( $(this).scrollTop() <= 200 ) {
    $('header').removeClass('open');
    $('header').slideUp();
  }
});



function getcitylist(value){
    document.getElementById("cv").value = value;
    var x = document.getElementById("cv").value;
    //window.location.href = window.location.origin+"/ragamixreloaded/profile/getter?cv="+x;
}

function loadSubmit() {ProgressImage = document.getElementById('progress_image');document.getElementById("loader-wrapper").style.visibility = "visible"; setTimeout("ProgressImage.src = ProgressImage.src",100);return true;} 
function showValid(value){ var crx = document.getElementById("user_category").value;if(value === crx){
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn = document.getElementById("user_category_1");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal 
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
}

