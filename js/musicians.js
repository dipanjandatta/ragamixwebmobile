/**
 * Created by ots-dev7 on 20/10/16.
 */
$.ajax({
    url: window.location.origin+"/ragamixwebmobile/ajaxcontent/getfeaturedmusician", type: "GET",
    beforeSend: function() {
        $("#featuredmusician").html("<img src='"+ window.location.origin + "/ragamixwebmobile/images/loading.gif' align='absmiddle' />");
    },
    success: function(data) {
        $("#featuredmusician").html(data);
    }
});


$.ajax({
    url: window.location.origin+"/ragamixwebmobile/ajaxcontent/getmusicianinarea", type: "GET",
    beforeSend: function() {
        $("#listermusicianarea").html();
    },
    success: function(data) {
        $("#listermusicianarea").html(data);
    }
});

$.ajax({
    url: window.location.origin+"/ragamixwebmobile/ajaxcontent/gettrendingvideoslist", type: "GET",
    beforeSend: function() {
        $("#listers").html("<img src='"+ window.location.origin + "/images/loading.gif' align='absmiddle' />");
    },
    success: function(data) {
        $("#listers").html(data);
    }
});