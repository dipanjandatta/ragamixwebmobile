/**
 * Created by ots-dev7 on 19/10/16.
 */

$(document).ready(function() {
    $.ajax({
        url: window.location.origin + "/ragamixwebmobile/ajaxcontent/getMiddlewareSeeking", type: "GET",
        beforeSend: function () {
            $("#seeking").html("<img src='"+ window.location.origin + "/ragamixwebmobile/images/loading.gif' align='absmiddle' />");
        },
        success: function (data) {
            $("#seeking").html(data);
        }
    });

    $("#slidertesti").responsiveSlides({
        auto: true,
        nav: true,
        speed: 500,
        namespace: "centered-btns",
        timeout: 10000
    });
});
