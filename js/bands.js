/**
 * Created by ots-dev7 on 20/10/16.
 */


$.ajax({
    url: window.location.origin+"/ragamixwebmobile/ajaxcontent/getfeaturedbands", type: "GET",
    beforeSend: function() {
        $("#featuredbands").html("<img src='"+ window.location.origin + "/ragamixwebmobile/images/loading.gif' align='absmiddle' />");
    },
    success: function(data) {
        $("#featuredbands").html(data);
    }
});

$.ajax({
    url: window.location.origin+"/ragamixwebmobile/ajaxcontent/getbandsarea", type: "GET",
    beforeSend: function() {
        $("#listerbandarea").html();
    },
    success: function(data) {
        $("#listerbandarea").html(data);
    }
});

$.ajax({
    url: window.location.origin+"/ragamixwebmobile/ajaxcontent/gettrendingvideosband", type: "GET",
    beforeSend: function() {
        $("#listersbandvids").html("<img src='"+ window.location.origin + "/images/loading.gif' align='absmiddle' />");
    },
    success: function(data) {
        $("#listersbandvids").html(data);
    }
});